<?php

date_default_timezone_set("Asia/Jakarta");

if (!function_exists('get_curr_yearmonth')) {
        
        function get_curr_yearmonth() {
                return date('ym');
        }
}

if (!function_exists('get_curr_month')) {
        
        function get_curr_month() {
                return date('m');
        }
}

if (!function_exists('get_curr_year')) {
        
        function get_curr_year() {
                return date('y');
        }
}

if (!function_exists('get_curr_datetime')) {
        
        function get_curr_datetime() {
                return date('Y-m-d H:i:s');
        }
}

if (!function_exists('get_curr_date')) {
        
        function get_curr_date() {
                return date('Y-m-d');
        }
}

if (!function_exists('get_curr_time')) {
        
        function get_curr_time() {
                return date('H:i:s');
        }
}