<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  |--------------------------------------------------------------------------
  | Display Debug backtrace
  |--------------------------------------------------------------------------
  |
  | If set to TRUE, a backtrace will be displayed along with php errors. If
  | error_reporting is disabled, the backtrace will not display, regardless
  | of this setting
  |
 */
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
defined('FILE_READ_MODE') OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE') OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE') OR define('DIR_WRITE_MODE', 0755);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */
defined('FOPEN_READ') OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE') OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE') OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE') OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT') OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT') OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
  |--------------------------------------------------------------------------
  | Exit Status Codes
  |--------------------------------------------------------------------------
  |
  | Used to indicate the conditions under which the script is exit()ing.
  | While there is no universal standard for error codes, there are some
  | broad conventions.  Three such conventions are mentioned below, for
  | those who wish to make use of them.  The CodeIgniter defaults were
  | chosen for the least overlap with these conventions, while still
  | leaving room for others to be defined in future versions and user
  | applications.
  |
  | The three main conventions used for determining exit status codes
  | are as follows:
  |
  |    Standard C/C++ Library (stdlibc):
  |       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
  |       (This link also contains other GNU-specific conventions)
  |    BSD sysexits.h:
  |       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
  |    Bash scripting:
  |       http://tldp.org/LDP/abs/html/exitcodes.html
  |
 */
defined('EXIT_SUCCESS') OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR') OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG') OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE') OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS') OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT') OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE') OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN') OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX') OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/* USER SESSION */
define('USER_SESSION', 'user_session');

/* LAYOUT */
define('DEFAULT_LAYOUT', 'default');

/* USER LEVEL */
define('USER_LEVEL', serialize(array(
    0 => "ADMIN",
    1 => "PUSAT",
    2 => "REGIONAL",
    3 => "KPRK",
    4 => "KPC",
    5 => "NASIONAL"
)));

define('PATH_TO_INIT_DATA', realpath(APPPATH . '../init-data'));

define('TRC_DRAFT_TYPE_DRAFT', 0);
define('TRC_DRAFT_TYPE_APPROVED', 3);

//define('TRC_TYPE_BEGIN_BALANCE', '10010');
//define('TRC_TYPE_TRANSACTION', '10020');
define('TRC_TYPE_BEGIN_BALANCE', '1010');
define('TRC_TYPE_TRANSACTION', '1020');
define('TRC_TYPE_TRANSFER_INTERNAL', '1031');
define('TRC_TYPE_TRANSFER_EXTERNAL', '1032');

define('TRC_TAB_PELANGGAN', 1);
define('TRC_TAB_PROGRAM', 2);
define('TRC_TAB_INTERNAL_OPERASIONAL', 3);

define('TRF_INT_TAB_TRANSFER_MASUK', 1);
define('TRF_INT_TAB_TRANSFER_KELUAR', 2);
define('TRF_INT_TAB_TRANSFER_KAS_BANK', 3);
define('TRF_INT_TAB_NOTIFIKASI', 4);
define('TRF_INT_TAB_HISTORI', 5);

define('TRF_EXT_TAB_TRANSFER_MASUK', 1);
define('TRF_EXT_TAB_TRANSFER_KELUAR', 2);

define('TRC_TAB_IDS', serialize(array(
    TRC_TAB_PELANGGAN,
    TRC_TAB_PROGRAM,
    TRC_TAB_INTERNAL_OPERASIONAL,
)));

define('TRC_TABS', serialize(array(
    array(
        "id" => TRC_TAB_PELANGGAN,
        "code" => "trc-pelanggan",
        "label" => "Transaksi Pelanggan",
        "view_link" => "transaction/tab/pelanggan",
        "icon" => "icon-home",
    ),
    array(
        "id" => TRC_TAB_PROGRAM,
        "code" => "trc-program",
        "label" => "Transaksi Program",
        "view_link" => "transaction/tab/program",
        "icon" => "icon-user",
    ),
    array(
        "id" => TRC_TAB_INTERNAL_OPERASIONAL,
        "code" => "trc-operasional",
        "label" => "Internal Operasional",
        "view_link" => "transaction/tab/operasional",
        "icon" => "icon-cloud-download",
    ),
)));

define('TRF_INT_TABS', serialize(array(
    array(
        "id" => TRF_INT_TAB_TRANSFER_MASUK,
        "code" => "trf-int-masuk",
        "label" => "Transfer Masuk",
        "view_link" => "transaction/tab/internal_masuk",
        "icon" => "icon-cloud-download",
    ),
    array(
        "id" => TRF_INT_TAB_TRANSFER_KELUAR,
        "code" => "trf-int-keluar",
        "label" => "Transfer Keluar",
        "view_link" => "transaction/tab/internal_keluar",
        "icon" => "icon-cloud-download",
    ),
    array(
        "id" => TRF_INT_TAB_TRANSFER_KAS_BANK,
        "code" => "trf-int-kasbank",
        "label" => "Transfer Kas Bank",
        "view_link" => "transaction/tab/internal_kasbank",
        "icon" => "icon-cloud-download",
    ),
//    array(
//        "id" => TRF_INT_TAB_NOTIFIKASI,
//        "code" => "trf-int-notifikasi",
//        "label" => "Notifikasi",
//        "view_link" => "transaction/tab/internal_masuk",
//        "icon" => "icon-cloud-download",
//    ),
    array(
        "id" => TRF_INT_TAB_HISTORI,
        "code" => "trf-int-histori",
        "label" => "Histori Transfer Keluar",
        "view_link" => "transaction/tab/histori_internal_keluar",
        "icon" => "icon-cloud-download",
    ),
)));

define('TRF_EXT_TABS', serialize(array(
    array(
        "id" => TRF_EXT_TAB_TRANSFER_MASUK,
        "code" => "trf-ext-masuk",
        "label" => "Transfer Masuk",
        "view_link" => "transaction/tab/external_masuk",
        "icon" => "icon-cloud-download",
    ),
    array(
        "id" => TRF_EXT_TAB_TRANSFER_KELUAR,
        "code" => "trf-ext-keluar",
        "label" => "Transfer Keluar",
        "view_link" => "transaction/tab/external_keluar",
        "icon" => "icon-cloud-download",
    ),
)));

define('TRF_INT_TAB_IDS', serialize(array(
    TRF_INT_TAB_TRANSFER_MASUK,
    TRF_INT_TAB_TRANSFER_KELUAR,
    TRF_INT_TAB_TRANSFER_KAS_BANK,
)));

define('TRF_EXT_TAB_IDS', serialize(array(
    TRF_EXT_TAB_TRANSFER_MASUK,
    TRF_EXT_TAB_TRANSFER_KELUAR,
)));

define('TRC_INPUT_NAME_MLEDGER', 'MLedgerID'); // KAS/BANK/..
define('TRC_INPUT_NAME_MLEDGER_1', 'MLedgerID_1');
define('TRC_INPUT_NAME_AMOUNT', 'Amount');
define('TRC_INPUT_NAME_AMOUNT_1', 'Amount_1');
define('TRC_INPUT_NAME_SUBLEDGER1ID', 'SubLedger1ID'); // BANK ACCOUNT
define('TRC_INPUT_NAME_SUBLEDGER2ID', 'SubLedger2ID'); // Virtual account: operation/mensos
define('TRC_INPUT_NAME_SUBLEDGER3ID', 'SubLedger3ID'); // Mensos Type
define('TRC_INPUT_NAME_DESCRIPTION', 'Description');
define('TRC_INPUT_NAME_BANK_NAME', 'bank_name');
define('TRC_INPUT_NAME_OFFICE_LEVEL', 'office_level');
define('TRC_INPUT_NAME_SLEDGERID', 'SLedgerID');
define('TRC_INPUT_NAME_LINEID', 'LineID');
define('TRC_KEY_NAME_SUBTRCTYPE', 'sub_trc_type');

define('TRC_INPUT_NAME_MLEDGERIDTO', 'MLedgerIDTo');
define('TRC_INPUT_NAME_SLEDGERIDTO', 'SLedgerIDTo');
define('TRC_INPUT_NAME_SUBLEDGER1IDTO', 'SubLedger1IDTo'); // BANK ACCOUNT
define('TRC_INPUT_NAME_SUBLEDGER2IDTO', 'SubLedger2IDTo'); // Virtual account: operation/mensos
define('TRC_INPUT_NAME_SUBLEDGER3IDTO', 'SubLedger3IDTo');

define('TRC_M_LEDGER_KAS', 2);
define('TRC_M_LEDGER_BANK', 3);

define('TRC_PELANGGAN_INPUT_NAMES', serialize(array(
    TRC_INPUT_NAME_LINEID,
    TRC_INPUT_NAME_MLEDGER,
    TRC_INPUT_NAME_MLEDGER_1,
    TRC_INPUT_NAME_AMOUNT,
    TRC_INPUT_NAME_AMOUNT_1,
    TRC_INPUT_NAME_SUBLEDGER1ID,
)));

define('INIT_DATA_TABLES', serialize(array(
    'T005_Project',
    'T010_Partner',
    'T023_UserGroup',
    'T023_UserGroupType',
    'T024_1TrcType',
    'T024_2TrcPanel',
    'T024_3SubTrcType',
    'T024_TrcPanelType',
    'T024_4MapToLedger',
    'T024_TrcType',
    'T030_2VirtualAccount',
    'T030_MLedger',
    'T059_Currency',
)));

define('OFFICE_LEVEL_PUSAT', 1);
define('OFFICE_LEVEL_REGIONAL', 2);
define('OFFICE_LEVEL_KPRK', 3);
define('OFFICE_LEVEL_KPC', 4);

define('OFFICE_LEVEL_LABELS', serialize(array(
    OFFICE_LEVEL_PUSAT => 'Pusat',
    OFFICE_LEVEL_REGIONAL => 'Regional',
    OFFICE_LEVEL_KPRK => 'KPRK',
    OFFICE_LEVEL_KPC => 'KPC',
)));

define('TRANSFER_MAP', serialize(array(
    OFFICE_LEVEL_PUSAT => array(
        OFFICE_LEVEL_KPRK
    ),
    OFFICE_LEVEL_KPRK => array(
        OFFICE_LEVEL_PUSAT,
        OFFICE_LEVEL_KPC
    ),
    OFFICE_LEVEL_KPC => array(
        OFFICE_LEVEL_KPRK,
    ),
)));

define('OPERATION_TYPE_CODES', serialize(array(
    '20.10',
    '20.20',
    '20.30',
    '20.50',
)));

define('CRUD_SAVED',    0);
define('CRUD_INSERT',   1);
define('CRUD_UPDATE',   2);
define('CRUD_DELETE',   3);

define('SUB_TRC_TYPE_CODE__TRF_INT_MSK_APP',   'TrfIntMskApp');
define('SUB_TRC_TYPE_CODE__TRF_INT_MSK_REJ',   'TrfIntMskRej');
