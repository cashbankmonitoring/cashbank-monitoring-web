<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <title>CashBank Monitoring | Login User</title>
        <meta content="" name="description" />
        <meta content="themes-lab" name="author" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		{{metas}}

		<script type="text/javascript">
			var base_url = <?php echo json_encode(base_url());?>;		
		</script>

		<script type="text/javascript" src="<?= base_url(); ?>assets/global/plugins/jquery/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>assets/global/plugins/jquery/jquery-migrate-1.2.1.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>assets/global/plugins/gsap/main-gsap.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>assets/global/plugins/backstretch/backstretch.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>assets/global/plugins/bootstrap-loading/lada.min.js"></script>
        <!-- <script type="text/javascript" src="<?= base_url(); ?>script/md5.js"></script> -->
		<script type="text/javascript" src="<?= base_url(); ?>assets/global/js/pages/login-v2.js"></script>
		{{scripts}}

		<link rel="stylesheet" href="<?= base_url(); ?>assets/global/css/style.css" rel="stylesheet">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/global/css/ui.css" rel="stylesheet">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/global/plugins/bootstrap-loading/lada.min.css" rel="stylesheet">
		{{styles}}

        <link rel="shortcut icon" href="<?= base_url(); ?>assets/global/images/favicon.png">
    </head>
    <body class="account2" data-page="login">
    	{{content}}
    </body>
</html>