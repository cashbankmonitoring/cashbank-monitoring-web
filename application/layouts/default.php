<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
	<head>
		<title>CashBank Monitoring | {{title}}</title>
		
		<meta charset="utf-8">
		<meta http-equiv="Expires" content="-1">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	    <meta name="description" content="admin-themes-lab">
	    <meta name="author" content="themes-lab">
		{{metas}}

		<script type="text/javascript">
			var base_url = <?php echo json_encode(base_url());?>;		
		</script>
		
		<!-- <script src="<?= base_url(); ?>assets/global/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"></script> -->
		<!-- <script src="<?= base_url(); ?>assets/global/plugins/jquery/jquery-1.11.1.min.js"></script> -->
		<script src="<?= base_url(); ?>assets/js/jquery-3.1.1.min.js"></script>
		<script src="<?= base_url(); ?>assets/js/jquery-ui.min.js"></script>
                <!-- <script src="<?= base_url(); ?>assets/global/plugins/jquery-ui/jquery-ui-1.11.2.min.js"></script> -->
		<!--<script src="<?= base_url(); ?>assets/global/plugins/jquery/jquery-3.1.0.min.js"></script>-->
		<!-- <script src="<?= base_url(); ?>assets/js/jquery-3.1.0.min.js"></script> -->
	    <!-- <script src="<?= base_url(); ?>assets/global/plugins/jquery/jquery-migrate-1.2.1.min.js"></script> -->
	    <script src="<?= base_url(); ?>assets/global/plugins/moment/moment.min.js"></script>
        <script src="<?= base_url(); ?>assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
	    <script src="<?= base_url(); ?>assets/global/plugins/bootstrap-typeahead/bootstrap-typeahead.min.js"></script>
	    <script src="<?= base_url(); ?>assets/js/bootstrap-datetimepicker.min.js"></script>
	    
	    <script src="<?= base_url(); ?>assets/global/plugins/jquery-validation/jquery.validate.min.js"></script>
	    <!-- <script src="<?= base_url(); ?>assets/global/plugins/gsap/main-gsap.min.js"></script> -->
	    <!-- <script src="<?= base_url(); ?>assets/global/plugins/jquery-cookies/jquery.cookies.min.js"></script> <!-- Jquery Cookies, for theme --> 
	    <script src="<?= base_url(); ?>assets/global/plugins/jquery-block-ui/jquery.blockUI.min.js"></script> <!-- simulate synchronous behavior when using AJAX -->
	    <script src="<?= base_url(); ?>assets/global/plugins/bootbox/bootbox.min.js"></script> <!-- Modal with Validation -->
	    <!-- <script src="<?= base_url(); ?>assets/global/plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>  -->
	    <!-- Custom Scrollbar sidebar -->
	    <script src="<?= base_url(); ?>assets/global/plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"></script> <!-- Show Dropdown on Mouseover 
	    <!-- <script src="<?= base_url(); ?>assets/global/plugins/charts-sparkline/sparkline.min.js"></script> <!-- Charts Sparkline --> 
	    <!-- <script src="<?= base_url(); ?>assets/global/plugins/retina/retina.min.js"></script> <!-- Retina Display --> 
	    <!-- <script src="<?= base_url(); ?>assets/global/plugins/select2/select2.min.js"></script> <!-- Select Inputs  -->
	    <script src="<?= base_url(); ?>assets/js/select2.full.min.js"></script> <!-- Select Inputs 
	    <!-- <script src="<?= base_url(); ?>assets/global/plugins/icheck/icheck.min.js"></script> <!-- Checkbox & Radio Inputs --> 
	    <!-- <script src="<?= base_url(); ?>assets/global/plugins/backstretch/backstretch.min.js"></script> <!-- Background Image --> 
	    <!-- <script src="<?= base_url(); ?>assets/global/plugins/bootstrap-progressbar/bootstrap-progressbar.min.js"></script> <!-- Animated Progress Bar -->
		
		<!-- <script src="<?= base_url(); ?>assets/global/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker --> 
		
	    <!-- <script src="<?= base_url(); ?>assets/global/js/builder.js"></script> <!-- Theme Builder --> 
	    <!-- <script src="<?= base_url(); ?>assets/global/js/sidebar_hover.js"></script> <!-- Sidebar on Hover --> 
	    <!-- <script src="<?= base_url(); ?>assets/global/js/application.js"></script> <!-- Main Application Script --> 
	    <script src="<?= base_url(); ?>assets/global/js/plugins.js"></script> <!-- Main Plugin Initialization Script 
	    <!-- <script src="<?= base_url(); ?>assets/global/js/widgets/notes.js"></script> <!-- Notes Widget --> 
	    <!-- <script src="<?= base_url(); ?>assets/global/js/quickview.js"></script> <!-- Chat Script --> 
	    <script src="<?= base_url(); ?>assets/global/js/pages/search.js"></script> <!-- Search Script
	    <!-- BEGIN PAGE SCRIPT -->
	    <!-- <script src="<?= base_url(); ?>assets/global/plugins/noty/jquery.noty.packaged.min.js"></script>  <!-- Notifications --> 
	    <!-- <script src="<?= base_url(); ?>assets/global/plugins/bootstrap-editable/js/bootstrap-editable.min.js"></script> <!-- Inline Edition X-editable --> 
	    <!-- <script src="<?= base_url(); ?>assets/global/plugins/bootstrap-context-menu/bootstrap-contextmenu.min.js"></script> <!-- Context Menu --> 
	    <!--<script src="<?= base_url(); ?>assets/global/plugins/multidatepicker/multidatespicker.min.js"></script> <!-- Multi dates Picker --> 
	    <!-- <script src="<?= base_url(); ?>assets/global/js/widgets/todo_list.js"></script> -->
	    <!-- <script src="<?= base_url(); ?>assets/global/plugins/metrojs/metrojs.min.js"></script> <!-- Flipping Panel --> 

		<!-- <script type="text/javascript" src="<?= base_url(); ?>assets/global/plugins/nvd3/lib/d3.v3.js"></script>         -->
	    <!-- <script type="text/javascript" src="<?= base_url(); ?>assets/global/plugins/nvd3/nv.d3.min.js"></script> -->
		
		
	    <!-- <script src="<?= base_url(); ?>assets/global/plugins/maps-amcharts/ammap/ammap.min.js"></script> 
	    <script src="<?= base_url(); ?>assets/global/plugins/maps-amcharts/ammap/maps/js/worldLow.min.js"></script> 
	    <script src="<?= base_url(); ?>assets/global/plugins/maps-amcharts/ammap/themes/black.min.js"></script> 
		 -->
	    <!-- <script src="<?= base_url(); ?>assets/global/plugins/skycons/skycons.min.js"></script> <!-- Animated Weather Icons --> 
	    <!-- <script src="<?= base_url(); ?>assets/global/plugins/simple-weather/jquery.simpleWeather.js"></script> <!-- Weather Plugin --> 
	    <!-- <script src="<?= base_url(); ?>assets/global/js/widgets/widget_weather.js"></script> -->
		
	    <!-- END PAGE SCRIPT -->
	    <script src="<?= base_url(); ?>assets/admin/layout1/js/layout.js"></script>
		<!-- <script src="<?= base_url(); ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script> -->
		<!-- <script src="<?= base_url(); ?>assets/global/plugins/countup/countUp.min.js"></script> -->

		<!-- <script src="<?= base_url(); ?>assets/global/plugins/easy-pie-chart/jquery.easy-pie-chart.min.js"></script> -->
		<script src="<?= base_url(); ?>assets/js/numeral.min.js"></script>
		<script type="text/javascript">
			$(window).on('load', function() {
			    "use strict";
			    setTimeout(function() {
			        $('.loader-overlay').addClass('loaded');
			        $('body > section').animate({
			            opacity: 1,
			        }, 600);
			    }, 1000);
			});
		</script>
		{{scripts}}
		
		<link href="<?= base_url(); ?>assets/global/css/style.css" rel="stylesheet">
	    <link href="<?= base_url(); ?>assets/global/css/theme.css" rel="stylesheet">
	    <link href="<?= base_url(); ?>assets/global/css/ui.css" rel="stylesheet">
	    <link href="<?= base_url(); ?>assets/css/jquery-ui.min.css" rel="stylesheet">
	    <!-- <link href="<?= base_url(); ?>assets/global/plugins/select2/select2.css" rel="stylesheet"> -->
	    <!-- <link href="<?= base_url(); ?>assets/admin/layout1/css/layout.css" rel="stylesheet"> -->
		
		<link href="<?= base_url(); ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url(); ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet"></link>
	    <link href="<?= base_url(); ?>assets/css/select2.min.css" rel="stylesheet">
		
	    <!-- BEGIN PAGE STYLE --><!-- 
	    <link href="<?= base_url(); ?>assets/global/plugins/metrojs/metrojs.min.css" rel="stylesheet">
	    <link href="<?= base_url(); ?>assets/global/plugins/maps-amcharts/ammap/ammap.min.css" rel="stylesheet">
	    <link href="<?= base_url(); ?>assets/global/plugins/nvd3/css/nv.d3.css" rel="stylesheet"> -->

	    <!-- END PAGE STYLE -->
		{{styles}}
                
	    <link rel="shortcut icon" href="<?= base_url(); ?>assets/global/images/favicon.png" type="image/png">
	</head>
	<body class="fixed-topbar fixed-sidebar theme-sdtl color-default dashboard">
		<section>
		{{sidebar}}
      		<div class="main-content">
      		{{topbar}}
	        	<div class="page-content page-thin" id="trc_dv">
					<div class="new-content">
						{{content}}						
					</div>
			  		{{footer}}
	        	</div>
      		</div>
    	</section>
	    <div class="loader-overlay">
	      <div class="spinner">
	        <div class="bounce1"></div>
	        <div class="bounce2"></div>
	        <div class="bounce3"></div>
	      </div>
	    </div>
	    <!-- END PRELOADER -->
	    <a href="#" class="scrollup"><i class="fa fa-angle-up"></i></a>
	</body>
</html>