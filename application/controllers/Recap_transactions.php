<?php require_once('Common_transaction.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Recap_transactions extends Common_transaction {
	function __construct() {
		parent::__construct("RecTrc");
		
		$this->meta 			= array();
		$this->scripts 			= array('../global/plugins/datatables/jquery.dataTables.min', 'rekap');
		$this->styles 			= array();
		$this->title = "Rekap Transaksi";
		$this->load->model(array('user_session','office','t510_rekap'));
	}

	public function index(){
		$this->load->model(array('partner','project','program_alocation'));
		$office = $this->office->get(array('C001_ParentID' => $this->user_data['C002_OfficeID']))->result_array();

		foreach ($this->menu as $key => $menu) {
			if($menu['C010_Code'] == 'RecTrc'){
				$data['panels'] = $this->get_trc_panel($menu['C000_SysID']);
			}
		}

		if($this->input->post('select_date') != null){
			$type_date = $this->input->post('select_date');
			$type_data = 'post';

			$data_post = array(
				'type_date' => $type_date,
				'office_id' => $this->input->post('office_id'),
				'date_now' => $this->input->post('date'),
				'date_from' => $this->input->post('date_from'),
				'date_to' => $this->input->post('date_to')
			);
		}else{
			$today = date('Y-m-d');
			$new_date = date('Y-m-d', strtotime(str_replace('-', '/', $today)));
			$type_data = '';
			$data_post = array(
				'type_date' => 1,
				'office_id' => $this->user_data['C002_OfficeID'],
				'date_now' => $new_date,
				'date_from' => '',
				'date_to' => '',
			);
		}

		$tabs = array();
        foreach ($data['panels'] AS $panel) {
            $dt_panel = $data;
			$dt_panel['data_post'] = $data_post;
            if ($panel['C010_Code'] == 'RcpTrcPlg') {
				$dt_panel['panel_code'] = 'TrcPlg';
                $tabs[] = $this->load->view('recap_transaction/pelanggan', $dt_panel, TRUE);
            }else if($panel['C010_Code'] == 'RcpTrcPrg') {
				$dt_panel['panel_code'] = 'TrcPrg';
                $tabs[] = $this->load->view('recap_transaction/program', $dt_panel, TRUE);
            }else if($panel['C010_Code'] == 'RcpTrfInt'){
				$dt_panel['panel_code'] = 'TrfIntMasuk-TrfIntKeluar';
                $tabs[] = $this->load->view('recap_transaction/transfer_internal', $dt_panel, TRUE);
            }else if($panel['C010_Code'] == 'RcpTrfExt'){
            	$dt_panel['panel_code'] = 'TrfExtMasuk-TrfExtKeluar';
                $tabs[] = $this->load->view('recap_transaction/transfer_external', $dt_panel, TRUE);
            }else if($panel['C010_Code'] == 'RcpTrcIntOpr'){
            	$dt_panel['panel_code'] = 'TrcOprMasuk-TrcOprKeluar';
                $tabs[] = $this->load->view('recap_transaction/operasional', $dt_panel, TRUE);
            }else if($panel['C010_Code'] == 'RcpTrfKasBank'){
            	$dt_panel['panel_code'] = 'TrfIntKasBank';
                $tabs[] = $this->load->view('recap_transaction/transfer_kasbank', $dt_panel, TRUE);
            }
       	}

		$data = array(
			'type_data' => $type_data,
			'data_post' => $data_post,
			'user' => $this->user_data,
			'offices' => $office,
			'tabs' => $tabs
		);
		
		// print_r($data);die;
		$this->load->view("recap_transaction/index",$data);
	}

	function get_rekap($panel_code) {
        $this->layout = FALSE;

        $this->load->model('transaction_panel');
        $this->load->model('t510_rekap');
        $this->load->model('sub_trc_type');

        $get = $this->input->get();
        
        $type_date = $get['typeDate'];

        $panel = explode('-',$panel_code);
        if(count($panel) > 1){
        	if($panel_code == 'TrfIntMasuk-TrfIntKeluar'){
        		$trc_panel = $this->transaction_panel->get_panel(array(
                    'T024_2TrcPanel.C010_Code LIKE' => $panel[1],
                ))->row_array();

            	if($type_date == 1){
		        	$where = "(C200_SLedgerID = ".$get['officeId']." AND C010_TrcPanelID = ".$trc_panel['C000_SysID']." AND C050_DocDate = '".$get['dateNow']."') OR (C201_SLedgerIDTo = ".$get['officeId']." AND C010_TrcPanelID = ".$trc_panel['C000_SysID']." AND C050_DocDate = '".$get['dateNow']."')";
		        }else{
		        	$where = "(C200_SLedgerID = ".$get['officeId']." AND C010_TrcPanelID = ".$trc_panel['C000_SysID']." AND C050_DocDate >= '".$get['dateFrom']."' AND C050_DocDate <= '".$get['dateTo']."') OR (C201_SLedgerIDTo = ".$get['officeId']." AND C010_TrcPanelID = ".$trc_panel['C000_SysID']." AND C050_DocDate >= '".$get['dateFrom']."' AND C050_DocDate <= '".$get['dateTo']."')";
		        }
        	}else{
        		foreach ($panel as $key => $value) {	
	        		$trc_panel[] = $this->transaction_panel->get_panel(array(
	                    'T024_2TrcPanel.C010_Code LIKE' => $value,
	                ))->row_array();
        		}

        		if($type_date == 1){
		        	$where = "C200_SLedgerID = ".$get['officeId']." AND (C010_TrcPanelID = ".$trc_panel[0]['C000_SysID']." OR C010_TrcPanelID = ".$trc_panel[1]['C000_SysID'].") AND C050_DocDate = '".$get['dateNow']."'";
		        }else{
		        	$where = "C200_SLedgerID = ".$get['officeId']." AND (C010_TrcPanelID = ".$trc_panel[0]['C000_SysID']." OR C010_TrcPanelID = ".$trc_panel[1]['C000_SysID'].") AND C050_DocDate >= '".$get['dateFrom']."' AND C050_DocDate <= '".$get['dateTo']."'";
		        }
        	}

        }else{
        	$trc_panel = $this->transaction_panel->get_panel(array(
                    'T024_2TrcPanel.C010_Code LIKE' => $panel_code,
                ))->row_array();

	        $where = array(
	        	'C200_SLedgerID' => $get['officeId'],
	        	'C010_TrcPanelID' => $trc_panel['C000_SysID']
	        );

	        if($type_date == 1){
	        	$where['C050_DocDate'] = $get['dateNow']; 
	        }else{
	        	$where['C050_DocDate >='] = $get['dateFrom']; 
	        	$where['C050_DocDate <='] = $get['dateTo']; 
	        }
        }
        

        $page = $this->t510_rekap->get_doc_day_rekap($where, $get['start'], $get['length']
        );

        $page['draw'] = $get['draw'];
        $page['start'] = $get['start'];
        $page['recordsTotal'] = $page['total'];
        $page['recordsFiltered'] = $page['total'];

        $rows = array();
        foreach ($page['data'] as $key => $value) {
                if ($key >= $get['length']) {
                        break;
                }
                $rows[] = $value;
        }
        $page['data'] = $rows;

        // $this->generate_extra_doc_day($trc_panel['C010_Code'], $page);

        echo json_encode($page);
    }
	
}