<?php require_once('Common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class References extends Common {
	function __construct() {
		parent::__construct("reference");
		
		$this->meta 			= array();
		$this->scripts 			= (array('site/references'));
		$this->styles 			= array();
		$this->load->model(array('user_session','office','program_alocation'));
	}

	public function office(){
		$this->load->model(array('partner','project','program_alocation'));

		$this->scripts = (array('site/references', 'site/office', '..\global\plugins\datatables\jquery.dataTables.min'));
		$partners = $this->partner->get()->result_array();
		$offices = $this->office->get(array("C012_Level" => 3))->result_array();
		$user = $this->user_session->get();
		$data = array(
			'user' => $this->user_data,
			'alert_text' => $this->session->flashdata('alert_text'),
			'partners' => $partners,
			'offices' => $offices
		);
		$this->load->view("references/office",$data);
	}

	public function get_office_by_id(){
		$this->layout = false;
		$idx = $_POST["idx"];
	
		$result = $this->office->get($criteria='C000_SysID = '.$idx.'',$order='',$order_by='',$limit='',$start=0,$offset = 0)->row_array();

		echo json_encode($result);
	}

	public function user_accounts(){
		$this->load->model(array('partner','project','program_alocation','User_group_type'));
		$this->scripts = (array('site/references', 'site/user', '..\global\plugins\datatables\jquery.dataTables.min'));
		$partners = $this->partner->get()->result_array();
		$userType = $this->User_group_type->get()->result_array();
		$offices = $this->office->get(array("C012_Level" => 3))->result_array();
		$user = $this->user_session->get();
		$data = array(
			'user' => $this->user_data,
			// 'alert_text' => $this->session->flashdata('alert_text'),
			'partners' => $partners,
			'offices' => $offices,
			'userType' => $userType
		);
		$this->load->view("references/user",$data);
	}

	public function bank_accounts(){
		$this->load->model(array('KBAccount','project','program_alocation'));
		$this->scripts = (array('site/references', 'site/bank_account','..\global\plugins\datatables\jquery.dataTables.min'));
		$offices = $this->office->get()->result_array();
		$user = $this->user_session->get();
		$data = array(
			'user' => $this->user_data,
			// 'alert_text' => $this->session->flashdata('alert_text'),
			// 'partners' => $partners,
			'offices' => $offices
		);
		$this->load->view("references/bank_accounts",$data);
	}

	public function program_partner(){
		$this->load->model(array('KBAccount','project','program_alocation'));
		$this->scripts = (array('site/references', 'site/partner', '..\global\plugins\datatables\jquery.dataTables.min'));
		$offices = $this->office->get(array("C012_Level" => 3))->result_array();
		$user = $this->user_session->get();
		$data = array(
			'user' => $this->user_data
		);
		$this->load->view("references/partner",$data);
	}

    public function program(){
		$this->load->model(array('partner','project','program_alocation', 'virtual_account'));
		$this->scripts = (array('site/references', 'site/project','..\global\plugins\datatables\jquery.dataTables.min'));
		$partners = $this->partner->get()->result_array();
		$offices = $this->office->get(array("C012_Level" => 3))->result_array();
		$user = $this->user_session->get();
		$vitual = $this->virtual_account->get();
		$data = array(
			'user' => $this->user_data,
			// 'alert_text' => $this->session->flashdata('alert_text'),
			'partners' => $partners,
			'offices' => $offices,
			'virtuals' => $vitual
		);
		$this->load->view("references/project",$data);
	}

    public function program_allocations(){
		$this->load->model(array('partner','project','program_alocation'));
		$this->scripts = (array('site/references', 'site/alokasi','..\global\plugins\datatables\jquery.dataTables.min'));
		$projects = $this->project->get()->result_array();
		
		$user = $this->user_session->get();
		
		if($user['C110_UserGroupType'] == 4 || $user['C110_UserGroupType'] == 5 || $user['C110_UserGroupType'] == 6){
			$offices = $this->office->get(array("C012_Level" => 3))->result_array();
		}else{
			$offices = $this->office->get(array("C012_Level" => 4, "C001_ParentID"=> $user['C002_OfficeID']))->result_array();	
		}
		$projectByOff = $this->program_alocation->get_program_by_off($criteria = 'T027_ProgramAlocation.C010_OfficeID = '.$user['C002_OfficeID'].'')->result_array();
		$data = array(
			'user' => $this->user_data,
			'offices' => $offices,
			'projects' => $projects,
			'projectByOff' => $projectByOff
		);
		$this->load->view("references/program_allocation",$data);
	}

    public function virtual_account(){
		$this->load->model(array('virtual_account'));
		$this->scripts = (array('site/references','site/virtual_account','..\global\plugins\datatables\jquery.dataTables.min'));
		$virtual = $this->virtual_account->get();
		$data = array(
			'user' => $this->user_data,
			'virtual_accounts' => $virtual
		);
		$this->load->view("references/virtual_account",$data);
	}

    public function user_group_types(){
		$this->load->model(array('User_group_type','User_group','Trc_menu_type'));
		$this->scripts = (array('site/user_group_type','..\global\plugins\datatables\jquery.dataTables.min'));
		$userGroupTypes = $this->User_group_type->get(null, 'SysID')->result_array();
		// echo $this->db->last_query();die;
		$userGroup = $this->User_group->get()->result_array();
		$panels = $this->Trc_menu_type->get_panel_menu(null, 'C010_TrcTypeID')->result_array();
		// print_r($panels);die;

		for ($i=0; $i < count($panels); $i++) { 
			if($panels[$i]['menu_parent'] != 0){
				$menu = $this->Trc_menu_type->get(array('C000_SysID' => $panels[$i]['menu_parent']))->row_array();
				$panels[$i]['menu_head_label'] = $menu['C013_Label'];
			}else{
				$panels[$i]['menu_head_label'] = '';
			}
		}

		$data = array(
			'user' => $this->user_data,
			'userGroupTypes' => $userGroupTypes,
			'trcType' => $panels,
			'userGroup' => $userGroup
		);
		$this->load->view("references/user_group_types",$data);
	}

	public function menu(){
		$this->load->model(array('Trc_menu_type'));
		$this->scripts = (array('site/user_menu','..\global\plugins\datatables\jquery.dataTables.min'));
		// $userGroupTypes = $this->User_group_type->get()->result_array();
		// $userGroup = $this->User_group->get()->result_array();
		$trcType = $this->Trc_menu_type->get()->result_array();
		$trcTypePanel = $this->Trc_menu_type->get_panel()->result_array();
		$data = array(
			'user' => $this->user_data,
			// 'userGroupTypes' => $userGroupTypes,
			'trcType' => $trcType,
			'trcTypePanel' => $trcTypePanel
			// 'userGroup' => $userGroup
		);
		$this->load->view("references/user_menu_types",$data);
	}

	public function panel(){
		$this->load->model(array('Trc_menu_type'));
		$this->scripts = (array('site/user_panels','..\global\plugins\datatables\jquery.dataTables.min'));
		// $userGroupTypes = $this->User_group_type->get()->result_array();
		// $userGroup = $this->User_group->get()->result_array();
		$trcSubPanel = $this->Trc_menu_type->get_sub_panel()->result_array();
		$trcTypePanel = $this->Trc_menu_type->get_panel()->result_array();
		$data = array(
			'user' => $this->user_data,
			// 'userGroupTypes' => $userGroupTypes,
			'trcSubPanel' => $trcSubPanel,
			'trcTypePanel' => $trcTypePanel
			// 'userGroup' => $userGroup
		);
		$this->load->view("references/user_menu_panels",$data);
	}
	
	function post_group_type(){
		$this->layout = false;
		$this->load->model(array('User_group_type',"User_group"));
		$post = $_POST['dt_ins'];
		if($_POST['trcType'] != 0){
			$trc = $_POST['trcType'];
		}
		// print_r($post);
		// print_r($trc);
		// die();
		// print_r($post);
		$data['UserGroupTypeName'] = $post[0];
		$data['UserGroupTypeDesc'] = $post[1];
		
		// $data['SysID'] = $this->User_group_type->get()->num_rows()+1;
		if($this->User_group_type->insert($data)){
			// echo "1";
		}
		// print_r(count($trc));
		if($_POST['trcType'] != 0){
			$last_id = $this->User_group_type->get_last_id();
			for ($i=0; $i < count($trc); $i++) {
				$data1 = [];
				$data1['UserGroupTypeID'] = $last_id;
				$data1['TrcPanelID'] = $trc[$i];
				$this->User_group->insert($data1);
			}
		}
		// die();
		echo"1";

	}

	
	function search_office($level = null, $parent = null) {
            $this->layout = FALSE;
            $result = array('total_count' => 410979, 'incomplete_results' => false, 'items' => "");

            $q = $this->input->get('q');
            if($parent){
            	$where = array('C012_Level' => $level, 'C001_ParentID' => $parent);
            }else if($level){	
            	$where = array('C012_Level' => $level);
            }else{
            	$where = null;
            }

            $result['items'] = $this->office->search($q, $where);

            header('Content-type: application/json');
            echo json_encode($result);
    }

    function search_office2($level = null, $parent = null) {
            $this->layout = FALSE;

            $q = $this->input->get('q');
            if($parent){
            	$where = array('C012_Level' => $level, 'C001_ParentID' => $parent);
            }else if($level){	
            	$where = array('C012_Level' => $level);
            }else{
            	$where = null;
            }

            $result = $this->office->search($q, $where);

            header('Content-type: application/json');
            echo json_encode($result);
    }
}