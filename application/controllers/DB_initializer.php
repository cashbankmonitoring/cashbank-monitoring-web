<?php

class DB_Initializer extends CI_Controller {

        function __construct() {
                parent::__construct();

                $this->layout = FALSE;

                $this->load->model('initializer');
        }

        function back_up() {
                $tables = unserialize(INIT_DATA_TABLES);
                foreach ($tables as $key => $table) {
                        $this->back_up_one_table($table);
                }

                echo 'Success';
        }

        function restore() {
                $tables = unserialize(INIT_DATA_TABLES);
                foreach ($tables as $key => $table) {
                        $this->restore_one_table($table);
                }

                echo 'Success';
        }
        
        function back_up_one_table($table_name) {
                $this->initializer->init_table($table_name);

                $data = $this->initializer->find_all();
                $rows = "";
                foreach ($data AS $key => $value) {
                        $values = array();
                        foreach ($value as $key1 => $value1) {
                                $values[] = json_encode($value1 && is_string($value1) ? trim($value1) : NULL);
                        }
                        $rows .= implode(";", $values);
                        $rows .= "\n";
                }
                $path = PATH_TO_INIT_DATA . DIRECTORY_SEPARATOR . $table_name . ".csv";
                $file = fopen($path, "w") or die("Unable to open file!");
                fwrite($file, $rows);
                fclose($file);
                
                return 1;
        }

        function restore_one_table($table_name) {
                $this->initializer->init_table($table_name);
                $this->initializer->reset_cache_columns();
                
                $this->initializer->delete_all();
                
                $columns = $this->initializer->get_columns();
                
                $path = PATH_TO_INIT_DATA . DIRECTORY_SEPARATOR . $table_name . ".csv";
                $file = fopen($path, "r") or die("Unable to open file!");
                $lines = array();
                while (($line = fgets($file)) !== false) {
                        if(!$line){
                                continue; 
                        }
                        $line = explode(';', $line);
                        foreach ($columns as $key1 => $column) {
                                $line[$column] = json_decode($line[$key1]);
                                unset($line[$key1]);
                        }
                        
                        $this->initializer->insert($line);
                        
                        $lines[] = $line;
                }
                
                fclose($file);
                
                return 1;
        }
        
        function init_day_pos_data() {
                $this->load->model('trc');
                $this->load->model('day_pos');
                
				$this->day_pos->delete_all();
				
                $data = $this->trc->get_all_latest();
				
                foreach ($data as $key => $day_pos) {
                        if ($day_pos['MLedgerID'] == 0 || $day_pos['SLedgerID'] == 0) {
                                continue;
                        }
                        
                        $day_pos['datePeriod'] = $day_pos['C015_DatePos'];
                        $day_pos['Amount'] = $day_pos['AmountTrc'];
                        $day_pos['SubLedger4ID'] = 0;
                        
                        $where = array(
                            'SLedgerID' => $day_pos['SLedgerID'],
                            'MLedgerID' => $day_pos['MLedgerID'],
                            'SubLedger1ID' => $day_pos['SubLedger1ID'],
                            'SubLedger2ID' => $day_pos['SubLedger2ID'],
                            'SubLedger3ID' => $day_pos['SubLedger3ID'],
                        );
                        
                        $old_day_post = $this->day_pos->get($where);
                        
                        if(!$old_day_post){
                                $this->day_pos->insert($day_pos);
                        }else{
                                $this->day_pos->update($where, $day_pos);
                        }
                }

                echo 'Success';
        }

}
