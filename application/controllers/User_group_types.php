<?php require_once('Common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_group_types extends Common {
	function __construct() {
		parent::__construct("UsrTyp");
		
		$this->meta 			= array();
		$this->scripts 			= array('../global/plugins/datatables/jquery.dataTables.min');
		$this->styles 			= array();
		$this->load->model(array('user_session','User_group_type',"User_group"));
	}

	// function post_group_type(){
	// 	$this->layout = false;
	// 	$post = $_POST['dt_ins'];
	// 	$data['UserGroupTypeName'] = $post[0];
	// 	$data['userGroupTypeDesc'] = $post[1];
	// 	if($this->User_group_type->insert($data)){
	// 		return 1; 
	// 	}
	// 	return 0;
	// }
	function delete(){
		$this->layout = false;
		$id = $_POST['usr_id'];
		$this->User_group_type->delete($id);
		$this->User_group->deleteByType($id);
		echo "1";
	}

	function update(){
		$this->layout = false;
		$id=$_POST['usr_id'];
		$post = $_POST['dt_ins'];
		$data['UserGroupTypeName'] = $post[0];
		$data['userGroupTypeDesc'] = $post[1];
		if($_POST['arr'] != 0){
			$trc = $_POST['arr'];
		}
		// $trc = $_POST['arr'];
		if($this->User_group_type->update($id,$data)){
			// 
			if($_POST['arr'] != 0){
			$query = $this->User_group->get_trc_type(array("UserGroupTypeID"=>$id))->result_array();
			$result = [];
			foreach ($query as $row) {
				array_push($result, $row['TrcPanelID']);
			}
			// print_r($result);
			// print_r($trc);
			$added = array_values(array_diff($trc,$result));
			// echo"added";
			// print_r($added);
				if(count($added)>0){
					for ($i=0; $i < count($added); $i++) { 
						$data1 = [];
						$data1['UserGroupTypeID'] = $id;
						$data1['TrcPanelID'] = $added[$i];
						$this->User_group->insert($data1);
					}
				}
				// echo"deleted";
			$deleted = array_values(array_diff($result,$trc));
			// print_r($deleted);
				if(count($deleted)>0){
					for ($i=0; $i < count($deleted); $i++) { 
						$this->User_group->delete(array('UserGroupTypeID'=>$id,'TrcPanelID'=>$deleted[$i]));
					}
				}
			}
			echo "1"; 
		}
		// return false;
	}
	function get_trc_type_by_group(){
		$this->layout = false;
		$id = $_POST['idx'];
		$query = $this->User_group->get_trc_type(array("UserGroupTypeID"=>$id))->result_array();
		$result = "";
		foreach($query as $row){
			// $result = $row['TrcTypeID']+
		}
		// print_r($query);
		echo json_encode($query);
	}

	function get_data_excel($sheet, $highestRow, $highestColumn, $media){
		for ($row = 2; $row <= $highestRow; $row++){ //  Read a row of data into an array                 
	        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
	                                         
	        //Sesuaikan sama nama kolom tabel di database                               
	        $data = array(
	            "UserGroupTypeName" => $rowData[0][0],
	            "UserGroupTypeDesc" => $rowData[0][1]
	        );
	         
	        //sesuaikan nama dengan nama tabel
	        $this->User_group_type->add_export($data);
	        delete_files($media['file_path']);
	             
	    }

		redirect(base_url()."references/user_group_types");
	}
	
}