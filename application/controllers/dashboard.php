<?php require_once('Common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends Common {
	function __construct() {
		parent::__construct("Dsbrd");
		
		$this->meta 			= array();
		$this->scripts 			= array("highcharts", "highcharts-3d");
		$this->styles 			= array();
		$this->title			= 'Dashboard';
		$this->load->model(array('user_session', 'office', 'day_pos', 'mledger'));
	}

	public function index(){
		$data['user'] = $this->user_data;
		$data['office_id'] = $this->user_data['C002_OfficeID'];
		$office = $this->office->get(array('C001_ParentID' => $this->user_data['C002_OfficeID']))->row_array();

		if($office == null){
			$office = $this->office->get(array('C001_ParentID' => 2))->row_array();
		}

		$is_dashboard = false;
		foreach ($this->menu as $key => $menu) {
			if($menu['C013_Label'] == 'Dashboard'){
				$is_dashboard = true;
				$data['panels'] = $this->get_trc_panel($menu['C000_SysID']);
			}
		}

		$data['user_lever'] = $this->user_level($office['C012_Level']);		

		$tabs = array();
		if($is_dashboard){
	        foreach ($data['panels'] AS $panel) {
	            $dt_panel = $data;
	            if ($panel['C010_Code'] == 'DsbMap') {
					$this->scripts[] = "dashboard/get_map";
	                $tabs[] = $this->load->view('dashboard/map', $dt_panel, TRUE);
	            }else if($panel['C010_Code'] == 'DsbTotalCash') {
					$this->scripts[] = "dashboard/get_piechart";
	                $tabs[] = $this->load->view('dashboard/total_cash', $dt_panel, TRUE);
	            }else if($panel['C010_Code'] == 'DsbTotalCashLevel'){
					$this->scripts[] = "dashboard/get_barchart";
	                $tabs[] = $this->load->view('dashboard/total_cash_level', $dt_panel, TRUE);
	            }
	       	}
	       	
	       	$data['tabs'] = $tabs;
				
			$this->load->view("site/dashboard", $data);
	    }else{
	    	$this->load->view("dashboard/dashboard_kosong", $data);
	    }
	}

	function get_map(){
		$this->layout = false;
		// ini_set('max_execution_time', 300);

		if($this->user_data['office_detail'] == null || $this->user_data['user_level'] == 'NASIONAL'){
			$office = $this->office->get(array('C001_ParentID' => 2))->result_array();
		}else{
			$office = $this->office->get(array('C001_ParentID' => $this->user_data['C002_OfficeID']))->result_array();
		}

		for ($i=0; $i < count($office); $i++) { 
			$amount = $this->day_pos->get(array('SLedgerID' => $office[$i]['C000_SysID']));
			$amount_kas = 0;
			$amount_bank = 0;

			$get_kprk = $this->office->get_parent_only(array('C001_ParentID' => $office[$i]['C000_SysID']))->result_array();
			foreach ($get_kprk as $key => $kprk) {
				$amount_kas = $amount_kas + $this->day_pos->sum_balance(array('SLedgerID' => $kprk['C000_SysID'], 'MLedgerID' => 2));
				$amount_bank = $amount_bank + $this->day_pos->sum_balance(array('SLedgerID' => $kprk['C000_SysID'], 'MLedgerID' => 3));

				$get_kpc = $this->office->get_parent_only(array('C001_ParentID' => $kprk['C000_SysID']))->result_array();
				foreach ($get_kpc as $key => $kpc) {
					$amount_kas = $amount_kas + $this->day_pos->sum_balance(array('SLedgerID' => $kpc['C000_SysID'], 'MLedgerID' => 2));
					$amount_bank = $amount_bank + $this->day_pos->sum_balance(array('SLedgerID' => $kpc['C000_SysID'], 'MLedgerID' => 3));
				}
			}
			// for ($a=0; $a < count($amount); $a++) { 
			// 	if($amount[$a]['MLedgerID'] == 2){
			// 		$amount_kas = $amount_kas + $amount[$a]['Amount'];
			// 	}else if($amount[$a]['MLedgerID'] == 3){
			// 		$amount_bank = $amount_bank + $amount[$a]['Amount'];
			// 	}
			// }

			$new_office[] = array($office[$i]['C020_Descr'].'<br>Kas : '.number_format($amount_kas).' IDR<br>Bank : '.number_format($amount_bank).' IDR', $office[$i]['C101_Lat'], $office[$i]['C100_Long'], $office[$i]['C000_SysID']);
		}
		
		echo json_encode($new_office);
	}

	function get_cashbank(){
		$this->layout = false;
		// ini_set('max_execution_time', 500);

		if($this->user_data['user_level'] == 'KPRK' || $this->user_data['user_level'] == 'REGIONAL'){
			$ledger = $this->mledger->get(array(
                'Lft > ' => 1,
                'Rgt < ' => 6,
                'IsDetail' => 1,
            ));
		}else{
			$ledger = $this->mledger->get();
		}


		$data = array();
		$pos = 0;
		$total = 0;
		for ($i=0; $i < count($ledger); $i++) {
			$where = array();
			if($this->user_data['user_level'] == 'ADMIN' || $this->user_data['user_level'] == 'NASIONAL'){
				$where = array('MLedgerID' => $ledger[$i]['SysID']);
			}else{
				$where = array('MLedgerID' => $ledger[$i]['SysID'], 'SLedgerID' => $this->user_data['C002_OfficeID']);
			}

			$amount = abs($this->day_pos->sum_balance($where));

			if($this->user_data['user_level'] == 'REGIONAL' || $this->user_data['user_level'] == 'KPRK'){
				$get_kprk = $this->office->get_parent_only(array('C001_ParentID' => $this->user_data['C002_OfficeID']))->result_array();
				foreach ($get_kprk as $key => $kprk) {
					$amount = $amount + abs($this->day_pos->sum_balance(array('MLedgerID' => $ledger[$i]['SysID'], 'SLedgerID' => $kprk['C000_SysID'])));
					$get_kpc = $this->office->get_parent_only(array('C001_ParentID' => $kprk['C000_SysID']))->result_array();
					if($get_kpc != null){
						foreach ($get_kpc as $key => $kpc) {
							$amount = $amount + abs($this->day_pos->sum_balance(array('MLedgerID' => $ledger[$i]['SysID'], 'SLedgerID' => $kpc['C000_SysID'])));
						}
					}
				}
			}

			if($this->user_data['user_level'] == 'PUSAT' || $this->user_data['user_level'] == 'NASIONAL'){
				if($ledger[$i]['SysID'] == 2){
					$pos = $pos + $amount;
					$total = $total + $amount;
				}else if($ledger[$i]['SysID'] == 3){
					$pos = $pos + $amount;
					$total = $total + $amount;
				}elseif ($ledger[$i]['SysID'] == 9) {
					$pos = $pos + $amount;
					$total = $total + $amount;
				}else{
					$pos = $pos - $amount;
				}
			}else{

				if($ledger[$i]['SysID'] == 2){
					$total = $total + $amount;
				}else if($ledger[$i]['SysID'] == 3){
					$total = $total + $amount;
				}else{
					$pos = $pos - $amount;
				}

				$data['kasbank'][] = array(
					'name' => $ledger[$i]['Descr'], 
					'y' => ($amount == null ? 0 : $amount)
				);
			}

			if($ledger[$i]['SysID'] != 1 && $ledger[$i]['SysID'] != 2 && $ledger[$i]['SysID'] != 3 && $ledger[$i]['SysID'] != 4 && $ledger[$i]['SysID'] != 9 && $ledger[$i]['SysID'] != 23){
				$data['kasbank'][] = array(
					'name' => $ledger[$i]['Descr'], 
					'y' => ($amount == null ? 0 : $amount)
				);

			}
		}

		if($this->user_data['user_level'] == 'PUSAT' || $this->user_data['user_level'] == 'NASIONAL'){
			array_push($data['kasbank'], array('name' => 'POS', 'y' => $pos));
		}
		
		$data['total_kasbank'] = $total;

		echo json_encode($data);
	}

	function get_level_by(){
		$this->layout = false;
		// ini_set('max_execution_time', 300);

		if($this->user_data['office_detail'] == null || $this->user_data['user_level'] == 'NASIONAL'){
			$office = $this->office->get('C012_Level = 1 OR C012_Level = 2', 'C012_Level ASC')->result_array();
		}else{
			$office = $this->office->get(array('C001_ParentID' => $this->user_data['C002_OfficeID']))->result_array();
			if($this->user_data['user_level'] != 'REGIONAL'){
				$get_one_office = $this->office->get(array('C000_SysID' => $this->user_data['office_detail']['C000_SysID']))->row_array();

				array_unshift($office, $get_one_office);
			}
		}

		// print_r($office);die;
	
		$ledger = array('Kas', 'Bank');
		
		$kas_pusat = 0;
		$bank_pusat = 0;
		$count_pusat = 0;
		for ($i=0; $i < count($office); $i++) {
			$data['user_level'] = $this->user_level($office[$i]['C012_Level']);

			$amount_kas = 0;
			$amount_bank = 0;

			if($office[$i]['C012_Level'] == 1){
				$type[] = 'PUSAT';
				$count_pusat++;

				$kas_pusat = $kas_pusat + $this->day_pos->sum_balance(array('SLedgerID' => $office[$i]['C000_SysID'], 'MLedgerID' => 2));
				$amount_kas = $kas_pusat;

				$bank_pusat = $bank_pusat + $this->day_pos->sum_balance(array('SLedgerID' => $office[$i]['C000_SysID'], 'MLedgerID' => 3));
				$amount_bank = $bank_pusat;
			}else{
				$type[] = $office[$i]['C020_Descr'];
				
				$amount_kas = $amount_kas + $this->day_pos->sum_balance(array('SLedgerID' => $office[$i]['C000_SysID'], 'MLedgerID' => 2));
				$amount_bank = $amount_bank + $this->day_pos->sum_balance(array('SLedgerID' => $office[$i]['C000_SysID'], 'MLedgerID' => 3));
				
				if($this->user_data['user_level'] != 'KPRK'){
					$get_kprk = $this->office->get_parent_only(array('C001_ParentID' => $office[$i]['C000_SysID']))->result_array();
					foreach ($get_kprk as $key => $kprk) {
						$amount_kas = $amount_kas + $this->day_pos->sum_balance(array('SLedgerID' => $kprk['C000_SysID'], 'MLedgerID' => 2));
						$amount_bank = $amount_bank + $this->day_pos->sum_balance(array('SLedgerID' => $kprk['C000_SysID'], 'MLedgerID' => 3));
						$get_kpc = $this->office->get_parent_only(array('C001_ParentID' => $kprk['C000_SysID']))->result_array();
						foreach ($get_kpc as $key => $kpc) {
							$amount_kas = $amount_kas + $this->day_pos->sum_balance(array('SLedgerID' => $kpc['C000_SysID'], 'MLedgerID' => 2));
							$amount_bank = $amount_bank + $this->day_pos->sum_balance(array('SLedgerID' => $kpc['C000_SysID'], 'MLedgerID' => 3));
						}
					}
				}
			}
			
			$data_kas[] = $amount_kas;
			$data_bank[] = $amount_bank;
		}
		
		if($count_pusat != 0){
			$count_pusat = $count_pusat - 1;
			for ($i=0; $i < $count_pusat; $i++) { 
				unset($type[$i]);
				unset($data_kas[$i]);
				unset($data_bank[$i]);
			}

			foreach ($type as $key => $value) {
				$data['type'][] = $value;
			}

			foreach ($data_kas as $key => $value) {
				$new_kas[] = $value;
			}

			foreach ($data_bank as $key => $value) {
				$new_bank[] = $value;
			}
		}else{
			$data['type'] = $type;
			$new_kas = $data_kas;
			$new_bank = $data_bank;
		}

		for ($b=0; $b < count($ledger); $b++) { 
			if($b == 0){
				$data_amount = $new_kas;
			}else{	
				$data_amount = $new_bank;
			}

			$data['content'][] = array(
				'name' => $ledger[$b],
				'data' => $data_amount 
			);
		}
		
		echo json_encode($data);
	}

	function dumping_user_type($first, $second){
		$this->load->model(array('user'));

		$get_user = $this->user->get('T023_User.C110_UserGroupType IS NULL AND T023_User.C000_SysID BETWEEN '.$first.' AND '.$second)->result_array();

		for ($i=0; $i < count($get_user); $i++) { 
			if($get_user[$i]['C012_Level'] == 2){
				$input['C110_UserGroupType'] = 6;
			}else if($get_user[$i]['C012_Level'] == 1){
				$input['C110_UserGroupType'] = 2;
			}else if($get_user[$i]['C012_Level'] == 3){
				$input['C110_UserGroupType'] = 7;
			}else if($get_user[$i]['C012_Level'] == 4){
				$input['C110_UserGroupType'] = 8;
			}else{
				$input['C110_UserGroupType'] = 3;
			}

			$update = $this->user->update($get_user[$i]['C000_SysID'], $input);

			if($update){
				print_r("expression");
				// redirect(base_url()."dashboard");
			}
		}

		redirect(base_url()."dashboard");
	}
}