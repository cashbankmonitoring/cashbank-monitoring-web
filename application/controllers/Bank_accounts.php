<?php require_once('Common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bank_accounts extends Common {
	function __construct() {
		parent::__construct("BankAcc");
		
		$this->meta 			= array();
		$this->scripts 			= array('site/references','site/user','site/bank_account','../global/plugins/datatables/jquery.dataTables.min');
		$this->styles 			= array();
		$this->load->model(array('user_session','KBAccount'));
	}

	public function get_ajax_data(){
		$this->layout = false;
		$this->load->model(array('datatable'));
		$table = 'T022_KBAccount';
		$column_order = array(null,'T020_Office.C020_Descr','T022_KBAccount.C020_Name','T022_KBAccount.C010_BankAccNumber','T022_KBAccount.C030_Descr', null); //set column field database for datatable orderable
	    $column_search = array('T020_Office.C020_Descr','T022_KBAccount.C020_Name','T022_KBAccount.C010_BankAccNumber','T022_KBAccount.C030_Descr'); //set column field database for datatable searchable just firstname , lastname , address are searchable
	    $order = array('T022_KBAccount.C000_SysID' => 'desc'); // default order
		
		$list = $this->datatable->get_datatables($table, $column_search, $column_order, $order);

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $key => $trow) {
            $no++;
            $number = $key + 1;
            $row = array();
            $row[] = $no;
            $row[] = $trow->office;
            $row[] = $trow->name;
            $row[] = $trow->account_number;
            $row[] = $trow->descr;
 
            //add html for action
            $row[] = '<a class="edit-bank btn btn-sm btn-primary" title="Edit" data-id="'.$trow->id.'"><i class="glyphicon glyphicon-pencil"></i></a>
                  <a class="delete-bank btn btn-sm btn-danger" title="Hapus" data-id="'.$trow->id.'")"><i class="glyphicon glyphicon-trash"></i> </a>';
 
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->datatable->count_all($table),
                        "recordsFiltered" => $this->datatable->count_filtered($table, $column_search, $column_order, $order),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
	}

	public function get_bankacc_by_id(){
		$this->layout = false;
		$idx = $_POST["idx"];
	
		$row = $this->KBAccount->get(array("C000_SysID"=>$idx))->row_array();
		echo json_encode($row);
	}

	public function save_bank(){
		$this->layout = false;
		$this->load->model(array('KBAccount'));

		$id_bank = $_POST['bank_id'];
        $data['C001_OfficeID'] = $_POST['kantor_id'];
        $data['C010_BankAccNumber'] = $_POST['no_rek'];
        $data['C020_Name'] = $_POST['bank'];
        $data['C030_Descr'] = $_POST['deskripsi'];

        $response = 0;
        if($id_bank == 0){
        	if($this->KBAccount->add($data)){
	            $response = 1;
	        }else{
	            $response = 0;
	        }
        }else{
        	if($this->KBAccount->edit($id_bank, $data)){
	            $response = 1;
	        }else{
	            $response = 0;
	        }
        }

        echo $response;
    }

	public function check_delete_bank(){
		$this->layout =false;
		$this->load->model(array('office'));
		$bank_id = $_POST["bank_id"];
		$is_trc = $this->office->get_form_day_delta(array("SubLedger1ID" => $bank_id))->num_rows();
		if($is_trc != 0){
			echo"0";
		}else{
			echo"1";
		}
	}

	public function delete_bankacc(){
		$ktr_id = $_POST["ktr_id"];
		if($this->KBAccount->delete($ktr_id)){
			echo "1";
		} else {
			echo "0";
		}
		die();
	}

	function get_data_excel($sheet, $highestRow, $highestColumn, $media){
		$this->load->model(array('office'));

		for ($row = 2; $row <= $highestRow; $row++){ 
			// Read a row of data into an array                 
	        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
	                                         
	        // Sesuaikan sama nama kolom tabel di database
	        $office = $this->office->get_one(array('C010_Code' => $rowData[0][0]));

	        $bank_exist = $this->KBAccount->get_one(array('C020_Name' => $rowData[0][1], 'C010_BankAccNumber' => $rowData[0][2]));

	        $data = array(
	            "C001_OfficeID" => $office['C000_SysID'],
	            "C020_Name" => $rowData[0][1],
	            "C010_BankAccNumber" => $rowData[0][2],
	            "C030_Descr" => $rowData[0][3]
	        );
	         
	        //sesuaikan nama dengan nama tabel
	        if(!$bank_exist){
	        	$this->KBAccount->add($data);
	        }
	        delete_files($media['file_path']);
	             
	    }

		redirect(base_url()."references/bank_accounts");
	}
}