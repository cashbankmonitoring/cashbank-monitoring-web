<?php require_once('Common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_panels extends Common {
	function __construct() {
		parent::__construct("TrcPanel");
		
		$this->meta 			= array();
		$this->scripts 			= array('../global/plugins/datatables/jquery.dataTables.min');
		$this->styles 			= array();
		$this->load->model(array('user_session','User_group_type',"User_group","Trc_menu_type"));
	}

	// function post_group_type(){
	// 	$this->layout = false;
	// 	$post = $_POST['dt_ins'];
	// 	$data['UserGroupTypeName'] = $post[0];
	// 	$data['userGroupTypeDesc'] = $post[1];
	// 	if($this->User_group_type->insert($data)){
	// 		return 1; 
	// 	}
	// 	return 0;
	// }
	function delete(){
		$this->layout = false;
		$id = $_POST['usr_id'];
		$this->Trc_menu_type->delete_panel_master($id);
		$this->Trc_menu_type->delete_sub_trc_panel(array("TrcPanelID"=>$id));
		echo "1";
	}

	function update(){
		$this->layout = false;
		$id=$_POST['usr_id'];
		$post = $_POST['dt_ins'];
		$data['C010_Code'] = $post[0];
		$data['C012_Label'] = $post[2];
		$data['C011_Descr'] = $post[1];
		if($_POST['arr']!=0){
			$trc = $_POST['arr'];
		}
		if($this->Trc_menu_type->update_panels($id,$data)){
			// 
			if($_POST['arr']!=0){
			$query = $this->Trc_menu_type->get_trc_sub_panel(array("TrcPanelID"=>$id))->result_array();
			$result = [];
			foreach ($query as $row) {
				array_push($result, $row['SubTrcTypeID']);
			}
			// print_r($result);
			// print_r($trc);
			$added = array_values(array_diff($trc,$result));
			// echo"added";
			// print_r($added);
				if(count($added)>0){
					for ($i=0; $i < count($added); $i++) { 
						$data1 = [];
						$data1['TrcPanelID'] = $id;
						$data1['SubTrcTypeID'] = $added[$i];
						$this->Trc_menu_type->insert_sub_panel($data1);
					}
				}
				// echo"deleted";
			$deleted = array_values(array_diff($result,$trc));
			// print_r($deleted);
				if(count($deleted)>0){
					for ($i=0; $i < count($deleted); $i++) { 
						$this->Trc_menu_type->delete_sub_trc_panel(array('TrcPanelID'=>$id,'SubTrcTypeID'=>$deleted[$i]));
					}
				}
			}
			echo "1"; 
		}
		// return false;
	}
	function get_panel_by_trc_id(){
		$this->layout = false;
		$id = $_POST['idx'];
		$query = $this->Trc_menu_type->get_trc_panel(array("TrcTypeID"=>$id))->result_array();
		$result = "";
		foreach($query as $row){
			// $result = $row['TrcTypeID']+
		}
		// print_r($query);
		echo json_encode($query);
	}
	
	function get_sub_trc_panels(){
		$this->layout = false;
		$id = $_POST['idx'];
		$query = $this->Trc_menu_type->get_trc_sub_panel(array("TrcPanelID"=>$id))->result_array();
		$result = "";
		foreach($query as $row){
			// $result = $row['TrcTypeID']+
		}
		// print_r($query);
		echo json_encode($query);
	}
	
	function get_trc_by_id(){
		$this->layout = false;
		$id = $_POST['idx'];
		$query = $this->Trc_menu_type->get(array("C000_SysID"=>$id))->result_array();
		$result = "";
		foreach($query as $row){
			// $result = $row['TrcTypeID']+
		}
		// print_r($query);
		echo json_encode($query);
	}

	function post(){
		$this->layout = false;
		$this->load->model(array('User_group_type',"User_group","Trc_menu_type"));
		$post = $_POST['dt_ins'];
		if($_POST['trcType'] != 0){
			$trc = $_POST['trcType'];
		}
		$data['C010_Code'] = $post[0];
		$data['C012_Label'] = $post[1];
		$data['C011_Descr'] = $post[2];

		if($this->Trc_menu_type->insert_panel_master($data)){
			// echo "1";
		}
		// print_r(count($trc));
		if($_POST['trcType'] != 0){
			$data['C000_SysID'] = $this->Trc_menu_type->get_last_panel_id();
			$last_id = $data['C000_SysID'];
			for ($i=0; $i < count($trc); $i++) {
				$data1 = [];
				$data1['TrcPanelID'] = $last_id;
				$data1['SubTrcTypeID'] = $trc[$i];
				$this->Trc_menu_type->insert_sub_panel($data1);
			}
		}
		// die();
		echo"0";

	}
	
}