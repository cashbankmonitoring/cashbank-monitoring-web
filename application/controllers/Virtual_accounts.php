<?php require_once('Common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Virtual_accounts extends Common {
	function __construct() {
		parent::__construct("VirtualAcc");
		
		$this->meta 			= array();
		$this->scripts 			= array('site/references','site/user','site/virtual_accounts','../global/plugins/datatables/jquery.dataTables.min');
		$this->styles 			= array();
		$this->load->model(array('user_session','virtual_account'));
	}

	public function get_ajax_data(){
		$this->layout = false;
		$this->load->model(array('datatable'));
		$table = 'T030_2VirtualAccount';
		$column_order = array(null,'C010_Code','C011_Descr', null); //set column field database for datatable orderable
	    $column_search = array('C010_Code','C011_Descr'); //set column field database for datatable searchable just firstname , lastname , address are searchable
	    $order = array('C000_SysID' => 'desc'); // default order
		
		$list = $this->datatable->get_datatables($table, $column_search, $column_order, $order);

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $key => $trow) {
            $no++;
            $number = $key + 1;
            $row = array();
            $row[] = $no;
            $row[] = $trow->C010_Code;
            $row[] = $trow->C011_Descr;
 
            //add html for action
            $row[] = '<a class="edit-virtual btn btn-sm btn-primary" title="Edit" data-id="'.$trow->C000_SysID.'"><i class="glyphicon glyphicon-pencil"></i></a>
                  <a class="delete-virtual btn btn-sm btn-danger" title="Hapus" data-id="'.$trow->C000_SysID.'")"><i class="glyphicon glyphicon-trash"></i> </a>';
 
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->datatable->count_all($table),
                        "recordsFiltered" => $this->datatable->count_filtered($table, $column_search, $column_order, $order),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
	}

	public function save_virtual(){
		$this->layout = false;
		$this->load->model(array('virtual_account'));

		$id_virtual = $_POST['virtual_id'];
        $data['C010_Code'] = $_POST['kode'];
        $data['C011_Descr'] = $_POST['nama'];

        $response = 0;
        if($id_virtual == 0){
        	if($this->virtual_account->add($data)){
	            $response = 1;
	        }else{
	            $response = 0;
	        }
        }else{
        	if($this->virtual_account->edit($id_virtual, $data)){
	            $response = 1;
	        }else{
	            $response = 0;
	        }
        }

        echo $response;
    }

	public function get_virtual_by_id(){
		$this->layout = false;
		$idx = $_POST["idx"];
	
		$row = $this->virtual_account->get_one(array("C000_SysID"=>$idx));
		echo json_encode($row);
	}

	public function check_delete_virtual(){
		$this->layout =false;
		$this->load->model(array('office'));
		$virtual_id = $_POST["virtual_id"];
		$is_trc = $this->office->get_form_day_delta(array("SubLedger2ID" => $virtual_id))->num_rows();
		if($is_trc != 0){
			echo"0";
		}else{
			echo"1";
		}
	}

	public function delete_virtual(){
		$id = $_POST["id"];
		if($this->virtual_account->delete($id)){
			echo "1";
		} else {
			echo "0";
		}
		die();
	}

	function get_data_excel($sheet, $highestRow, $highestColumn, $media){

		for ($row = 2; $row <= $highestRow; $row++){ 
			// Read a row of data into an array                 
	        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
	                                         
	        // Sesuaikan sama nama kolom tabel di database
	        $exist = $this->virtual_account->get_one(array('C010_Code' => $rowData[0][0]));

	        $data = array(
	            "C010_Code" => $rowData[0][0],
	            "C011_Descr" => $rowData[0][1]
	        );
	         
	        //sesuaikan nama dengan nama tabel
	        if(!$exist){
	        	$this->virtual_account->add($data);
	        }
	        delete_files($media['file_path']);
	             
	    }

		redirect(base_url()."references/virtual_account");
	}
}