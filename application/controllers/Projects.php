<?php

require_once('Common.php');
if (!defined('BASEPATH'))
        exit('No direct script access allowed');

class Projects extends Common {

        function __construct() {
                parent::__construct("Program");

                $this->meta = array();
                $this->scripts = array('site/references', 'site/user', 'site/partner', 'site/project', '../global/plugins/datatables/jquery.dataTables.min');
                $this->styles = array();
                $this->load->model(array('user_session', 'project'));
        }

        public function get_ajax_data() {
                $this->layout = false;
                $this->load->model(array('datatable'));
                $table = 'T005_Project';
                $column_order = array(null, 'PartnerName', 'C020_Descr', 'C060_DateStart', 'C070_DateFinished', 'C040_Value', 'C050_IsClose', null); //set column field database for datatable orderable
                $column_search = array('PartnerName', 'C020_Descr', 'C060_DateStart', 'C070_DateFinished', 'C040_Value', 'C050_IsClose'); //set column field database for datatable searchable just firstname , lastname , address are searchable
                $order = array('C000_SysID' => 'desc'); // default order

                $list = $this->datatable->get_datatables($table, $column_search, $column_order, $order);

                $data = array();
                $no = $_POST['start'];
                foreach ($list as $key => $trow) {
                        $no++;
                        $number = $key + 1;
                        $aw = date_create($trow->C060_DateStart);
                        $ak = date_create($trow->C070_DateFinished);
                        $row = array();
                        $row[] = $no;
                        $row[] = $trow->PartnerName;
                        $row[] = $trow->C020_Descr;
                        $row[] = (date_format($aw, 'd-m-Y') == '01-01-1970' ? '-' : date_format($aw, 'd-m-Y'));
                        $row[] = (date_format($ak, 'd-m-Y') == '01-01-1970' ? '-' : date_format($ak, 'd-m-Y'));
                        $row[] = number_format($trow->C040_Value, 0, ".", ",");
                        $row[] = ($trow->C050_IsClose == 0 ? 'OPEN' : 'CLOSE');

                        //add html for action
                        $row[] = '<a class="edit-program btn btn-sm btn-primary" title="Edit" data-id="' . $trow->C000_SysID . '"><i class="glyphicon glyphicon-pencil"></i></a>
                  <a class="delete-program btn btn-sm btn-danger" title="Hapus" data-id="' . $trow->C000_SysID . '")"><i class="glyphicon glyphicon-trash"></i> </a>';

                        $data[] = $row;
                }

                $output = array(
                    "draw" => $_POST['draw'],
                    "recordsTotal" => $this->datatable->count_all($table),
                    "recordsFiltered" => $this->datatable->count_filtered($table, $column_search, $column_order, $order),
                    "data" => $data,
                );
                //output to json format
                echo json_encode($output);
        }

        public function save_program() {
                $this->layout = false;
                $this->load->model(array('project'));

                if ($_POST['tgl_mulai'] == null) {
                        $_POST['tgl_mulai'] = '00-00-0000';
                }

                if ($_POST['tgl_selesai'] == null) {
                        $_POST['tgl_selesai'] = '00-00-0000';
                }

                $id_project = $_POST['program_id'];
                // $data['C010_Code'] = $_POST['nama'];
                $data['C020_Descr'] = $_POST['nama'];
                $data['C021_VirtualAccountID'] = $_POST['virtual'];
                $data['C030_PartnerID'] = $_POST['partner'];
                $data['C040_Value'] = str_replace(',', '', $_POST['nilai']);
                $data['C050_IsClose'] = $_POST['status'];
                $data['C060_DateStart'] = date('Y-m-d H:i:s', strtotime($_POST['tgl_mulai']));
                $data['C070_DateFinished'] = date('Y-m-d H:i:s', strtotime($_POST['tgl_selesai']));
                $data['C080_CurrencyID'] = $_POST['currency'];

                $response = 0;
                if ($id_project == 0) {
                        if ($this->project->add($data)) {
                                $response = 1;
                        } else {
                                $response = 0;
                        }
                } else {
                        if ($this->project->update($id_project, $data)) {
                                $response = 1;
                        } else {
                                $response = 0;
                        }
                }

                echo $response;
        }

        public function get_data() {
                $page = $_POST["h"];
                // $off_id = $_POST['id_off'];
                //$this->load->model('user');
                if ($page == "") {
                        $ofs = 0;
                        $lmt = 100;
                } else {
                        $ofs = ($page * 100) - 99;
                        $lmt = 100;
                }

                $tot = $this->project->get()->num_rows();
                // echo $tot+"+";
                // echo $lmt+"+";
                // echo $ofs+"+";

                $jml_page = ceil($tot / 100);
                // die();
                $tamplate = $this->project->get_data($criteria = '', $order = '', $order_by = 'desc', $limit = $lmt, $start = $ofs, $offset = 0)->result_array();
                $dt_out = "";
                // $result = mssql_query("
                // SELECT
                // 	dbo.T023_UserAccount.C000_SysID,
                // 	dbo.T023_UserAccount.C002_OfficeID,
                // 	dbo.T023_UserAccount.C010_Code,
                // 	dbo.T023_UserAccount.C020_Descr,
                // 	dbo.T023_UserAccount.C030_UserName,
                // 	dbo.T023_UserAccount.C040_UserFullname,
                // 	dbo.T023_UserAccount.C050_UserPasswd,
                // 	dbo.T023_UserAccount.C060_UserEmail,
                // 	dbo.T023_UserAccount.C070_UserState,
                // 	dbo.T023_UserAccount.C080_machineID,
                // 	dbo.T020_Office.C012_Level AS Off_Level,
                // 	dbo.T020_Office.C010_Code AS Off_Code,
                // 	dbo.T020_Office.C020_Descr AS Off_Descr
                // 	FROM
                // 	dbo.T023_UserAccount
                // 	LEFT JOIN dbo.T020_Office ON dbo.T023_UserAccount.C002_OfficeID = dbo.T020_Office.C000_SysID
                // 	WHERE C090_PartnerID = 0
                // 	");
                //$numRows = mssql_num_rows($result);
                // $dt_out = "";
                foreach ($tamplate as $key => $row) {
                        $number = $key + 1;
                        if ($row["C050_IsClose"] == "0") {
                                $st = "OPEN";
                        } else {
                                $st = "CLOSE";
                        }
                        $aw = date_create($row["C060_DateStart"]);
                        $ak = date_create($row["C070_DateFinished"]);
                        $dt_out .=
                                '<tr idx="' . $row["C000_SysID"] . '">
			<td>' . $number . '</td>
			<td idx="' . $row["C030_PartnerID"] . '">' . $row["PartnerName"] . '</td>
			<td>' . $row["C020_Descr"] . '</td>
			<td class="text-center">' . (date_format($aw, 'd-m-Y') == '01-01-1970' ? '-' : date_format($aw, 'd-m-Y')) . '</td>
			<td class="text-center">' . (date_format($ak, 'd-m-Y') == '01-01-1970' ? '-' : date_format($ak, 'd-m-Y')) . '</td>
			<td class="text-right">' . number_format($row["C040_Value"], 0, ".", ",") . '</td>
			<td idx="' . $row["C050_IsClose"] . '" class="text-center">' . $st . '</td>
			<td class="text-center">
				<a data-id="' . $row["C000_SysID"] . '" class="edit-program btn btn-sm btn-default" href="javascript:;" ><i class="icon-note"></i></a>
				<a class="delete-program btn btn-sm btn-danger" href="javascript:;"><i class="icons-office-52"></i></a>    
			</td>
		  </tr>';
                }
                //echo $dt_out;

                if ($page == "") {
                        $page = 1;
                }
                $btn = "";
                if ($page < 4) {
                        for ($i = 1; $i < 6; $i++) {
                                if ($page == $i) {
                                        // $btn .= '<button class="btn btn-warning active">'.$i.'</button>';
                                } else {
                                        $btn .= '<button onclick="javascript: getlist_partner($(this).html());" class="btn btn-warning">' . $i . '</button>';
                                }
                        }
                } else if ($page > ($jml_page - 2)) {
                        for ($i = ($jml_page - 5); $i < ($jml_page + 1); $i++) {
                                if ($page == $i) {
                                        $btn .= '<button class="btn btn-warning active">' . $i . '</button>';
                                } else {
                                        $btn .= '<button onclick="javascript: getlist_partner($(this).html());" class="btn btn-warning">' . $i . '</button>';
                                }
                        }
                } else {
                        for ($i = ($page - 2); $i < ($page + 3); $i++) {
                                if ($page == $i) {
                                        $btn .= '<button onclick="javascript: getlist_partner($(this).html());" class="btn btn-warning active">' . $i . '</button>';
                                } else {
                                        $btn .= '<button onclick="javascript: getlist_partner($(this).html());" class="btn btn-warning">' . $i . '</button>';
                                }
                        }
                }

                $pg = '
	<div act="' . $page . '" class="btn-group">
		<button onclick="javascript: getlist_partner(1);" type="button" class="btn btn-warning"><i class="fa fa-fast-backward"></i></button>
		<button onclick="javascript: getlist_partner(parseInt($(this).parent(\'div\').attr(\'act\')) -1);" type="button" class="btn btn-warning"><i class="fa fa-step-backward"></i></button>
		' . $btn . '
		<button onclick="javascript: getlist_partner(parseInt($(this).parent(\'div\').attr(\'act\')) +1);" type="button" class="btn btn-warning"><i class="fa fa-step-forward"></i></button>
		<button onclick="javascript: getlist_partner(' . $jml_page . ');" type="button" class="btn btn-warning"><i class="fa fa-fast-forward"></i></button>
	</div>';



                echo $dt_out . "|" . $pg;
        }

        public function get_program_by_id() {
                $this->layout = false;
                $idx = $_POST["idx"];

                $row = $this->project->get_data($criteria = 'C000_SysID = ' . $idx . '', $order = '', $order_by = '', $limit = '', $start = 0, $offset = 0)->row_array();

                echo json_encode($row);
        }

        public function post_project() {

                $dt_ins = $_POST["to_ins"];

                // 	$result = mssql_query("
                // 	INSERT INTO [db_bank].[dbo].[T005_Project]
                //           ([C010_Code]
                //           ,[C020_Descr]
                //           ,[C030_PartnerID]
                //           ,[C040_Value]
                //           ,[C050_IsClose]
                //           ,[C060_DateStart]
                //           ,[C070_DateFinished]
                //           ,[C080_CurrencyID])
                // 	VALUES
                //           ('".$to_ins[0]."' --<C010_Code, varchar(20),>
                //           ,'".$to_ins[1]."' --<C020_Descr, varchar(50),>
                //           ,".$to_ins[2]." --<C030_PartnerID, int,>
                //           ,".$to_ins[3]." --<C040_Value, float,>
                //           ,0 --<C050_IsClose, tinyint,>
                //           ,'".$to_ins[4]."' --<C060_DateStart, date,>
                //           ,'".$to_ins[5]."' --<C070_DateFinished, date,>
                //           ,".$to_ins[6]." --<C080_CurrencyID, int,>
                // 	   )
                // ");
                // $rslt_bank = mssql_query("
                // 	INSERT INTO [db_bank].[dbo].[T022_BankAccount]
                //           ([C001_OfficeID]
                //           ,[C010_BankAccNumber]
                //           ,[C020_Name]
                // 	   ,[C030_Descr])
                // 	VALUES
                //           (".$dt_ins[0]."
                //           ,'".$dt_ins[1]."'
                // 	   ,'".$dt_ins[2]."'
                // 	   ,'".$dt_ins[3]."')
                // ");
                // $tot = $this->partner->get()->num_rows();
                $data['C010_Code'] = $dt_ins[0];
                $data['C020_Descr'] = $dt_ins[1];
                $data['C030_PartnerID'] = $dt_ins[2];
                $data['C040_Value'] = $dt_ins[3];
                $data['C050_IsClose'] = 0;
                $data['C060_DateStart'] = $dt_ins[4];
                $data['C070_DateFinished'] = $dt_ins[5];
                $data['C080_CurrencyID'] = $dt_ins[6];

                if ($this->project->add($data)) {
                        echo "1";
                }
                die();
        }

        public function update_project() {
                $ktr_id = $_POST["prg_id"];
                $dt_ins = $_POST["dt_ins"];

                $aw = date_create($dt_ins[2]);
                $ak = date_create($dt_ins[3]);


                $data['C020_Descr'] = $dt_ins[1];
                $data['C030_PartnerID'] = $dt_ins[0];
                $data['C040_Value'] = $dt_ins[4];
                $data['C050_IsClose'] = $dt_ins[5];
                $data['C060_DateStart'] = date_format($aw, 'Y-m-d');
                $data['C070_DateFinished'] = date_format($aw, 'Y-m-d');
                // $data['C001_ParentID'] = 0;

                if ($this->project->update($ktr_id, $data)) {
                        echo "1";
                } else {
                        echo "0";
                }
                die();
        }

        public function delete_program() {
                $ktr_id = $_POST["ktr_id"];
                if ($this->project->delete($ktr_id)) {
                        echo "1";
                } else {
                        echo "0";
                }
                die();
        }
        
        public function all() {
                $this->layout = false;

                $projects = $this->project->find_all();

                echo json_encode($projects);
        }

    function get_data_excel($sheet, $highestRow, $highestColumn, $media){
        $this->load->model(array('partner', "virtual_account"));

        for ($row = 2; $row <= $highestRow; $row++){ 
            // Read a row of data into an array                 
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                                             
            // Sesuaikan sama nama kolom tabel di database
            $partner = $this->partner->get_one(array('PartnerCode' => $rowData[0][0]));

            $virtual = $this->virtual_account->get_one(array('C010_Code' => $rowData[0][2]));

            $user_exist = $this->project->get_one(array('C020_Descr' => $rowData[0][1]));
            
            $data = array(
                "C030_PartnerID" => $partner['SysID'],
                "C020_Descr" => $rowData[0][1],
                "C021_VirtualAccountID" => $virtual['C000_SysID'],
                "C060_DateStart" => date("Y-m-d", strtotime($rowData[0][3])),
                "C070_DateFinished" => date("Y-m-d", strtotime($rowData[0][4])),
                "C040_Value" => $rowData[0][5],
                "C080_CurrencyID" => $rowData[0][6],
                "C050_IsClose" => $rowData[0][7]
            );
             
            //sesuaikan nama dengan nama tabel
            if(!$user_exist){
                $this->project->add($data);
            }
            delete_files($media['file_path']);
                 
        }

        redirect(base_url()."references/program");
    }

}
