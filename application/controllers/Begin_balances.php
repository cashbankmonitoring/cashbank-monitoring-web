<?php

require_once('Common_transaction.php');
if (!defined('BASEPATH'))
        exit('No direct script access allowed');

class Begin_balances extends Common_transaction {

        function __construct() {
                parent::__construct("BegBal");

                $this->meta = array();
                $this->scripts = array('general', 'begin_balance');
                $this->styles = array();

                $this->load->model(array(
                    'transaction', 'trc', 'Debt', 'T510_rekap', 'kbaccount',
                    'Virtual_account', 'Day_delta', 'project', 'day_pos'));
        }

        public function index() {
                $header = $this->generate_header();
                $this->set_header($header);

                $data['header'] = $header;

                $data['read_only'] = $this->user_data['C130_IsReadOnly'];

                $office = $this->office->get_one(array(
                    'C000_SysID' => $this->office_id,
                ));
                $this->office->remove_suffix($office);
                $data['office'] = $office;

                $virtual = $this->Virtual_account->get();

                //get panel for begin balance ex: 1010
                for ($i = 0; $i < count($this->menu); $i++) {
                        if ($this->menu[$i]['C013_Label'] == 'Begin Balance') {
                                $data['panels'] = $this->get_trc_panel($this->menu[$i]['C000_SysID']);
                        }
                }

                $tabs = array();
                foreach ($data['panels'] AS $panel) {
                        $dt_panel = $data;
                        if ($panel['C010_Code'] == 'AcctBank') {
                                //get data bank
                                $dt_panel['banks'] = $this->kbaccount->get(array('C001_OfficeID' => $this->office_id), 'C020_Name ASC')->result_array();
                                $dt_panel['banks'] = $this->get_data_balance($dt_panel['banks'], count($dt_panel['banks']), $virtual, 1);
                                $tabs[] = $this->load->view('begin_balance/bank', $dt_panel, TRUE);
                        } else if ($panel['C010_Code'] == 'Kas') {
                                //get data kas
                                $dt_panel['kas'] = array();
                                $dt_panel['kas'] = $this->get_data_balance($dt_panel['kas'], 1, $virtual, 2);
                                $tabs[] = $this->load->view('begin_balance/kas', $dt_panel, TRUE);
                        } else if ($panel['C010_Code'] == 'Hutang') {
                                //get data hutang
                                $dt_panel['debts'] = $this->Debt->get(array(
                                            'Lft > ' => 7,
                                            'Rgt < ' => 16,
                                            'IsDetail' => 1,
                                        ))->result_array();
                                $dt_panel['debts'] = $this->get_data_balance($dt_panel['debts'], count($dt_panel['debts']), 1, 3);
                                $tabs[] = $this->load->view('begin_balance/hutang', $dt_panel, TRUE);
                        } else if ($panel['C010_Code'] == 'HutangAlc') {
                                //data hutang alokasi hutang salur dana (mensos)
                                $dt_panel['allocations'] = $this->project->get(array('C050_IsClose' => 0))->result_array();
                                $dt_panel['allocations'] = $this->get_data_balance($dt_panel['allocations'], count($dt_panel['allocations']), 1, 4);
                                $tabs[] = $this->load->view('begin_balance/hutang_alokasi', $dt_panel, TRUE);
                        }
                }

                $data['tabs'] = $tabs;

                $this->load->view("begin_balance/index", $data);
        }

        function get_data_balance($data_name, $count, $virtual, $panel) {
                for ($i = 0; $i < $count; $i++) {
                        $data_name[$i]['virtual_account'] = $virtual;
                        $total_amount = 0;
                        for ($a = 0; $a < count($virtual); $a++) {
                                $data_array = array(
                                    'SLedgerID' => $this->office_id
                                        // 'C010_TrcTypeID' => $check_node['C010_TrcTypeID']
                                );

                                if ($panel == 1) {
                                        $data_array['SubLedger2ID'] = $a + 1;
                                        $data_array['MLedgerID'] = 3;
                                        $data_array['SubLedger1ID'] = $data_name[$i]['C000_SysID'];
                                } else if ($panel == 2) {
                                        $data_array['SubLedger2ID'] = $a + 1;
                                        $data_array['MLedgerID'] = 2;
                                } else if ($panel == 3) {
                                        $data_array['SubLedger2ID'] = 1;
                                        $data_array['MLedgerID'] = $data_name[$i]['SysID'];
                                } else if ($panel == 4) {
                                        $data_array['SubLedger3ID'] = $data_name[$i]['C000_SysID'];
                                        $data_array['SubLedger2ID'] = 2;
                                        $data_array['MLedgerID'] = 19;
                                }

                                $balance = abs($this->day_pos->sum_balance($data_array));
                                if ($panel != 3 && $panel != 4) {
                                        $data_name[$i]['virtual_account'][$a]['rekap']['Amount'] = $balance;
                                } else {
                                        $data_name[$i]['rekap']['Amount'] = $balance;
                                }
                                $total_amount = $total_amount + $balance;
                        }
                        $data_name[$i]['total_amount'] = $total_amount;
                }

                return $data_name;
        }

        function save($panel_id = null) {
                $virtual = $this->Virtual_account->get();
                
                $header = $this->get_header();
                
                if (!$header) {
                        $this->session->set_flashdata('form_msg', array('failed' => true, 'msg' => "can't be saved."));
                        return;
                }
                
                if ($panel_id == 1) {
                        $count_obj = $this->input->post('count_bank');

                        $sub_trc = 1;
                        $mapper_id = 1;
                        $m_ledger = 3;
                        $type = 'bank';
                        $virtual = count($virtual);
                        $tpanel_id = 1;
                } else if ($panel_id == 2) {
                        $count_obj = 1;

                        $sub_trc = 2;
                        $mapper_id = 1;
                        $m_ledger = 2;
                        $type = 'kas';
                        $virtual = count($virtual);
                        $tpanel_id = 2;
                } else if ($panel_id == 3) {
                        $count_obj = $this->input->post('count_debts');

                        $sub_trc = 3;
                        $mapper_id = 1;
                        $m_ledger = 0;
                        $type = 'hutang';
                        $virtual = 1;
                        $tpanel_id = 3;
                } else {
                        $count_obj = $this->input->post('count_allocation');

                        $sub_trc = 4;
                        $mapper_id = 1;
                        $m_ledger = 19;
                        $type = 'hutangalk';
                        $virtual = 1;
                        $tpanel_id = 25;
                }

                for ($i = 0; $i < $count_obj; $i++) {
                        for ($a = 1; $a <= $virtual; $a++) {
                                $input_system = $this->input->post('system_' . $type . '_' . $a . '[' . $i . ']');
                                $input_aktual = $this->input->post('aktual_' . $type . '_' . $a . '[' . $i . ']');
                                $input_adjsmnt = $this->input->post('adjustment_' . $type . '_' . $a . '[' . $i . ']');

                                if (!$input_adjsmnt) {
                                        continue;
                                }

                                // input ke tabel T510_Rekap
                                $input_rekap = array(
                                    'C010_TrcTypeID' => $header['TrcTypeID'],
                                    'C011_Month' => $header['Month'],
                                    'C012_TrcID' => $this->t510_rekap->get_sys_id(),
                                    'C050_Rev' => $header['Rev'],
                                    'C000_LineID' => $i + 1,
                                    'C001_SubTrcTypeID' => $sub_trc,
                                    'C010_TrcPanelID' => $tpanel_id,
                                    'C050_DocDate' => $header['DocDate'],
                                    'C210_MLedgerID' => $m_ledger,
                                    'C200_SLedgerID' => $header['OfficeID'],
                                    'C212_SubLedger2ID' => $a,
                                    'C213_SubLedger3ID' => 0,
                                    'C072_Amount1' => str_replace(",", "", $input_system),
                                    'C073_Amount2' => str_replace(",", "", $input_aktual),
                                    'C073_Amount3' => str_replace(",", "", $input_adjsmnt)
                                );

                                if ($panel_id == 1) {
                                        $input_rekap['C211_SubLedger1ID'] = $this->input->post('bank_id[' . $i . ']');
                                } else if ($panel_id == 2) {
                                        $input_rekap['C211_SubLedger1ID'] = 0;
                                } else if ($panel_id == 3) {
                                        $input_rekap['C212_SubLedger2ID'] = 1;
                                        $input_rekap['C211_SubLedger1ID'] = 0;
                                        $input_rekap['C210_MLedgerID'] = $this->input->post('mlegder[' . $i . ']');
                                } else if ($panel_id == 4) {
                                        $input_rekap['C212_SubLedger2ID'] = 2;
                                        $input_rekap['C213_SubLedger3ID'] = $this->input->post('allocation_id[' . $i . ']');
                                        $input_rekap['C211_SubLedger1ID'] = 0;
                                }

                                if (!$this->T510_rekap->add($input_rekap)) {
                                        print_r("error add 510 rekap");
                                        die();
                                }
                                // input ke tabel T610_trc
                                $input_trc = array(
                                    'C010_TrcTypeID' => $input_rekap['C010_TrcTypeID'],
                                    'C011_Month' => $input_rekap['C011_Month'],
                                    'C000_TrcID' => $input_rekap['C012_TrcID'],
                                    'C050_Rev' => $input_rekap['C050_Rev'],
                                    'C012_LineID' => $input_rekap['C000_LineID'],
                                    'C001_SubTrcTypeID' => $input_rekap['C001_SubTrcTypeID'],
                                    'C013_MapperID' => $mapper_id,
                                    'C014_MonthPostIdx' => date('m'),
                                    'YearIdx' => date('y'),
                                    'MonthIdx' => date('m'),
                                    'C015_DatePos' => date("Y-m-d"),
                                    'C016_TimePost' => date("h:i:s"),
                                    'C050_DocNum' => $header['DocNum'],
                                    'C050_DocDate' => $input_rekap['C050_DocDate'],
                                    'C050_AccountDate' => date("Y-m-d h:i:s"),
                                    'MLedgerID' => $input_rekap['C210_MLedgerID'],
                                    'SLedgerID' => $input_rekap['C200_SLedgerID'],
                                    'SubLedger1ID' => $input_rekap['C211_SubLedger1ID'],
                                    'SubLedger2ID' => $input_rekap['C212_SubLedger2ID'],
                                    'SubLedger3ID' => $input_rekap['C213_SubLedger3ID']
                                );

                                $Amount1 = $input_rekap['C072_Amount1'];
                                $Amount2 = $input_rekap['C073_Amount2'];

                                if ($panel_id == 3 || $panel_id == 4) {
                                        $input_rekap['C073_Amount2'] = -1 * $input_rekap['C073_Amount2'];
                                } else {
                                        $Amount1 = -1 * $Amount1;
                                }

                                for ($b = 0; $b < 2; $b++) {
                                        if ($b == 0 && $input_rekap['C072_Amount1'] != 0) {
                                                $input_trc['MinPlus'] = $Amount1 > 0 ? 1 : -1;
                                                $input_trc['AmountTrc'] = abs($Amount1);
                                                $input_trc['AmountInTrc'] = 0;
                                                $input_trc['AmountOutTrc'] = 0;
                                                if ($Amount1 > 0) {
                                                        $input_trc['AmountInTrc'] = $Amount1;
                                                } else {
                                                        $input_trc['AmountOutTrc'] = $Amount1;
                                                }
                                        } else {
                                                $input_trc['MinPlus'] = $Amount2 > 0 ? 1 : -1;
                                                $input_trc['AmountTrc'] = abs($Amount2);
                                                $input_trc['AmountInTrc'] = 0;
                                                $input_trc['AmountOutTrc'] = 0;
                                                if ($input_rekap['C073_Amount2'] > 0) {
                                                        $input_trc['AmountInTrc'] = $Amount2;
                                                } else {
                                                        $input_trc['AmountOutTrc'] = $Amount2;
                                                }
                                                $b = 1;
                                        }

                                        if (!$this->transaction->add($input_trc)) {
                                                print_r("error add trc");
                                                die;
                                        }

                                        $day_delta = array(
                                            'SLedgerID' => $input_trc['SLedgerID'],
                                            'MLedgerID' => $input_trc['MLedgerID'],
                                            'SubLedger1ID' => $input_trc['SubLedger1ID'],
                                            'SubLedger2ID' => $input_trc['SubLedger2ID'],
                                            'SubLedger3ID' => $input_trc['SubLedger3ID'],
                                        );

                                        $this->Day_delta->update_amount($day_delta, $input_rekap['C073_Amount2']);
                                }
                        }
                }
                $this->session->set_flashdata('form_msg', array('success' => true, 'msg' => "has been saved."));
                redirect(base_url() . "begin_balances");
        }
}
