<?php require_once('Common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Offices extends Common {
	function __construct() {
		parent::__construct("Office");
		
		$this->meta 			= array();
		$this->scripts 			= array('site/references','site/office','../global/plugins/datatables/jquery.dataTables.min');
		$this->styles 			= array();
		$this->load->model(array('user_session','office'));
	}

	public function get_ajax_data(){
		$this->layout = false;
		$this->load->model(array('datatable'));
		$table = 'T020_Office';
		$column_order = array(null,'C012_Level','C010_Code','C020_Descr','C100_Long', null); //set column field database for datatable orderable
	    $column_search = array('C012_Level','C010_Code','C020_Descr','C100_Long', 'C101_Lat'); //set column field database for datatable searchable just firstname , lastname , address are searchable
	    $order = array('C000_SysID' => 'desc'); // default order
		
		$list = $this->datatable->get_datatables($table, $column_search, $column_order, $order);

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $key => $trow) {
            $no++;
            $number = $key + 1;
            $row = array();
            $row[] = $no;
            $level = $this->user_level($trow->C012_Level);
            $row[] = $level;
            $row[] = $trow->C010_Code;
            $row[] = $trow->C020_Descr;
            $row[] = $trow->C100_Long.' - '.$trow->C101_Lat;
 
            //add html for action
            $row[] = '<a class="edit-office btn btn-sm btn-primary" title="Edit" data-id="'.$trow->C000_SysID.'"><i class="glyphicon glyphicon-pencil"></i></a>
                  <a class="delete-office btn btn-sm btn-danger" title="Hapus" data-id="'.$trow->C000_SysID.'")"><i class="glyphicon glyphicon-trash"></i> </a>';
 
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->datatable->count_all($table),
                        "recordsFiltered" => $this->datatable->count_filtered($table, $column_search, $column_order, $order),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
	}

	public function get_office_by_id(){
		$this->layout = false;
		$idx = $_POST["idx"];
	
		$result = $this->office->get($criteria='C000_SysID = '.$idx.'',$order='',$order_by='',$limit='',$start=0,$offset = 0)->row_array();

		echo json_encode($result);
	}

	public function check_code(){
		$kode = $_POST['code_office'];
		$this->load->model(array('Office'));

		$query = $this->Office->get(array("C010_Code"=>$kode))->num_rows();
		echo $query;
		die();
	}

	public function save_office(){
		$this->layout = false;
		$id_kantor = $_POST['kantor_id'];
        $data['C012_Level'] = $_POST['level'];
        $data['C001_ParentID'] = 0;
        if($_POST['kprk_id'] != null){
        	$data['C001_ParentID'] = $_POST['kprk_id'];
        }else{
        	$data['C001_ParentID'] = $_POST['regional_id'];
        }
        $data['C010_Code'] = $_POST['kode'];
        $data['C020_Descr'] = $_POST['deskripsi'];
        $data['C100_Long'] = $_POST['koor_x'];
        $data['C101_Lat'] = $_POST['koor_y'];

        $response = 0;
        if($id_kantor == 0){
        	// $data['C000_SysID'] = $this->office->get_new_id('C000_SysID');
        	// $data['C000_SysID'] = $this->office->get_sys_id();
        	if($this->office->add($data)){
	            $response = 1;
	        }else{
	            $response = 0;
	        }
        }else{
        	if($this->office->update($id_kantor, $data)){
	            $response = 1;
	        }else{
	            $response = 0;
	        }
        }
        
        echo $response;
    }

	public function check_delete_office(){
		$this->layout =false;
		$id = $_POST["id"];
		$this->load->model(array('program_alocation'));
		
		$is_alokasi = $this->program_alocation->get_data($criteria = "C010_OfficeID = ".$id."", $limit = 1)->num_rows();
		$is_parent = $this->office->get($criteria = "C001_ParentID = ".$id."",$limit = 1)->num_rows();
		$is_trc = $this->office->get_form_day_delta(array("SLedgerID" => $id))->num_rows();
		if($is_parent != 0 || $is_trc != 0 || $is_alokasi != 0){
			echo"0";
		}else{
			echo"1";
		}
	}

	public function delete_office(){
		$id = $_POST["id"];
		if($this->office->delete($id)){
			echo "1";
		} else {
			echo "0";
		}
		die();
	}

	function get_data_excel($sheet, $highestRow, $highestColumn, $media){
		for ($row = 2; $row <= $highestRow; $row++){ //  Read a row of data into an array                 
	        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
	                                         
	        //Sesuaikan sama nama kolom tabel di database                               
	        $data = array(
	            "C012_Level" => $rowData[0][0],
	            "C001_ParentID" => $rowData[0][1],
	            "C010_Code" => substr($rowData[0][2],0,7),
	            "C020_Descr" => $rowData[0][3],
	            "C100_Long" => $rowData[0][4],
	            "C101_Lat" => $rowData[0][5]
	        );
	         
	        //sesuaikan nama dengan nama tabel
	        $this->office->add_export($data);
	        delete_files($media['file_path']);
	             
	    }

		redirect(base_url()."references/office");
	}
}