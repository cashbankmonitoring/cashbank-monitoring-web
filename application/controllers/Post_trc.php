<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author Fikran
 */
require_once('Common.php');
class Post_Trc extends Common{
        private $draft_type;
        private $sys_id;
        
        function __construct() {
                parent::__construct();
                
                $this->layout = FALSE;
                
                $this->draft_type = $this->input->post('draft_type') ?: TRC_DRAFT_TYPE_DRAFT;
                if(!$this->draft_type == TRC_DRAFT_TYPE_DRAFT 
                        && !$this->draft_type == TRC_DRAFT_TYPE_APPROVED ){
                        die(0);
                }
                
                $header = $this->input->post('header');
                if(!isset($header['C050_DocNum']) && $header['C050_DocNum']){
                        die(0);
                }
                
                $this->sys_id = $this->input->post('sys_id');
                
                $this->load->model('t500_rekap');
                $this->load->model('t510_rekap');
                $this->load->model('node');
                $this->load->model('trc');
                
                $C050_DocNum = $header['C050_DocNum'];
                
                $this->save_header($C050_DocNum);
	}
        
        
        
        private function save_header($C050_DocNum){
                $node = array(
                    'C010_TrcTypeID' => TRC_TYPE_TRANSACTION,
                    'C011_Month' => $this->node->get_curr_yearmonth(),
                    'C013_DraftReadyApprCancel' => $this->draft_type,
                    'C045_UserID' => $this->user_data['C002_OfficeID'],
                    'C045_Dtime' => $this->node->get_curr_datetime(),
                    'C050_DocDate' => $this->node->get_curr_datetime(),
                    'C050_DocNum' => $C050_DocNum,
                );
                $old_node = NULL;
                if($this->sys_id){
                        $old_node = $this->node->get_one(array(
                            'C000_SysID' => $this->sys_id,
                        ));
                }
                if($old_node){ // update node
                        if($old_node['C045_UserID'] != $this->user_data['C002_OfficeID']){ //Security
                                die(0);
                        }
                        $node['C050_Rev'] = $old_node['C050_Rev'] + 1;
                        
                        // Update DB
                        $where = array(
                            'C000_SysID' => $this->sys_id,
                        );
                        $this->node->update($where, $node);
                }else{ // add node
                        $node['C050_Rev'] = 1;
                        $this->node->insert($node);
                        
                        $node = $this->node->get_one($node); // retrieve inserted data for get SysID
                        $this->sys_id = $node['C000_SysID'];
                }
                
                // add to t500_rekap
                $this->t500_rekap->insert($node);
        }
        
        public function submit_pelanggan(){
                
        }
        
        public function submit_program(){
                
        }
        
        public function submit_transfer_keluar(){
                
        }
        
        public function submit_transfer_kasbank(){
                
        }
        
        public function submit_internal_operasional(){
                
        }
}
