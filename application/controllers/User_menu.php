<?php require_once('Common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_menu extends Common {
	function __construct() {
		parent::__construct("TrcType");
		
		$this->meta 			= array();
		$this->scripts 			= array('../global/plugins/datatables/jquery.dataTables.min');
		$this->styles 			= array();
		$this->load->model(array('user_session','User_group_type',"User_group","Trc_menu_type"));
	}

	// function post_group_type(){
	// 	$this->layout = false;
	// 	$post = $_POST['dt_ins'];
	// 	$data['UserGroupTypeName'] = $post[0];
	// 	$data['userGroupTypeDesc'] = $post[1];
	// 	if($this->User_group_type->insert($data)){
	// 		return 1; 
	// 	}
	// 	return 0;
	// }
	function delete(){
		$this->layout = false;
		$id = $_POST['usr_id'];
		$this->Trc_menu_type->delete($id);
		$this->Trc_menu_type->delete_by_type($id);
		echo "1";
	}

	function update(){
		$this->layout = false;
		$id=$_POST['usr_id'];
		$post = $_POST['dt_ins'];
		$data['C010_Code'] = $post[0];
		$data['C012_LineID'] = $post[1];
		$data['C013_Label'] = $post[2];
		$data['C014_Link'] = $post[3];
		$data['C015_Icon'] = $post[4];
		$data['C016_ParentID'] = $post[5];
		$data['C011_Descr'] = $post[6];
		if($_POST['arr'] != 0){
			$trc = $_POST['arr'];
		}
		if($this->Trc_menu_type->update($id,$data)){
			// 
			if($_POST['arr'] != 0){
			$query = $this->Trc_menu_type->get_trc_panel(array("TrcTypeID"=>$id))->result_array();
			$result = [];
			foreach ($query as $row) {
				array_push($result, $row['TrcPanelID']);
			}
			// print_r($result);
			// print_r($trc);
			$added = array_values(array_diff($trc,$result));
			// echo"added";
			// print_r($added);
				if(count($added)>0){
					for ($i=0; $i < count($added); $i++) { 
						$data1 = [];
						$data1['TrcTypeID'] = $id;
						$data1['TrcPanelID'] = $added[$i];
						$this->Trc_menu_type->insert_panel($data1);
					}
				}
				// echo"deleted";
			$deleted = array_values(array_diff($result,$trc));
			// print_r($deleted);
				if(count($deleted)>0){
					for ($i=0; $i < count($deleted); $i++) { 
						$this->Trc_menu_type->delete_panel(array('TrcTypeID'=>$id,'TrcPanelID'=>$deleted[$i]));
					}
				}
			}
			echo "1"; 
		}
		// return false;
	}
	function get_panel_by_trc_id(){
		$this->layout = false;
		$id = $_POST['idx'];
		$query = $this->Trc_menu_type->get_trc_panel(array("TrcTypeID"=>$id))->result_array();
		$result = "";
		foreach($query as $row){
			// $result = $row['TrcTypeID']+
		}
		// print_r($query);
		echo json_encode($query);
	}
	
	function get_trc_by_id(){
		$this->layout = false;
		$id = $_POST['idx'];
		$query = $this->Trc_menu_type->get(array("C000_SysID"=>$id))->result_array();
		$result = "";
		foreach($query as $row){
			// $result = $row['TrcTypeID']+
		}
		// print_r($query);
		echo json_encode($query);
	}

	function post(){
		$this->layout = false;
		$this->load->model(array('User_group_type',"User_group","Trc_menu_type"));
		$post = $_POST['dt_ins'];
		if($_POST['trcType'] != 0){
			$trc = $_POST['trcType'];
		}
		// print_r($post);
		// print_r($post);
		// die();
		// // print_r($post);
		// dt_ins.push($('[name="code"]').val());
		// dt_ins.push($('[name="lineID"]').val());
		// dt_ins.push($('[name="label"]').val());
		// dt_ins.push($('[name="link"]').val());
		// dt_ins.push($('[name="icon"]').val());
		// dt_ins.push($('[name="parent"]').val());
		// dt_ins.push($('[name="desc"]').val());
		// dt_ins.push($('[name="code"]').val());
		$data['C010_Code'] = $post[0];
		$data['C012_LineID'] = $post[1];
		$data['C013_Label'] = $post[2];
		$data['C014_Link'] = $post[3];
		$data['C015_Icon'] = $post[4];
		$data['C016_ParentID'] = $post[5];
		$data['C011_Descr'] = $post[6];
		$data['C000_SysID'] = $this->Trc_menu_type->get_last_id()+1;

		// $data['SysID'] = $this->User_group_type->get()->num_rows()+1;
		if($this->Trc_menu_type->insert($data)){
			// echo "1";
		}
		// print_r(count($trc));
		if($_POST['trcType'] != 0){
		$last_id = $data['C000_SysID'];
			for ($i=0; $i < count($trc); $i++) {
				$data1 = [];
				$data1['TrcTypeID'] = $last_id;
				$data1['TrcPanelID'] = $trc[$i];
				$this->Trc_menu_type->insert_panel($data1);
			}
		}
		// die();
		echo"0";

	}
	
}