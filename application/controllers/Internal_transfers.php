<?php

require_once('Common_transaction.php');
if (!defined('BASEPATH'))
        exit('No direct script access allowed');

class Internal_Transfers extends Common_Transaction {

        function __construct() {
                parent::__construct('TrfInt');
        }

        public function index() {
                $data = array();
                //get panel for begin balance ex: 1010
                for ($i = 0; $i < count($this->menu); $i++) {
                        if ($this->menu[$i]['C013_Label'] == 'Transfers') {
                                foreach ($this->menu[$i]['sub_menu'] as $row) {
                                        if ($row['C013_Label'] == 'Transfer Internal') {
                                                // print_r("expression");
                                                $data['panels'] = $this->get_trc_panel($row['C000_SysID']);
                                        }
                                }
                                // print_r($this->menu[$i]);
                        }
                }
                // print_r($data);
                // die;
                $this->load_index($data);
        }

        public function get_transfer_masuk() {
                $this->layout = false;
                $this->load->model('internal_transfer_status');

                $get = $this->input->get();

                $page = $this->t510_rekap->get_transfer_masuk(
                        $this->office_id, $get['start'], $get['length']
                );

                $page['draw'] = $get['draw'];
                $page['start'] = $get['start'];
                $page['recordsTotal'] = $page['total'];
                $page['recordsFiltered'] = $page['total'];

                $office_ids = array();
                $bank_acc_ids = array();

                $rows = array();
                foreach ($page['data'] as $key => $rekap) {
                        if ($key >= $get['length']) {
                                break;
                        }

                        $rows[$key] = $rekap;

                        $office_id = $rekap['SLedgerID'];
                        $office = $this->office->get(array('C000_SysID' => $office_id))->row_array();
                        $rows[$key]['office_name'] = $office['C020_Descr'];


                        if (!in_array($office_id, $office_ids)) {
                                $office_ids[] = $office_id;
                        }

                        $bank_acc_id = $rekap['SubLedger1ID'];
                        if ($rekap['MLedgerID'] == 2) {
                                $rows[$key]['bank_acc'] = 'Kas';
                        } else {
                                $bank_acc = $this->kbaccount->get(array('C000_SysID' => $bank_acc_id))->row_array();
                                $rows[$key]['bank_acc'] = $bank_acc['C030_Descr'] . ' - ' . $bank_acc['C010_BankAccNumber'];
                        }


                        $bank_acc_to_id = $rekap['SubLedger1IDTo'];
                        if ($rekap['MLedgerIDTo'] == 2) {
                                $rows[$key]['bank_acc_to'] = 'Kas';
                        } else {
                                $bank_acc_to = $this->kbaccount->get(array('C000_SysID' => $bank_acc_to_id))->row_array();
                                $rows[$key]['bank_acc_to'] = $bank_acc_to['C030_Descr'] . ' - ' . $bank_acc_to['C010_BankAccNumber'];
                        }


                        if ($bank_acc_id && !in_array($bank_acc_id, $bank_acc_ids)) {
                                $bank_acc_ids[] = $bank_acc_id;
                        }
                }

                $page['data'] = $rows;

                $page['offices'] = array();
                $page['bank_accounts'] = array();

                if ($office_ids) {
                        $page['offices'] = $this->office->get(
                                'C000_SysID IN (' . implode(',', $office_ids) . ')')->result_array();
                }

                if ($bank_acc_ids) {
                        $page['bank_accounts'] = $this->kbaccount->get(
                                        'C000_SysID IN (' . implode(',', $bank_acc_ids) . ')')->result_array();
                }

                echo json_encode($page);
        }

        function accept() {
                $this->response_to_transfer_masuk('accept');
        }

        function reject() {
                $this->response_to_transfer_masuk('reject');
        }

        // ACTION -> accept / reject
        private function response_to_transfer_masuk($action = 'accept') {
                $this->load->model('internal_transfer_status');

                $data = $_POST['data'];

                $header = $this->get_header();

                $SubTrcTypeCode = SUB_TRC_TYPE_CODE__TRF_INT_MSK_APP;
                $status = 1;
                if ($action != 'accept') {
                        $SubTrcTypeCode = SUB_TRC_TYPE_CODE__TRF_INT_MSK_REJ;
                        $status = 2;
                }
                $SubTrcType = $this->sub_trc_type->get_one(array(
                    'C010_Code LIKE' => $SubTrcTypeCode,
                ));
                foreach ($data as $key => $elmnt) {
                        $rekap = $this->t510_rekap->get_one(array(
                            'C012_TrcID' => $elmnt['TrcID'],
                            'C000_LineID' => $elmnt['LineID'],
                            'C201_SLedgerIDTo' => $this->office_id,
                        ));
                        
                        if (!$rekap) {
                                continue;
                        }

                        $rekap['C073_Amount2'] = $elmnt['Amount'];
                        $rekap['C226_DescriptionTo'] = isset($elmnt['Description']) ? $elmnt['Description'] : '';


                        $this->t510_rekap->update(array(
                            'C012_TrcID' => $elmnt['TrcID'],
                            'C000_LineID' => $elmnt['LineID'],
                                ), array(
                            'C073_Amount2' => $rekap['C073_Amount2'],
                            'C226_DescriptionTo' => $rekap['C226_DescriptionTo'],
                        ));

                        $rekap['C001_SubTrcTypeID'] = $SubTrcType['C000_SysID'];
                        $this->trc->save_from_rekap(CRUD_INSERT, $header, $rekap);

                        $this->internal_transfer_status->update(array(
                            'TrcID' => $elmnt['TrcID'],
                            'LineID' => $elmnt['LineID'],
                                ), array(
                            'Status' => $status,
                            'UpdatedAt' => $this->internal_transfer_status->get_curr_datetime(),
                        ));
                }

                $this->response(array(
                    'status' => 1,
                    'message' => 'Success'
                ));
        }

}
