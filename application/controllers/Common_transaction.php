<?php

require_once('Common.php');
if (!defined('BASEPATH'))
        exit('No direct script access allowed');

class Common_Transaction extends Common {

        function __construct($module = null) {
                parent::__construct($module);

                $this->meta = array();
                $this->scripts = array('transaction', 'post.transaction');
                $this->styles = array();

                $this->load->model(array(
                    'trc_type',
                    't500_rekap',
                    'office',
                    'node',
                    'program_alocation',
                    'project',
                    'transaction',
                    'Debt',
                    't510_rekap',
                    'kbaccount',
                    't024_sub_trc_type',
                    'trc',
                    'day_delta',
                    'external_transfer',
                    'virtual_account',
                    'm_ledger',
                    'map_to_ledger',
                    'day_pos',
                    'user_group',
                    'transaction_panel',
                    'sub_trc_type',
                ));
        }

        protected function load_index($data = array()) {
                $trc_type = $this->m_trc_type;

                $office_level = $this->user_data['office_detail']['C012_Level'];
                $office_id = $this->user_data['C002_OfficeID'];

                $header = $this->generate_header();
                $this->set_header($header);
                $data['header'] = $header;

                $data['office_level'] = $office_level;
                $data['office_id'] = $office_id;

                $data['trc_type'] = $trc_type;
                $data['kasbank'] = $this->m_ledger->get(array(
                    'Lft > ' => 1,
                    'Rgt < ' => 6,
                    'IsDetail' => 1,
                ));
                $data['banks'] = $this->kbaccount->get(
                                array(
                            'T022_KBAccount.C001_OfficeID' => $this->user_data['C002_OfficeID']
                                ), 'C020_Name ASC, C000_SysID ASC'
                        )->result_array();

                $banks = array();
                foreach ($data['banks'] AS $bank_account) {
                        $banks[$bank_account['C020_Name']][] = $bank_account;
                }
                ksort($banks);
                $data['banks2'] = $banks;

                $data['bank_balance'] = $this->day_pos->sum_balance(array(
                    'MLedgerID' => TRC_M_LEDGER_BANK,
                    'SLedgerID' => $this->office_id,
                ));
                $data['cash_balance'] = $this->day_pos->sum_balance(array(
                    'MLedgerID' => TRC_M_LEDGER_KAS,
                    'SLedgerID' => $this->office_id,
                ));

                // get in transfer
                $data['bank_intransfer_balance'] = $this->day_pos->sum_balance(array(
                    'MLedgerID' => 10,
                    'SLedgerID' => $this->office_id,
                    'SubLedger1ID > ' => 0,
                ));
                $data['cash_intransfer_balance'] = $this->day_pos->sum_balance(array(
                    'MLedgerID' => 10,
                    'SLedgerID' => $this->office_id,
                    'SubLedger1ID' => 0,
                ));

                $this->load->view("transaction/index", $data);
        }

        function get_panel_view($panel_code) {
                $this->layout = FALSE;

                $trc_panel = $this->transaction_panel->get_panel(array(
                            'T024_2TrcPanel.C010_Code LIKE' => $panel_code,
                        ))->row_array();
                $sub_trc_types = $this->sub_trc_type->get(array(
                    'C010_TrcPanelID' => $trc_panel['C000_SysID'],
                ));

                $user = $this->user_data;

                $data = array();
                $data['panel'] = $trc_panel;
                $data['sub_trc_types'] = $sub_trc_types;
                if (strcasecmp($panel_code, 'TrcPlg') == 0) {
                        $data['ledger'] = $this->m_ledger->get(array(
                            'Lft > ' => 1,
                            'Rgt < ' => 6,
                            'IsDetail' => 1,
                        ));
                        $data['banks'] = $this->kbaccount->get(
                                        array(
                                    'T022_KBAccount.C001_OfficeID' => $user['C002_OfficeID']
                                        ), 'C020_Name ASC, C000_SysID ASC'
                                )->result_array();
                } elseif (strcasecmp($panel_code, 'TrcPrg') == 0) {
                        $today = date('Y-m-d');
                        $data['projects'] = $this->project->get(array('T005_Project.C070_DateFinished >' => $today))->result_array();
                } elseif (strcasecmp($panel_code, 'TrcOprMasuk') == 0) {
                        $data['ledger'] = $this->m_ledger->get(array(
                            'Lft > ' => 1,
                            'Rgt < ' => 6,
                            'IsDetail' => 1,
                        ));
                        $data['banks'] = $this->kbaccount->get(
                                        array(
                                    'T022_KBAccount.C001_OfficeID' => $user['C002_OfficeID']
                                        ), 'C020_Name ASC, C000_SysID ASC'
                                )->result_array();
                } elseif (strcasecmp($panel_code, 'TrcOprKeluar') == 0) {
                        $data['ledger'] = $this->m_ledger->get(array(
                            'Lft > ' => 1,
                            'Rgt < ' => 6,
                            'IsDetail' => 1,
                        ));
                        $data['banks'] = $this->kbaccount->get(
                                        array(
                                    'T022_KBAccount.C001_OfficeID' => $user['C002_OfficeID']
                                        ), 'C020_Name ASC, C000_SysID ASC'
                                )->result_array();
                } elseif (strcasecmp($panel_code, 'TrfIntKasBank') == 0) {
                        $data['ledger'] = $this->m_ledger->get(array(
                            'Lft > ' => 1,
                            'Rgt < ' => 6,
                            'IsDetail' => 1,
                        ));
                        $data['banks'] = $this->kbaccount->get(
                                        array(
                                    'T022_KBAccount.C001_OfficeID' => $user['C002_OfficeID']
                                        ), 'C020_Name ASC, C000_SysID ASC'
                                )->result_array();
                        $data['virtual'] = $this->virtual_account->get();
                } else if (strcasecmp($panel_code, 'TrfIntMasuk') == 0) {
                        $data['ledger'] = $this->m_ledger->get(array(
                            'Lft > ' => 1,
                            'Rgt < ' => 6,
                            'IsDetail' => 1,
                        ));
                        $data['banks'] = $this->kbaccount->get(
                                        array(
                                    'T022_KBAccount.C001_OfficeID' => $user['C002_OfficeID']
                                        ), 'C020_Name ASC, C000_SysID ASC'
                                )->result_array();
                        $data['banks_all'] = $this->kbaccount->get(
                                        array(), 'C020_Name ASC, C000_SysID ASC'
                                )->result_array();
                        $data['transfer_masuk'] = $this->t510_rekap->get_transfer_masuk_internal($user['C002_OfficeID']);
                } else if (strcasecmp($panel_code, 'TrfIntKeluar') == 0) {
                        $data['ledger'] = $this->m_ledger->get(array(
                            'Lft > ' => 1,
                            'Rgt < ' => 6,
                            'IsDetail' => 1,
                        ));
                        $data['banks'] = $this->kbaccount->get(
                                        array(
                                    'T022_KBAccount.C001_OfficeID' => $user['C002_OfficeID']
                                        ), 'C020_Name ASC, C000_SysID ASC'
                                )->result_array();
                        $data['banks_all'] = $this->kbaccount->get(
                                        array(), 'C020_Name ASC, C000_SysID ASC'
                                )->result_array();
                        $data['virtual'] = $this->virtual_account->get();
                } else if (strcasecmp($panel_code, 'TrfExtMasuk') == 0) {
                        $data['ledger'] = $this->m_ledger->get(array(
                            'Lft > ' => 1,
                            'Rgt < ' => 6,
                            'IsDetail' => 1,
                        ));
                        $data['banks'] = $this->kbaccount->get(
                                        array(
                                    'T022_KBAccount.C001_OfficeID' => $user['C002_OfficeID']
                                        ), 'C020_Name ASC, C000_SysID ASC'
                                )->result_array();
                        $data['virtual'] = $this->virtual_account->get();
                } else if (strcasecmp($panel_code, 'TrfExtKeluar') == 0) {
                        $data['ledger'] = $this->m_ledger->get(array(
                            'Lft > ' => 1,
                            'Rgt < ' => 6,
                            'IsDetail' => 1,
                        ));
                        $data['banks'] = $this->kbaccount->get(
                                        array(
                                    'T022_KBAccount.C001_OfficeID' => $user['C002_OfficeID']
                                        ), 'C020_Name ASC, C000_SysID ASC'
                                )->result_array();
                        $data['virtual'] = $this->virtual_account->get();
                }

                $data['sub_trc_type'] = $sub_trc_types[0];

                $this->load->view($trc_panel['C014_View'], $data);
        }

        function get_doc_day($panel_code) {
                $this->layout = FALSE;

                $this->load->model('transaction_panel');
                $this->load->model('t510_rekap');
                $this->load->model('sub_trc_type');

                $get = $this->input->get();

                $trc_panel = $this->transaction_panel->get_panel(array(
                            'T024_2TrcPanel.C010_Code LIKE' => $panel_code,
                        ))->row_array();
                $page = $this->t510_rekap->get_doc_day(
                        $this->office_id, $trc_panel['C000_SysID'], $get['start'], $get['length']
                );

                $page['draw'] = $get['draw'];
                $page['start'] = $get['start'];
                $page['recordsTotal'] = $page['total'];
                $page['recordsFiltered'] = $page['total'];

                $rows = array();
                foreach ($page['data'] as $key => $value) {
                        if ($key >= $get['length']) {
                                break;
                        }
                        $rows[] = $value;
                }
                $page['data'] = $rows;

                $this->generate_extra_doc_day($trc_panel['C010_Code'], $page);

                echo json_encode($page);
        }

        private function generate_extra_doc_day($panel_code, &$page) {
                $data = $page['data'];
                if (!$data) {
                        return;
                }
                if ($panel_code == 'TrfExtMasuk' || $panel_code == 'TrfExtKeluar') {
                        $this->load->model('external_transfer');
                        $this->external_transfer->generate_doc_day($data);
                } elseif ($panel_code == 'TrfIntKeluar') {
                        $this->load->model('internal_transfer_status');
                        $this->load->model('office');
                        $this->load->model('kbaccount');
                        $this->internal_transfer_status->generate_doc_day($data);

                        $office_ids = array();
                        $bank_acc_ids = array();

                        foreach ($data as $key => $rekap) {
                                $office_id = $rekap['SLedgerIDTo'];
                                $office = $this->office->get(array('C000_SysID' => $office_id))->row_array();
                                $data[$key]['office_name'] = $office['C020_Descr'];

                                if (!in_array($office_id, $office_ids)) {
                                        $office_ids[] = $office_id;
                                }
                                $bank_acc_id = $rekap['SubLedger1IDTo'];
                                if ($bank_acc_id && !in_array($bank_acc_id, $bank_acc_ids)) {
                                        $bank_acc_ids[] = $bank_acc_id;
                                }
                        }
                        $page['offices'] = array();
                        $page['bank_accounts'] = array();

                        if ($office_ids) {
                                $page['offices'] = $this->office->get(
                                                'C000_SysID IN (' . implode(',', $office_ids) . ')'
                                        )->result_array();
                        }

                        if ($bank_acc_ids) {
                                $page['bank_accounts'] = $this->kbaccount->get(
                                                'C000_SysID IN (' . implode(',', $bank_acc_ids) . ')'
                                        )->result_array();
                        }
                }

                $page['data'] = $data;
        }

        function save() {
                $this->layout = false;

                $data = $_POST['data'];

                $sub_trc_type_id = reset($data)['data']['SubTrcTypeID'];

                $sub_trc_type = $this->sub_trc_type->get_one(array(
                    'C000_SysID' => $sub_trc_type_id,
                ));

                $user_group = $this->user_group->get_one(array(
                    'TrcPanelID' => $sub_trc_type['C010_TrcPanelID'],
                    'UserGroupTypeID' => $this->user_group_type_id,
                ));

                if (!$user_group) {
                        $this->response(array(
                            'status' => -1,
                            'message' => 'You are not authorized to access this function!'
                        ));
                }

                $m_sub_trc_types = $this->sub_trc_type->get(array(
                    'C010_TrcPanelID' => $sub_trc_type['C010_TrcPanelID'],
                ));
                $sub_trc_type_ids = array();
                foreach ($m_sub_trc_types as $key => $val) {
                        $sub_trc_type_ids[] = $val['C000_SysID'];
                }

                $header = $this->get_header();

                $trc_panel = $this->transaction_panel->get_one(array(
                    'C000_SysID' => $sub_trc_type['C010_TrcPanelID'],
                ));

                $trc_panel_code = $trc_panel['C010_Code'];

                $insert_line_id = 1;
                foreach ($data as $key => $val) {
                        $record = array();

                        $crud_status = $val['status'];
                        if ($crud_status == CRUD_INSERT) {
                                $record['TrcID'] = $this->t510_rekap->get_sys_id();
                                $record['LineID'] = $insert_line_id++;
                        } else {
                                $a = explode('_', $key);
                                $record['TrcID'] = $a[0];
                                $record['LineID'] = $a[1];
                        }

                        if ($crud_status == CRUD_INSERT || $crud_status == CRUD_UPDATE) {
                                $record = array_merge($val['data'], $record);
                                
                                $sub_trc_type_id = $record['SubTrcTypeID'];
                                if (!in_array($sub_trc_type_id, $sub_trc_type_ids)) {
                                        continue;
                                }

                                $record['SubTrcTypeCode'] = $this->sub_trc_type->get_code($sub_trc_type_id);
                                $record['TrcTypeID'] = $trc_panel['C010_TrcTypeID'];
                                $record['TrcPanelID'] = $sub_trc_type['C010_TrcPanelID'];
                                $record['Month'] = $header['Month'];
                                $record['DocDate'] = $header['DocDate'];
                                $record['SLedgerID'] = $this->office_id;

                                if ($trc_panel['C010_Code'] == 'TrcPrg') {
                                        $program_id = $record['SubLedger3IDTo'];
                                        $program = $this->project->get_one(array(
                                            'C000_SysID' => $program_id,
                                        ));
                                        $record['SubLedger2ID'] = $program['C021_VirtualAccountID'];
                                } elseif ($trc_panel['C010_Code'] == 'TrfIntKasBank' || $trc_panel['C010_Code'] == 'TrfIntKeluar') {
                                        $record['SubLedger2IDTo'] = $record['SubLedger2ID'];
                                }
                        }

                        $this->t510_rekap->save_from_field($crud_status, $header, $record);
                        $this->save_extra_record($trc_panel_code, $record);
                }

                // $this->response(array(
                //     'status' => 1,
                //     'message' => 'Success'
                // ));
                echo "1";
        }

        private function save_extra_record($trc_panel_code, $record) {
                if ($trc_panel_code == 'TrfIntKeluar') {
                        $this->load->model('internal_transfer_status');
                        $old = $this->internal_transfer_status->get_one(array(
                            'TrcID' => $record['TrcID'],
                            'LineID' => $record['LineID'],
                        ));
                        if ($old) {
                                return;
                        }
                        $this->internal_transfer_status->insert(array(
                            'TrcID' => $record['TrcID'],
                            'LineID' => $record['LineID'],
                            'Status' => 0,
                            'CreatedAt' => $this->internal_transfer_status->get_curr_datetime(),
                            'UpdatedAt' => $this->internal_transfer_status->get_curr_datetime(),
                        ));
                } elseif ($trc_panel_code == 'TrfExtMasuk' || $trc_panel_code == 'TrfExtKeluar') {
                        $this->load->model('external_transfer');
                        $old = $this->external_transfer->get_one(array(
                            'C000_TrcID' => $record['TrcID'],
                            'C001_LineID' => $record['LineID'],
                        ));
                        if ($old) {
                                return;
                        }
                        $change_date = date("Y-m-d", strtotime($record['DateTransfer']));
                        $this->external_transfer->insert(array(
                            'C000_TrcID' => $record['TrcID'],
                            'C001_LineID' => $record['LineID'],
                            'C002_DateTransfer' => $change_date,
                            'C003_Name' => $record['Name'],
//                          'C004_PartnerID' => $record['PartnerID'],
                            'C005_BankAccName' => $record['BankAccName'],
                            'C006_BankName' => $record['BankName'],
                            'C007_BankAccNumber' => $record['BankAccNumber'],
                        ));
                }
        }

        function get_balance() {
                $this->layout = FALSE;

                $where = $this->genereate_where_balance();

                $data = $this->day_pos->get_balance($where);
                // echo $this->db->last_query();
                echo json_encode($data);
        }

        function sum_balance() {
                $this->layout = FALSE;

                $where = $this->genereate_where_balance();

                $balance = $this->day_pos->sum_balance($where);

                echo $balance;
        }

        private function genereate_where_balance() {
                $where = array();
                $where['SLedgerID'] = $this->input->get('office_id') ? : $this->office_id;
                if ($where['SLedgerID'] && $where['SLedgerID'] < 0) {
                        unset($where['SLedgerID']);
                }
                $search_columns = array(
                    'MLedgerID',
                    'SubLedger1ID',
                    'SubLedger2ID',
                    'SubLedger3ID',
                    'SubLedger4ID',
                );
                foreach ($search_columns as $key => $column) {
                        if ($this->input->get($column)) {
                                $where[$column] = $this->input->get($column);
                        }
                }

                return $where;
        }

        function get_bank_id($id) {
                $this->layout = false;
                $this->load->model(array('kbaccount'));

                $bank = $this->kbaccount->get(
                                array(
                            'T022_KBAccount.C001_OfficeID' => $id
                                ), 'C020_Name ASC, C000_SysID ASC'
                        )->result_array();

                echo json_encode($bank);
        }

        protected function get_header() {
                return $this->session->userdata('header-' . $this->trc_type_code);
        }

        protected function set_header($header) {
                $this->session->set_userdata('header-' . $this->trc_type_code, $header);
        }

        protected function generate_header() {
                $trc_type_id = $this->m_trc_type['C000_SysID'];

                $where_rekap = array(
                    'C010_TrcTypeID' => $trc_type_id,
                    'C045_OfficeID' => $this->office_id,
                    'C050_DocDate' => get_curr_date(),
                );
                $t500_rekap = $this->t500_rekap->get_one($where_rekap);

                if (!$t500_rekap) {
                        $rev = 1;
                        $t500_rekap = array(
                            'C010_TrcTypeID' => $trc_type_id,
                            'C011_Month' => get_curr_yearmonth(),
                            'C000_SysID' => $this->t500_rekap->get_sys_id(),
                            'C050_Rev' => $rev,
                            'C013_DraftReadyApprCancel' => 3,
                            'C045_OfficeID' => $this->office_id,
                            'C045_UserID' => $this->user_id,
                            'C045_Dtime' => get_curr_datetime(),
                            'C050_DocDate' => get_curr_date(),
                            'C050_DocNum' => $this->generate_doc_num($rev),
                        );

                        $this->t500_rekap->insert($t500_rekap);
                } else {
                        $rev = $t500_rekap['C050_Rev'] + 1;

                        $t500_rekap['C050_Rev'] = $rev;
                        $t500_rekap['C045_UserID'] = $this->user_id;
                        $t500_rekap['C045_Dtime'] = get_curr_datetime();
                        $t500_rekap['C050_DocNum'] = $this->generate_doc_num($rev);

                        $this->t500_rekap->delete($where_rekap);
                        $this->t500_rekap->insert($t500_rekap);
                }

                $where_node = array(
                    'C010_TrcTypeID' => $trc_type_id,
                    'C045_OfficeID' => $this->office_id,
                );
                $t500_node = $this->node->get_one($where_node);

                if (!$t500_node) {
                        $t500_node = $t500_rekap;
                        $t500_node['C050_Rev'] = 1;
                        $this->node->insert($t500_node);
                } else {
                        $rev = $t500_node['C050_Rev'] + 1;

                        $t500_node['C050_Rev'] = $rev;
                        $t500_node['C045_UserID'] = $this->user_id;
                        $t500_node['C045_Dtime'] = get_curr_datetime();
                        $t500_node['C050_DocNum'] = $t500_rekap['C050_DocNum'];

                        $this->node->delete($where_node);
                        $this->node->insert($t500_node);
                }

                $this->t500_rekap->remove_suffix($t500_rekap);

                return $t500_rekap;
        }

        private function generate_doc_num($rev = 1) {
                return $this->office_level . '/'
                        . date('d-y/m') . '/'
                        . str_pad($rev, 2, '0', STR_PAD_LEFT);
        }

}
