<?php
if (!defined('BASEPATH'))
        exit('No direct script access allowed');

require_once('Common_transaction.php');

class Transactions extends Common_Transaction {

        function __construct() {
                parent::__construct('Trc');
        }

        public function index() {
            $data = array();
            //get panel for begin balance ex: 1010
            for ($i=0; $i < count($this->menu); $i++) { 
                if($this->menu[$i]['C013_Label'] == 'Transaction'){
                    $data['panels'] = $this->get_trc_panel($this->menu[$i]['C000_SysID']);
                }
            }
            
            $this->load_index($data);
        }

        function search_office() {
                $this->layout = FALSE;
                $result = array('total_count' => 410979, 'incomplete_results' => false, 'items' => "");

                $q = $this->input->get('q');

                $result['items'] = $this->office->search($q);

                header('Content-type: application/json');
                echo json_encode($result);
        }

        function get_office() {
                $this->layout = FALSE;

                $office_id = $this->user_data['C002_OfficeID'];
                $office_level = $this->user_data['office_detail']['C012_Level'];

                $q = $this->input->get('query') ? : '';
                $office_level_to = $this->input->get('office_level') ? : 4;

                $transfer_map = unserialize(TRANSFER_MAP);
                if (!isset($transfer_map[$office_level])) {
                        echo json_encode(array());
                        die();
                }
                $office_levels = $transfer_map[$office_level];
                if (array_search($office_level_to, $office_levels) === FALSE) {
                        echo json_encode(array());
                        die();
                }

                $offices = array();
                if ($office_level_to < $office_level) {
                        $offices[] = $this->office->get_parent($office_id, $office_level_to);
                } elseif ($office_level_to > $office_level) {
                        if ($office_level == OFFICE_LEVEL_KPRK) { //Search KPC
                                $offices = $this->office->get_childs_by_parent($office_id, $q);
                        } elseif ($office_level == OFFICE_LEVEL_PUSAT) { //Search KPRK
                                $offices = $this->office->get_kprk_by_pusat($office_id, $q);
                        }
                }

                foreach ($offices AS $key => $office) {
                        $offices[$key] = array(
                            'id' => $office['C000_SysID'],
                            'name' => $office['C020_Descr'],
                        );
                }

                echo json_encode($offices);
        }

        function get_bank_name() {
                $this->layout = FALSE;

                $office_id = $this->input->get('office_id') ? : 0;

                $result = $this->kbaccount->get_bank_name($office_id);

                echo json_encode($result);
        }

        function get_bank_account() {
                $this->layout = FALSE;

                $office_id = $this->input->get('office_id') ? : 0;
                $bank_name = $this->input->get('bank_name') ? : '';

                $result = $this->kbaccount->get_bank_account($office_id, $bank_name);

                echo json_encode($result);
        }
}
