<?php require_once('Common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Partners extends Common {
	function __construct() {
		parent::__construct("ProgPtr");
		
		$this->meta 			= array();
		$this->scripts 			= array('site/references','site/user','site/partner','../global/plugins/datatables/jquery.dataTables.min');
		$this->styles 			= array();
		$this->load->model(array('user_session','partner'));
	}

	public function get_ajax_data(){
		$this->layout = false;
		$this->load->model(array('datatable'));
		$table = 'T010_Partner';
		$column_order = array(null,'PartnerCode','PartnerName', null); //set column field database for datatable orderable
	    $column_search = array('PartnerCode','PartnerName'); //set column field database for datatable searchable just firstname , lastname , address are searchable
	    $order = array('SysID' => 'desc'); // default order
		
		$list = $this->datatable->get_datatables($table, $column_search, $column_order, $order);

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $key => $trow) {
            $no++;
            $number = $key + 1;
            $row = array();
            $row[] = $no;
            $row[] = $trow->PartnerCode;
            $row[] = $trow->PartnerName;
 
            //add html for action
            $row[] = '<a class="edit-partner btn btn-sm btn-primary" title="Edit" data-id="'.$trow->SysID.'"><i class="glyphicon glyphicon-pencil"></i></a>
                  <a class="delete-partner btn btn-sm btn-danger" title="Hapus" data-id="'.$trow->SysID.'")"><i class="glyphicon glyphicon-trash"></i> </a>';
 
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->datatable->count_all($table),
                        "recordsFiltered" => $this->datatable->count_filtered($table, $column_search, $column_order, $order),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
	}

	public function save_partner(){
		$this->layout = false;
		$this->load->model(array('partner'));

		$id_partner = $_POST['partner_id'];
        $data['PartnerCode'] = $_POST['kode'];
        $data['PartnerName'] = $_POST['nama'];

        $response = 0;
        if($id_partner == 0){
        	if($this->partner->add($data)){
	            $response = 1;
	        }else{
	            $response = 0;
	        }
        }else{
        	if($this->partner->edit($id_partner, $data)){
	            $response = 1;
	        }else{
	            $response = 0;
	        }
        }

        echo $response;
    }

	public function get_partner_by_id(){
		$this->layout = false;
		$idx = $_POST["idx"];
	
		// $rslt = mssql_query("SELECT * FROM [dbo].[T020_Office] WHERE C000_SysID = ".$idx);
		$row = $this->partner->get_data($criteria='SysID = '.$idx.'',$order='',$order_by='',$limit='',$start=0,$offset = 0)->row_array();
		
		echo json_encode($row);		
	}

	public function delete_partner(){
		$id = $_POST["id"];
		if($this->partner->delete($id)){
			echo "1";
		} else {
			echo "0";
		}
		die();
	}

	function get_data_excel($sheet, $highestRow, $highestColumn, $media){
		$this->load->model(array('office'));

		for ($row = 2; $row <= $highestRow; $row++){ 
			// Read a row of data into an array                 
	        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
	                                         
	        // Sesuaikan sama nama kolom tabel di database
	        $exist = $this->partner->get_one(array('PartnerCode' => $rowData[0][0]));

	        $data = array(
	            "PartnerCode" => $rowData[0][0],
	            "PartnerName" => $rowData[0][1]
	        );
	         
	        //sesuaikan nama dengan nama tabel
	        if(!$exist){
	        	$this->partner->add($data);
	        }
	        delete_files($media['file_path']);
	             
	    }

		redirect(base_url()."references/program_partner");
	}
}