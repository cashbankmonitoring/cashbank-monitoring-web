<?php

class Day_Pos_Updater extends CI_Controller {

        function __construct() {
                parent::__construct();
                $this->layout = FALSE;

                $this->load->model('day_pos');
                $this->load->model('day_delta');
        }

        function update() {
                $yesterday = date('Y-m-d', strtotime('-1 days'));
                
                $day_delta_list = $this->day_delta->get(array(
                    'datePeriod' => $yesterday,
                ));
                
                foreach ($day_delta_list as $key => $day_delta) {
                        $where_day_pos = $day_delta;
                        unset($where_day_pos['datePeriod']);
                        unset($where_day_pos['Amount']);
                        
                        $where_day_pos['datePeriod < '] = $yesterday;
                        
                        $day_pos = $this->day_pos->get_one($where_day_pos);
                        
                        if($day_pos){
                                $where_day_pos = $day_pos;
                                unset($where_day_pos['datePeriod']);
                                unset($where_day_pos['Amount']);
                                
                                $this->day_pos->update($where_day_pos, array(
                                    'datePeriod' => $yesterday,
                                    'Amount' => $day_pos['Amount'] + $day_delta['Amount'],
                                ));
                        }else{
                                $day_pos = $where_day_pos;
                                unset($day_pos['datePeriod < ']);
                                $day_pos['datePeriod'] = $yesterday;
                                $day_pos['Amount'] = $day_delta['Amount'];
                                
                                $this->day_pos->insert($day_pos);
                        }
                }
                
                $this->day_pos->update(NULL, array(
                    'datePeriod' => $yesterday,
                ));
                
                echo 'Success';
        }

}
