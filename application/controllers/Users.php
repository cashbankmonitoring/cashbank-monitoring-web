<?php require_once('Common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends Common {
	function __construct() {
		parent::__construct("UsrAcc");
		
		$this->meta 			= array();
		$this->scripts 			= array('site/references','site/user','site/bank_account','../global/plugins/datatables/jquery.dataTables.min');
		$this->styles 			= array();
		$this->load->model(array('user_session','user'));
		$this->load->library(array('Excel'));
	}

	public function get_ajax_data(){
		$this->layout = false;
		$this->load->model(array('datatable'));
		$table = 'T023_User';
		$column_order = array(null,'T020_Office.C010_Code','T023_User.C030_UserName','T023_User.C040_UserFullname','T023_User.C060_UserEmail', null); //set column field database for datatable orderable
	    $column_search = array('T020_Office.C010_Code','T020_Office.C020_Descr','T023_User.C030_UserName','T023_User.C040_UserFullname','T023_User.C060_UserEmail'); //set column field database for datatable searchable just firstname , lastname , address are searchable
	    $order = array('T023_User.C000_SysID' => 'desc'); // default order
		
		$list = $this->datatable->get_datatables($table, $column_search, $column_order, $order);

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $key => $trow) {
            $no++;
            $number = $key + 1;
            $row = array();
            $row[] = $no;
            $level = $this->user_level($trow->level);
            $row[] = $level;
            $row[] = '['.$trow->code.'] '.$trow->desc;
            $row[] = $trow->username;
            $row[] = $trow->name;
            $row[] = $trow->email;
            $status = ($trow->status == 1 ? 'Aktif' : 'Tidak Aktif');
            $row[] = $status;
 
            //add html for action
            $row[] = '<a class="edit-user btn btn-sm btn-primary" title="Edit" data-id="'.$trow->id.'"><i class="glyphicon glyphicon-pencil"></i></a>
            	<a data-target="#reset-pass" class="btn btn-sm btn-warning reset-password" data-id="'.$trow->id.'"><i class="fa fa-key"></i></a>
                  <a class="delete-user btn btn-sm btn-danger" title="Hapus" data-id="'.$trow->id.'" data-office-id="'.$trow->office_id.'"><i class="glyphicon glyphicon-trash"></i> </a>';
 
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->datatable->count_all($table),
                        "recordsFiltered" => $this->datatable->count_filtered($table, $column_search, $column_order, $order),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
	}

	public function search_pass(){
		$pass = $_POST["pass"];
		$id = $_POST["user_id"];
		$user = $this->user_session->get();

		$check = $this->user->check_password(array("C000_SysID" => $user['C000_SysID'],"C050_UserPasswd" => md5($pass)));
		$test = 0;
		if($check == 1){
			$test = $this->user->reset_password($id);
		}

		echo $test;
		die();

	}
	public function check_user_exist(){
		$kode = $_POST["kode"];
		$numRows = $this->user->cek_kode(array('C030_UserName'=>$kode));
		echo $numRows;
		die();
	}

	public function save_user(){
		$this->layout = false;
		$this->load->model(array('user'));
		if(!isset($_POST['is_begin_balance'])){
			$is_begin_balance = 1;
		}else{
			$is_begin_balance = 0;
		}

		if(!isset($_POST['is_read_only'])){
			$is_read_only = 0;
		}else{
			$is_read_only = 1;
		}

		$id_user = $_POST['user_id'];
        $data['C002_OfficeID'] = $_POST['kantor_id'];
        $data['C010_Code'] = $_POST['username'];
        $data['C030_UserName'] = $_POST['username'];
        $data['C040_UserFullname'] = $_POST['nama'];
        $data['C060_UserEmail'] = $_POST['email'];
        $data['C070_UserState'] = $_POST['status'];
        $data['C080_machineID'] = $_POST['komputer_id'];
        $data['C110_UserGroupType'] = $_POST['user_type'];
        $data['C120_IsBeginBalance'] = $is_begin_balance;
        $data['C130_IsReadOnly'] = $is_read_only;
        $data['C020_Descr'] = $_POST['deskripsi'];

        $response = 0;
        if($id_user == 0){
        	$data['C050_UserPasswd'] = $_POST['password'];
        	
        	if($this->user->add($data)){
	            $response = 1;
	        }else{
	            $response = 0;
	        }
        }else{
        	if($this->user->update($id_user, $data)){
	            $response = 1;
	        }else{
	            $response = 0;
	        }
        }

        echo $response;
    }

    public function get_user_by_id(){
		$this->layout = false;
		$idx = $_POST["idx"];
		
		$result = $this->user->get_data($criteria='T023_User.C000_SysID = '.$idx.'',$order='',$order_by='',$limit='',$start=0,$offset = 0)->row_array();

		echo json_encode($result);
	}

	public function delete_user(){
		$id = $_POST["id"];
		if($this->user->delete($id)){
			echo "1";
		} else {
			echo "0";
		}
		die();
	}

	public function enable_all_begin_balance($id){
		ini_set('max_execution_time', 300);
		$this->load->model(array('user'));
		if($id == 0){
			$get_begin = 1;
		}else{
			$get_begin = 0;
		}
		$get_user = $this->user->get_user(array('C120_IsBeginBalance' => $get_begin))->result_array();
		// print_r($get_user);
		// print_r("<br>".count($get_user));die; 
		for ($i=0; $i < count($get_user); $i++) {
			$input['C120_IsBeginBalance'] = $id;

			$update = $this->user->update($get_user[$i]['C000_SysID'], $input);

			if($update){
				// print_r("berhasil =".$i);
				// print_r("<br>");
				// redirect(base_url()."dashboard");
			}else{
				print_r("gagal");die;
			}
		}

		// die;

		redirect(base_url()."references/user_accounts");
	}

	function get_data_excel($sheet, $highestRow, $highestColumn, $media){
		$this->load->model(array('office', "user_group_type"));

		for ($row = 2; $row <= $highestRow; $row++){ 
			// Read a row of data into an array                 
	        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
	                                         
	        // Sesuaikan sama nama kolom tabel di database
	        $office = $this->office->get_one(array('C010_Code' => $rowData[0][0]));

	        $user_type = $this->user_group_type->get_one(array('UserGroupTypeName' => $rowData[0][7]));

	        $user_exist = $this->user->get_one(array('C030_UserName' => $rowData[0][1]));

	        $data = array(
	            "C002_OfficeID" => $office['C000_SysID'],
	            "C010_Code" => $rowData[0][1],
	            "C030_UserName" => $rowData[0][1],
	            "C040_UserFullname" => $rowData[0][2],
	            "C050_UserPasswd" => $rowData[0][3],
	            "C060_UserEmail" => $rowData[0][4],
	            "C080_machineID" => $rowData[0][5],
	            "C020_Descr" => $rowData[0][6],
	            "C110_UserGroupType" => $user_type['SysID'],
	            "C070_UserState" => $rowData[0][8],
	            "C120_IsBeginBalance" => $rowData[0][9],
	            "C130_IsReadOnly" => $rowData[0][10]
	        );
	         
	        //sesuaikan nama dengan nama tabel
	        if(!$user_exist){
	        	$this->user->add($data);
	        }
	        delete_files($media['file_path']);
	             
	    }

		redirect(base_url()."references/user_accounts");
	}
}