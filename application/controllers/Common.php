<?php

if (!defined('BASEPATH'))
        exit('No direct script access allowed');

class Common extends CI_Controller {

        var $office_id;
        var $office_level;
        var $user_id;
        var $user_group_type_id;
        var $m_trc_type;
        var $trc_type_id;
        var $trc_type_code;

        function __construct($module = null) {
                parent::__construct();
                date_default_timezone_set("Asia/Jakarta");
                //language
                // $this->site_lang = $this->session->userdata('language');
                // if($this->site_lang == NULL){
                // 	$this->session->set_userdata('language', LANGUAGE_MELAYU);
                // }
                // $this->lang->load('cms',$this->site_lang);	
                //load model
                $this->load->model(array(
                    'user_session',
                    'office',
                    'User_group',
                    'trc_menu_type',
                    'trc_type',
                ));

                //get user session
                $user = $this->user_session->get();
                
                $this->user_data = $user;
                
                $this->office_id = $user['C002_OfficeID'];
                $this->user_id = $user['C000_SysID'];
                $this->user_group_type_id = $user['C110_UserGroupType'];
//		$this->user_data['C110_UserGroupType'] = 8;
//                print_r($this->user_data);die();
                if (!$user) {
                        $this->layout = "user_login";
                        $this->is_logged_in = false;
                        if ($module != 'site') {
                                redirect(site_url("site/no_access/"));
                        }
                        return;
                }

                $this->is_logged_in = true;

                $this->user_data['office_detail'] = $this->office->get(array('C000_SysID' => $this->user_data['C002_OfficeID']))->row_array();
                $this->office_level = $this->user_data['user_level'] = $this->user_level($this->user_data['office_detail']['C012_Level']);

                $this->m_trc_type = $module ? $this->trc_type->get_one(array(
                                    'C010_Code LIKE' => $module,
                                )) : null;
                
                if($this->m_trc_type){
                        $this->trc_type_id = $this->m_trc_type['C000_SysID'];
                        $this->trc_type_code = $this->m_trc_type['C010_Code'];
                }
                
                $data_head['user_session'] = $this->user_data;

//			 print_r($this->user_data);die;
                $panel = $this->User_group->get_panel('UserGroupTypeID = ' . $this->user_data['C110_UserGroupType'])->result_array();
//                 print_r($panel);die;
                $menu = array();
                $sub_menu = array();
                for ($i = 0; $i < count($panel); $i++) {
                        $trc = $this->trc_menu_type->get(array('C000_SysID' => $panel[$i]['C010_TrcTypeID']))->row_array();
                        if ($trc['C016_ParentID'] == 0) {
                                $menu[] = $trc;
                        } else {
                                $sub_menu[] = $trc;
                                $parent = $this->trc_menu_type->get(array('C000_SysID' => $trc['C016_ParentID']))->row_array();

                                $menu[] = $parent;
                        }
                }

//                 print_r($menu);die;
                $menus = array();
                $check_id = 0;
                for ($i = 0; $i < count($menu); $i++) {
                        $id_trc = $menu[$i]['C000_SysID'];
                        if ($id_trc != $check_id) {
                                $menus[] = $menu[$i];
                                $line_id[] = $menu[$i]['C012_LineID'];
                                $check_id = $menu[$i]['C000_SysID'];
                        }
                }
                if($menus != null){
	                array_multisort($line_id, SORT_ASC, $menus);
                }
//                 print_r($menus);die;
                //menu for sidebar
                $data_head['menus'] = $menus;
                // print_r($data_head['menus']);die;
//print_r($this->db->last_query());die;
                if ($this->user_data['C120_IsBeginBalance'] == 0) {
                        // $data_head['menus'][] = $this->User_group->get_trc_type(array('T024_1TrcType.C010_Code' => 'BegBal'), 'T024_1TrcType.C012_LineID ASC')->row_array();
                        $data_head['menus'][] = $this->trc_type->get_one(array('C010_Code' => 'BegBal'));
//                        print_r($this->db->last_query());die;
                        
                }
               // print_r($data_head['menus']);die;

                $can_access = false;
                $count_balance = 0;
                // print_r($data_head['menus']);
                $count_new_menu = count($data_head['menus']);
                $new_menu = array();
                for ($i = 0; $i < $count_new_menu; $i++) {
                        if ($data_head['menus'][$i]['C016_ParentID'] == 1) {
                                $menu_id = $data_head['menus'][$i]['C000_SysID'];
                                $sub_menu_a = array();

                                for ($a = 0; $a < count($sub_menu); $a++) {
                                        if ($sub_menu[$a]['C016_ParentID'] == $menu_id) {
                                                $sub_menu_a[] = $sub_menu[$a];
                                        }
                                }
                                $data_head['menus'][$i]['sub_menu'] = $sub_menu_a;
                        } else {
                                $data_head['menus'][$i]['sub_menu'][] = null;
                        }
                        // print_r($data_head['menus'][$i]);

                        if ($data_head['menus'][$i]['C010_Code'] == 'BegBal') {
                                if ($this->user_data['C120_IsBeginBalance'] == 1 || $count_balance > 0) {
                                        // print_r('expression');
                                        unset($data_head['menus'][$i]);
                                } else {
                                        $new_menu[] = $data_head['menus'][$i];
                                }
                                $count_balance++;
                        } else {
                                // print_r($i);
                                $new_menu[] = $data_head['menus'][$i];
                        }
                }
                $data_head['menus'] = $new_menu;
                // print_r($new_menu);die();
                // die;
                
                if ($module != "site" && $module != "Dsbrd" && count($_POST) == 0 && !$this->input->is_ajax_request()) {
                        $this->title = $this->m_trc_type ? $this->m_trc_type['C013_Label'] : '';
                        if ($this->uri->segment(2)) {
                                $url = $this->uri->segment(1) . "/" . $this->uri->segment(2);
                        } else {
                                $url = $this->uri->segment(1);
                        }

                        $menus = $data_head['menus'];
                        // print_r($menus);
                        // die;
                        // print_r(count($menus));
                        $segment_status = false;
                        $i = 0;

                        while ($segment_status == false && $i < count($menus)) {
                                if ($this->uri->segment(2)) {
                                        $j = 0;
                                        while ($segment_status == false && $j < count($menus[$i]['sub_menu'])) {
                                                if (is_array($menus[$i]['sub_menu'][$j]) && in_array($url, $menus[$i]['sub_menu'][$j])) {
                                                $segment_status = true;
                                                        	
                                                }
                                                $j++;
                                        }
                                } else {
                                		// print_r('yes');
                                		// die;
                                        // print_r($menus[$i]['C014_Link']);
                                        // print_r(':');
                                        // print_r($url);
                                        // print_r('<br>');
                                		// die;
                                        if ($url == $menus[$i]['C014_Link']) {
                                                // print_r('here');
                                                $segment_status = true;
                                        }else{
                                        	$j = 0;
	                                        while ($segment_status == false && $j < count($menus[$i]['sub_menu'])) {
	                                                if (is_array($menus[$i]['sub_menu'][$j]) && in_array($url, $menus[$i]['sub_menu'][$j])) {
                                                		$segment_status = true;    	
	                                                }
	                                                $j++;
	                                        }
                                        }
                                }
                                $i++;
                        }
                        // die;


                        if ($url == 'Users/enable_all_begin_balance') {
                                $segment_status = true;
                        }
                        $segment_status = true;

                        // print_r($segment_status);
                        // die;
                        if (!$segment_status) {
                                echo "Access denied.";
                                die;
                        }
                }
                
                $this->menu = $data_head['menus'];

                //partial head
                $this->parts['topbar'] = $this->load->view('partial/topbar', $data_head, true);
                $this->parts['sidebar'] = $this->load->view('partial/sidebar', $data_head, true);
                $this->parts['footer'] = $this->load->view('partial/footer', null, true);
        }

        protected function response($data, $http_code = 200) {
                header('HTTP/1.1: ' . $http_code);
                header('Status: ' . $http_code);
                header('Content-Type: application/json');
                header('Access-Control-Allow-Origin: *');

                exit(json_encode($data));
        }

        function user_level($id = null) {
                if ($id == null) {
                        $id = 0;
                }
                foreach (unserialize(USER_LEVEL) as $key => $level) {
                        if ($id == $key) {
                                return $level;
                        }
                }
        }
        /*
        protected function get_doc_header($office_id, $trc_type, $date) {
                $this->load->model('T500_rekap');

                $header = $this->T500_rekap->get_q500_rekap(array('C045_OfficeID' => $office_id, 'C010_TrcTypeID' => $trc_type, 'C050_DocDate' => $date))->row_array();
                if ($header != null) {
                        $header['office_detail'] = $this->office->get(array('C000_SysID' => $office_id))->row_array();
                        $header['user_level'] = $this->user_level($this->user_data['office_detail']['C012_Level']);
                }

                return $header;
        }

        protected function new_header($trc_type, $rev, $check_node, $path) {
                $this->create_header($trc_type, $rev, $check_node);
                redirect(base_url() . $path);
        }

        function create_header($trc_type, $rev, $check_node) {
                $this->load->model('T500_rekap');
                $this->load->model('Node');

                $data_post = array(
                    'C010_TrcTypeID' => $trc_type,
                    'C011_Month' => date('ym'),
                    'C000_SysID' => $this->T500_rekap->get_sys_id($trc_type),
                    'C050_Rev' => $rev,
                    'C013_DraftReadyApprCancel' => 3,
                    'C045_OfficeID' => $this->user_data['C002_OfficeID'],
                    'C045_UserID' => $this->user_data['C000_SysID'],
                    'C045_Dtime' => date("Y-m-d h:i:s"),
                    'C050_DocDate' => date("Y-m-d"),
                    'C050_DocNum' => $this->user_data['user_level'] . '/' . date('d-y/m') . '/' . str_pad(rand(0, 29), 2, "0", STR_PAD_LEFT)
                );

                $this->T500_rekap->add($data_post);

                $success_add_header = null;
                if ($check_node) {
                        $where = array(
                            'C010_TrcTypeID' => $data_post['C010_TrcTypeID'],
                            'C011_Month' => $data_post['C011_Month'],
                        );
                        $success_add_header = $this->Node->update(
                                $where, $data_post
                        );
                } else {
                        $tmp_rekap_sys_id = $data_post['C000_SysID'];
                        $data_post['C000_SysID'] = $this->Node->get_sys_id($trc_type);
                        $success_add_header = $this->Node->add($data_post);

                        $data_post['C000_SysID'] = $tmp_rekap_sys_id;
                }

                if ($success_add_header) {
                        return $data_post;
                } else {
                        return 0;
                }
        }*/

        function get_trc_panel($trc_type) {
                $this->load->model(array('Transaction_panel', 'User_group'));
                $panel = $this->Transaction_panel->get_panel(array('C010_TrcTypeID' => $trc_type))->result_array();
                // echo $this->db->last_query();
                // print_r($panel);

                $user_group = $this->User_group->get(array('UserGroupTypeID' => $this->user_group_type_id))->result_array();
                // print_r($panel);die;
                $data = array();

                foreach ($panel as $row) {
                	foreach ($user_group as $value) {
                			// print_r($row['C000_SysID']." = ".$value['SysID']."<br>");

                			if($row['C000_SysID'] == $value['TrcPanelID']){
                				// print_r("expression");
                				$data[] = $row;
                			}
                		}	
                }

                // die;
                return $data;
        }

        function get_rekap_bank_detail($office_id, $trc_type, $month, $trc_id, $rev, $sub_trc) {
                $this->load->model('T510_rekap');

                $data_510 = $this->T510_rekap->get_q510_rekap_kas_bank('C001_OfficeID = ' . $office_id . ' AND (C010_TrcTypeID = ' . $trc_type . ' OR C010_TrcTypeID IS NULL) AND (C011_Month = ' . $month . ' OR C011_Month IS NULL) AND (C012_TrcID = ' . $trc_id . ' OR C012_TrcID IS NULL) AND (C050_Rev = ' . $rev . ' OR C050_Rev IS NULL) AND C001_SubTrcTypeID = ' . $sub_trc)->result_array();

                return $data_510;
        }

        // function set_lang($lang){
        // 	$this->load->library('user_agent');
        // 	$this->session->set_userdata('language', $lang);
        // 	redirect($this->agent->referrer());
        // }

        public function upload_excel(){
            $this->layout = false;
            $this->load->model(array('user'));
            $this->load->library(array('Excel'));

            $fileName = time().$_FILES['file']['name'];
             
            $config['upload_path'] = './assets/upload/'; //buat folder dengan nama assets di root folder
            $config['file_name'] = $fileName;
            $config['allowed_types'] = 'xls|xlsx|csv';
            $config['max_size'] = 10000;
            
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            
            if(!$this->upload->do_upload('file')){
                $this->upload->display_errors();
            }else{
                $media = $this->upload->data();
                $inputFileName = './assets/upload/'.$media['file_name'];
                 
                try {
                    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFileName);
                } catch(Exception $e) {
                    die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
                }

                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                
                $this->get_data_excel($sheet, $highestRow, $highestColumn, $media);
            }
        }
}
