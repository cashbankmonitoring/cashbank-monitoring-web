<?php

require_once('Common_transaction.php');
if (!defined('BASEPATH'))
        exit('No direct script access allowed');

class External_Transfers extends Common_transaction{
        function __construct() {
                parent::__construct('TrfExt');
        }
        
        public function index() {
            $data = array();
            //get panel for begin balance ex: 1010
            for ($i=0; $i < count($this->menu); $i++) { 
                if($this->menu[$i]['C013_Label'] == 'Transfers'){
                    foreach ($this->menu[$i]['sub_menu'] as $row) {
                        if($row['C013_Label'] == 'Transfer External'){
                            // print_r("expression");
                            $data['panels'] = $this->get_trc_panel($row['C000_SysID']);
                        }
                    }
                    // print_r($this->menu[$i]);
                }
            }
            // print_r($data);
            // die;
            $this->load_index($data);
        }
}
