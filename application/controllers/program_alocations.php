<?php require_once('Common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Program_alocations extends Common {
	function __construct() {
		parent::__construct("ProgAllc");
		
		$this->meta 			= array();
		$this->scripts 			= array('site/references','site/alokasi','../global/plugins/datatables/jquery.dataTables.min');
		$this->styles 			= array();
		$this->load->model(array('user_session','Program_alocation',"Project"));
	}

	public function get_ajax_data(){
		$this->layout = false;
		$this->load->model(array('datatable'));
		$table = 'T027_ProgramAlocation';
		$column_order = array(null,'T020_Office.C020_Descr','T005_Project.C020_Descr', 'T027_ProgramAlocation.C030_Alokasi', 'T027_ProgramAlocation.C060_Descr', null); //set column field database for datatable orderable
	    $column_search = array('T020_Office.C020_Descr','T005_Project.C020_Descr', 'T027_ProgramAlocation.C030_Alokasi', 'T027_ProgramAlocation.C060_Descr'); //set column field database for datatable searchable just firstname , lastname , address are searchable
	    $order = array('T027_ProgramAlocation.C000_SysID' => 'desc'); // default order
		
		$list = $this->datatable->get_datatables($table, $column_search, $column_order, $order);

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $key => $trow) {
            $no++;
            $number = $key + 1;
            $row = array();
            $row[] = $no;
            $row[] = $trow->office;
            $row[] = $trow->name;
            $row[] = number_format($trow->alokasi,0,".",",");
            $row[] = $trow->desc;
 
            //add html for action
            $row[] = '<a class="edit-alokasi btn btn-sm btn-primary" title="Edit" data-id="'.$trow->id.'"><i class="glyphicon glyphicon-pencil"></i></a>
                  <a class="delete-alokasi btn btn-sm btn-danger" title="Hapus" data-id="'.$trow->id.'")"><i class="glyphicon glyphicon-trash"></i> </a>';
 
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->datatable->count_all($table),
                        "recordsFiltered" => $this->datatable->count_filtered($table, $column_search, $column_order, $order),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
	}

	public function save_allocations(){
		$this->layout = false;
		$this->load->model(array('program_alocation'));

		$id_alokasi = $_POST['alokasi_id'];
        $data['C010_OfficeID'] = $_POST['kantor_id'];
        $data['C020_ProjectID'] = $_POST['program'];
        $data['C030_Alokasi'] = str_replace(',', '', $_POST['alokasi']);
        $data['C060_Descr'] = $_POST['deskripsi'];

        $response = 0;
        if($id_alokasi == 0){
        	if($this->program_alocation->add($data)){
	            $response = 1;
	        }else{
	            $response = 0;
	        }
        }else{
        	if($this->program_alocation->edit($id_alokasi, $data)){
	            $response = 1;
	        }else{
	            $response = 0;
	        }
        }

        echo $response;
    }

	public function get_data(){
		$page = $_POST["h"];
		// $off_id = $_POST['id_off'];
	$this->load->model('office');
	if ($page == ""){
		$ofs = 0;
		$lmt = 100;
	} else if($page > 0){
		$ofs = ($page * 100) - 99;
		$lmt =100;
		
	}else{
		$ofs = 0;
		$lmt = 100;
		$page = 1;
	}
	
	$tot = $this->Program_alocation->get()->num_rows();

	$jml_page = ceil($tot / 100);

	
	$dt_out = "";
	$user = $this->user_session->get();

	// if ($user['C002_OfficeID'] == 1){
	if($user['C110_UserGroupType'] == 4 || $user['C110_UserGroupType'] == 5 || $user['C110_UserGroupType'] == 6 || $user['C110_UserGroupType'] == 1){
		$result = $this->Program_alocation->get_data($criteria='',$order='',$order_by='desc',$limit=$lmt,$start=$ofs,$offset = 0)->result_array();

	} else {
		$off_id = $user['C002_OfficeID'];
		$result = $this->Program_alocation->get_data($criteria="T027_ProgramAlocation.C010_OfficeID = '$user[C002_OfficeID]' OR T020_Office.C001_ParentID = '$user[C002_OfficeID]'",$order='',$order_by='desc',$limit=$lmt,$start=$ofs,$offset = 0)->result_array();
	}
		
		
		
	$dt_out = "";
	
	$user_level = $this->office->get(array('C000_SysID' => $user['C002_OfficeID']))->row_array();

	$count=1;				
	foreach($result as $row)
	{
		if ($row["C001_ParentID"] != $user['C002_OfficeID'] AND $user['C110_UserGroupType'] != 4 AND $user['C110_UserGroupType'] != 5 AND $user['C110_UserGroupType'] != 6 AND $user['C110_UserGroupType'] != 1){
			$btn1 = "";
		} else {
			$btn1 = '
			<a href="javascript:;" data-id="'.$row["C000_SysID"].'" class="edit-alokasi btn btn-sm btn-default"><i class="icon-note"></i></a>  
			<a href="javascript:;" class="delete-alokasi btn btn-sm btn-danger"><i class="icons-office-52"></i></a>';
		}
		
		$dt_out .= '
		<tr idx="'.$row["C000_SysID"].'" idp="'.$row["C001_ParentID"].'">
			<td align="center">'.$count.'</td>
			<td>'.$row["off_name"].'</td>
			<td>'.$row["prog_name"].'</td>
			<td align="right">'.number_format($row["C030_Alokasi"],0,".",",").'</td>
			<td>'.$row["C060_Descr"].'</td>
			<td align="center">
				'.$btn1.'
			</td>
		  </tr>
		';
		$count++;
	}
	
	$pg="";
											  
	if($jml_page > 1){
	if ($page == "") { $page = 1; }
	$btn ="";
	if ($page < 4){
		for ($i=1;$i<6;$i++){
			if ($page == $i){
				// $btn .= '<button class="btn btn-warning active">'.$i.'</button>';
			} else {
				$btn .= '<button onclick="javascript: getlist_alokasi($(this).html());" class="btn btn-warning">'.$i.'</button>';
			}
			
		}
	} else if ($page > ($jml_page - 2)){
		for ($i=($jml_page - 5);$i<($jml_page + 1);$i++){
			if ($page == $i){
				$btn .= '<button class="btn btn-warning active">'.$i.'</button>';
			} else {
				$btn .= '<button onclick="javascript: getlist_alokasi($(this).html());" class="btn btn-warning">'.$i.'</button>';
			}
			
		}
	} else {
		for ($i=($page - 2);$i<($page + 3);$i++){
			if ($page == $i){
				$btn .= '<button onclick="javascript: getlist_alokasi($(this).html());" class="btn btn-warning active">'.$i.'</button>';
			} else {
				$btn .= '<button onclick="javascript: getlist_alokasi($(this).html());" class="btn btn-warning">'.$i.'</button>';
			}
			
		}
	}
	
	$pg = '
	<div act="'.$page.'" class="btn-group">
		<button onclick="javascript: getlist_alokasi(1);" type="button" class="btn btn-warning"><i class="fa fa-fast-backward"></i></button>
		<button onclick="javascript: getlist_alokasi(parseInt($(this).parent(\'div\').attr(\'act\')) -1);" type="button" class="btn btn-warning"><i class="fa fa-step-backward"></i></button>
		'.$btn.'
		<button onclick="javascript: getlist_alokasi(parseInt($(this).parent(\'div\').attr(\'act\')) +1);" type="button" class="btn btn-warning"><i class="fa fa-step-forward"></i></button>
		<button onclick="javascript: getlist_alokasi('.$jml_page.');" type="button" class="btn btn-warning"><i class="fa fa-fast-forward"></i></button>
	</div>';
	}
	
	
	echo $dt_out."|".$pg;
	}

	public function get_alokasi_by_id(){
		$this->layout = false;
		$idx = $_POST["idx"];
	
		$row = $this->Program_alocation->get_data($criteria='T027_ProgramAlocation.C000_SysID = '.$idx.'',$order='',$order_by='',$limit='',$start=0,$offset = 0)->row_array();
		
		echo json_encode($row);		
	}
	
	public function get_sisa_alokasi(){
		$user = $this->user_session->get();
		$prg_id = $_POST["prg_id"];
		$this->load->model('Office');
		if ($user['C002_OfficeID'] == 2 || $user['C110_UserGroupType'] == 4 || $user['C110_UserGroupType'] == 5 || $user['C110_UserGroupType'] == 6){
			
			$total = $this->Project->get(array('C000_SysID'=>$prg_id))->row_array();
			$total = $total['C040_Value'];

			$result = $this->Program_alocation->get_alocation_value($criteria='C020_ProjectID = '.$prg_id.'')->row_array();
		
			$val = $total - $result['tot_alok'];
			
		} else {
			
			$total = $this->Program_alocation->get(array('C010_OfficeID'=>$user['C002_OfficeID'], 'C020_ProjectID' => $prg_id))->row_array();
			$val = $total["C030_Alokasi"];
		}
		echo number_format($val,0,".",",");
		die();
	}

	public function delete_alokasi(){
		$id = $_POST["id"];
		if($this->Program_alocation->delete($id)){
			echo "1";
		} else {
			echo "0";
		}
		die();
	}

	function get_data_excel($sheet, $highestRow, $highestColumn, $media){
		$this->load->model(array("office", "project"));

		for ($row = 2; $row <= $highestRow; $row++){ 
			// Read a row of data into an array                 
	        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
	                                         
	        // Sesuaikan sama nama kolom tabel di database
	        $office = $this->office->get_one(array('C010_Code' => $rowData[0][1]));

	        $project = $this->project->get_one(array('C020_Descr' => $rowData[0][0]));

	        $data = array(
	            "C010_OfficeID" => $office['C000_SysID'],
	            "C020_ProjectID" => $project['C000_SysID'],
	            "C030_Alokasi" => $rowData[0][2],
	            "C060_Descr" => $rowData[0][3]
	        );
	         
	        //sesuaikan nama dengan nama tabel
	        $this->Program_alocation->add($data);
	        delete_files($media['file_path']);
	             
	    }

		redirect(base_url()."references/program_allocations");
	}
}
