<?php require_once('Common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends Common {
	function __construct() {
		parent::__construct("site");
		
		$this->meta 			= array();
		$this->parts 			= array();
		$this->scripts 			= array();
		$this->styles 			= array();
		$this->title 			= "Welcome";
		$this->load->model(array('user_session'));
	}

	public function index(){
		if ($this->is_logged_in) {
			redirect(site_url("dashboard"));
		}else{
			redirect(site_url("site/login"));
		}
	}

	function login() {
		$data = array('flashdata'	=> $this->session->flashdata('form_msg'));
		$this->load->view('site/login',$data);
	}

	function validate() {
		$this->layout = false;
		$login_success = false;
		if ($this->input->post("submit")) {
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			if (isset($username) && isset($password)) {
				if ($this->user_session->create($username, $password)) {
					$login_success = true;
				}
			}
		}
		if(!$login_success){
			$this->session->set_flashdata('form_msg', array('success'=>false,'fail' =>true, 'msg' => 'Access denied. Incorrect username/password'));
			redirect(site_url('site/login'));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>true, 'fail'=> false, 'msg' => 'Login Success'));
			redirect(site_url('site'));
		}
		echo json_encode($response);
	}

	function logout() {
		$this->user_session->clear();
		redirect(site_url());
	}
	
	function no_access(){
		redirect(site_url());
	}

}