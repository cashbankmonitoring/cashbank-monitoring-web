<div class="col-lg-12">
  <div class="row">
    <div class="col-md-12">
      <div class="panel no-bd bd-3 panel-stat">
        <div class="panel-header">
          <h3><i class="fa fa-pie-chart" aria-hidden="true"></i> <strong>Cash Bank</strong> Position</h3>
          <div class="control-btn">
            <a onclick="javascript: get_cashbank_chart();" href="#" class="panel-reload hidden"><i class="icon-reload"></i></a>
          </div>
        </div>
        <div class="panel-body">
		<!--	
          <div id="chart-9" style="height: 300px;"><svg></svg></div>
		-->
		  <div id="cashbank_chart" style="height: 400px;"></div>
        </div>
      </div>
    </div>
  </div>
</div>