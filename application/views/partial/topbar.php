<div class="topbar">
  <div class="header-left">
    <div class="topnav">
      <a class="menutoggle" href="#" data-toggle="sidebar-collapsed"><span class="menu__handle"><span>Menu</span></span></a>
      <ul class="nav nav-icons">
        <li><a href="#" class="toggle-sidebar-top"><span class="icon-user-following"></span></a></li>
      </ul>
    </div>
  </div>
  <div class="header-right">
    <ul class="header-menu nav navbar-nav">
	  <li class="dropdown" id="user-header">
        <a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                <img src="<?= base_url() ?>assets/global/images/avatars/user1.png" alt="user image">
        <span class="username">Hi, <?= $user_session['C040_UserFullname']; ?></span>
        </a>
        <ul class="dropdown-menu">
          <li><a href="<?=base_url()."site/logout"?>"><i class="icon-logout"></i><span>Logout</span></a></li>
        </ul>
      </li>
	<!--	
      <li id="quickview-toggle"><a href="#"><i class="icon-bubbles"></i></a></li>
	-->
    </ul>
  </div>
</div>
