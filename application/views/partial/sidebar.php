<style type="text/css">
	.nav-sidebar {
		font-family: 'Open Sans';
	    font-size: 13px;
	    letter-spacing: 0.6px;
	    line-height: 21px;
	    color: #ffffff;
	}
	.nav-sidebar .list-group .list-group-item:hover{
		background-color: #cc6600 !important;
	}

	.nav-sidebar .list-group .list-group-item{
		background-color: #2b2e33 !important;
	    color: #ffffff;
	    border: none;
	}

	.nav-sidebar .list-group .list-group-sub-item{
		background-color: #2b2e33 !important;
	    color: #ffffff;
	    border: none;
	    margin-left: 10px;
	}
</style>
<div class="sidebar">
    <div class="logopanel">
      <h1>
        <a href="dashboard.html" style="background-size: 45px 31px;text-indent: 50px;font-size: 23px;color:#cc6600">POS INDONESIA</a>
      </h1>
    </div>
    <div class="sidebar-inner">
	    <div class="sidebar-top">
	        <form action="search-result.html" method="post" class="searchform hide" id="search-results">
	          <input type="text" class="form-control" name="keyword" placeholder="Search...">
	        </form>
	        <div class="userlogged clearfix">
	          <i class="icon icons-faces-users-01 m-b-0" style="font-size: 49px"></i>
	          <div class="user-details">
	            <h4 style="font-size: 22px;" class="text-center m-t-20"><b><?= $user_session['C040_UserFullname']; ?></b></h4>
	          </div>
			  
	        </div>
			<p class="text-center c-white"><?= $user_session['C020_Descr']; ?></p>
	    </div>
	    <div id="MainMenu" class="nav nav-sidebar">
	        <div class="list-group">
	        <?php foreach ($menus as $key => $menu) { 
      			if($menu['C016_ParentID'] == 0){ ?>
	         	<a href="<?=base_url().$menu['C014_Link']; ?>" class="list-group-item" data-parent="#MainMenu"><i class="fa fa-<?= $menu['C015_Icon']; ?>"></i> <?= $menu['C013_Label']; ?></a>
	         	<?php } else if($menu['C016_ParentID'] == 1){ ?>
	          	<a href="#<?= $menu['C000_SysID']; ?>" class="list-group-item" data-toggle="collapse" data-parent="#MainMenu"><i class="fa fa-<?= $menu['C015_Icon']; ?>"></i> <?= $menu['C013_Label']; ?> <i class="fa fa-caret-down"></i></a>
      				<div class="collapse" id="<?= $menu['C000_SysID']; ?>">
      					<?php foreach ($menu['sub_menu'] as $sub) { ?>
			           	 	<a href="<?=base_url().$sub['C014_Link']; ?>" class="list-group-item list-group-sub-item"><?= $sub['C013_Label']; ?></a>
		      			<?php } ?>
	          		</div>
      			<?php }
      			} ?>
      			<!-- <a href="#" class="list-group-item test" data-parent="#MainMenu"><i class="fa fa-home"></i> test</a> -->
      		</div>
      	</div>
      	
	  	<div class="sidebar-footer clearfix">
	        <a class="pull-left footer-settings" href="#" data-rel="tooltip" data-placement="top" data-original-title="Settings">
	        <i class="icon-settings"></i></a>
	        <a class="pull-left toggle_fullscreen" href="#" data-rel="tooltip" data-placement="top" data-original-title="Fullscreen">
	        <i class="icon-size-fullscreen"></i></a>
	        <a class="pull-left" href="#" data-rel="tooltip" data-placement="top" data-original-title="Lockscreen">
	        <i class="icon-lock"></i></a>
	        <a class="pull-left btn-effect" href="<?=base_url()."site/logout"?>" data-modal="modal-1" data-rel="tooltip" data-placement="top" data-original-title="Logout">
	        <i class="icon-power"></i></a>
      	</div>
    </div>
</div>