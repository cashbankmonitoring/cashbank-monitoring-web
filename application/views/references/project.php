<h2><strong>Program</strong> Data Master</h2>
<div class="row">
<div class="col-md-12">
	  <div class="panel">
		<div class="panel-header bg-orange">
		  <h3><strong>Daftar</strong> Data Program</h3>
		  <div class="control-btn">
		  	<div class="btn-group" style="margin-bottom:10px;">
			  <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			    <i class="fa fa-gear"></i> Aksi Lainnya<span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu">
			    <li><a href="#" id="import-program">Import Excel</a></li>
				<li><a href="<?=base_url(); ?>assets/download/format_program.xlsx">Download Format Excel</a></li>
			  </ul>
			</div>
			<button id="add-program" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Program</button>
		  </div>
		</div>
		<div class="panel-content p-5">
			<div id="lst_program" class="row">
				<div class="col-md-12 p-0">
				  <div class="panel">
					<div class="panel-content">
					  <table id="table" class="table table-hover table-bordered f-12" cellspacing="0" width="100%">
						<thead>
						  <tr>
							<th>#</th>
							<th>Mitra</th>
							<th>Nama Program</th>
							<th>Mulai</th>
							<th>Selesai</th>
							<th>Nilai</th>
							<th>Status</th>
							<th width="135">Aksi</th>
						  </tr>
						</thead>
						<tbody>
						</tbody>
					  </table>
					</div>
				  </div>
				</div>
			  </div>
		</div>
	  </div>
	</div>
</div>

<!-- Start Modal -->
<div class="modal fade" id="program-modal">
	<div class="modal-dialog">
	  <div class="modal-content">
	  	<div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
		  <h4 class="modal-title"><strong id="title-type"></strong> DATA PROGRAM</h4>
		  <!-- <p>(<label class="required"> </label> ) <label class="control-label">Mandatori / Harus di isi</label>	</p> -->
		</div>
		<div class="modal-body p-t-0 p-b-0">
			<div class="row">
				<!-- (<label class="required"> </label> ) <label class="control-label">Mandatori / Harus di isi</label> -->
				<form class="col-md-12 form-horizontal" id="program-form">
					<div class="col-md-12">
						<input type="hidden" name="program_id">
						<div class="form-group">
						  <label class="col-md-4 control-label required">Mitra</label>
						  <div class="col-md-8">
						  	<select class="form-control" name="partner" required="">
								<option value="">-- Pilih Mitra--</option>
								<?php foreach($partners as $row){
										echo '<option value="'.$row["SysID"].'">'.$row["PartnerName"].'</option>';
									} ?>															
							</select>
						  </div>
						</div>

						<div class="form-group">
						  <label class="col-md-4 control-label required">Nama Program</label>
						  <div class="col-md-8">
						  	<input type="text" class="form-control" required="" name="nama">
						  </div>
						</div>

						<div class="form-group">
						  <label class="col-md-4 control-label required">Akun Virtual</label>
						  <div class="col-md-8">
						  	<select class="form-control" name="virtual" required="">
								<option value="">-- Pilih Akun Virtual--</option>
								<?php foreach($virtuals as $row){
										echo '<option value="'.$row["C000_SysID"].'">'.$row["C011_Descr"].'</option>';
									} ?>															
							</select>
						  </div>
						</div>

						<div class="form-group">
						  <label class="col-md-4 control-label">Mulai - Selesai</label>
						  <div class="col-md-4">
								<input type="text" class="form-control input-sm prg-tgl" name="tgl_mulai">
						  </div>
						  <div class="col-md-4">
								<input type="text" class="form-control input-sm prg-tgl" name="tgl_selesai">
						  </div>
						</div>

						<div class="form-group">
						  <label class="col-md-4 control-label required">Nilai Program</label>
						  <div class="col-md-8">
						  	<input type="text" class="form-control nomor text-right" required="" name="nilai">
						  </div>
						</div>

						<div class="form-group">
						  <label class="col-md-4 control-label required">Currency</label>
						  <div class="col-md-8">
						  	<select class="form-control" name="currency" required="">
								<option value="1">IDR</option>
								<option value="2">USD</option>
							</select>
						  </div>
						</div>

						<div class="form-group">
						  <label class="col-md-4 control-label required">Status</label>
						  <div class="col-md-8">
						  	<select class="form-control" name="status" required="">
								<option value="0">OPEN</option>
								<option value="1">CLOSE</option>												
							</select>
						  </div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="modal-footer">
		  <button type="button" class="keluar btn btn-default btn-embossed" onclick="cancel_post('program','program')">Batal</button>
		  <button type="button" class="simpan btn btn-primary btn-embossed" onclick="post_data('program', 'Projects/save_program')">Simpan</button>
		</div>
	  </div>
	</div>
</div>
<!-- End Modal -->

<!-- Start Modal -->
<div class="modal fade" id="program-import-modal">
	<div class="modal-dialog">
	  <div class="modal-content">
	  	<div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
		  <h4 class="modal-title"><strong>IMPORT</strong> DATA PROGRAM</h4>
		</div>
		<div class="modal-body p-t-0 p-b-0">
			<div class="row">
				<form class="col-md-12 form-horizontal" action="<?=base_url(); ?>projects/upload_excel" method="post" enctype="multipart/form-data">
					<div class="col-md-12">
						<!-- <div class="form-group">
						  <label class="col-md-4 control-label">Judul</label>
						  <div class="col-md-8">
						    	<input type="text" name="judul" placeholder="Minimal 3 Karakter" minlength="3" maxlength="7" class="form-control">
						  </div>
						</div> -->
						<div class="form-group">
						  <label class="col-md-4 control-label">Upload Excel</label>
						  <div class="col-md-8">
						    	<input type="file" name="file" id="import_excel" required="" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
						  </div>
						</div>
					</div>
					<div class="col-md-12">
		  				<input type="submit" class="simpan btn btn-primary btn-embossed  pull-right" value="Upload">
					</div>
					
				</form>
			</div>
		</div>
	  </div>
	</div>
</div>
<!-- End Modal -->
