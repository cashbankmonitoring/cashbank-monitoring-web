<h2><strong>Akun Virtual</strong> Data Master</h2>
<div class="row">
<div class="col-md-12">
	  <div class="panel">
		<div class="panel-header bg-orange">
		  <h3><strong>Daftar</strong> Akun Virtual</h3>
		  <div class="control-btn">
		  	<div class="btn-group" style="margin-bottom:10px;">
				  <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    <i class="fa fa-gear"></i> Aksi Lainnya<span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu">
				    <li><a href="#" id="import-virtual">Import Excel</a></li>
					<li><a href="<?=base_url(); ?>assets/download/format_virtual.xlsx">Download Format Excel</a></li>
				  </ul>
				</div>
			<button id="add-virtual" class="btn btn-sm btn-success"><i class="fa fa-plus"></i>Akun Virtual</button>
		  </div>
		</div>
		<div class="panel-content p-5">
			<div id="lst_virtual" class="row">
				<div class="col-md-12 p-0">
				  <div class="panel">
					<div class="panel-content">
					  <table class="table table-hover table-bordered f-12" id="table" cellspacing="0" width="100%">
						<thead>
						  <tr>
							<th>#</th>
							<th>Kode</th>
							<th>Nama</th>
							<th>Aksi</th>
						  </tr>
						</thead>
						<tbody>
						</tbody>
					  </table>
					</div>
				  </div>
				</div>
			  </div>
		</div>
	  </div>
	</div>
</div>
<!-- Start Modal -->
<div class="modal fade" id="virtual-modal">
	<div class="modal-dialog">
	  <div class="modal-content">
	  	<div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
		  <h4 class="modal-title"><strong id="title-type"></strong> DATA AKUN VIRTUAL</h4>
		  <!-- <p>(<label class="required"> </label> ) <label class="control-label">Mandatori / Harus di isi</label>	</p> -->
		</div>
		<div class="modal-body p-t-0 p-b-0">
			<div class="row">
				<!-- (<label class="required"> </label> ) <label class="control-label">Mandatori / Harus di isi</label> -->
				<form class="col-md-12 form-horizontal" id="virtual-form">
					<div class="col-md-12">
						<input type="hidden" name="virtual_id">
						<div class="form-group">
						  <label class="col-md-4 control-label required">Kode</label>
						  <div class="col-md-8">
						  	<input type="text" required="" minlength="3" class="form-control" name="kode">
						  </div>
						</div>

						<div class="form-group">
						  <label class="col-md-4 control-label required">Nama Akun Virtual</label>
						  <div class="col-md-8">
						  	<input type="text" required="" minlength="3" class="form-control" name="nama">
						  </div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="modal-footer">
		  <button type="button" class="keluar btn btn-default btn-embossed" onclick="cancel_post('virtual','akun virtual')">Batal</button>
		  <button type="button" class="simpan btn btn-primary btn-embossed" onclick="post_data('virtual', 'Virtual_accounts/save_virtual')">Simpan</button>
		</div>
	  </div>
	</div>
</div>
<!-- End Modal -->

<!-- Start Modal -->
<div class="modal fade" id="virtual-import-modal">
	<div class="modal-dialog">
	  <div class="modal-content">
	  	<div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
		  <h4 class="modal-title"><strong>IMPORT</strong> DATA AKUN VIRTUAL</h4>
		</div>
		<div class="modal-body p-t-0 p-b-0">
			<div class="row">
				<form class="col-md-12 form-horizontal" action="<?=base_url(); ?>Virtual_accounts/upload_excel" method="post" enctype="multipart/form-data">
					<div class="col-md-12">
						<!-- <div class="form-group">
						  <label class="col-md-4 control-label">Judul</label>
						  <div class="col-md-8">
						    	<input type="text" name="judul" placeholder="Minimal 3 Karakter" minlength="3" maxlength="7" class="form-control">
						  </div>
						</div> -->
						<div class="form-group">
						  <label class="col-md-4 control-label">Upload Excel</label>
						  <div class="col-md-8">
						    	<input type="file" name="file" id="import_excel" required="" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
						  </div>
						</div>
					</div>
					<div class="col-md-12">
		  				<input type="submit" class="simpan btn btn-primary btn-embossed  pull-right" value="Upload">
					</div>
					
				</form>
			</div>
		</div>
	  </div>
	</div>
</div>
<!-- End Modal -->