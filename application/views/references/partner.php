<h2><strong>Mitra</strong> Data Master</h2>
<div class="row">
<div class="col-md-12">
	<div class="panel">
		<div class="panel-header bg-orange">
		  <h3><strong>Daftar</strong> Data Mitra</h3>
		  <div class="control-btn">
		  	<div class="btn-group" style="margin-bottom:10px;">
			  <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			    <i class="fa fa-gear"></i> Aksi Lainnya<span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu">
			    <li><a href="#" id="import-partner">Import Excel</a></li>
				<li><a href="<?=base_url(); ?>assets/download/format_partner.xlsx">Download Format Excel</a></li>
			  </ul>
			</div>
			<button id="add-partner" class="btn-frm-partner btn btn-sm btn-success"><i class="fa fa-plus"></i>Mitra</button>
		  </div>
		</div>
		<div class="panel-content p-5">
			<div id="lst_partner" class="row">
				<div class="col-md-12 p-0">
				  <div class="panel">
					<div class="panel-content">
					  <table id="table" class="table table-hover table-bordered f-12" cellspacing="0" width="100%">
						<thead>
						  <tr>
							<th>#</th>
							<th>Kode</th>
							<th>Mitra</th>
							<th>Aksi</th>
						  </tr>
						</thead>
						<tbody>
						</tbody>
					  </table>
					</div>
				  </div>
				</div>
			</div>
		</div>
	  </div>
	</div>
</div>

<!-- Start Modal -->
<div class="modal fade" id="partner-modal">
	<div class="modal-dialog">
	  <div class="modal-content">
	  	<div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
		  <h4 class="modal-title"><strong id="title-type"></strong> DATA MITRA</h4>
		</div>
		<div class="modal-body p-t-0 p-b-0">
			<div class="row">
				<form class="col-md-12 form-horizontal" id="partner-form">
					<div class="col-md-12">
						<input type="hidden" name="partner_id">
						<div class="form-group">
						  <label class="col-md-4 control-label required">Kode</label>
						  <div class="col-md-8">
						  	<input type="text" class="form-control" required="" name="kode" placeholder="Maximal 10 karakter" maxlength="10">
						  </div>
						</div>

						<div class="form-group">
						  <label class="col-md-4 control-label required">Nama Mitra</label>
						  <div class="col-md-8">
						  	<input type="text" class="form-control" required="" name="nama">
						  </div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="modal-footer">
		  <button type="button" class="keluar btn btn-default btn-embossed" onclick="cancel_post('partner','mitra')">Batal</button>
		  <button type="button" class="simpan btn btn-primary btn-embossed" onclick="post_data('partner', 'Partners/save_partner')">Simpan</button>
		</div>
	  </div>
	</div>
</div>
<!-- End Modal -->

<!-- Start Modal -->
<div class="modal fade" id="partner-import-modal">
	<div class="modal-dialog">
	  <div class="modal-content">
	  	<div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
		  <h4 class="modal-title"><strong>IMPORT</strong> DATA MITRA</h4>
		</div>
		<div class="modal-body p-t-0 p-b-0">
			<div class="row">
				<form class="col-md-12 form-horizontal" action="<?=base_url(); ?>partners/upload_excel" method="post" enctype="multipart/form-data">
					<div class="col-md-12">
						<!-- <div class="form-group">
						  <label class="col-md-4 control-label">Judul</label>
						  <div class="col-md-8">
						    	<input type="text" name="judul" placeholder="Minimal 3 Karakter" minlength="3" maxlength="7" class="form-control">
						  </div>
						</div> -->
						<div class="form-group">
						  <label class="col-md-4 control-label">Upload Excel</label>
						  <div class="col-md-8">
						    	<input type="file" name="file" id="import_excel" required="" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
						  </div>
						</div>
					</div>
					<div class="col-md-12">
		  				<input type="submit" class="simpan btn btn-primary btn-embossed  pull-right" value="Upload">
					</div>
					
				</form>
			</div>
		</div>
	  </div>
	</div>
</div>
<!-- End Modal -->