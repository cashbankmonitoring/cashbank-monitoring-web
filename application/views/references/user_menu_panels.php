
<script type="text/javascript">

	$(document).ready(function(){
         $('#tampTable').DataTable({
            "order": [[ 0, "asc" ]]
         });
    });
</script>
<h2><strong>REFERENCES</strong></h2>
<div class="row">
<div class="col-md-12">
	  <div class="panel">
		<div class="panel-header bg-orange">
		  <h3><strong>USER PANELS</strong></h3>
		  <div class="control-btn">
			<button class="btn-frm-user-panels btn btn-sm btn-success"><i class="fa fa-plus"></i>Panels</button>
		  </div>
		</div>
		<div class="panel-content p-5">
			<div id="frm_add_user_panels" class="row" style="display:none">
				<div class="col-md-12">
					<form role="form" method="POST" id="user-panel-frm">
						<h3><strong>ADD</strong> Data User Panels</h3>
						<div class="row">
							<div class="col-sm-12">
								<div class="row">
									<div class="col-sm-6">	
										<div class="form-group">
										  <label class="control-label required">Code</label>
										  <div class="append-icon">
											<input id="user_group" type="text" required="" placeholder="Minimum 3 characters..." minlength="3" class="form-control" name="code" aria-required="true">
											<i class="icon-user"></i>
										  </div>
										</div>
									</div>
									<div class="col-sm-6">	
										<div class="form-group">
										  <label class="control-label required">Label</label>
										  <div class="append-icon">
											<input id="user_group" type="text" required="" placeholder="Minimum 3 characters..." minlength="3" class="form-control" name="label" aria-required="true">
											<i class="icon-user"></i>
										  </div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
										  <label class="control-label">Description</label>
										  <div class="append-icon">
											<!-- <input id="desc" type="text" required="" placeholder="Minimum 3 characters..." minlength="3" class="form-control" name="part-name" aria-required="true"> -->
											<textarea id="desc" class="form-control" name="desc">
											</textarea>
											<i class="icon-user"></i>
										  </div>
										</div>
									</div>	
									<div class="col-sm-6">	
										<div class="form-group">
										  <label class="control-label">Sub Panels</label>
										  <div class="append-icon">
											<div id="user-group" style="overflow: scroll;height: 150px">
											<?php
												foreach($trcSubPanel as $trc){
											?>
												<input type="checkbox" name="trcType" value="<?= $trc['C000_SysID']; ?>"> <?= $trc['C011_Descr'];?><br>
											<?php
												}
											?>
											</div>
										  </div>
										</div>
									</div>
								</div>
								
							
							<div class="text-center  m-t-0">
							  <button class="btn-add-user-panels btn btn-embossed btn-primary" type="button">Add</button>
							  <button class="cancel-add-user-panels btn btn-embossed btn-default m-b-10 m-r-0" type="reset">Cancel</button>
							</div>
						  </div>
						</div>
						(<label class="required"> </label> ) <label class="control-label">Mandatori / Harus di isi</label>			
					</form>
				</div>
			</div>
			<div id="lst_user" class="row">
				<div class="col-md-12 p-0">
				  <div class="panel">
					<div class="panel-content">
					  <table class="table table-hover table-bordered f-12" id="tampTable">
						<thead>
						  <tr>
							<th>#</th>

							<th>Code</th>
							<th>Description</th>
							<th>Label</th>
							<th></th>
						  </tr>
						</thead>
						<tbody id="tbl_listpartner">
							<?php
									// print_r($userGroupTypes);	
								foreach($trcTypePanel as $row){
							?>
								<tr idx="<?= $row['C000_SysID']; ?>"><td><?= $row['C000_SysID']; ?></td><td><?= $row['C010_Code']; ?></td><td><?= $row['C011_Descr']; ?></td><td><?= $row['C012_Label']; ?></td>
								<td>
								<a data-target="#edit-user-panels" data-toggle="modal" class="edit-user-panels btn btn-sm btn-default"><i class="icon-note"></i></a>  
								<a class="delete-user-panels btn btn-sm btn-danger" href="javascript:;"><i class="icons-office-52"></i></a>
								</td></tr>
							<?php
								}
							?>
						</tbody>
					  </table>
					</div>
				  </div>
				</div>
			  </div>
		</div>
	  </div>
	</div>
</div>

<!--EDIT BANK ACC-->
<div class="modal fade" id="edit-user-panels" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog">
  <div class="modal-content">
	<div class="modal-header">
	  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
	  <h4 class="modal-title"><strong>EDIT</strong> DATA PANELS</h4>
	</div>
	<div class="modal-body">
		<form id="form-user-panels" class=" form-horizontal">
		  <div class="col-md-12">
			<div style="margin-bottom: 5px;" class="form-group">
			  <label class="col-sm-4 control-label">Code :</label>
			  <div class="col-sm-8">
				<input type="text" class="form-control input-sm" name="name_edit">
			  </div>
			</div>
			<div style="margin-bottom: 5px;" class="form-group">
			  <label class="col-sm-4 control-label">Label :</label>
			  <div class="col-sm-8">
				<input type="text" class="form-control input-sm" name="label_edit">
			  </div>
			</div>
			<div style="margin-bottom: 5px;" class="form-group">
			  <label class="col-sm-4 control-label">Description :</label>
			  <div class="col-sm-8">
				<textarea id="desc"  class="form-control" name="desc_edit">
				</textarea>
			  </div>
			</div>
			<div style="margin-bottom: 5px;" class="form-group">

			<label class="col-sm-4 control-label">Sub Panels</label>
			  <div class="col-sm-8">							
				  <div class="append-icon">
					<div id="user-list" style="overflow: scroll;height: 150px">
					<?php
						foreach($trcSubPanel as $trc){
					?>
						<input type="checkbox"  name="trcTypeEd" value="<?= $trc['C000_SysID']; ?>"> <?= $trc['C011_Descr'];?><br>
					<?php
						}
					?>
					</div>
				  </div>
				</div>
			</div>
		  </div>
		</form>
	</div>
	<div class="modal-footer">
	  <button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">Close</button>
	  <button type="button" class="post-upd-user-edit btn btn-primary btn-embossed">Update</button>
	</div>
  </div>
</div>
</div>