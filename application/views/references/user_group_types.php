
<script type="text/javascript">

	$(document).ready(function(){
         $('#tampTable').DataTable({
            "order": [[ 0, "asc" ]]
         });
    });
</script>
<h2><strong>Grup User</strong> Data Master</h2>
<div class="row">
<div class="col-md-12">
	  <div class="panel">
		<div class="panel-header bg-orange">
		  <h3><strong>Daftar</strong> Data Grup User</h3>
		  <div class="control-btn">
		  	<div class="btn-group" style="margin-bottom:10px;">
			  <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			    <i class="fa fa-gear"></i> Aksi Lainnya<span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu">
			    <li><a href="#" id="import-user_type">Import Excel</a></li>
			    <li><a href="<?=base_url(); ?>assets/download/format_user_type.xlsx">Download Format Excel</a></li>
			  </ul>
			</div>
			<button id="add-group" class="btn btn-sm btn-success"><i class="fa fa-plus"></i>Grup User</button>
		  </div>
		</div>
		<div class="panel-content p-5">
			<!-- <div id="frm_add_user_type" class="row" style="display:none">
				<div class="col-md-12">
					<form role="form" method="POST" id="group-form">
						<h3><strong>ADD</strong> Data User Group Type</h3>
						<div class="row">
							<div class="col-sm-6">
								<div class="row">
									<div class="col-sm-6">	
										<div class="form-group">
										  <label class="control-label required">User Group Type</label>
										  <div class="append-icon">
											<input id="user_group" type="text" required="" placeholder="Minimum 3 characters..." minlength="3" class="form-control" name="user_group" aria-required="true">
											<i class="icon-user"></i>
										  </div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
										  <label class="control-label">Description</label>
										  <div class="append-icon">
											<textarea id="desc" class="form-control" name="desc">
											</textarea>
											<i class="icon-user"></i>
										  </div>
										</div>
									</div>	
									<div class="col-sm-12">	
										<div class="form-group">
										  <label class="control-label">User Group</label>
										  <div class="append-icon">
											<div id="user-group" style="overflow: scroll;height: 150px">
											<?php
												foreach($trcType as $trc){
											?>
												<input type="checkbox" name="trcType" value="<?= $trc['C000_SysID']; ?>"> <?= $trc['C011_Descr'];?> <?= ($trc['C016_ParentID'] != 0 ? ($trc['C016_ParentID'] != 1 ? '| Sub' : '| Has Sub') : ''); ?><br>
											<?php
												}
											?>
											</div>
										  </div>
										</div>
									</div>
								</div>
								
							
							<div class="text-center  m-t-0">
							  <button class="btn-add-user-group-type btn btn-embossed btn-primary" type="button">Add</button>
							  <button class="cancel-add-user-group-type btn btn-embossed btn-default m-b-10 m-r-0" type="reset">Cancel</button>
							</div>
						  </div>
						</div>
						(<label class="required"> </label> ) <label class="control-label">Mandatori / Harus di isi</label>			
					</form>
				</div>
			</div> -->
			<div id="lst_group" class="row">
				<div class="col-md-12 p-0">
				  <div class="panel">
					<div class="panel-content">
					  <table class="table table-hover table-bordered f-12" id="tampTable">
						<thead>
						  <tr>
							<th>#</th>

							<th>Nama Grup User</th>
							<th>Deskripsi</th>
							<th></th>
						  </tr>
						</thead>
						<tbody id="tbl_listpartner">
							<?php	
								foreach($userGroupTypes as $key => $row){
									$number = $key+1;
							?>
								<tr idx="<?= $row['SysID']; ?>"><td><?= $number; ?></td><td><?= $row['UserGroupTypeName']; ?></td><td><?= $row['UserGroupTypeDesc']; ?></td>
								<td>
								<a class="edit-group btn btn-sm btn-default"><i class="icon-note"></i></a>  
								<a class="delete-user-type btn btn-sm btn-danger" href="javascript:;"><i class="icons-office-52"></i></a>
								</td></tr>
							<?php
								}
							?>
						</tbody>
					  </table>
					</div>
				  </div>
				</div>
			  </div>
		</div>
	  </div>
	</div>
</div>

<!-- Start Modal -->
<div class="modal fade" id="group-modal">
	<div class="modal-dialog">
	  <div class="modal-content">
	  	<div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
		  <h4 class="modal-title"><strong id="title-type"></strong> DATA GRUP USER</h4>
		  <!-- <p>(<label class="required"> </label> ) <label class="control-label">Mandatori / Harus di isi</label>	</p> -->
		</div>
		<div class="modal-body p-t-0 p-b-0">
			<div class="row">
				<!-- (<label class="required"> </label> ) <label class="control-label">Mandatori / Harus di isi</label> -->
				<form class="col-md-12 form-horizontal" id="group-form">
					<div class="col-md-12">
						<input type="hidden" name="group_id">
						<div class="form-group">
						  <label class="col-md-4 control-label required">Nama Grup User</label>
						  <div class="col-md-8">
						  	<input type="text" required="" minlength="3" class="form-control" name="nama">
						  </div>
						</div>

						<div class="form-group">
						  <label class="col-md-4 control-label">Deskripsi</label>
						  <div class="col-md-8">
							<textarea class="form-control" rows="4" name="deskripsi"></textarea>
						  </div>
						</div>

						<div class="form-group">
						  <label class="col-md-4 control-label required">Grup Panel</label>
						  <div class="col-md-8">
							<div id="user-list" style="overflow: scroll;height: 150px">
							<?php
								foreach($trcType as $trc){
							?>
								<input type="checkbox" name="trcType" value="<?= $trc['C000_SysID']; ?>"> 
								<?= ($trc['menu_head_label'] == '' ? '' : $trc['menu_head_label'].' - ' ).$trc['menu_label'].' | '.$trc['C012_Label'];?> <br>
							<?php
								}
							?>
							</div>
						  </div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="modal-footer">
		  <button type="button" class="keluar btn btn-default btn-embossed">Batal</button>
		  <button type="button" class="simpan btn btn-primary btn-embossed" onclick="post_data()">Simpan</button>
		</div>
	  </div>
	</div>
</div>
<!-- End Modal -->

<!-- Start Modal Export-->
<div class="modal fade" id="user_type-import-modal">
	<div class="modal-dialog">
	  <div class="modal-content">
	  	<div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
		  <h4 class="modal-title"><strong>IMPORT</strong> DATA OFFICE</h4>
		</div>
		<div class="modal-body p-t-0 p-b-0">
			<div class="row">
				<form class="col-md-12 form-horizontal" action="<?=base_url(); ?>user_group_types/upload_excel" method="post" enctype="multipart/form-data">
					<div class="col-md-12">
						<!-- <div class="form-group">
						  <label class="col-md-4 control-label">Judul</label>
						  <div class="col-md-8">
						    	<input type="text" name="judul" placeholder="Minimal 3 Karakter" minlength="3" maxlength="7" class="form-control">
						  </div>
						</div> -->
						<div class="form-group">
						  <label class="col-md-4 control-label">Upload Excel</label>
						  <div class="col-md-8">
						    	<input type="file" name="file" id="import_excel" required="" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
						  </div>
						</div>
					</div>
					<div class="col-md-12">
		  				<input type="submit" class="simpan btn btn-primary btn-embossed  pull-right" value="Upload">
					</div>
					
				</form>
			</div>
		</div>
	  </div>
	</div>
</div>
<!-- End Modal -->

<script type="text/javascript">
	$(document).ready(function(){
		$('.modal-footer').on('click', '.keluar', function(){
			bootbox.confirm("Anda yakin ingin membatalkan data form Grup User ini?", function(result){
				if (result) {
					$('#group-modal').modal('hide');
				}else{

				}
			});
		});

		$('.control-btn').on('click', '#import-user_type', function(){
			$('#user_type-import-modal').modal();
		});
	});
</script>

