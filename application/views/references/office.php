<style type="text/css">
	.select2-dropdown {
	    background-color: white;
	    border: 1px solid #aaa;
	    border-radius: 4px;
	    box-sizing: border-box;
	    display: block;
	    position: absolute;
	    left: -100000px;
	    width: 100%;
	    z-index: 105100000;
	}
	.select2-container{
		width: 100% !important;
	}

	.select2-container--default .select2-selection--single{
		-webkit-box-shadow: none !important;
	    border: 1px solid #ECEDEE !important;
	    box-shadow: none !important;
	    color: #555555;
	    display: inline-block;
	    font-size: 13px;
	    height: auto;
	    line-height: normal;
	    padding: 3px 10px;
	    vertical-align: middle;
	    width: 100%;
	    -webkit-border-radius: 2px;
	    -moz-border-radius: 2px;
	    border-radius: 2px;
	    -webkit-transition: all 0.2s ease-out;
	    -moz-transition: all 0.2s ease-out;
	    -o-transition: all 0.2s ease-out;
	    -ms-transition: all 0.2s ease-out;
	    transition: all 0.2s ease-out;
	}

	.help-block{
	    color:#dd4b39;
	 }
	 
	.error .select2-selection {
	    border: 1px solid #a94442;
	    border-radius: 4px;
	}

</style>
<h2><strong>Kantor</strong> Data Master</h2>
<div class="row reference" idx="<?= $user_session['C002_OfficeID'];?>">
	<div class="col-md-12">
		<div class="row">
		    <div class="col-md-12">
			  <div class="panel">
				<div class="row panel-header bg-orange">
					<div class="col-md-7">
						<h3><strong>Daftar</strong> Data Kantor</h3>
					</div>
					<!-- <div class="col-md-3 prepend-icon m-t-5">
						<input type="text" id="search" class="form-control form-white input-sm p-l-30" placeholder="Cari..." onchange="javascript: getlist_office	('');">
						<i class="icon-magnifier m-l-5"></i>
					</div> -->
					<div class="control-btn">
						<div class="btn-group" style="margin-bottom:10px;">
						  <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    <i class="fa fa-gear"></i> Aksi Lainnya<span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu">
						    <li><a href="#" id="import-office">Import Excel</a></li>
						    <li><a href="<?=base_url(); ?>assets/download/format_office.xlsx">Download Format Excel</a></li>
						  </ul>
						</div>
						<button id="add-office" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Kantor</button>
					</div>
				</div>
				<div class="panel-content p-5">
					<div id="lst_office" class="row">
						<div class="col-md-12 p-0">
						  <div class="panel">
							<div class="panel-content">
							  <table id="table" class="table table-hover table-bordered f-12"  cellspacing="0" width="100%">
								<thead>
								  <tr>
									<th>#</th>
									<th>Kantor Induk</th>	
									<th>Kode Kantor</th>
									<th>Deskripsi</th>
									<th>Koordinat</th>
									<th width="135">Aksi</th>
								  </tr>
								</thead>
								<tbody>
								</tbody>
							  </table>
							</div>
						  </div>
						</div>
					</div>
				</div>
			  </div>
			</div>
		</div>
	</div>
</div>
<!-- Start Modal -->
<div class="modal fade" id="office-modal">
	<div class="modal-dialog">
	  <div class="modal-content">
	  	<div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
		  <h4 class="modal-title"><strong id="title-type"></strong> DATA KANTOR</h4>
		</div>
		<div class="modal-body p-t-0 p-b-0">
			<div class="row">
				<form class="col-md-12 form-horizontal" id="office-form">
					<div class="col-md-12">
						<input type="hidden" name="kantor_id">
						<div class="form-group">
						  <label class="col-md-4 control-label required">Level Kantor</label>
						  <div class="col-md-8">
							<select name="level" id="change-level" class="form-control" required>
								<option value="">-- Pilih Level Kantor --</option>
								<option value="1">PUSAT</option>
								<option value="2">REGIONAL</option>
								<option value="3">KPRK</option>
								<option value="4">KPC</option>
								<option value="5">NASIONAL</option>
							</select>
						  </div>
						</div>

						<div class="form-group" id="show-regional" style="display:none">
						  <label class="col-md-4 control-label required">Regional</label>
						  <div class="col-md-8">
						  	<select class="form-control select2" id="get_regional" name="regional_id" required="">
                            	<option value="">-- Pilih Kantor --</option>
                            </select>
						  	<!-- <input type="text" id="get_regional" class="form-control" required="" name="regional_id" data-search="true" readonly=""> -->
						  </div>
						</div>

						<div class="form-group" id="show-kprk" style="display:none">
						  <label class="col-md-4 control-label required">KPRK</label>
						  <div class="col-md-8">
						  	<select class="form-control select2" id="get_kprk" name="kprk_id" required="">
                            	<option value="">-- Pilih Kantor --</option>
                            </select>
						  	<!-- <input type="text" id="get_kprk" class="form-control" required="" name="kprk_id" data-search="true" readonly=""> -->
						  </div>
						</div>

						<div class="form-group">
						  <label class="col-md-4 control-label required">Kode Kantor</label>
						  <div class="col-md-8">
							<input type="text" id="kode_kantor" name="kode" required="" placeholder="Minimal 3 Karakter" minlength="3" maxlength="7" class="form-control">
						  </div>
						</div>

						<div class="form-group">
						  <label class="col-md-4 control-label required">Deskripsi</label>
						  <div class="col-md-8">
							<textarea placeholder="Minimal 10 Karakter" class="form-control" rows="4" name="deskripsi" required=""></textarea>
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-4 control-label">Koordinat</label>
						  <div class="col-md-4">
						  	<div class="input-group">
							  <input type="text" class="form-control input-sm" name="koor_x" value="0">
							  <span class="input-group-addon"><i class="">X</i></span>
							</div>
						  </div>
						  <div class="col-md-4">
						  	<div class="input-group">
							  <input type="text" class="form-control input-sm" name="koor_y" value="0">
							  <span class="input-group-addon"><i class="">Y</i></span>
							</div>
						  </div>	
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="modal-footer">
		  <button type="button" class="keluar btn btn-default btn-embossed" onclick="cancel_post('office','kantor')">Batal</button>
		  <button type="button" class="simpan btn btn-primary btn-embossed" onclick="post_data_office()">Simpan</button>
		</div>
	  </div>
	</div>
</div>
<!-- End Modal -->

<!-- Start Modal Export-->
<div class="modal fade" id="office-import-modal">
	<div class="modal-dialog">
	  <div class="modal-content">
	  	<div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
		  <h4 class="modal-title"><strong>IMPORT</strong> DATA OFFICE</h4>
		</div>
		<div class="modal-body p-t-0 p-b-0">
			<div class="row">
				<form class="col-md-12 form-horizontal" action="<?=base_url(); ?>offices/upload_excel" method="post" enctype="multipart/form-data">
					<div class="col-md-12">
						<!-- <div class="form-group">
						  <label class="col-md-4 control-label">Judul</label>
						  <div class="col-md-8">
						    	<input type="text" name="judul" placeholder="Minimal 3 Karakter" minlength="3" maxlength="7" class="form-control">
						  </div>
						</div> -->
						<div class="form-group">
						  <label class="col-md-4 control-label">Upload Excel</label>
						  <div class="col-md-8">
						    	<input type="file" name="file" id="import_excel" required="" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
						  </div>
						</div>
					</div>
					<div class="col-md-12">
		  				<input type="submit" class="simpan btn btn-primary btn-embossed  pull-right" value="Upload">
					</div>
					
				</form>
			</div>
		</div>
	  </div>
	</div>
</div>
<!-- End Modal -->