<style type="text/css">
	label.myErrorClass {
	    color: red;
	    font-size: 11px;
	    display: block;
	}

	ul.myErrorClass input {
	    color: #666 !important;
	}

	ul.myErrorClass, input.myErrorClass, textarea.myErrorClass, select.myErrorClass {
	    border-width: 1px !important;
	    border-style: solid !important;
	    border-color: #cc0000 !important;
	    background-color: #f3d8d8 !important;
	    background-position: 50% 50% !important;
	    background-repeat: repeat !important;
	    background-image: url(http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/themes/blitzer/images/ui-bg_diagonals-thick_75_f3d8d8_40x40.png) !important;
	}

	.select2-dropdown {
	    background-color: white;
	    border: 1px solid #aaa;
	    border-radius: 4px;
	    box-sizing: border-box;
	    display: block;
	    position: absolute;
	    left: -100000px;
	    width: 100%;
	    z-index: 105100000;
	}
	.select2-container{
		width: 100% !important;
	}

	.select2-container--default .select2-selection--single{
		-webkit-box-shadow: none !important;
	    border: 1px solid #ECEDEE !important;
	    box-shadow: none !important;
	    color: #555555;
	    display: inline-block;
	    font-size: 13px;
	    height: auto;
	    line-height: normal;
	    padding: 3px 10px;
	    vertical-align: middle;
	    width: 100%;
	    -webkit-border-radius: 2px;
	    -moz-border-radius: 2px;
	    border-radius: 2px;
	    -webkit-transition: all 0.2s ease-out;
	    -moz-transition: all 0.2s ease-out;
	    -o-transition: all 0.2s ease-out;
	    -ms-transition: all 0.2s ease-out;
	    transition: all 0.2s ease-out;
	}
	
</style>
<h2><strong>User</strong> Data Master</h2>
<div class="row">
<div class="col-md-12">
	  <div class="panel">
		<div class="row panel-header bg-orange">
			<div class="col-md-5">
				<h3><strong>Daftar</strong> Data User</h3>
			</div>
			<!-- <div class="col-md-3 prepend-icon m-t-5">
				<input type="text" id="searchUser" class="form-control form-white input-sm p-l-30" placeholder="Cari..." onchange="javascript: getlist_user('');">
				<i class="icon-magnifier m-l-5"></i>
			</div> -->
			<div class="control-btn">
				<!-- <a href="<?=base_url(); ?>references/enable_all_begin_balance" class="btn-balance btn btn-sm btn-primary">Aktifkan Begin Balance Semua User</a> -->
				<div class="btn-group" style="margin-bottom:10px;">
				  <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    <i class="fa fa-gear"></i> Aksi Lainnya<span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu">
				    <li><a href="<?=base_url(); ?>Users/enable_all_begin_balance/0">Aktifkan Menu Begin Balance</a></li>
				    <li><a href="<?=base_url(); ?>Users/enable_all_begin_balance/1">Non Aktifkan Menu Begin Balance</a></li>
				    <li><a href="#" id="import-user">Import Excel</a></li>
					<li><a href="<?=base_url(); ?>assets/download/format_user.xlsx">Download Format Excel</a></li>
				  </ul>
				</div>
				<button id="add-user" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> User</button>
				<!-- <button id="import-user" class="btn btn-sm btn-success">Import Excel</button> -->
			</div>
		</div>
		<div class="panel-content p-5">
			<div id="lst_user" class="row">
				<div class="col-md-12 p-0">
				  <div class="panel">
					<div class="panel-content">
					  <table id="table" class="table table-hover table-bordered f-12" cellspacing="0" width="100%">
						<thead>
						  <tr>
							<th>#</th>
							<th>Level Kantor</th>
							<th>Kantor</th>
							<th>Username</th>
							<th>Nama Lengkap</th>
							<th>E-mail</th>
							<th>Status</th>
							<th>Aksi</th>
						  </tr>
						</thead>
						<tbody id="tbl_listuser">
						</tbody>
					  </table>
					</div>
				  </div>
				</div>
			</div>
		</div>
	  </div>
	</div>
</div>
<div class="modal fade" id="reset-pass" tabindex="-1" role="dialog" aria-hidden="false" style="display: none; padding-right: 10px;"><div class="modal-backdrop fade in" style="height: 783px;"></div>
<div class="modal-dialog" style="width: 350px;">
  <div class="modal-content">
	<div class="modal-header p-b-10">
	  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
	  <h4 class="modal-title"><strong>ENTER</strong> PASSWORD ADMINISTRATOR</h4>
	  <p class="small m-b-0">Password reset ke default..!</p>
	</div>
	<div class="modal-body p-0">
		
		<form class=" form-horizontal">
		  <div class="col-md-12">
			<div style="margin-bottom: 5px;" class="form-group">
			  <label class="col-sm-4 control-label">Password :</label>
			  <div class="col-sm-8">
				<input type="password" class="form-control input-sm" name="admin_pass">
			  </div>
			</div>
		  </div>
		</form>
	</div>
	<div class="modal-footer">
	  <button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">CANCEL</button>
	  <button type="button" class="confirm-rst-pass btn btn-primary btn-embossed">RESET</button>
	</div>
  </div>
</div>
</div>

<!-- Start Modal -->
<div class="modal fade" id="user-modal">
	<div class="modal-dialog">
	  <div class="modal-content">
	  	<div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
		  <h4 class="modal-title"><strong id="title-type"></strong> DATA USER</h4>
		</div>
		<div class="modal-body p-t-0 p-b-0">
			<div class="row">
				<form class="col-md-12 form-horizontal" id="user-form">
					<div class="col-md-12">
						<input type="hidden" name="user_id">
						<div class="form-group">
						  <label class="col-md-4 control-label required">Kantor</label>
						  <div class="col-md-8">
						  	<select class="form-control select2" id="get_kantor" name="kantor_id" required="">
                            	<option value="">-- Pilih Kantor --</option>
                            </select>
						  </div>
						</div>

						<div class="form-group hide">
						  <label class="col-md-4 control-label">Kode Kantor</label>
						  <div class="col-md-8">
							<input type="text" id="kode_kantor" name="kode" readonly="" placeholder="Minimal 3 Karakter" minlength="3" maxlength="7" class="form-control">
						  </div>
						</div>

						<div class="form-group">
						  <label class="col-md-4 control-label required">Username</label>
						  <div class="col-md-8">
							<input id="user_name" type="text" required="" maxlength="7" class="form-control" name="username">
						  </div>
						</div>

						<div class="form-group">
						  <label class="col-md-4 control-label required">Nama Lengkap</label>
						  <div class="col-md-8">
							<input id="User_Fullname" type="text" required="" placeholder="Minimal 3 Karakter" minlength="3" class="form-control" name="nama">
						  </div>
						</div>

						<div class="form-group" id="field_password">
						  <label class="col-md-4 control-label required">Password</label>
						  <div class="col-md-8">
							<input type="password" id="show_password" required="" placeholder="Minimal 6 Karakter" minlength="3" class="form-control" name="password">
						    <input type="checkbox" onchange="document.getElementById('show_password').type = this.checked ? 'text' : 'password'"> <span>Show password</span> 
						  </div>
						</div>

						<div class="form-group" id="field_confirm_password">
						  <label class="col-md-4 control-label required">Retype Password</label>
						  <div class="col-md-8">
							<input type="password" id="confirm_password" required="" placeholder="Minimal 6 Karakter" minlength="6" class="form-control" name="confirm_password" onkeyup="checkPass(); return false;">
							<span id="confirmMessage" class="confirmMessage"></span>
						  </div>
						</div>

						<div class="form-group">
						  <label class="col-md-4 control-label required">Email</label>
						  <div class="col-md-8">
							<input type="email" required="" class="form-control" name="email">
						  </div>
						</div>

						<div class="form-group">
						  <label class="col-md-4 control-label">ID Komputer</label>
						  <div class="col-md-8">
							<input type="text" placeholder="Minimal 3 Karakter" minlength="3" class="form-control" name="komputer_id">
						  </div>
						</div>

						<div class="form-group">
						  <label class="col-md-4 control-label">Deskripsi</label>
						  <div class="col-md-8">
							<textarea placeholder="Minimal 10 Karakter" class="form-control" rows="4" name="deskripsi" ></textarea>
						  </div>
						</div>

						<div class="form-group">
						  <label class="col-md-4 control-label required">User Type</label>
						  <div class="col-md-8">
							<select class="form-control" required="" name="user_type" id="parent">
								<option value="">-- Pilih User Type --</option>
								<?php
								foreach ($userType as $row) {
								?>
									<option value="<?= $row['SysID'];?>"><?= $row['UserGroupTypeName'];?></option>
								<?php
								}
								?>
							</select>
						  </div>
						</div>

						<div class="form-group">
						  <label class="col-md-4 control-label required">Status</label>
						  <div class="col-md-8">
							<select class="form-control" required="" name="status">
								<option value="1">Aktif</option>
								<option value="2">Tidak Aktif</option>
							</select>
						  </div>
						</div>

						<div class="form-group">
						  <label class="col-md-4 control-label">Aktifkan Begin Balance</label>
						  <div class="col-md-8">
						    	<input type="checkbox" name="is_begin_balance" id="is_begin_balance" style="margin-top: 15px;">
						  </div>
						</div>

						<div class="form-group">
						  <label class="col-md-4 control-label">Aktifkan Read Only Begin Balance</label>
						  <div class="col-md-8">
						    	<input type="checkbox" name="is_read_only" id="is_read_only" style="margin-top: 15px;">
						  </div>
						</div>
						<p><tt id="results"></tt></p>
					</div>
				</form>
			</div>
		</div>
		<div class="modal-footer">
		  <button type="button" class="keluar btn btn-default btn-embossed" onclick="cancel_post('user','user')">Batal</button>
		  <button type="button" class="simpan btn btn-primary btn-embossed" onclick="post_data_user()">Simpan</button>
		</div>
	  </div>
	</div>
</div>
<!-- End Modal -->

<!-- Start Modal -->
<div class="modal fade" id="user-import-modal">
	<div class="modal-dialog">
	  <div class="modal-content">
	  	<div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
		  <h4 class="modal-title"><strong>IMPORT</strong> DATA USER</h4>
		</div>
		<div class="modal-body p-t-0 p-b-0">
			<div class="row">
				<form class="col-md-12 form-horizontal" action="<?=base_url(); ?>users/upload_excel" method="post" enctype="multipart/form-data">
					<div class="col-md-12">
						<!-- <div class="form-group">
						  <label class="col-md-4 control-label">Judul</label>
						  <div class="col-md-8">
						    	<input type="text" name="judul" placeholder="Minimal 3 Karakter" minlength="3" maxlength="7" class="form-control">
						  </div>
						</div> -->
						<div class="form-group">
						  <label class="col-md-4 control-label">Upload Excel</label>
						  <div class="col-md-8">
						    	<input type="file" name="file" id="import_excel" required="" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
						  </div>
						</div>
					</div>
					<div class="col-md-12">
		  				<input type="submit" class="simpan btn btn-primary btn-embossed  pull-right" value="Upload">
					</div>
					
				</form>
			</div>
		</div>
	  </div>
	</div>
</div>
<!-- End Modal -->