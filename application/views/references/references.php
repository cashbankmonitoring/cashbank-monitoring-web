		
		<style>
		.main-content .page-content.page-thin {
		    padding: 25px 25px 0 25px;
		    overflow: visible;
		}
		.main-content .page-content .panel .panel-header {
		    /*display: block;*/
		    height: 50px;
		    /*padding: 6px 15px 6px 18px;*/
		    /*overflow: auto;*/
		}
		</style>
        <h2 style="margin-top : 0px"><strong>REFERENCES</strong></h2>
        <div class="row reference" idx="<?= $user_session['C002_OfficeID'];?>">
                <div class="col-md-12">
                  <div class="panel">
                    <div class="panel-header panel-controls hide">
                      <h3>Colored  <strong>version</strong></h3>
                    </div>
                    <div class="panel-content">
                      <ul class="nav nav-tabs nav-primary">
                        <?php 
							if ($user_session['C002_OfficeID'] == 1){
								echo '
								<li class="active"><a href="#tab2_1" data-toggle="tab"><i class="icon-home"></i> Office</a></li>
								<li><a href="#tab2_4" class="user-account" data-toggle="tab"><i class="icon-cloud-download"></i> User Account</a></li>
								<li><a href="#tab2_3" class="bank-account" data-toggle="tab"><i class="icon-cloud-download"></i> Bank Account</a></li>
								<!--<li class=""><a href="#tab2_5" data-toggle="tab"><i class="icon-cloud-download"></i> Pagu Kas</a></li>-->
								<li class="program-partner"><a href="#tab2_7" data-toggle="tab"><i class="icon-cloud-download"></i> Program Partner</a></li>
								<li class="program"><a href="#tab2_6" data-toggle="tab"><i class="icon-cloud-download"></i> Program</a></li>
								';
							} else if ($user_session['C002_OfficeID'] == 2){
								echo '
								<li class="active bank-account"><a href="#tab2_3" class="user-account" data-toggle="tab"><i class="icon-cloud-download"></i> Bank Account</a></li>
								<!--<li class="pagu-kas"><a href="#tab2_5" data-toggle="tab"><i class="icon-cloud-download"></i> Pagu Kas</a></li>-->
								<li class="program"><a href="#tab2_6" data-toggle="tab"><i class="icon-cloud-download"></i> Program</a></li>
								';
							} else {
								echo '
								<li class="active user-account"><a href="#tab2_3" class="user-account" data-toggle="tab"><i class="icon-cloud-download"></i> Bank Account</a></li>
								';
							}
						?>
						
						<li class="alokasi-program"><a href="#tab2_8" data-toggle="tab"><i class="icon-cloud-download"></i> Alokasi Program</a></li>
                      </ul>
                      <div class="tab-content">
                        <div class="tab-pane fade <?php if ($user_session['C002_OfficeID'] == 1){ echo ' active in'; } else { echo ''; } ?>  " id="tab2_1">
                          <div class="row">
							    <div class="col-md-12">
								  <div class="panel">
									<div class="row panel-header bg-orange">
										<div class="col-md-7">
											<h3><strong>OFFICE</strong> Data Master</h3>
										</div>
										<div class="col-md-3 prepend-icon m-t-5">
											<input type="text" id="search" class="form-control form-white input-sm p-l-30" placeholder="Search..." onchange="javascript: getlist_office	('');">
											<i class="icon-magnifier m-l-5"></i>
										</div>
										<div class="control-btn">
											<button class="btn-frm-office btn btn-sm btn-success"><i class="fa fa-plus"></i> Office</button>
										</div>
									</div>
									<div class="panel-content p-5">
										<div id="frm_add_office" class="row" style="display:none">
											<div class="col-md-12">
												<form role="form">
													<h3><strong>ADD</strong> Data Office</h3>
													<div class="row">
														<div class="col-sm-6">
															<div class="row">
																<div class="col-sm-6">	
																	<div class="form-group">
																	  <label class="control-label">Level</label>
																	  <div class="append-icon">
																		<select id="Off_C012_Level" class="form-control">
																			<option value="1">Pusat</option>
																			<option value="2">Regional</option>
																			<option value="3">KPRK</option>
																			<option value="4">KPC</option>
																		</select>
																	  </div>
																	</div>
																</div>
																<div class="col-sm-6">
																	<div class="form-group">
																	  <label class="control-label">Code</label>
																	  <div class="append-icon">
																		<input id="C010_Code" type="text" required="" placeholder="Minimum 3 characters..." minlength="3" class="form-control" name="firstname" aria-required="true">
																		<i class="icon-user"></i>
																	  </div>
																	</div>
																</div>	
																
															</div>
															<div class="row">
																<div id="Off_Row_Regional" class="col-sm-12" style="display:none">
																	<div class="form-group">
																	  <label class="control-label">Regional</label>
																	  <div class="append-icon">
																		<select id="Off_Sel_Regional" class="form-control">
																			<option value="1">Pusat</option>
																			<option value="2">Regional</option>
																			<option value="3">KPRK</option>
																			<option value="4">KPC</option>
																		</select>
																	  </div>
																	</div>
																</div>	
																<div id="Off_Row_KPRK" class="col-sm-12" style="display:none">	
																	<div class="form-group">
																	  <label class="control-label">KPRK</label>
																	  <div class="append-icon">
																		<select id="Off_Sel_KPRK" class="form-control">
																			<option value="1">Pusat</option>
																			<option value="2">Regional</option>
																			<option value="3">KPRK</option>
																			<option value="4">KPC</option>
																		</select>
																	  </div>
																	</div>
																</div>
															</div>
															
														</div>
													  <div class="col-sm-6">
														<div class="form-group m-b-6">
														  <label class="control-label">Description</label>
														  <div class="append-icon">
															<textarea id="C020_Descr" placeholder="Write your comment... (minimum 30 characters)" class="form-control" rows="4" name="comment" aria-required="true"></textarea>
															
														  </div>
														</div>
														<div class="form-group">
														  <label class="control-label">Coordinat</label>
														  <div class="row">
																<div class="col-sm-6">
																  <div class="append-icon">
																	<input id="C100_Long" type="text" required="" placeholder="Minimum 3 characters..." minlength="3" class="form-control" name="firstname" aria-required="true">
																	<i class="">X</i>
																  </div>
																</div>  
																<div class="col-sm-6">
																  <div class="append-icon">
																	<input id="C101_Lat" type="text" required="" placeholder="Minimum 3 characters..." minlength="3" class="form-control" name="firstname" aria-required="true">
																	<i class="">Y</i>
																  </div>
																</div>
														  </div>	
														</div>
														<div class="text-center  m-t-0">
														  <button class="btn-add-office btn btn-embossed btn-primary" type="button">Add</button>
														  <button class="cancel-add-office btn btn-embossed btn-default m-b-10 m-r-0" type="reset">Cancel</button>
														</div>
													  </div>
													</div>
													
												</form>
											</div>
										</div>
										<div id="lst_office" class="row">
											<div class="col-md-12 p-0">
											  <div class="panel">
												<div class="panel-content">
												  <table id="tampTable" class="table table-hover f-12">
													<thead>
													  <tr>
														<th width="8%">#</th>
														<th>Kantor Induk</th>	
														<th>Code</th>
														<th width="10%">Level</th>
														<th>Description</th>
														<th>Coordinate</th>
														<th></th>
													  </tr>
													</thead>
													<tbody id="tbl_listoffice">
														  	
									
													</tbody>
												  </table>
												
													<div id="pg_office" class="text-center">
														<!-- <div act="3" class="btn-group"> -->
															<!-- <button onclick="javascript: getlist_office(parseInt($(this).parent('div').attr('act')) -1);" type="button" class="btn btn-warning"><i class="fa fa-angle-left"></i></button>
															<button onclick="javascript: getlist_office($(this).html());" class="btn btn-warning">1</button>
															<button onclick="javascript: getlist_office($(this).html());" class="btn btn-warning">2</button>
															<button onclick="javascript: getlist_office($(this).html());" class="btn btn-warning active">3</button>
															<button onclick="javascript: getlist_office($(this).html());" class="btn btn-warning">4</button>
															<button onclick="javascript: getlist_office($(this).html());" class="btn btn-warning">5</button>
															<button onclick="javascript: getlist_office(parseInt($(this).parent('div').attr('act')) +1);" type="button" class="btn btn-warning"><i class="fa fa-angle-right"></i></button> -->
														<!-- </div> -->
													</div>
												
												</div>
											  </div>
											</div>
										  </div>
									</div>
								  </div>
								</div>

                          </div>
						  
                        </div>
                        <div class="tab-pane fade <?php if ($user_session['C002_OfficeID'] != 1){ echo ' active in'; } else { echo ''; } ?>" id="tab2_3">
                          <div class="row">
							<div class="col-md-12">
								  <div class="panel">
									<div class="row panel-header bg-orange">
										<div class="col-md-7">
											<h3><strong>BANK ACCOUNT</strong> Data Master</h3>
										</div>
										<div class="col-md-3 prepend-icon m-t-5">
											<input type="text" id="searchBank" class="form-control form-white input-sm p-l-30" placeholder="Search..." onchange="getlist_bankacc('','')">
											<i class="icon-magnifier m-l-5"></i>
										</div>
										<div class="control-btn">
											<button class="btn-frm-bankacc btn btn-sm btn-success"><i class="fa fa-plus"></i> Bank Account</button>
										</div>
									</div>
									<div class="panel-content p-5">
										<div id="frm_add_bankacc" class="row" style="display:none">
											<div class="col-md-12">
												<form role="form">
													<h3><strong>ADD</strong> Data Bank Account</h3>
													<div class="row">
														<div class="col-sm-6 <?php if ($user_session['C030_UserName'] != "superadmin"){ echo ' hide'; } else { echo ''; } ?>">
															<div class="row m-l-0">
																<div class="form-group">
																  <label class="control-label">Office Level</label>
																  <div class="append-icon">
																	<select id="BankAcc_C012_Level" class="form-control">
																		<option value="">-- Select --</option>
																		<option value="1">Pusat</option>
																		<option value="2">Regional</option>
																		<option value="3">KPRK</option>
																		<option value="4">KPC</option>
																	</select>
																  </div>
																</div>
																
																<div class="form-group">
																  <label class="control-label">Office</label>
																  <div class="append-icon">
																	<input type="text" class="form-control" id="get_idoff_bankacc" 
																	value="<?php if ($user_session['C030_UserName']  != "superadmin"){ echo $user_session['C002_OfficeID'] ; } else { echo ''; } ?>">
																	
																  </div>
																</div>
															</div>
															
														</div>
													  <div class="col-sm-6">
														<div class="row">
															<div class="col-sm-6">	
																<div class="form-group">
																  <label class="control-label">Bank Name</label>
																  <div class="append-icon">
																	<input type="text" required="" minlength="3" class="form-control" name="BankAcc_Name" aria-required="true">
																	<i class="icon-user"></i>
																  </div>
																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-group">
																  <label class="control-label">Bank Account Number</label>
																  <div class="append-icon">
																	<input type="text" required="" minlength="3" class="form-control" name="BankAcc_Number" aria-required="true">
																	<i class="icon-user"></i>
																  </div>
																</div>
															</div>	
															
														</div>
													  
													  
														<div class="form-group m-b-6">
														  <label class="control-label">Description</label>
														  <div class="append-icon">
															<textarea class="form-control" rows="4" name="BankAcc_Descr" aria-required="true"></textarea>
															
														  </div>
														</div>

														<div class="text-center  m-t-0">
														  <button class="btn-add-bankacc btn btn-embossed btn-primary" type="button">Add</button>
														  <button class="cancel-add-bankacc btn btn-embossed btn-default m-b-10 m-r-0" type="reset">Cancel</button>
														</div>
													  </div>
													</div>
													
												</form>
											</div>
										</div>
										<div id="lst_bankacc" class="row">
											<div class="col-md-12 p-0">
											  <div class="panel">
												<div class="panel-content">
												  <table class="table table-hover f-12">
													<thead>
													  <tr>
														<th>#</th>
														<th>Office</th>
														<th>Bank</th>
														<th>Account Number</th>
														<th>Description</th>
														<th></th>
													  </tr>
													</thead>
													<tbody id="tbl_listbankacc">
													</tbody>
												  </table>
												   <div id="pg_bankacc" class="text-center">
														<div act="3" class="btn-group">
															<button onclick="javascript: getlist_bankacc(<?= $user_session['C002_OfficeID']; ?>, parseInt($(this).parent('div').attr('act')) -1);" type="button" class="btn btn-warning"><i class="fa fa-angle-left"></i></button>
															<button onclick="javascript: getlist_bankacc(<?= $user_session['C002_OfficeID']; ?>, $(this).html());" class="btn btn-warning">1</button>
															<button onclick="javascript: getlist_bankacc(<?= $user_session['C002_OfficeID']; ?>, $(this).html());" class="btn btn-warning">2</button>
															<button onclick="javascript: getlist_bankacc(<?= $user_session['C002_OfficeID']; ?>, $(this).html());" class="btn btn-warning active">3</button>
															<button onclick="javascript: getlist_bankacc(<?= $user_session['C002_OfficeID']; ?>, $(this).html());" class="btn btn-warning">4</button>
															<button onclick="javascript: getlist_bankacc(<?= $user_session['C002_OfficeID']; ?>, $(this).html());" class="btn btn-warning">5</button>
															<button onclick="javascript: getlist_bankacc(<?= $user_session['C002_OfficeID']; ?>, parseInt($(this).parent('div').attr('act')) +1);" type="button" class="btn btn-warning"><i class="fa fa-angle-right"></i></button>
														</div>
													</div>
												</div>
											  </div>
											</div>
										  </div>
									</div>
								  </div>
								</div>
						  
						  
						  
                          </div>
                          <!--EDIT BANK ACC-->
							<div class="modal fade ulala" id="edit-bankacc" tabindex="-1" role="dialog" aria-hidden="true">
							<div class="modal-dialog">
							  <div class="modal-content">
								<div class="modal-header">
								  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
								  <h4 class="modal-title"><strong>EDIT</strong> DATA REKENING BANK</h4>
								</div>
								<div class="modal-body">
									<form class=" form-horizontal">
									  <div class="col-md-12">
										<div style="margin-bottom: 5px;" class="form-group">
										  <label class="col-sm-4 control-label">Nama BANK :</label>
										  <div class="col-sm-8">
											<input type="text" class="form-control input-sm" name="nm_bankacc">
										  </div>
										</div>
										<div style="margin-bottom: 5px;" class="form-group">
										  <label class="col-sm-4 control-label">Nomor Rekening :</label>
										  <div class="col-sm-8">
											<input type="text" class="form-control input-sm" name="nmr_bankacc">
										  </div>
										</div>
										<div style="margin-bottom: 5px;" class="form-group">
										  <label class="col-sm-4 control-label">Keterangan :</label>
										  <div class="col-sm-8">
											<textarea style="width:100%;background-color: #ecedee;border: 1px solid #ecedee;height:103px" class="p-10 f-12" name="ket_bankacc"></textarea>
										  </div>
										</div>
									  </div>
									</form>
								</div>
								<div class="modal-footer">
								  <button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">Close</button>
								  <button type="button" class="post-upd-bankacc btn btn-primary btn-embossed">Update</button>
								</div>
							  </div>
							</div>
							</div>

                        </div>
                        <div class="tab-pane fade" id="tab2_4">
                          <div class="row">
							<div class="col-md-12">
								  <div class="panel">
									<div class="row panel-header bg-orange">
										<div class="col-md-7">
											<h3><strong>USER ACCOUNT</strong> Data Master</h3>
										</div>
										<div class="col-md-3 prepend-icon m-t-5">
											<input type="text" id="searchUser" class="form-control form-white input-sm p-l-30" placeholder="Search..." onchange="javascript: getlist_user('');">
											<i class="icon-magnifier m-l-5"></i>
										</div>
										<div class="control-btn">
											<button class="btn-frm-user btn btn-sm btn-success"><i class="fa fa-plus"></i> User</button>
										</div>
									</div>
									<div class="panel-content p-5">
										<div id="frm_add_user" class="row" style="display:none">
											<div class="col-md-12">
												<form role="form">
													<h3><strong>ADD</strong> Data User Account</h3>
													<div class="row">
														<div class="col-sm-6">
															<div class="row">
																<div class="col-sm-6">	
																	<div class="form-group">
																	  <label class="control-label">Office Level</label>
																	  <div class="append-icon">
																		<select id="User_C012_Level" class="form-control">
																			<option value="1">Pusat</option>
																			<option value="2">Regional</option>
																			<option value="3">KPRK</option>
																			<option value="4">KPC</option>
																		</select>
																	  </div>
																	</div>
																</div>
																<div class="col-sm-6">
																	<div class="form-group">
																	  <label class="control-label">User Account Code</label>
																	  <div class="append-icon">
																		<input id="User_Code" type="text" required="" placeholder="Minimum 3 characters..." minlength="3" class="form-control" name="firstname" aria-required="true">
																		<i class="icon-user"></i>
																	  </div>
																	</div>
																</div>	
																
															</div>
															<div class="row">
																<div id="User_Row_Regional" class="col-sm-12" style="display:none">
																	<div class="form-group">
																	  <label class="control-label">Regional</label>
																	  <div class="append-icon">
																		<select id="User_Sel_Regional" class="form-control">
																			<option value="1">Pusat</option>
																			<option value="2">Regional</option>
																			<option value="3">KPRK</option>
																			<option value="4">KPC</option>
																		</select>
																	  </div>
																	</div>
																</div>	
																<div id="User_Row_KPRK" class="col-sm-12" style="display:none">	
																	<div class="form-group">
																	  <label class="control-label">KPRK</label>
																	  <div class="append-icon">
																		<select id="User_Sel_KPRK" class="form-control">
																			<option value="1">Pusat</option>
																			<option value="2">Regional</option>
																			<option value="3">KPRK</option>
																			<option value="4">KPC</option>
																		</select>
																	  </div>
																	</div>
																</div>
																<div id="User_Row_KPC" class="col-sm-12" style="display:none">	
																	<div class="form-group">
																	  <label class="control-label">KPC</label>
																	  <div class="append-icon">
																		<select id="User_Sel_KPC" class="form-control">
																			<option value="1">Pusat</option>
																			<option value="2">Regional</option>
																			<option value="3">KPRK</option>
																			<option value="4">KPC</option>
																		</select>
																	  </div>
																	</div>
																</div>
															</div>
															
														</div>
													  <div class="col-sm-6">
														<div class="row">
															<div class="col-sm-6">
																<div class="form-group">
																  <label class="control-label">Username</label>
																  <div class="append-icon">
																	<input id="User_Name" type="text" required="" placeholder="Minimum 3 characters..." minlength="3" class="form-control" name="firstname" aria-required="true">
																	<i class="icon-user"></i>
																  </div>
																</div>
																<div class="form-group">
																  <label class="control-label">Password</label>
																  <div class="append-icon">
																	<input id="User_Password" type="text" required="" placeholder="Minimum 3 characters..." minlength="3" class="form-control" name="firstname" aria-required="true">
																	<i class="icon-user"></i>
																  </div>
																</div>
																<div class="form-group">
																  <label class="control-label">E-mail</label>
																  <div class="append-icon">
																	<input id="User_Email" type="text" required="" placeholder="Minimum 3 characters..." minlength="3" class="form-control" name="firstname" aria-required="true">
																	<i class="icon-user"></i>
																  </div>
																</div>
															</div>	
															<div class="col-sm-6">
																<div class="form-group">
																  <label class="control-label">Fullname</label>
																  <div class="append-icon">
																	<input id="User_Fullname" type="text" required="" placeholder="Minimum 3 characters..." minlength="3" class="form-control" name="firstname" aria-required="true">
																	<i class="icon-user"></i>
																  </div>
																</div>
																<div class="form-group">
																  <label class="control-label">Retype Password</label>
																  <div class="append-icon">
																	<input id="User_RPassword" type="text" required="" placeholder="Minimum 3 characters..." minlength="3" class="form-control" name="firstname" aria-required="true">
																	<i class="icon-user"></i>
																  </div>
																</div>
																<div class="form-group">
																  <label class="control-label">Computer-ID</label>
																  <div class="append-icon">
																	<input id="User_CompID" type="text" required="" placeholder="Minimum 3 characters..." minlength="3" class="form-control" name="firstname" aria-required="true">
																	<i class="icon-user"></i>
																  </div>
																</div>
															</div>
														</div>
														
														
														<div class="form-group m-b-6">
														  <label class="control-label">Description</label>
														  <div class="append-icon">
															<textarea id="User_Descr" placeholder="Write your comment... (minimum 30 characters)" class="form-control" rows="4" name="comment" aria-required="true"></textarea>
															
														  </div>
														</div>

														<div class="text-center  m-t-0">
														  <button class="btn-add-user btn btn-embossed btn-primary" type="button">Add</button>
														  <button class="cancel-add-user btn btn-embossed btn-default m-b-10 m-r-0" type="reset">Cancel</button>
														</div>
													  </div>
													</div>
													
												</form>
											</div>
										</div>
										<div id="lst_user" class="row">
											<div class="col-md-12 p-0">
											  <div class="panel">
												<div class="panel-content">
												  <table class="table table-hover table-bordered f-12">
													<thead>
													  <tr>
														<th>#</th>
														<th>Office Level</th>
														<th>Office</th>
														<th>Username</th>
														<th>Fullname</th>
														<th>E-mail</th>
														<th>Stat</th>
														<th width="120"></th>
													  </tr>
													</thead>
													<tbody id="tbl_listuser">
													</tbody>
												  </table>
												   <div id="pg_user" class="text-center">
														<div act="3" class="btn-group">
															<button onclick="javascript: getlist_user(parseInt($(this).parent('div').attr('act')) -1);" type="button" class="btn btn-warning"><i class="fa fa-angle-left"></i></button>
															<button onclick="javascript: getlist_user($(this).html());" class="btn btn-warning">1</button>
															<button onclick="javascript: getlist_user($(this).html());" class="btn btn-warning">2</button>
															<button onclick="javascript: getlist_user($(this).html());" class="btn btn-warning active">3</button>
															<button onclick="javascript: getlist_user($(this).html());" class="btn btn-warning">4</button>
															<button onclick="javascript: getlist_user($(this).html());" class="btn btn-warning">5</button>
															<button onclick="javascript: getlist_user(parseInt($(this).parent('div').attr('act')) +1);" type="button" class="btn btn-warning"><i class="fa fa-angle-right"></i></button>
														</div>
													</div>
												</div>
											  </div>
											</div>
										  </div>
									</div>
								  </div>
								</div>
                          </div>
                          <div class="modal fade" id="reset-pass" tabindex="-1" role="dialog" aria-hidden="false" style="display: none; padding-right: 10px;"><div class="modal-backdrop fade in" style="height: 783px;"></div>
							<div class="modal-dialog" style="width: 350px;">
							  <div class="modal-content">
								<div class="modal-header p-b-10">
								  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
								  <h4 class="modal-title"><strong>ENTER</strong> PASSWORD ADMINISTRATOR</h4>
								  <p class="small m-b-0">Password reset ke default..!</p>
								</div>
								<div class="modal-body p-0">
									
									<form class=" form-horizontal">
									  <div class="col-md-12">
										<div style="margin-bottom: 5px;" class="form-group">
										  <label class="col-sm-4 control-label">Password :</label>
										  <div class="col-sm-8">
											<input type="password" class="form-control input-sm" name="admin_pass">
										  </div>
										</div>
									  </div>
									</form>
								</div>
								<div class="modal-footer">
								  <button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">CANCEL</button>
								  <button type="button" class="confirm-rst-pass btn btn-primary btn-embossed">RESET</button>
								</div>
							  </div>
							</div>
							</div>
                        </div>
						<div class="tab-pane fade" id="tab2_5">
                          <div class="row">
							<div class="col-md-12">
								  <div class="panel">
									<div class="panel-header bg-orange">
									  <h3><strong>Pagu</strong> Kas</h3>
									  <div class="control-btn">
										<button class="btn-frm-coa btn btn-sm btn-success"><i class="fa fa-plus"></i> Pagu Kas</button>
									  </div>
									</div>
									<div class="panel-content p-5">
										<div id="frm_add_coa" class="row" style="display:none">
											<div class="col-md-12">
												<form role="form">
													<h3><strong>ADD</strong> Data User Account</h3>
													<div class="row">
														<div class="col-sm-6">
															<div class="row">
																<div class="col-sm-6">	
																	<div class="form-group">
																	  <label class="control-label">Office Level</label>
																	  <div class="append-icon">
																		<select id="COA_C012_Level" class="form-control">
																			<option value="1">Pusat</option>
																			<option value="2">Regional</option>
																			<option value="3">KPRK</option>
																			<option value="4">KPC</option>
																		</select>
																	  </div>
																	</div>
																</div>
																<div class="col-sm-6">
																	<div class="form-group">
																	  <label class="control-label">User Account Code</label>
																	  <div class="append-icon">
																		<input id="COA_Code" type="text" required="" placeholder="Minimum 3 characters..." minlength="3" class="form-control" name="firstname" aria-required="true">
																		<i class="icon-user"></i>
																	  </div>
																	</div>
																</div>	
																
															</div>
															<div class="row">
																<div id="COA_Row_Regional" class="col-sm-12" style="display:none">
																	<div class="form-group">
																	  <label class="control-label">Regional</label>
																	  <div class="append-icon">
																		<select id="COA_Sel_Regional" class="form-control">
																			<option value="1">Pusat</option>
																			<option value="2">Regional</option>
																			<option value="3">KPRK</option>
																			<option value="4">KPC</option>
																		</select>
																	  </div>
																	</div>
																</div>	
																<div id="COA_Row_KPRK" class="col-sm-12" style="display:none">	
																	<div class="form-group">
																	  <label class="control-label">KPRK</label>
																	  <div class="append-icon">
																		<select id="COA_Sel_KPRK" class="form-control">
																			<option value="1">Pusat</option>
																			<option value="2">Regional</option>
																			<option value="3">KPRK</option>
																			<option value="4">KPC</option>
																		</select>
																	  </div>
																	</div>
																</div>
																<div id="COA_Row_KPC" class="col-sm-12" style="display:none">	
																	<div class="form-group">
																	  <label class="control-label">KPC</label>
																	  <div class="append-icon">
																		<select id="COA_Sel_KPC" class="form-control">
																			<option value="1">Pusat</option>
																			<option value="2">Regional</option>
																			<option value="3">KPRK</option>
																			<option value="4">KPC</option>
																		</select>
																	  </div>
																	</div>
																</div>
															</div>
															
														</div>
													  <div class="col-sm-6">
														<div class="row">
															<div class="col-sm-6">
																<div class="form-group">
																  <label class="control-label">Username</label>
																  <div class="append-icon">
																	<input id="COA_Name" type="text" required="" placeholder="Minimum 3 characters..." minlength="3" class="form-control" name="firstname" aria-required="true">
																	<i class="icon-user"></i>
																  </div>
																</div>
																<div class="form-group">
																  <label class="control-label">Password</label>
																  <div class="append-icon">
																	<input id="COA_Password" type="text" required="" placeholder="Minimum 3 characters..." minlength="3" class="form-control" name="firstname" aria-required="true">
																	<i class="icon-user"></i>
																  </div>
																</div>
																<div class="form-group">
																  <label class="control-label">E-mail</label>
																  <div class="append-icon">
																	<input id="COA_Email" type="text" required="" placeholder="Minimum 3 characters..." minlength="3" class="form-control" name="firstname" aria-required="true">
																	<i class="icon-user"></i>
																  </div>
																</div>
															</div>	
															<div class="col-sm-6">
																<div class="form-group">
																  <label class="control-label">Fullname</label>
																  <div class="append-icon">
																	<input id="COA_Fullname" type="text" required="" placeholder="Minimum 3 characters..." minlength="3" class="form-control" name="firstname" aria-required="true">
																	<i class="icon-user"></i>
																  </div>
																</div>
																<div class="form-group">
																  <label class="control-label">Retype Password</label>
																  <div class="append-icon">
																	<input id="COA_RPassword" type="text" required="" placeholder="Minimum 3 characters..." minlength="3" class="form-control" name="firstname" aria-required="true">
																	<i class="icon-user"></i>
																  </div>
																</div>
																<div class="form-group">
																  <label class="control-label">Computer-ID</label>
																  <div class="append-icon">
																	<input id="COA_CompID" type="text" required="" placeholder="Minimum 3 characters..." minlength="3" class="form-control" name="firstname" aria-required="true">
																	<i class="icon-user"></i>
																  </div>
																</div>
															</div>
														</div>
														
														
														<div class="form-group m-b-6">
														  <label class="control-label">Description</label>
														  <div class="append-icon">
															<textarea id="COA_Descr" placeholder="Write your comment... (minimum 30 characters)" class="form-control" rows="4" name="comment" aria-required="true"></textarea>
															
														  </div>
														</div>

														<div class="text-center  m-t-0">
														  <button class="btn-add-coa btn btn-embossed btn-primary" type="button">Add</button>
														  <button class="cancel-add-coa btn btn-embossed btn-default m-b-10 m-r-0" type="reset">Cancel</button>
														</div>
													  </div>
													</div>
													
												</form>
											</div>
										</div>
										<div id="lst_coa" class="row">
											<div class="col-md-12 p-0">
											  <div class="panel">
												<div class="panel-content">
												  <table class="table table-hover table-bordered">
													<thead>
													  <tr>
														<th>#</th>
														<th>Level</th>
														<th>Code</th>
														<th>Description</th>
														<th>SquencesID</th>
														<th>ParentID</th>
														<th>L</th>
														<th>R</th>
														<th></th>
													  </tr>
													</thead>
													<tbody id="tbl_listcoa">
													</tbody>
												  </table>
												</div>
											  </div>
											</div>
										  </div>
									</div>
								  </div>
								</div>
                          </div>
                        </div>
                        <div class="tab-pane fade" id="tab2_6">
                          <div class="row">
							<div class="col-md-12">
								  <div class="panel">
									<div class="panel-header bg-orange">
									  <h3><strong>PROJECT</strong> Data Master</h3>
									  <div class="control-btn">
										<button class="btn-frm-project btn btn-sm btn-success"><i class="fa fa-plus"></i> Program</button>
									  </div>
									</div>
									<div class="panel-content p-5">
										<div id="frm_add_project" class="row" style="display:none">
											<div class="col-md-12">
												<form role="form">
													<h3><strong>ADD</strong> Data Project</h3>
													<div class="row">
														<div class="col-sm-6">
															
															
															<div class="row">
																<div class="col-sm-6">
																	<div class="form-group">
																	  <label class="control-label">Project Partner</label>
																	  <div class="append-icon">
																		<select id="Project_PartnerID" class="form-control">
																			<option value="">-- Select --</option>
																			<?php
																				foreach($partners as $row)
																				{
																					echo '<option value="'.$row["SysID"].'">'.$row["PartnerName"].'</option>';
																				}
																			
																			?>
																			
																		</select>
																	  </div>
																	</div>
																</div>
																<div class="col-sm-6">	
																	<div class="form-group">
																	  <label class="control-label">Project Code</label>
																	  <div class="append-icon">
																		<input id="Project_Code" type="text" class="form-control" aria-required="true">
																		<i class="icon-user"></i>
																	  </div>
																	</div>
																</div>
																	
																
															</div>
															<div class="row">
																<div class="col-sm-6">	
																	<div class="form-group">
																	  <label class="control-label">Project Value</label>
																	  <div class="append-icon">
																		<input id="Project_Value" type="text" class="form-control nomor text-right" aria-required="true">
																		<i class="fa fa-money"></i>
																	  </div>
																	</div>
																</div>
																<div class="col-sm-6">
																	<div class="form-group">
																	  <label class="control-label">Currency</label>
																	  <div class="append-icon">
																		<select id="Project_CurrencyID" class="form-control">
																			<option value="1">IDR</option>
																			<option value="2">USD</option>
																		</select>
																	  </div>
																	</div>
																</div>	
																
															</div>
															
														</div>
													  <div class="col-sm-6">
													  
														<div class="form-group m-b-6">
														  <label class="control-label">Description</label>
														  <div class="append-icon">
															<textarea id="Project_Descr" class="form-control" rows="2" name="comment" aria-required="true"></textarea>
															
														  </div>
														</div>
														
														<div class="row">
															<div class="col-sm-6">	
																<div class="form-group">
																  <label class="control-label">Project Start</label>
																  <div class="append-icon">
																	<input id="Project_DateStart" type="text" class="date-picker form-control" aria-required="true">
																	<i class="fa fa-calendar"></i>
																  </div>
																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-group">
																  <label class="control-label">Project Finished</label>
																  <div class="append-icon">
																	<input id="Project_DateFinished" type="text" class="date-picker form-control" aria-required="true">
																	<i class="fa fa-calendar"></i>
																  </div>
																</div>
															</div>	
															
														</div>
														<div class="text-center  m-t-0">
														  <button class="btn-add-project btn btn-embossed btn-primary" type="button">Add</button>
														  <button class="cancel-add-project btn btn-embossed btn-default m-b-10 m-r-0" type="reset">Cancel</button>
														</div>
													  </div>
													</div>
													
												</form>
											</div>
										</div>
										<div id="lst_project" class="row">
											<div class="col-md-12 p-0">
											  <div class="panel">
												<div class="panel-content">
												  <table class="table table-hover table-bordered f-12">
													<thead>
													  <tr>
														<th>#</th>
														<th>Partner</th>
														<th>Nama Program</th>
														<th>Mulai</th>
														<th>Selesai</th>
														<th>Nilai</th>
														<th>Status</th>
														<th width="55"></th>
													  </tr>
													</thead>
													<tbody id="tbl_listproject">
														
													</tbody>
												  </table>
												</div>
											  </div>
											</div>
										  </div>
									</div>
								  </div>
								</div>
                          </div>
                          
							<!--EDIT PROGRAM-->
							<div class="modal fade" id="edit-program" tabindex="-1" role="dialog" aria-hidden="true">
							<div class="modal-dialog">
							  <div class="modal-content">
								<div class="modal-header">
								  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
								  <h4 class="modal-title"><strong>EDIT</strong> DATA PROGRAM</h4>
								</div>
								<div class="modal-body p-t-0 p-b-0">
									<div class="row">
										<form class=" form-horizontal">
										  <div class="col-md-12">
											<div style="margin-bottom: 5px;" class="form-group">
											  <label class="col-sm-4 control-label">Partner :</label>
											  <div class="col-sm-8">
												<select class="form-control" id="prg_partner" tabindex="-1" title="">
													<option value="">-- Select --</option>
													<?php
														foreach($partners as $row)
														{
															echo '<option value="'.$row["SysID"].'">'.$row["PartnerName"].'</option>';
														}
													
													?>
																													
												</select>
											  </div>
											</div>
											
											<div style="margin-bottom: 5px;" class="form-group">
											  <label class="col-sm-4 control-label">Nama Program :</label>
											  <div class="col-sm-8">
												<input type="text" class="form-control input-sm" name="nm_prg">
											  </div>
											</div>
											
											<div style="margin-bottom: 5px;" class="form-group">
											  <label class="col-sm-4 control-label">Mulai - Selesai :</label>
											  <div class="col-sm-4">
												<input type="text" class="form-control input-sm prg-tgl" name="tgl_mulai">
											  </div>
											  <div class="col-sm-4">
												<input type="text" class="form-control input-sm prg-tgl" name="tgl_selesai">
											  </div>
											</div>
											<div style="margin-bottom: 5px;" class="form-group">
											  <label class="col-sm-4 control-label">Nilai Program :</label>
											  <div class="col-sm-4">
												<input type="text" class="form-control input-sm nomor text-right" name="prg_val">
											  </div>
											</div>
											<div style="margin-bottom: 5px;" class="form-group">
											  <label class="col-sm-4 control-label">Status :</label>
											  <div class="col-sm-4">
												<select class="form-control" id="prg_stat" tabindex="-1" title="">
													<option value="0">OPEN</option>
													<option value="1">CLOSE</option>																	
												</select>
											  </div>
											</div>
										  </div>
										</form>
									</div>
								</div>
								<div class="modal-footer">
								  <button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">Close</button>
								  <button type="button" class="post-edit-program btn btn-primary btn-embossed">Update</button>
								</div>
							  </div>
							</div>
							</div>

                        </div>
                        <div class="tab-pane fade" id="tab2_7">
                          <div class="row">
							<div class="col-md-12">
								  <div class="panel">
									<div class="panel-header bg-orange">
									  <h3><strong>PROGRAM PARTNER</strong> Data Master</h3>
									  <div class="control-btn">
										<button class="btn-frm-partner btn btn-sm btn-success"><i class="fa fa-plus"></i>Partner</button>
									  </div>
									</div>
									<div class="panel-content p-5">
										<div id="frm_add_partner" class="row" style="display:none">
											<div class="col-md-12">
												<form role="form">
													<h3><strong>ADD</strong> Data Program Partner</h3>
													<div class="row">
														<div class="col-sm-6">
															<div class="row">
																<div class="col-sm-6">	
																	<div class="form-group">
																	  <label class="control-label">Partner Code</label>
																	  <div class="append-icon">
																		<input id="partner_Code" type="text" required="" placeholder="Minimum 3 characters..." minlength="3" class="form-control" name="part-code" aria-required="true">
																		<i class="icon-user"></i>
																	  </div>
																	</div>
																</div>
																<div class="col-sm-6">
																	<div class="form-group">
																	  <label class="control-label">Partner Name</label>
																	  <div class="append-icon">
																		<input id="partner_name" type="text" required="" placeholder="Minimum 3 characters..." minlength="3" class="form-control" name="part-name" aria-required="true">
																		<i class="icon-user"></i>
																	  </div>
																	</div>
																</div>	
																
															</div>
															
														
														<div class="text-center  m-t-0">
														  <button class="btn-add-partner btn btn-embossed btn-primary" type="button">Add</button>
														  <button class="cancel-add-partner btn btn-embossed btn-default m-b-10 m-r-0" type="reset">Cancel</button>
														</div>
													  </div>
													</div>
													
												</form>
											</div>
										</div>
										<div id="lst_user" class="row">
											<div class="col-md-12 p-0">
											  <div class="panel">
												<div class="panel-content">
												  <table class="table table-hover table-bordered f-12">
													<thead>
													  <tr>
														<th>#</th>
			
														<th>Partner Code</th>
														<th>Partner Name</th>
														<th></th>
													  </tr>
													</thead>
													<tbody id="tbl_listpartner">
													</tbody>
												  </table>
												</div>
											  </div>
											</div>
										  </div>
									</div>
								  </div>
								</div>
                          </div>

							<!--EDIT BANK ACC-->
							<div class="modal fade" id="edit-partner" tabindex="-1" role="dialog" aria-hidden="true">
							<div class="modal-dialog">
							  <div class="modal-content">
								<div class="modal-header">
								  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
								  <h4 class="modal-title"><strong>EDIT</strong> DATA PARTNER</h4>
								</div>
								<div class="modal-body">
									<form class=" form-horizontal">
									  <div class="col-md-12">
										<div style="margin-bottom: 5px;" class="form-group">
										  <label class="col-sm-4 control-label">Kode Partner :</label>
										  <div class="col-sm-8">
											<input type="text" class="form-control input-sm" name="kd_part">
										  </div>
										</div>
										<div style="margin-bottom: 5px;" class="form-group">
										  <label class="col-sm-4 control-label">Nama Partner :</label>
										  <div class="col-sm-8">
											<input type="text" class="form-control input-sm" name="nm_part">
										  </div>
										</div>
										<div style="margin-bottom: 5px;" class="form-group">
										  
										</div>
									  </div>
									</form>
								</div>
								<div class="modal-footer">
								  <button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">Close</button>
								  <button type="button" class="post-upd-partner btn btn-primary btn-embossed">Update</button>
								</div>
							  </div>
							</div>
							</div>
                        </div>

                        <div class="tab-pane fade" id="tab2_8">
                          <div class="row">
							<div class="col-md-12">
								  <div class="panel">
									<div class="panel-header bg-orange ">
									  <h3><strong>ALOKASI</strong> PROGRAM</h3>
									  <div class="control-btn">
										<button class="btn-frm-alokasi btn btn-sm btn-success"><i class="fa fa-plus"></i> Alokasi Program</button>
									  </div>
									</div>
									<div class="panel-content p-5">
										<div id="frm_add_alokasi" class="row" style="display:none">
											<div class="col-md-12">
												<form role="form" id="alokasi-frm">
													<h3><strong>ADD</strong> Data Alokasi</h3>
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
															  <label class="control-label">Nama Program</label>
															  <div class="append-icon">
																<select id="nm_program" onchange="javascript: get_sisa_alokasi($(this).val());" class="form-control" data-search="true">
																	<option value="">-- Select --</option>
																	<?php
																		if ($user_session['user_level'] == "PUSAT"){
																			$result = $projects;
																			foreach($result as $row)
																			{
																				echo '<option value="'.$row["C000_SysID"].'">'.$row["C020_Descr"].'</option>';

																			}
																		} else {
																			$result = $projectByOff;	
																			// print_r($result);
																			// die();
																			foreach($result as $row)
																			{
																				echo '<option value="'.$row["C020_ProjectID"].'">'.$row["C020_Descr"].'</option>';

																			}				
																		}
																		
																	
																	?>
																	
																</select>
															  </div>
															</div>
															<div class="form-group">
															  <label class="control-label">Nama Kantor</label>
															  <div class="append-icon">
																<select id="nm_kantor" class="form-control" data-search="true">
																	<option value="">-- Select --</option>
																	<?php
																		// $result = mssql_query("SELECT * FROM [dbo].[T020_Office] WHERE C012_Level = 3 ORDER BY C000_SysID ASC");
																		foreach($offices as $row)
																		{
																			echo '<option value="'.$row["C000_SysID"].'">'.$row["C020_Descr"].'</option>';
																		}
																	
																	?>
																	
																</select>
															  </div>
															</div>
															<div class="form-group">
															  <label class="control-label">Jumlah Alokasi</label>
															  <div class="append-icon">
																<input id="jml_alokasi" type="text" required="" minlength="3" class="form-control nomor" name="firstname" aria-required="true">
																<div class="text">Sisa alokasi : <b id="sisa-alokasi">0</b></div>
																<i class="fa fa-money"></i>
															  </div>
															</div>
															<div class="text-center  m-t-0">
															  <button class="btn-add-alokasi btn btn-embossed btn-primary" type="button">Add</button>
															  <button class="cancel-add-alokasi btn btn-embossed btn-default m-b-10 m-r-0" type="reset">Cancel</button>
															</div>
															
														</div>
														<div class="col-sm-6">
															<div class="form-group m-b-6">
															  <label class="control-label">Keterangan</label>
															  <div class="append-icon">
																<textarea id="ket_alokasi" class="form-control" rows="4" name="comment" aria-required="true"></textarea>
																
															  </div>
															</div>
														</div>
													</div>
													
												</form>
											</div>
										</div>
										<div id="alok_project" class="row">
											<div class="col-md-12 p-0">
											  <div class="panel">
												<div class="panel-content">
												  <table class="table table-hover table-bordered f-12">
													<thead>
													  <tr>
														<th>#</th>
														<th>Kantor</th>
														<th>Nama Program</th>
														<th>Total Alokasi</th>
														<th>Mulai</th>
														<th>Selesai</th>
														<th>Keterangan</th>
														<th></th>
													  </tr>
													</thead>
													<tbody id="tbl_listalokasi">
														
													</tbody>
												  </table>
												</div>
											  </div>
											</div>
										  </div>
									</div>
								  </div>
								</div>
                          </div>
                          <!--EDIT ALOKASI-->
							<div class="modal fade" id="edit-alokasi" tabindex="-1" role="dialog" aria-hidden="true">
							<div class="modal-dialog">
							  <div class="modal-content">
								<div class="modal-header">
								  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
								  <h4 class="modal-title"><strong>EDIT</strong> DATA ALOKASI PROGRAM</h4>
								</div>
								<div class="modal-body">
									<form class=" form-horizontal">
									  <div class="col-md-12">
										<div style="margin-bottom: 5px;" class="form-group">
										  <label class="col-sm-4 control-label">Nama Program :</label>
										  <div class="col-sm-8">
											<input type="text" style="width:100%;background-color: #ecedee;border: 1px solid #ecedee;height:30px;padding: 5px 10px;border-radius: 2px;" class="fp-10 f-12" name="nm_prg" readonly>
										  </div>
										</div>
										<div style="margin-bottom: 5px;" class="form-group">
										  <label class="col-sm-4 control-label">Alokasi :</label>
										  <div class="col-sm-8">
											<input type="text" class="form-control input-sm nomor" name="tot_alokasi">
										  </div>
										</div>
										<div style="margin-bottom: 5px;" class="form-group">
										  <label class="col-sm-4 control-label">Keterangan :</label>
										  <div class="col-sm-8">
											<textarea style="width:100%;background-color: #ecedee;border: 1px solid #ecedee;height:103px" class="p-10 f-12" name="ket_alokasi"></textarea>
										  </div>
										</div>
									  </div>
									</form>
								</div>
								<div class="modal-footer">
								  <button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">Close</button>
								  <button type="button" class="post-edit-alokasi btn btn-primary btn-embossed">Update</button>
								</div>
							  </div>
							</div>
							</div>

                        </div>
						
					  
					  </div>
                    </div>
                  </div>
                </div>

<!-- KANTOR-->


<!--EDIT OFFICE-->



        </div>


