<style type="text/css">
	.select2-dropdown {
	    background-color: white;
	    border: 1px solid #aaa;
	    border-radius: 4px;
	    box-sizing: border-box;
	    display: block;
	    position: absolute;
	    left: -100000px;
	    width: 100%;
	    z-index: 105100000;
	}
	.select2-container{
		width: 100% !important;
	}

	.select2-container--default .select2-selection--single{
		-webkit-box-shadow: none !important;
	    border: 1px solid #ECEDEE !important;
	    box-shadow: none !important;
	    color: #555555;
	    display: inline-block;
	    font-size: 13px;
	    height: auto;
	    line-height: normal;
	    padding: 3px 10px;
	    vertical-align: middle;
	    width: 100%;
	    -webkit-border-radius: 2px;
	    -moz-border-radius: 2px;
	    border-radius: 2px;
	    -webkit-transition: all 0.2s ease-out;
	    -moz-transition: all 0.2s ease-out;
	    -o-transition: all 0.2s ease-out;
	    -ms-transition: all 0.2s ease-out;
	    transition: all 0.2s ease-out;
	}

	.help-block{
	    color:#dd4b39;
	 }
	 
	.error .select2-selection {
	    border: 1px solid #a94442;
	    border-radius: 4px;
	}

</style>
 <h2><strong>Alokasi Program</strong> Data Master</h2>
 <div class="row">
	<div class="col-md-12">
		  <div class="panel">
			<div class="panel-header bg-orange ">
			  <h3><strong>Daftar</strong> Data Alokasi Program</h3>
			  <div class="control-btn">
			  	<div class="btn-group" style="margin-bottom:10px;">
				  <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    <i class="fa fa-gear"></i> Aksi Lainnya<span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu">
				    <li><a href="#" id="import-alokasi">Import Excel</a></li>
					<li><a href="<?=base_url(); ?>assets/download/format_program_allocation.xlsx">Download Format Excel</a></li>
				  </ul>
				</div>
				<button id="add-alokasi" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Alokasi Program</button>
			  </div>
			</div>
			<div class="panel-content p-5">
				<div id="lst_alokasi" class="row">
					<div class="col-md-12 p-0">
					  <div class="panel">
						<div class="panel-content">
						  <table id="table" class="table table-hover table-bordered f-12" cellspacing="0" width="100%">
							<thead>
							  <tr>
								<th>#</th>
								<th>Kantor</th>
								<th>Nama Program</th>
								<th>Total Alokasi</th>
								<th>Keterangan</th>
								<th>aksi</th>
							  </tr>
							</thead>
							<tbody>
								
							</tbody>
						  </table>
						</div>
					  </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Start Modal -->
<div class="modal fade" id="alokasi-modal">
	<div class="modal-dialog">
	  <div class="modal-content">
	  	<div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
		  <h4 class="modal-title"><strong id="title-type"></strong> DATA ALOKASI PROGRAM</h4>
		  <!-- <p>(<label class="required"> </label> ) <label class="control-label">Mandatori / Harus di isi</label>	</p> -->
		</div>
		<div class="modal-body p-t-0 p-b-0">
			<div class="row">
				<!-- (<label class="required"> </label> ) <label class="control-label">Mandatori / Harus di isi</label> -->
				<form class="col-md-12 form-horizontal" id="alokasi-form">
					<div class="col-md-12">
						<input type="hidden" name="alokasi_id">
						<div class="form-group">
						  <label class="col-md-4 control-label required">Nama Program</label>
						  <div class="col-md-8">
						  	<select class="form-control" name="program" required="" onchange="javascript: get_sisa_alokasi($(this).val());">
								<option value="">-- Pilih Nama Program--</option>
								<?php
								if ($user_session['user_level'] == "PUSAT" || $user_session['user_level'] == "ADMIN"){
									$result = $projects;
									foreach($result as $row){
										echo '<option value="'.$row["C000_SysID"].'">'.$row["C020_Descr"].'</option>';
									}
								} else {
									$result = $projectByOff;
									foreach($result as $row){
										echo '<option value="'.$row["C020_ProjectID"].'">'.$row["C020_Descr"].'</option>';
									}				
								} ?>
							</select>
						  </div>
						</div>

						<div class="form-group">
						  <label class="col-md-4 control-label required">Kantor</label>
						  <div class="col-md-8">
						  	<select class="form-control select2" id="get_kantor" name="kantor_id" required="">
                            	<option value="">-- Pilih Kantor --</option>
                            </select>
						  </div>
						</div>

						<div class="form-group">
						  <label class="col-md-4 control-label required">Jumlah Alokasi</label>
						  <div class="col-md-8">
						  	<input type="text" required="" minlength="3" class="form-control nomor text-right" name="alokasi">
							<div class="text">Sisa alokasi : <b id="sisa-alokasi">0</b></div>
						  </div>
						</div>

						<div class="form-group">
						  <label class="col-md-4 control-label">Keterangan</label>
						  <div class="col-md-8">
							<textarea class="form-control" rows="4" name="deskripsi"></textarea>
						  </div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="modal-footer">
		  <button type="button" class="keluar btn btn-default btn-embossed" onclick="cancel_post('alokasi','program alokasi')">Batal</button>
		  <button type="button" class="simpan btn btn-primary btn-embossed" onclick="post_data_alocation()">Simpan</button>
		</div>
	  </div>
	</div>
</div>
<!-- End Modal -->

<!-- Start Modal -->
<div class="modal fade" id="alokasi-import-modal">
	<div class="modal-dialog">
	  <div class="modal-content">
	  	<div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
		  <h4 class="modal-title"><strong>IMPORT</strong> DATA PROGRAM ALOKASI</h4>
		</div>
		<div class="modal-body p-t-0 p-b-0">
			<div class="row">
				<form class="col-md-12 form-horizontal" action="<?=base_url(); ?>program_alocations/upload_excel" method="post" enctype="multipart/form-data">
					<div class="col-md-12">
						<!-- <div class="form-group">
						  <label class="col-md-4 control-label">Judul</label>
						  <div class="col-md-8">
						    	<input type="text" name="judul" placeholder="Minimal 3 Karakter" minlength="3" maxlength="7" class="form-control">
						  </div>
						</div> -->
						<div class="form-group">
						  <label class="col-md-4 control-label">Upload Excel</label>
						  <div class="col-md-8">
						    	<input type="file" name="file" id="import_excel" required="" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
						  </div>
						</div>
					</div>
					<div class="col-md-12">
		  				<input type="submit" class="simpan btn btn-primary btn-embossed  pull-right" value="Upload">
					</div>
					
				</form>
			</div>
		</div>
	  </div>
	</div>
</div>
<!-- End Modal -->