<input type="hidden" id="dt-banks" value='<?= json_encode($banks); ?>' />
<h2><strong>TRANSFER</strong></h2>
<div class="row">
        <div class="col-md-12">
                <div class="panel">
                        <div class="panel-header bg-orange">
                                <h3>Account <strong>Info</strong></h3>
                                <div class="control-btn">
                                        <a onclick="javascript:;" href="#" class="panel-reload"><i class="icon-reload"></i></a>
                                </div>
                        </div>
                        <div class="panel-content">
                                <div class="row">
                                        <div class="col-md-7">
                                                <form class=" form-horizontal">
                                                        <div class="col-md-12">
                                                                <div class="form-group" style="margin-bottom: 5px;">
                                                                        <label class="col-sm-2 control-label">Tanggal</label>
                                                                        <div class="col-sm-10">
                                                                                <input class="form-control input-sm" type="text" value="<?= date("d M Y / h:i:s"); ?>">
                                                                        </div>
                                                                </div>
                                                                <div class="form-group" style="margin-bottom: 5px;">
                                                                        <label class="col-sm-2 control-label">No. Dok</label>
                                                                        <div class="col-sm-10">
                                                                                <input name="C050_DocNum" class="form-control input-sm" type="text" 
                                                                                       value="<?= $user_session['user_level'] . '/' . date('d-y/m') . '/' . str_pad(rand(0, 29), 2, "0", STR_PAD_LEFT); ?>">
                                                                        </div>
                                                                </div>
                                                                <div class="form-group" style="margin-bottom: 5px;">
                                                                        <label class="col-sm-2 control-label">Kantor</label>
                                                                        <div class="col-sm-10">
                                                                                <input class="form-control input-sm" type="text" value="<?= '[' . $user_session['C010_Code'] . '] ' . $user_session['C020_Descr']; ?>">
                                                                        </div>
                                                                </div>
                                                                <div class="form-group" style="margin-bottom: 5px;">
                                                                        <label class="col-sm-2 control-label">Level</label>
                                                                        <div class="col-sm-10">
                                                                                <input class="form-control input-sm" type="text" value="Kantor <?= $user_session['user_level']; ?>">
                                                                        </div>
                                                                </div>
                                                        </div>
                                                </form>
                                        </div>
                                        <div class="col-md-5">
                                                <form class=" form-horizontal">
                                                        <div class="col-md-12">
                                                                <div class="form-group" style="margin-bottom: 5px;">
                                                                        <label class="col-sm-5 control-label">KasBank Awal</label>
                                                                        <div class="col-sm-7">
                                                                                <input class="kas-awal form-control input-sm text-right" type="text" value="<?= $kasbank_awal; ?>" readonly>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group" style="margin-bottom: 5px;">
                                                                        <label class="col-sm-5 control-label">KasBank Mutasi</label>
                                                                        <div class="col-sm-7">
                                                                                <input class="kas-mutasi form-control input-sm text-right" type="text" value="<?= $kasbank_mutasi; ?>" readonly>
                                                                        </div>  
                                                                </div>
                                                                <div class="form-group" style="margin-bottom: 5px;">
                                                                        <label class="col-sm-5 control-label">KasBank Akhir</label>
                                                                        <div class="col-sm-7">
                                                                                <input id="kas-aktual" class="kas-aktual form-control input-sm text-right" type="text" value="<?= $kasbank_awal-$kasbank_mutasi; ?>" readonly>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group" style="margin-bottom: 5px;">
                                                                        <label class="col-sm-5 control-label">In Transfer</label>
                                                                        <div class="col-sm-7">
                                                                                <input class="kas-mit form-control input-sm text-right" type="text" value="<?= $kasbank_intransfer; ?>" readonly>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group" style="margin-bottom: 5px;">
                                                                        <label class="col-sm-5 control-label">KasBank Total Akhir</label>
                                                                        <div class="col-sm-7">
                                                                                <input class="kas-tot form-control input-sm text-right" type="text" value="<?= $kasbank_awal-$kasbank_mutasi+$kasbank_intransfer; ?>" readonly>
                                                                        </div>
                                                                </div>
                                                        </div>
                                                </form>
                                        </div>
                                </div>

                        </div>
                </div>

        </div>
</div>

<div class="row">
        <div class="col-md-12">
                <div class="panel">
                        <div class="panel-header bg-orange">
                                <h3><strong>TYPE OF TRANSFER</strong></h3>
                        </div>
                        <div class="panel-content">
                                <ul class="nav nav-tabs nav-primary">
                                	<?php foreach ($panels as $key => $panel) { ?>
										<li class="<?= ($key == 0 ? 'active' : ''); ?>" style="display:<?= ($key == 2 ? ($header['office_detail']['C012_Level'] == 1 || $header['office_detail']['C012_Level'] == 3 ? '' : 'none') : ''); ?>;" tab-idx="<?= $panel['C000_SysID']; ?>"><a href="#tab2_<?= $panel['C000_SysID']; ?>" data-toggle="tab"><i class="icon-home"></i> <?= $panel['C011_Descr']; ?></a></li>
								  	<?php } ?>
                                        <!-- <li class="active"><a href="#tab2_1" data-toggle="tab"><i class="icon-home"></i> Transaksi Pelanggan</a></li>
                                        <li><a href="#tab2_2" data-toggle="tab"><i class="icon-user"></i> Transaksi Program </a></li>
                                        <li><a href="#tab2_3" data-toggle="tab"><i class="icon-cloud-download"></i> Transfer Masuk <span class="jml-trans-masuk badge badge-warning">0</span></a></li>
                                        <li><a href="#tab2_4" data-toggle="tab"><i class="icon-cloud-download"></i> Transfer Keluar</a></li>
                                        <li><a href="#tab2_5" data-toggle="tab"><i class="icon-cloud-download"></i> Transfer Kas Bank</a></li>
                                        <li><a href="#tab2_6" data-toggle="tab"><i class="icon-cloud-download"></i> Internal Operasional</a></li> -->
                                </ul>
                                <div class="tab-content">
                                        <?php foreach ($tabs AS $tab) { ?>
                                                <?= $tab; ?>
                                        <?php } ?>
                                </div>
                        </div>
                </div>
        </div>
</div>