<script type="text/javascript">
  var columns;
  $(function () {
  columns = [     
    { "data": "i" },
    { "data": "tanggal" },
    { "data": "transaksi" },
    { "data": "nama_kas" },
    { "data": "masuk" },
    { "data": "keluar" },
    { "data": "pendapatan" },
    { "data": "kas" },
    { "data": "utang" }
  ];
    
  var url = "recap_transactions/get_rekap/<?= $panel_code; ?>?typeDate=<?= $data_post['type_date']; ?>&officeId=<?= $data_post['office_id']; ?>&dateNow=<?= $data_post['date_now']; ?>&dateFrom=<?= $data_post['date_from']; ?>&dateTo=<?= $data_post['date_to']; ?>";

  $('[name="table-<?= $panel_code; ?>"]').DataTable({
    "bSort": false,
    "searching": false,
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": base_url+url,
      "type": "GET",
      "dataSrc" : function (json) {
        var data = json['data'];
        var j = json['start'];

        for(var i = 0; i < data.length; i ++){
              var item = data[i];
              
              if(item['MLedgerID'] == 2){
                      item['nama_kas'] = item['DescrKasBank'];
              }else{
                      item['nama_kas'] = item['BankName']+' - '+item['BankAccount'];
              }

              item['tanggal'] = item['DocDate'];
              item['transaksi'] = item['DescrTransaction'];
              var masuk = Number(item['Amount1']);
              var kas = Number(item['Amount2']);
              var keluar = Number(item['Amount1']) - kas;
              var utang = Number(item['Amount3']);
              var pendapatan = kas - utang;

              item['masuk'] = change_format_number(masuk);
              item['kas'] = change_format_number(kas);
              item['keluar'] = change_format_number(keluar);
              item['utang'] = change_format_number(utang);
              item['pendapatan'] = change_format_number(pendapatan);
              item['i'] = ++j;
              
              data[i] = item;
        }
        
        return data;
      },
    },
    "columns": columns,
  });

  });
</script>
<div class="col-md-12">
  <div class="panel">
    <div class="panel-header bg-orange">
      <h3>TRANSAKSI <strong>PELANGGAN</strong></h3>
    </div>
    <div class="panel-content">
      <div class="row">	
    		<table name="table-<?= $panel_code; ?>" class="col-md-12 table f-12  table-bordered table-hover">
    			<thead>
            <tr>
                <th width="8%">#</th>
                <th>Tanggal</th>  
                <th>Transaksi</th>  
                <th>Kas/Bank</th>
                <th>Masuk</th>
                <th>Keluar</th>
                <th>Pendapatan</th>
                <th>Kas</th>
                <th>Utang</th>
                <!-- <th>Status</th>
                <th>Action</th> -->
            </tr>
          </thead>
          <tbody id="tbodyPlg">
                                                     
          </tbody>
    		</table>
	  </div>
    </div>
  </div>
</div>