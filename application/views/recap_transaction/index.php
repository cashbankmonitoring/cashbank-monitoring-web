<h2><strong>REKAP TRANSAKSI</strong></h2>

<div class="row">
    <div class="col-md-12">
      <div class="panel">
        <div class="panel-header bg-orange">
          <h3><strong>Filter</strong></h3>
        </div>
        <div class="panel-content">
			<div class="row">
			<form method="POST" action="<?=base_url()."recap_transactions"?>" id="form-rekap" novalidate>
			  <div class="col-md-6">
				<div class="form-group">
				  <label class="col-md-4 control-label" style="padding:0px;">Tanggal Transaksi :</label>
				  <div class="col-md-8">
					<input type="radio" name="select_date" value="1" style="margin:5px;" id="today" <?= ($type_data != '' ? ($data_post['type_date'] == 1 ? 'checked' : 'checked') : 'checked' ) ?>> Per Hari <br>
					<input type="radio" name="select_date" value="2" style="margin:5px;" id="date_range" <?= ($type_data != '' ? ($data_post['type_date'] == 2 ? 'checked' : '') : '' ) ?>> Rentang Tanggal
				  </div>
				  <div class="col-md-offset-4 col-md-8" id="per_date" style="display:<?= ($type_data != '' ? ($data_post['type_date'] == 1 ? '' : 'none') : ''); ?>">
					<input type="text" name="date" class="date-picker form-control input-md" value="<?= ($type_data != '' ? $data_post['date_now'] : '' ) ?>" required>
				  </div>
				  <div class="col-md-offset-4 col-md-8 no-padding" id="per_date_range" style="display:<?= ($data_post['type_date'] == 2 ? '' : 'none') ?>; padding:0 !important;">
					  <div class="col-md-5">
						<input type="text" name="date_from" class="form-control input-md" id="from" value="<?= ($type_data != '' ? $data_post['date_from'] : '' ) ?>" required>
					  </div>
					  <div class="col-md-2">
				  		<label class="control-label" style="margin-top:5px;">Ke</label>
					  </div>
					  <div class="col-md-5">
						<input type="text" name="date_to" class="form-control input-md" id="to" value="<?= ($type_data != '' ? $data_post['date_to'] : '' ) ?>" required>
					  </div>	
				  </div>
				</div>
			  </div>

			  <div class="col-md-6">
				<div class="form-group">
				  <label class="col-md-3 control-label">Office :</label>
				  <div class="col-md-9">
					<select id="User_Sel_KPC" name="office_id" class="form-control">
						<!-- <option value="">ALL</option> -->
						<option value="<?= $user['C002_OfficeID']; ?>"><?= $user['C020_Descr']; ?></option>
						<?php foreach ($offices as $key => $office) { ?>
							<option value="<?= $office['C000_SysID']; ?>" <?= ($type_data != '' ? ($office['C000_SysID'] == $data_post['office_id'] ? 'selected' : '' ) : '' ) ?> ><?= $office['C020_Descr']; ?></option>
						<?php } ?>
					</select>
				  </div>
				</div>
			  </div>
			  <div class="col-md-12">
				<input type="submit" class="view-trc btn btn-primary btn-embossed" value="Submit">
			  </div>
			 </form>
			</div>
        </div>
      </div>
    </div>
</div>
<div class="row">
	<?php foreach ($tabs AS $tab) { ?>
	        <?= $tab; ?>
	<?php } ?>
</div>