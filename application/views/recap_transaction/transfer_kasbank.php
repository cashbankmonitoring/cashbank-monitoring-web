<script type="text/javascript">
  var columns;
  $(function () {
  columns = [     
    { "data": "i" },
    { "data": "tanggal" },
    { "data": "asal" },
    { "data": "tujuan" },
    { "data": "jumlah" },
    { "data": "keterangan" }
  ];
    
  var url = "recap_transactions/get_rekap/<?= $panel_code; ?>?typeDate=<?= $data_post['type_date']; ?>&officeId=<?= $data_post['office_id']; ?>&dateNow=<?= $data_post['date_now']; ?>&dateFrom=<?= $data_post['date_from']; ?>&dateTo=<?= $data_post['date_to']; ?>";

  $('[name="table-<?= $panel_code; ?>"]').DataTable({
    "bSort": false,
    "searching": false,
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": base_url+url,
      "type": "GET",
      "dataSrc" : function (json) {
        var data = json['data'];
        var j = json['start'];

        for(var i = 0; i < data.length; i ++){
              var item = data[i];
              
              item['tanggal'] = item['DocDate'];

              if(item['MLedgerID'] == 2){
                  item['asal'] = item['DescrKasBank'];
                }else{
                  item['asal'] = item['BankName']+' - '+item['BankAccount'];
                }
                
                if(item['MLedgerIDTo'] == 2){
                  item['tujuan'] = item['DescrKasBankTo'];
                }else{
                  item['tujuan'] = item['BankNameTo']+' - '+item['BankAccountTo'];
                }

              item['jumlah'] = change_format_number(item['Amount1']);
              item['keterangan'] = item['Description'];

              item['i'] = ++j;
              
              data[i] = item;
        }
        
        return data;
      },
    },
    "columns": columns,
  });

  });
</script>
<div class="col-md-12">
  <div class="panel">
    <div class="panel-header bg-orange">
      <h3>TRANSAKSI <strong>KasBank</strong></h3>
    </div>
    <div class="panel-content">
      <div class="row"> 
        <table name="table-<?= $panel_code; ?>" class="col-md-12 table f-12  table-bordered table-hover">
          <thead>
            <tr>
                <th width="8%">#</th>
                <th>Tanggal</th>  
                <th>Asal</th>  
                <th>Tujuan</th>  
                <th>Jumlah</th>
                <th>Keterangan</th>
            </tr>
          </thead>
          <tbody id="tbodyKasBank">
                                                     
          </tbody>
        </table>
    </div>
    </div>
  </div>
</div>