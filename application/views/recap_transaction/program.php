<script type="text/javascript">
  var columns;
  $(function () {
  columns = [     
    { "data": "i" },
    { "data": "tanggal" },
    { "data": "program_name" },
    { "data": "penyaluran" },
    { "data": "keterangan" }
  ];
    
  var url = "recap_transactions/get_rekap/<?= $panel_code; ?>?typeDate=<?= $data_post['type_date']; ?>&officeId=<?= $data_post['office_id']; ?>&dateNow=<?= $data_post['date_now']; ?>&dateFrom=<?= $data_post['date_from']; ?>&dateTo=<?= $data_post['date_to']; ?>";

  $('[name="table-<?= $panel_code; ?>"]').DataTable({
    "bSort": false,
    "searching": false,
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": base_url+url,
      "type": "GET",
      "dataSrc" : function (json) {
        var data = json['data'];
        var j = json['start'];

        for(var i = 0; i < data.length; i ++){
              var item = data[i];
              
              item['tanggal'] = item['DocDate'];
              item['penyaluran'] = change_format_number(item['Amount1']);
              item['program_name'] = item['SubLedger3IDTo'];
              item['keterangan'] = item['DescriptionTo'];

              item['i'] = ++j;
              
              data[i] = item;
        }
        
        return data;
      },
    },
    "columns": columns,
  });

  });
</script>
<div class="col-md-12">
  <div class="panel">
    <div class="panel-header bg-orange">
      <h3>TRANSAKSI <strong>PROGRAM</strong></h3>
    </div>
    <div class="panel-content">
      <div class="row"> 
        <table name="table-<?= $panel_code; ?>" class="col-md-12 table f-12  table-bordered table-hover">
          <thead>
            <tr>
                <th width="8%">#</th>
                <th>Tanggal</th>  
                <th>Program</th>  
                <th>Total Penyaluran</th>
                <th>Keterangan</th>
            </tr>
          </thead>
          <tbody id="tbodyPrg">
                                                     
          </tbody>
        </table>
    </div>
    </div>
  </div>
</div>