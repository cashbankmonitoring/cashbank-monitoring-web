<script type="text/javascript">
  var columns;
  $(function () {
  columns = [     
    { "data": "i" },
    { "data": "tanggal" },
    { "data": "kantor" },
    { "data": "asal" },
    { "data": "tujuan" },
    { "data": "jumlah" },
    { "data": "status_transfer" },
    { "data": "status" }
  ];
    
  var url = "recap_transactions/get_rekap/<?= $panel_code; ?>?typeDate=<?= $data_post['type_date']; ?>&officeId=<?= $data_post['office_id']; ?>&dateNow=<?= $data_post['date_now']; ?>&dateFrom=<?= $data_post['date_from']; ?>&dateTo=<?= $data_post['date_to']; ?>";

  $('[name="table-<?= $panel_code; ?>"]').DataTable({
    "bSort": false,
    "searching": false,
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": base_url+url,
      "type": "GET",
      "dataSrc" : function (json) {
        var data = json['data'];
        var j = json['start'];

        for(var i = 0; i < data.length; i ++){
              var item = data[i];

              item['tanggal'] = item['DocDate'];
              if(<?= $data_post['office_id']; ?> != item['SLedgerID']){
                item['kantor'] = item['DescrKantor'];
                
                if(item['MLedgerID'] == 2){
                  item['asal'] = item['DescrKasBank'];
                }else{
                  item['asal'] = item['BankName']+' - '+item['BankAccount'];
                }
                
                if(item['MLedgerIDTo'] == 2){
                  item['tujuan'] = item['DescrKasBankTo'];
                }else{
                  item['tujuan'] = item['BankNameTo']+' - '+item['BankAccountTo'];
                }

                item['jumlah'] = change_format_number(item['Amount1']);
                item['status'] = 'Transfer Masuk';
              }else{
                item['kantor'] = item['DescrKantorTo'];
                
                if(item['MLedgerIDTo'] == 2){
                  item['asal'] = item['DescrKasBankTo'];
                }else{
                  item['asal'] = item['BankNameTo']+' - '+item['BankAccountTo'];
                }
                
                if(item['MLedgerID'] == 2){
                  item['tujuan'] = item['DescrKasBank'];
                }else{
                  item['tujuan'] = item['BankName']+' - '+item['BankAccount'];
                }

                item['jumlah'] = change_format_number(item['Amount1']);
                item['status'] = 'Transfer Keluar';
              }

              if(item['TransferStatus'] == 0){
                item['status_transfer'] = 'Money in Transfer'; 
              }else if(item['TransferStatus'] == 1){
                item['status_transfer'] = 'Diterima'; 
              }else{
                item['status_transfer'] = 'Ditolak'; 

              }

              item['i'] = ++j;
              
              data[i] = item;
        }
        
        return data;
      },
    },
    "columns": columns,
  });

  });
</script>
<div class="col-md-12">
  <div class="panel">
    <div class="panel-header bg-orange">
      <h3>TRANSFER <strong>INTERNAL</strong></h3>
    </div>
    <div class="panel-content">
      <div class="row"> 
        <table name="table-<?= $panel_code; ?>" class="col-md-12 table f-12  table-bordered table-hover">
          <thead>
            <tr>
                <th width="8%">#</th>
                <th>Tanggal</th>  
                <th>Kantor</th>  
                <th>Asal</th>
                <th>Tujuan</th>
                <th>Jumlah</th>
                <th>Status Transfer</th>
                <th>Status</th>
            </tr>
          </thead>
          <tbody id="tbodyTrfInt">
                                                     
          </tbody>
        </table>
    </div>
    </div>
  </div>
</div>