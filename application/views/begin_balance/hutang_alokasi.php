<div class="tab-pane fade " id="tab2_4">
<form method="POST" action="<?=base_url()."begin_balances/save/4"?>" id="form-hutang-alokasi">      
	<div class="row">
		<table class="table">
			<thead>
			  <tr>
				<th width="200">Kantor</th>
				<th>Nilai Awal</th>
				<th>Aktual</th>
				<th>Adjustment</th>
			  </tr>
			</thead>
			<tbody id="list_hutang_pusat">
				<input type="hidden" name="panel_id" value="4" >
				<input type="hidden" name="count_allocation" value="<?= count($allocations); ?>" >
				<?php foreach ($allocations as $key => $allocation) { ?>
						<input type="hidden" name="allocation_id[<?= $key ?>]" value="<?= $allocation["C000_SysID"]; ?>" >
						<tr class="bank">
							<td>
								<div>
									<strong><?= $allocation["C020_Descr"]; ?></strong>
								</div>
							</td>
							<td>
								<input type="text" name="system_hutangalk_1[<?= $key ?>]" value="<?= ($allocation['rekap'] != null ? $allocation['rekap']['Amount'] : 0); ?>" class="form-control input-sm nomor text-right" readonly>
							</td>
							<td>
								<input type="text" name="aktual_hutangalk_1[<?= $key ?>]" value="0" class" class="form-control input-sm nomor text-right" onkeyup="sum(<?= $key ?>, 1, 'hutangalk', false);" <?= ($read_only == 1 ? "readonly" : ""); ?>>
							</td>
							<td>
								<input type="text" name="adjustment_hutangalk_1[<?= $key ?>]" value="0" class="form-control input-sm nomor text-right" readonly>
							</td>
						</tr>
						<?php 
					}
				?>
			</tbody>
		  </table>
	</div>
	<div class="row">
		<div class="col-md-12">
		  <div class="panel">
			<div class="panel-content">
				<input type="submit" class="btn-up-bb-kas btn btn-embossed btn-primary" value="Update">
				<a href="#" class="btn btn-embossed btn-default" onclick="location.reload();">Cancel</a>
				<input type="reset" class="btn btn-embossed btn-success" value="Clear" onclick="header_submit('reset');">
			  
			</div>
		  </div>
		</div>
	</div>
</form>
</div>