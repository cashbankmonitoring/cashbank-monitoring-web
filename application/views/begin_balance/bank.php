<div class="tab-pane fade active in" id="tab2_1">
<form method="POST" action="<?=base_url()."begin_balances/save/1"?>" id="form-bank">      
  <div class="row">
	<table class="table">
		<thead>
		  <tr>
			<th width="200">BANK</th>
			<th>Nilai Awal</th>
			<th>Aktual</th>
			<th>Adjustment</th>
		  </tr>
		</thead>
		<tbody id="list_rek_bank">
			<input type="hidden" name="panel_id" value="1" >
			<input type="hidden" name="count_bank" value="<?= count($banks); ?>" >
			<?php
				foreach ($banks as $key => $bank) { ?>
				<input type="hidden" name="bank_id[<?= $key ?>]" value="<?= $bank["C000_SysID"]; ?>" >
					<tr class="bank">
						<td>
							<div>
								<strong><?= $bank["C020_Name"]; ?></strong> <br>
								<span> <?= $bank["C010_BankAccNumber"]; ?> </span>
							</div>
						</td>
						<td>
							<input type="text" name="system_bank_total[<?= $key ?>]" value="<?= $bank['total_amount'] ?>" class="form-control input-sm nomor text-right" readonly>
						</td>
						<td>
							<input type="text" name="aktual_bank_total[<?= $key ?>]" value="0" class" class="form-control input-sm nomor text-right" readonly>
						</td>
						<td>
							<input type="text" name="adjustment_bank_total[<?= $key ?>]" value="0" class="form-control input-sm nomor text-right" readonly>
						</td>
					</tr>
					<?php foreach ($bank["virtual_account"] as $virtual_account) { ?>
						<tr class="sub-bank">
							<td>
								<div class="sub-bank-label"><?= $virtual_account['C011_Descr']; ?></div>
							</td>
							<td>
								<input type="text" name="system_bank_<?= $virtual_account['C000_SysID']; ?>[<?= $key ?>]" value="<?= ($virtual_account['rekap'] != null ? $virtual_account['rekap']['Amount'] : 0); ?>" class="form-control input-sm nomor text-right" readonly>
							</td>
							<td>
								<input type="text" name="aktual_bank_<?= $virtual_account['C000_SysID']; ?>[<?= $key ?>]" value="0" class" class="form-control input-sm nomor text-right" id="aktual_operasional[<?= $key ?>]" onkeyup="sum(<?= $key ?>, <?= $virtual_account['C000_SysID']; ?>, 'bank', true);"  <?= ($read_only == 1 ? "readonly" : ""); ?>>
							</td>
							<td>
								<input type="text" name="adjustment_bank_<?= $virtual_account['C000_SysID']; ?>[<?= $key ?>]" id="adjustment_operasional[<?= $key ?>]" value="0" class="form-control input-sm nomor text-right" readonly>
							</td>
						</tr>
					<?php }
				}
			?> 
		</tbody>
	  </table>
  </div>

  <div class="row">
	<div class="col-md-12">
	  <div class="panel">
		<div class="panel-content">
			<input type="submit" class="btn-up-bb-bank btn btn-embossed btn-primary" value="Update">
			<a href="#" class="btn btn-embossed btn-default" onclick="location.reload();">Cancel</a>
			<input type="reset" class="btn btn-embossed btn-success" value="Clear" onclick="header_submit('reset');">
		  
		</div>
	  </div>
	</div>
  </div>
</form>
</div>