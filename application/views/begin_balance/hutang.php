<div class="tab-pane fade " id="tab2_3">
<form method="POST" action="<?=base_url()."begin_balances/save/3"?>" id="form-hutang">      
	<div class="row">
		<table class="table">
			<thead>
			  <tr>
				<th width="200">Kantor</th>
				<th>Nilai Awal</th>
				<th>Aktual</th>
				<th>Adjustment</th>
			  </tr>
			</thead>
			<tbody id="list_hutang_pusat">
				<input type="hidden" name="panel_id" value="3" >
				<input type="hidden" name="count_debts" value="<?= count($debts); ?>" >
				<?php foreach ($debts as $key => $debt) { ?>
					<input type="hidden" name="mlegder[<?= $key ?>]" value="<?= $debt["SysID"]; ?>" >
						<tr class="bank">
							<td>
								<div>
									<strong><?= $debt["Descr"]; ?></strong>
								</div>
							</td>
							<td>
								<input type="text" name="system_hutang_1[<?= $key ?>]" value="<?= ($debt['rekap'] != null ? $debt['rekap']['Amount'] : 0); ?>" class="form-control input-sm nomor text-right" readonly>
							</td>
							<td>
								<input type="text" name="aktual_hutang_1[<?= $key ?>]" class="form-control input-sm nomor text-right" onkeyup="sum(<?= $key ?>, 1, 'hutang', false);" <?= ($read_only == 1 ? "readonly" : ""); ?>>
							</td>
							<td>
								<input type="text" name="adjustment_hutang_1[<?= $key ?>]" value="0" class="form-control input-sm nomor text-right" readonly>
							</td>
						</tr>
						<?php 
					}
				?> 
			</tbody>
		  </table>
	</div>
	<div class="row">
		<div class="col-md-12">
		  <div class="panel">
			<div class="panel-content">
				<input type="submit" class="btn-up-bb-kas btn btn-embossed btn-primary" value="Update">
				<a href="#" class="btn btn-embossed btn-default" onclick="location.reload();">Cancel</a>
				<input type="reset" class="btn btn-embossed btn-success" value="Clear" onclick="header_submit('reset');">
			  
			</div>
		  </div>
		</div>
	</div>
</form>
</div>