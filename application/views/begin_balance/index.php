<style type="text/css">
	/*body { margin: 0; }*/

	.fixed {
	  position: fixed;
	  top: 50px;
	  width: 100%; 
	  z-index: 100;
	}

	.fixed .control-btn{
		position: fixed !important;
    	top: 64px !important;
	}

	.bank td{
		border-bottom: 1px solid #ddd !important;
	}

	.bank span{
		font-size: 12px;
	}

	.sub-bank .sub-bank-label{
		font-size: 13px;
		padding-left: 35px;
	}

	.sub-bank td{
		border-top: none !important;
	}

	.no-padding{
		padding: 0px !important;
	}
</style>
<h2>BEGIN <strong>BALANCE / ADJUSTMENT</strong></h2>
<div class="row">
    <div class="col-md-6">
      <div class="panel">
        <div class="panel-header bg-orange">
          <h3>Begin Balance</h3>
        </div>
        <div class="panel-content">
          <div class="row">
              <div class="col-md-12 no-padding">
              	<div class="col-md-12">
              		<div class="form-group" style="margin-bottom: 5px;">
                      <label class="col-md-2 control-label">Tanggal</label>
                      <div class="col-md-10">
                      	: <?= $header['DocDate']; ?>
                      </div>
                    </div>	
              	</div>
              	<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
                      <label class="col-md-2 control-label">No. Dok</label>
                      <div class="col-md-10">
                      	: <?= $header['DocNum']; ?>
                      </div>
                    </div>
                </div>
                <div class="col-md-12">
                	<div class="form-group" style="margin-bottom: 5px;">
                      <label class="col-md-2 control-label">Kantor</label>
                      <div class="col-md-10">
                      	: [<?= $office['Code']; ?>] <?= $office['Descr']; ?>
                      </div>
                    </div>
                </div>
                <div class="col-md-12">
                	<div class="form-group" style="margin-bottom: 5px;">
                      <label class="col-md-2 control-label">Level</label>
					  <div class="col-md-10">
					  	: <?= $office['Level']; ?>
                      </div>
                    </div>
                </div>
              </div>
          </div>
          
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="panel">
        <div class="panel-header bg-orange">
          <h3><strong>Remark</strong></h3>
        </div>
        <div class="panel-content">
          <textarea name="Remark" class="form-control" style="height:100px"></textarea>
          
        </div>
      </div>
    </div>
</div>
<div class="row">
	<div class="col-md-12">
	  <div class="panel section">
		<div class="panel-header bg-orange sticky">
		  <h3><strong>OPERATION</strong></h3>
		  <div class="control-btn">
			<input type="submit" class="btn btn-sm btn-primary" value="Update" onclick="header_submit('submit');">
			<button class="btn btn-sm btn-default" onclick="location.reload();">Cancel</button>
			<input type="reset" class="btn btn-sm btn-success" value="Clear" onclick="header_submit('reset');">
		  </div>
		</div>
		<!-- <section>
		  <div class="sticky">This div will stick to the top</div>
		</section> -->
		<div class="panel-content">
		  <ul class="nav nav-tabs nav-primary">
		  	<?php foreach ($panels as $key => $panel) { ?>
				<li class="<?= ($key == 0 ? 'active' : ''); ?>" tab-idx="<?= $key+1; ?>"><a href="#tab2_<?= $key+1; ?>" data-toggle="tab"><i class="icon-home"></i> <?= $panel['C011_Descr']; ?></a></li>
		  	<?php } ?>
		  </ul>
		  <div class="tab-content">
		  	<?php foreach ($tabs AS $tab) { ?>
                <?= $tab; ?>
            <?php } ?>
		  </div>
		</div>
	  </div>
	</div>
</div>
<!-- </form> -->