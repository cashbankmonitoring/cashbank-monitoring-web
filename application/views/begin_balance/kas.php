<div class="tab-pane fade" id="tab2_2">
<form method="POST" action="<?=base_url()."begin_balances/save/2"?>" id="form-kas">      
  <div class="row">
	<table class="table">
		<thead>
		  <tr>
			<th>Purpose</th>
			<th>Nilai Awal</th>
			<th>Aktual</th>
			<th>Adjustment</th>
		  </tr>
		</thead>
		<tbody id="list_kas">
			<tr class="bank">
				<input type="hidden" name="panel_id" value="2" >
				<td>
					<div>
						<strong>KAS</strong>
					</div>
				</td>
				<td>
					<input type="text" name="system_kas_total[0]" value="<?= $kas[0]['total_amount'] ?>" class="form-control input-sm nomor text-right" readonly>
				</td>
				<td>
					<input type="text" name="aktual_kas_total[0]" value="0" class" class="form-control input-sm nomor text-right" readonly>
				</td>
				<td>
					<input type="text" name="adjustment_kas_total[0]" value="0" class="form-control input-sm nomor text-right" readonly>
				</td>
			</tr>
			<?php foreach ($kas[0]["virtual_account"] as $virtual_account) { ?>
				<tr class="sub-bank">
					<td>
						<div class="sub-bank-label"><?= $virtual_account['C011_Descr']; ?></div>
					</td>
					<td>
						<input type="text" name="system_kas_<?= $virtual_account['C000_SysID']; ?>[0]" value="<?= ($virtual_account['rekap'] != null ? $virtual_account['rekap']['Amount'] : 0); ?>" class="form-control input-sm nomor text-right" readonly>
					</td>
					<td>
						<input type="text" name="aktual_kas_<?= $virtual_account['C000_SysID']; ?>[0]" value="0" class" class="form-control input-sm nomor text-right" onkeyup="sum(0, <?= $virtual_account['C000_SysID']; ?>, 'kas', true);" <?= ($read_only == 1 ? "readonly" : ""); ?>>
					</td>
					<td>
						<input type="text" name="adjustment_kas_<?= $virtual_account['C000_SysID']; ?>[0]" value="0" class="form-control input-sm nomor text-right" readonly>
					</td>
				</tr>
			<?php } ?>

		</tbody>
	  </table>
  </div>
  <div class="row">
	<div class="col-md-12">
	  <div class="panel">
		<div class="panel-content">
			<input type="submit" class="btn-up-bb-kas btn btn-embossed btn-primary" value="Update">
			<a href="#" class="btn btn-embossed btn-default" onclick="location.reload();">Cancel</a>
			<input type="reset" class="btn btn-embossed btn-success" value="Clear" onclick="header_submit('reset');">
		</div>
	  </div>
	</div>
  </div>
</form>
</div>