<script type="text/javascript">
$(document).ready(function(){
    setTimeout(function() {
        $('#pass-error').delay(2000).fadeOut();
    }, 500);
});
    
</script>
<!-- BEGIN LOGIN BOX -->
        <div class="container" id="login-block">
            <i class="user-img icons-faces-users-03"></i>
            <div class="account-info" style="background:#c9c9c9">
                <a href="<?= base_url(); ?>" class="logo m-0" style="height:123px"></a> 
                <h3 style="text-align: center;color: black;">CashBank Monitoring</h3>
                <ul class="hide">
                    <li><i class="icon-magic-wand"></i> Fully customizable</li>
                    <li><i class="icon-layers"></i> Various sibebars look</li>
                    <li><i class="icon-arrow-right"></i> RTL direction support</li>
                    <li><i class="icon-drop"></i> Colors options</li>
                </ul>
            </div>
            
            <div class="account-form">
                <a href="<?= base_url(); ?>" class="logo m-0" style="height:123px"></a>
				<h3 style="text-align: center;color: black;" class="jdl m-t-5">CashBank Monitoring</h3>
				<form id="frm_login" class="form-signin form-validation" role="form" action="<?= base_url()."site/validate/" ?>" method="POST" novalidate="novalidate">
                    <h3><strong>Sign in</strong> to your account</h3>
                    <div class="append-icon">
                        <input type="text" name="username" id="username" class="form-control form-white username" placeholder="Username" required="" aria-required="true">
                        <i class="icon-user"></i>
                    </div>
                    <div class="append-icon m-b-20">
                        <input type="password" name="password" class="form-control form-white password" placeholder="Password" required="" aria-required="true">
                        <?php if($flashdata != NULL){ ?>
                            <div id="pass-error" class="form-error" >Kesalahan username atau password...</div>
                        <?php } ?>
                        <i class="icon-lock"></i>
                    </div>
                    
					<button type="button" id="submit-form" name="submit" class="btn btn-lg btn-dark btn-rounded ladda-button" data-style="expand-left" style="display:none;">Sign In</button>
                    <input type="submit" id="submit-form" value="Sign In" name="submit" class="btn btn-lg btn-dark btn-rounded ladda-button" data-style="expand-left" />
                    
					<span class="forgot-password"><a id="password" href="account-forgot-password.html">Forgot password?</a></span>
                    <div class="form-footer hide">
                        <div class="clearfix">
                            <p class="new-here"><a href="user-signup.php">New here? Sign up</a></p>
                        </div>
                    </div>
                </form>
                <form class="form-password" role="form">
                    <h3><strong>Reset</strong> your password</h3>
                    <div class="append-icon m-b-20">
                        <input type="email" name="email-reset" class="form-control form-white email" placeholder="E-mail" required>
                        <div id="email-salah" class="form-error" style="display:none">Please enter a valid email address.</div>
						<i class="icon-envelope"></i>
                    </div>
                    <button type="button" id="submit-reset-password" class="btn btn-lg btn-danger btn-block ladda-button" data-style="expand-left">Send Password Reset Link</button>
                    <div class="clearfix m-t-10">
                        <p class="pull-left m-t-20 m-b-0"><a id="login" href="#">Have an account? Sign In</a></p>
                        
                    </div>
                </form>
                <div style="font-size: 12px; margin-top: 20px;">
                    Untuk kenyamanan menggunakan aplikasi ini, silahkan anda mendownload browser <a href="https://www.google.com/chrome/browser/desktop/" target="_blank">Chrome</a>
                </div>
            </div>
            <p class="account-copyright">
	            <span>Copyright © 2016 </span><span>Web Master</span>.<span>All rights reserved.</span>
	        </p>
        </div>
        <!-- END LOCKSCREEN BOX -->
        
		
		<!--SET NEW PASSWORD-->
		<div class="modal fade" id="new-pass" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" style="width: 350px;">
		  <div class="modal-content">
			<div class="modal-header p-b-10">
			  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
			  <h4 class="modal-title"><strong>ENTER</strong> NEW PASSWORD</h4>
			  <p class="small m-b-0">Anda harus merubah password dari default..!</p>
			</div>
			<div class="modal-body p-0">
				
				<form class=" form-horizontal">
				  <div class="col-md-12">
					<div style="margin-bottom: 5px;" class="form-group">
					  <label class="col-sm-4 control-label">Password :</label>
					  <div class="col-sm-8">
						<input type="password" class="form-control input-sm" name="new_pass">
					  </div>
					</div>
					<div style="margin-bottom: 5px;" class="form-group">
					  <label class="col-sm-4 control-label">Konfirmasi :</label>
					  <div class="col-sm-8">
						<input type="password" class="form-control input-sm" name="konf_new_pass">
					  </div>
					</div>

				  </div>
				</form>
			</div>
			<div class="modal-footer">
			  <button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">Close</button>
			  <button type="button" class="post-new-pass btn btn-primary btn-embossed">Update</button>
			</div>
		  </div>
		</div>
		</div>