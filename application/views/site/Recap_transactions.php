        <h2><strong>REKAP TRANSAKSI</strong></h2>
        
		<div class="row">
            <div class="col-md-12">
              <div class="panel">
                <div class="panel-header bg-orange">
                  <h3><strong>Filter</strong></h3>
                </div>
                <div class="panel-content">
					<div class="row">
					<form method="POST" action="<?=base_url()."recap_transactions"?>" id="form-rekap" novalidate>
					  <div class="col-md-6">
						<div class="form-group">
						  <label class="col-md-4 control-label" style="padding:0px;">Tanggal Transaksi :</label>
						  <div class="col-md-8">
							<input type="radio" name="select_date" value="1" style="margin:5px;" id="today" <?= ($type_data != '' ? ($data_post['type_date'] == 1 ? 'checked' : 'checked') : 'checked' ) ?>> Per Hari <br>
							<input type="radio" name="select_date" value="2" style="margin:5px;" id="date_range" <?= ($type_data != '' ? ($data_post['type_date'] == 2 ? 'checked' : '') : '' ) ?>> Rentang Tanggal
						  </div>
						  <div class="col-md-offset-4 col-md-8" id="per_date" style="display:<?= ($type_data != '' ? ($data_post['type_date'] == 1 ? '' : 'none') : ''); ?>">
							<input type="text" name="date" class="date-picker form-control input-md" value="<?= ($type_data != '' ? $data_post['date_now'] : '' ) ?>" required>
						  </div>
						  <div class="col-md-offset-4 col-md-8 no-padding" id="per_date_range" style="display:<?= ($data_post['type_date'] == 2 ? '' : 'none') ?>; padding:0 !important;">
							  <div class="col-md-5">
								<input type="text" name="date_from" class="form-control input-md" id="from" value="<?= ($type_data != '' ? $data_post['date_from'] : '' ) ?>" required>
							  </div>
							  <div class="col-md-2">
						  		<label class="control-label" style="margin-top:5px;">Ke</label>
							  </div>
							  <div class="col-md-5">
								<input type="text" name="date_to" class="form-control input-md" id="to" value="<?= ($type_data != '' ? $data_post['date_to'] : '' ) ?>" required>
							  </div>	
						  </div>
						</div>
					  </div>

					  <div class="col-md-6">
						<div class="form-group">
						  <label class="col-md-3 control-label">Office :</label>
						  <div class="col-md-9">
							<select id="User_Sel_KPC" name="office_id" class="form-control">
								<!-- <option value="">ALL</option> -->
								<option value="<?= $user['C002_OfficeID']; ?>"><?= $user['C020_Descr']; ?></option>
								<?php foreach ($offices as $key => $office) { ?>
									<option value="<?= $office['C000_SysID']; ?>" <?= ($type_data != '' ? ($office['C000_SysID'] == $data_post['office_id'] ? 'selected' : '' ) : '' ) ?> ><?= $office['C020_Descr']; ?></option>
								<?php } ?>
							</select>
						  </div>
						</div>
					  </div>
					  <div class="col-md-12">
						<input type="submit" class="view-trc btn btn-primary btn-embossed" value="Submit">
					  </div>
					 </form>
					</div>
                </div>
              </div>
            </div>
        </div>
		<div class="row">
            <div class="col-md-12">
              <div class="panel">
                <div class="panel-header bg-orange">
                  <h3>TRANSAKSI <strong>PELANGGAN</strong></h3>
                </div>
                <div class="panel-content">
                  <div class="row">
							
					<table class="table f-12 table-hover">
						<thead>
						  <tr>
							<th>#</th>
							<th>TANGGAL</th>
							<th>JENIS TRANSAKSI</th>
							<th>MASUK</th>
							<th>KELUAR</th>
							<th>PENDAPATAN</th>
							<th>KAS</th>
							<th>UTANG</th>
							<th></th>
						  </tr>
						</thead>
						<tbody id="list_trc">
							<?php $today = date('Y-m-d');
							if($transaksi_pelanggan != null){
								foreach ($transaksi_pelanggan as $key => $row) { ?>
								<tr>
									<td><?= $key+1; ?></td>
									<td><?= $row['C050_DocDate']; ?></td>
									<td><?= $row['Descr']; ?></td>
									<td align="right"><?= number_format($row["AmountIn"],0,".",","); ?></td>
									<td align="right"><?= number_format($row["AmountOut"],0,".",","); ?></td>
									<td align="right"><?= number_format($row["AmountGet"],0,".",","); ?></td>
									<td align="right"><?= number_format(($row["AmountIn"] - $row["AmountOut"]),0,".",",") ?></td>
									<td align="right"><?= number_format((($row["AmountIn"] - $row["AmountOut"]) - $row["AmountGet"]),0,".",",") ?></td>
									<td class="text-right">
										<!--<a href="javascript:;" data-target="#edit-trans" data-toggle="modal" class="edit-trans btn btn-sm btn-default"><i class="icon-note"></i></a> -->  
										<a href="javascript:;" class="delete-trans btn btn-sm btn-danger" style="display: <?= ($row['C050_DocDate'] != $today ? 'none':'') ?>;"><i class="icons-office-52"></i></a>
									</td>
								</tr>
								<?php } 
							} ?>
							<?php
								/*
								$rslt = mssql_query("
								SELECT DISTINCT
								dbo.T510_Rekap.C012_TrcID,
								dbo.T510_Rekap.C000_LineID,
								dbo.T510_Rekap.C072_AmountIn,
								dbo.T510_Rekap.C073_AmountOut,
								dbo.T510_Rekap.C073_AmountService,
								dbo.T510_Rekap.C210_MLedgerID,
								dbo.T510_Rekap.C200_SLedgerID,
								dbo.T510_Rekap.C211_SubLedger1ID,
								dbo.T510_Rekap.C212_SubLedger2ID,
								dbo.T510_Rekap.C213_SubLedger3ID,
								dbo.T510_Rekap.C214_SubLedger4ID,
								dbo.T610_Trc.C015_DatePos,
								dbo.T610_Trc.C016_TimePost,
								dbo.T024_TrcType.C001_TrcName,
								dbo.T024_TrcType.C002_Descr

								FROM
								dbo.T510_Rekap
								LEFT JOIN dbo.T610_Trc ON dbo.T510_Rekap.C010_TrcTypeID = dbo.T610_Trc.C010_TrcTypeID AND dbo.T510_Rekap.C012_TrcID = dbo.T610_Trc.C000_TrcID
								INNER JOIN dbo.T024_TrcType ON dbo.T510_Rekap.C212_SubLedger2ID = dbo.T024_TrcType.MLedgerID
								WHERE
								dbo.T510_Rekap.C010_TrcTypeID = 10020 AND dbo.T510_Rekap.C214_SubLedger4ID = 0 AND dbo.T510_Rekap.C211_SubLedger1ID = ".$_SESSION['off_id']);
								
								$tr = "";
								while($row = mssql_fetch_array($rslt)){
									$time = date_create($row["C016_TimePost"]);
									$tr .= '
									<tr idx="'.$row["C012_TrcID"].'">
										<td>'.$row["C012_TrcID"].'</td>
										<td>'.$row["C015_DatePos"].' / '.date_format($time, 'H:i:s').'</td>
										<td>'.$row["C001_TrcName"].'</td>
										<td align="right">'.number_format($row["C072_AmountIn"],0,".",",").'</td>
										<td align="right">'.number_format($row["C073_AmountOut"],0,".",",").'</td>
										<td align="right">'.number_format($row["C073_AmountService"],0,".",",").'</td>
										<td align="right">'.number_format(($row["C072_AmountIn"] - $row["C073_AmountOut"]),0,".",",").'</td>
										<td align="right">'.number_format((($row["C072_AmountIn"] - $row["C073_AmountOut"]) - $row["C073_AmountService"]),0,".",",").'</td>
										<td class="text-right">
											<a href="javascript:;" data-target="#edit-trans" data-toggle="modal" class="edit-trans btn btn-sm btn-default"><i class="icon-note"></i></a>  
											<a href="javascript:;" class="delete-trans btn btn-sm btn-danger"><i class="icons-office-52"></i></a>
										</td>
									 </tr>';
								}
								echo $tr;
								*/
							?>						  
						</tbody>
					  </table>
					
				</div>
                  
                </div>
              </div>
            </div>
        </div>
		<!-- <div class="row">
            <div class="col-md-12">
              <div class="panel">
                <div class="panel-header bg-orange">
                  <h3>TRANSAKSI <strong>PROGRAM</strong></h3>
                </div>
                <div class="panel-content">
					<div class="row">
						<table class="table f-12 table-hover">
						<thead>
						  <tr>
							<th width="60">#</th>
							<th width="150">TANGGAL</th>
							<th>NAMA PROGRAM</th>
							<th width="200">PENYALURAN</th>
							<th width="120"></th>
						  </tr>
						</thead>
						<tbody id="list_trc_prg">
							<?php
								/*
								$rslt = mssql_query("
								SELECT DISTINCT
								dbo.T510_Rekap.C012_TrcID,
								dbo.T510_Rekap.C000_LineID,
								dbo.T510_Rekap.C072_AmountIn,
								dbo.T510_Rekap.C073_AmountOut,
								dbo.T510_Rekap.C073_AmountService,
								dbo.T510_Rekap.C210_MLedgerID,
								dbo.T510_Rekap.C200_SLedgerID,
								dbo.T510_Rekap.C211_SubLedger1ID,
								dbo.T510_Rekap.C212_SubLedger2ID,
								dbo.T510_Rekap.C213_SubLedger3ID,
								dbo.T510_Rekap.C214_SubLedger4ID,
								dbo.T610_Trc.C015_DatePos,
								dbo.T610_Trc.C016_TimePost,
								dbo.T005_Project.C020_Descr AS nm_prg

								FROM
								dbo.T510_Rekap
								LEFT JOIN dbo.T610_Trc ON dbo.T510_Rekap.C010_TrcTypeID = dbo.T610_Trc.C010_TrcTypeID AND dbo.T510_Rekap.C012_TrcID = dbo.T610_Trc.C000_TrcID
								INNER JOIN dbo.T005_Project ON dbo.T510_Rekap.C214_SubLedger4ID = dbo.T005_Project.C000_SysID
								WHERE
								dbo.T510_Rekap.C010_TrcTypeID = 10020 AND
								dbo.T510_Rekap.C210_MLedgerID = 4 AND
								dbo.T510_Rekap.C211_SubLedger1ID = ".$_SESSION['off_id']);
								
								$tr = "";
								while($row = mssql_fetch_array($rslt)){
									$time = date_create($row["C016_TimePost"]);
									$tr .= '
									<tr idx="'.$row["C012_TrcID"].'">
										<td>'.$row["C012_TrcID"].'</td>
										<td>'.$row["C015_DatePos"].' / '.date_format($time, 'H:i:s').'</td>
										<td>'.$row["nm_prg"].'</td>
										<td align="right">'.number_format($row["C073_AmountOut"],0,".",",").'</td>
										<td class="text-right">
											<a href="javascript:;" data-target="#edit-trans" data-toggle="modal" class="edit-trans-prg btn btn-sm btn-default"><i class="icon-note"></i></a>  
											<a href="javascript:;" class="delete-trans-prg btn btn-sm btn-danger"><i class="icons-office-52"></i></a>
										</td>
									 </tr>';
								}
								echo $tr;
								*/
							?>
							
						  
						  
						</tbody>
					  </table>
					</div>

                  
                </div>
              </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
              <div class="panel">
                <div class="panel-header bg-orange">
                  <h3>TRANSAKSI <strong>TRANSFER</strong></h3>
                </div>
                <div class="panel-content">
					<div class="row">
						<table class="table f-12 table-hover">
						<thead>
						  <tr>
							<th width="60">#</th>
							<th width="150">TANGGAL</th>
							<th>NAMA PROGRAM</th>
							<th width="200">PENYALURAN</th>
							<th width="120"></th>
						  </tr>
						</thead>
						<tbody id="list_trc_prg">
							
						  
						</tbody>
					  </table>
					</div>

                  
                </div>
              </div>
            </div>
        </div>
		<div class="row">
            <div class="col-md-12">
              <div class="panel">
                <div class="panel-header bg-orange">
                  <h3>TRANSAKSI <strong>INTERNAL OPERASIONAL</strong></h3>
                </div>
                <div class="panel-content">
					<div class="row">
						<table class="table f-12 table-hover">
						<thead>
						  <tr>
							<th width="60">#</th>
							<th width="150">TANGGAL</th>
							<th>KAS/BANK</th>
							<th width="200">JENIS TRANSAKSI</th>
							<th width="200">DESKRIPSI</th>
							<th width="200">JUMLAH TRANSAKSI</th>
						  </tr>
						</thead>
						<tbody id="list_trc_prg">
							
						  
						</tbody>
					  </table>
					</div>

                  
                </div>
              </div>
            </div>
        </div> -->
		