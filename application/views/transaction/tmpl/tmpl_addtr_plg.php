<?
	session_start();
	include_once("../php/koneksi.php");
	
	$jml_row = $_POST["jml_row"];

	$result = mssql_query(" SELECT * FROM T024_TrcType WHERE C003_TrcJenisID = 1 ORDER BY C000_SysID ASC");
	$dt_out = "";
	while($row = mssql_fetch_array($result))
	{
		$dt_out .= '<option value="'.$row["MLedgerID"].'">'.$row["C001_TrcName"].'</option>';
	}
	
	
	
	$result = mssql_query("SELECT * FROM [dbo].[T022_KBAccount] WHERE C001_OfficeID = ".$_SESSION['off_id']." ORDER BY C020_Name ASC");
	$bankacc = "";
	while($row = mssql_fetch_array($result))
	{
		$bankacc .= '<option value="'.$row["C000_SysID"].'">'.$row["C020_Name"].' ['.$row["C010_BankAccNumber"].']</option>';
	}
	
	echo '
	<tr>
		<td>'.($jml_row + 1).'</td>
		<td>
			<select name="SubLedger2ID" class="form-control">
				<option value="0">-- Select --</option>'.
				$dt_out
			.'</select>
		</td>
		<td>
			<div class="jns-bayar" style="display:block">
				<select name="jns_bayar" class="sel-jns-bayar form-control">
					<option value="0">-- Select --</option>
					<option value="2">KAS</option>
					<option value="3">BANK</option>
				</select>
			</div>
			<div class="input-group m-t-5" style="display:none">
			  <select name="rek_bank" class="form-control" >
				<option value="0">-- Select Bank Acc --</option>'.
				$bankacc
			  .'</select>
			  <span class="close-bankacc input-group-addon bg-red p-10" style="cursor:pointer"><i class="icons-office-52"></i></span>
			</div>
		</td>
		<td><input type="text" name="AmountInTrc" class="form-control input-sm nomor trc text-right" value="0"></td>
		<td><input type="text" name="AmountOutTrc" class="form-control input-sm nomor trc text-right" value="0"></td>
		<td><input type="text" name="AmountService" class="form-control input-sm nomor trc text-right" value="0"></td>
		<td><input type="text" name="AmountKas" class="form-control input-sm nomor trc text-right" value="0"></td>
		<td><input type="text" name="AmountTrc" class="form-control input-sm nomor text-right" value="0" readonly></td>
	<td><a href="javascript:;" class="remrow-trc btn btn-sm btn-danger"><i class="fa fa-remove"></i></a></td>
	</tr>';
?>