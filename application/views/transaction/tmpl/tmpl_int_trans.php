<div class="col-md-12 form-field" id="tmpl_int_trans" style="display:">
        <?php if(false){?>
        <div class="form-group">
                <label class="col-sm-3 control-label">Sumber</label>
                <div class="col-sm-9">
                        <select name="<?= TRC_INPUT_NAME_SUBLEDGER2IDTO; ?>" seq_id="2" class="form-control input-post">
                                <!--<option value="0">-- Select --</option>-->
                                <?php foreach ($virtual_accounts AS $virtual_account) { ?>
                                        <option value="<?= $virtual_account['C000_SysID']; ?>"><?= $virtual_account['C011_Descr']; ?></option>
                                <?php } ?>
                        </select>
                </div>
        </div>
        <div id="dvtrans_prog_tujuan" class="form-group div-sub-ledger-3" style="display:none">
                <label class="col-sm-3 control-label">Program :</label>
                <div class="col-sm-9">
                        <select name="<?= TRC_INPUT_NAME_SUBLEDGER3IDTO; ?>" class="form-control sub-ledger-3">
                                <!--<option value="0">-- Select --</option>-->
                                <?php foreach ($programs AS $program) { ?>
                                        <option value="<?= $program['C000_SysID']; ?>"><?= $program['C020_Descr']; ?></option>
                                <?php } ?>
                        </select>
                </div>
        </div>
        <?php } ?>
        <div class="form-group">
                <label class="col-sm-3 control-label">Level Kantor</label>
                <div class="col-sm-9">
                        <select name="<?= TRC_INPUT_NAME_OFFICE_LEVEL; ?>" seq_id="2" class="form-control">
                                <?php if(count($office_levels_to) != 1){ ?>
                                <option value="0">-- Select --</option>
                                <?php } ?>
                                <?php 
                                foreach($office_levels_to AS $i => $level){?>
                                <option value="<?= $level['Level']; ?>"><?= $level['Descr']; ?></option>
                                <?php } ?>
                        </select>
                        <label style="display:none;" class="error">Mohon dipilih!</label>
                </div>
        </div>
        <div class="form-group">
                <label class="col-sm-3 control-label">Kantor Tujuan</label>
                <div class="col-sm-9">
                        <input type="text" class="form-control" id="office" />
                        <input type="hidden" class="input-post" value="0" seq_id="2" name="<?= TRC_INPUT_NAME_SLEDGERIDTO; ?>" />
                        <label style="display:none;" class="error">Mohon diisi berdasarkan <i>suggestion</i>!</label>
                </div>

        </div>
        <div id="dvseltrans_kasbank_tujuan" class="form-group" style="display:">
                <label class="col-sm-3 control-label">Kas / Bank</label>
                <div class="col-sm-9">
                        <select name="<?= TRC_INPUT_NAME_MLEDGERIDTO; ?>" seq_id="2" class="form-control m-ledger-int-trans input-post">
                                <!--<option value="0">-- Select --</option>-->
                                <option value="2">KAS</option>
                                <option value="3">BANK</option>
                        </select>
                </div>
        </div>

        <div id="dvtrans_bank_tujuan" class="form-group div-bank" style="display:none">
                <label class="col-sm-3 control-label">Nama Bank :</label>
                <div class="col-sm-9">
                        <select name="<?= TRC_INPUT_NAME_BANK_NAME; ?>" class="form-control" seq_id="2">
                                <option value="0">-- Select --</option>
                                <?php foreach ($banks AS $key => $bank) { ?>
                                        <option value="<?= $key; ?>"><?= $key; ?></option>
                                <?php } ?>
                        </select>
                        <label style="display:none;" class="error">Mohon dipilih!</label>
                </div>
        </div>
        <div id="dvtrans_bankacc_tujuan" class="form-group div-bank" style="display:none">
                <label class="col-sm-3 control-label">Nomor Rek. :</label>
                <div class="col-sm-9">
                        <select name="<?= TRC_INPUT_NAME_SUBLEDGER1IDTO; ?>" seq_id="2" class="form-control input-post">
                                <option value="0">-- Select --</option>
                        </select>
                        <label style="display:none;" class="error">Mohon dipilih!</label>
                </div>
        </div>
</div>
