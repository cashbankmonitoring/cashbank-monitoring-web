<?
	session_start();
	include_once("../php/koneksi.php");
	
	$jml_row = $_POST["jml_row"];

	$result = mssql_query(" 
		SELECT
		dbo.T027_ProgramAlocation.C000_SysID,
		dbo.T027_ProgramAlocation.C010_OfficeID,
		dbo.T027_ProgramAlocation.C020_ProjectID,
		dbo.T005_Project.C020_Descr
		

		FROM
		dbo.T027_ProgramAlocation
		INNER JOIN dbo.T005_Project ON dbo.T027_ProgramAlocation.C020_ProjectID = dbo.T005_Project.C000_SysID
		
		WHERE 
			dbo.T005_Project.C050_IsClose = 0 AND dbo.T027_ProgramAlocation.C010_OfficeID = ".$_SESSION['off_id']." 
		
		GROUP BY
		dbo.T027_ProgramAlocation.C000_SysID,
		dbo.T027_ProgramAlocation.C010_OfficeID,
		dbo.T027_ProgramAlocation.C020_ProjectID,
		dbo.T005_Project.C020_Descr");
	$dt_out = "";
	while($row = mssql_fetch_array($result))
	{
		$dt_out .= '<option value="'.$row["C020_ProjectID"].'">'.$row["C020_Descr"].'</option>';
	}
	
	
	
	echo '
	<tr>
		<td>'.($jml_row + 1).'</td>
		<td>
			<select id="selsal_program" name="SubLedger4ID" class="C010_TrcTypeID form-control">
				<option value="">-- Select --</option>'.
				$dt_out
			.'</select>
		</td>
		<td><input type="text" name="sisa_kas_prg" value="0" class="form-control input-sm nomor text-right" readonly></td>
		<td><input type="text" name="AmountOutTrc" value="0" class="form-control input-sm nomor text-right"></td>
		<td><a href="javascript:;" class="remrow-trc btn btn-sm btn-danger"><i class="fa fa-remove"></i></a></td>
	</tr>';
?>