<div class="col-md-12" id="tmpl_ext_trans" style="display:">
        <div class="form-group">
                <label class="col-sm-3 control-label">Tanggal</label>
                <div class="col-sm-9">
                        <input type="text" name="C001_DateTransfer" class="datetimepicker form-control input-post-extra input-sm">
                        <label style="display:none;" class="error">Mohon diisi!</label>
                </div>
        </div>
        <div class="form-group">
                <label class="col-sm-3 control-label">Bank</label>
                <div class="col-sm-9">
                        <input type="text" name="C002_BankName" class="form-control input-post-extra input-sm">
                        <label style="display:none;" class="error">Mohon diisi!</label>
                </div>
        </div>
        <div class="form-group">
                <label class="col-sm-3 control-label">No. Rek.</label>
                <div class="col-sm-9">
                        <input type="text" name="C003_BankAccNumber" class="form-control input-post-extra input-sm">
                        <label style="display:none;" class="error">Mohon diisi!</label>
                </div>
        </div>
        <div class="form-group">
                <label class="col-sm-3 control-label">Atas Nama</label>
                <div class="col-sm-9">
                        <input type="text" name="C004_BankAccName" class="form-control input-post-extra input-sm">
                        <label style="display:none;" class="error">Mohon diisi!</label>
                </div>
        </div>
        <div class="form-group">
                <label class="col-sm-3 control-label">Keterangan</label>
                <div class="col-sm-9">
                        <textarea class="form-control input-post-extra" rows="3" name="C005_Descr" aria-required="true"></textarea>
                </div>
        </div>
</div>