<script>
        var banks = [];
        var mapToBank = {};
        var kasBank = [];

        $(function () {
                init_elmnt();
                startTime();
        });
        function init_elmnt() {
                var strBanks = '<?= json_encode($banks); ?>';

                bankAccs = jQuery.parseJSON(strBanks);
                for (var i = 0; i < banks.length; i++) {
                        var bank = banks[i];
                        mapToBank[bank['C000_SysID']] = bank;
                }

                var strKasBank = '<?= json_encode($kasbank); ?>';
                kasBank = jQuery.parseJSON(strKasBank);
                for (var i = 0; i < kasBank.length; i++) {
                        var item = kasBank[i];
                }
        }
        function startTime() {
                var monthNames = ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun",
                        "Jul", "Agu", "Sep", "Okt", "Nov", "Des"];

                var today = new Date();
                var d = today.getDate();
                var mount = monthNames[today.getMonth()];
                var y = today.getFullYear();
                var h = today.getHours();
                var m = today.getMinutes();
                var s = today.getSeconds();
                h = checkTime(h);
                m = checkTime(m);
                s = checkTime(s);
                var val = d + "-" + mount + "-" + y + " / " + h + ":" + m + ":" + s;
                $('#doc-date').val(val);
                var t = setTimeout(startTime, 500);
        }
        function checkTime(i) {
                if (i < 10) {
                        i = "0" + i;
                }
                // add zero in front of numbers < 10
                return i;
        }
</script>
<input type="hidden" id="trc_type_code" value='<?= $trc_type['C010_Code']; ?>' />
<input type="hidden" id="trc_link" value='<?= $trc_type['C014_Link']; ?>' />
<input type="hidden" id="dt-banks" value='<?= json_encode($banks2); ?>' />
<input type="hidden" id="dt-office-id" value='<?= json_encode($office_id); ?>' />
<input type="hidden" id="dt-office-level" value='<?= json_encode($office_level); ?>' />
<h2 style="margin-top: 20px"><strong><?= $trc_type['C013_Label']; ?></strong></h2>
<div class="row">
        <div class="col-md-12">
                <div class="panel">
                        <div class="panel-header bg-orange">
                                <h3><strong>INFORMASI</strong> AKUN</h3>
                                <!--<h3>Account <strong>Info</strong></h3>-->
                                <div class="control-btn">
                                        <a onclick="javascript:;" href="#" class="panel-reload"><i class="icon-reload"></i></a>
                                </div>
                        </div>
                        <div class="panel-content">
                                <div class="row">
                                        <div class="col-md-6">
                                                <form class="form-horizontal">
                                                        <div class="col-md-12">
                                                                <div class="form-group" style="margin-bottom: 5px;">
                                                                        <label class="col-sm-2 control-label">Tanggal</label>
                                                                        <div class="col-sm-10">
                                                                                <input readonly class="form-control input-sm" id="doc-date" type="text" value="<?= date("d M Y / h:i:s"); ?>">
                                                                        </div>
                                                                </div>
                                                                <div class="form-group" style="margin-bottom: 5px;">
                                                                        <label class="col-sm-2 control-label">No Dok</label>
                                                                        <div class="col-sm-10">
                                                                                <input readonly name="C050_DocNum" class="form-control input-sm" type="text" 
                                                                                       value="<?= $header['DocNum']//$user_session['user_level'] . '/' . date('d-y/m') . '/' . str_pad(rand(0, 29), 2, "0", STR_PAD_LEFT);   ?>">
                                                                        </div>
                                                                </div>
                                                                <div class="form-group" style="margin-bottom: 5px;">
                                                                        <label class="col-sm-2 control-label">Kantor</label>
                                                                        <div class="col-sm-10">
                                                                                <input readonly class="form-control input-sm" type="text" value="<?= '[' . $user_session['C010_Code'] . '] ' . $user_session['C020_Descr']; ?>">
                                                                        </div>
                                                                </div>
                                                                <div class="form-group" style="margin-bottom: 5px;">
                                                                        <label class="col-sm-2 control-label">Level</label>
                                                                        <div class="col-sm-10">
                                                                                <input readonly class="form-control input-sm" type="text" value="Kantor <?= $user_session['user_level']; ?>">
                                                                        </div>
                                                                </div>
                                                        </div>
                                                </form>
                                        </div>
                                        <div class="col-md-6">
                                                <form class=" form-horizontal">
                                                        <div class="col-md-12">
                                                                <div class="form-group" style="margin-bottom: 5px;">
                                                                        <div class="col-sm-4"></div>
                                                                        <label class="col-sm-4" style="text-align: center">Bank</label>
                                                                        <label class="col-sm-4" style="text-align: center">Kas</label>
                                                                </div>
                                                                <div class="form-group" style="margin-bottom: 5px;">
                                                                        <label class="col-sm-4 control-label">Saldo Awal</label>
                                                                        <div class="col-sm-4">
                                                                                <input class="pre-bank-balance nomor form-control input-sm text-right" type="text" value="<?= $bank_balance; ?>" readonly>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                                <input class="pre-cash-balance nomor form-control input-sm text-right" type="text" value="<?= $cash_balance; ?>" readonly>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group" style="margin-bottom: 5px;">
                                                                        <label class="col-sm-4 control-label">Saldo Mutasi</label>
                                                                        <div class="col-sm-4">
                                                                                <input class="bank-mutation-balance nomor form-control input-sm text-right" type="text" value="0" readonly>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                                <input class="cash-mutation-balance nomor form-control input-sm text-right" type="text" value="0" readonly>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group" style="margin-bottom: 5px;">
                                                                        <label class="col-sm-4 control-label">Saldo Akhir</label>
                                                                        <div class="col-sm-4">
                                                                                <input id="kas-aktual" class="post-bank-balance nomor form-control input-sm text-right" type="text" value="<?= $bank_balance; ?>" readonly>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                                <input id="kas-aktual" class="post-cash-balance nomor form-control input-sm text-right" type="text" value="<?= $cash_balance; ?>" readonly>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group" style="margin-bottom: 5px;">
                                                                        <label class="col-sm-4 control-label">In Transfer</label>
                                                                        <div class="col-sm-4">
                                                                                <input class="bank-mit-balance nomor form-control input-sm text-right" type="text" value="<?= $bank_intransfer_balance; ?>" readonly>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                                <input class="cash-mit-balance nomor form-control input-sm text-right" type="text" value="<?= $cash_intransfer_balance; ?>" readonly>
                                                                        </div>
                                                                </div>
                                                                <div class="form-group" style="margin-bottom: 5px;">
                                                                        <label class="col-sm-4 control-label">Total</label>
                                                                        <div class="col-sm-4">
                                                                                <input class="total-bank-balance nomor form-control input-sm text-right" type="text" value="<?= $bank_balance + $bank_intransfer_balance; ?>" readonly>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                                <input class="total-cash-balance nomor form-control input-sm text-right" type="text" value="<?= $cash_balance + $cash_intransfer_balance; ?>" readonly>
                                                                        </div>
                                                                </div>
                                                        </div>
                                                </form>
                                        </div>
                                </div>

                        </div>
                </div>

        </div>
</div>

<div class="row">
        <div class="col-md-12">
                <div class="panel">
                        <div class="row panel-header bg-orange">
                                <div class="col-md-7">
                                        <h3><strong>JENIS <?= strtoupper($trc_type['C013_Label']); ?></strong></h3>
                                </div>
                                <!-- <div class="control-btn">
                                    <button id="add-program" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah Transaksi</button>
                                    <button class="btn btn-sm btn-default" onclick="post_all_data();">Post</button>
                                </div> -->
                                        <!--<h3><strong>TYPE OF TRANSACTION</strong></h3>-->

                        </div>
                        <style type="text/css">
                                .tengah{
                                        margin-left:auto;
                                        margin-right:auto;
                                        margin-top:auto;
                                        margin-bottom:auto;
                                        left:0;
                                        right:0;
                                        top:0;
                                        bottom:0;
                                }
                        </style>
                        <div class="panel-content panel-tab">
                                <ul class="nav nav-tabs nav-primary">
                                        <?php foreach ($panels AS $i => $panel) { ?>
                                                <li panel-code="<?= $panel['C010_Code']; ?>" <?= $i == 0 ? 'class="active"' : ''; ?>><a href="#tab2_<?= $i + 1; ?>" data-toggle="tab"><i class="<?= $panel['C015_Icon']; ?>"></i> <?= $panel['C012_Label']; ?></a></li>
                                        <?php } ?>
                                        <?php if (FALSE) {
                                                foreach ($tabs AS $i => $tab) {
                                                        ?>
                                                        <li <?= $i == 0 ? 'class="active"' : ''; ?>><a href="#tab2_<?= $tab['id']; ?>" data-toggle="tab"><i class="<?= $tabs[$i]['icon']; ?>"></i> <?= $tabs[$i]['label']; ?></a></li>
                                                <?php }
                                        }
                                        ?>
                                </ul>
                                <div class="loading-block">
                                        <img style="display: block;margin-left: auto;margin-right: auto;" src="<?= base_url(); ?>assets/global/images/balls.gif" alt="user image">
                                </div>
                                <div class="tab-content" style="display: none">
                                        <?php if (FALSE) { ?>
                                                <?php
                                                foreach ($tabs AS $tab) {
                                                        echo isset($tab['view']) ? $tab['view'] : '';
                                                }
                                                ?>
<?php } ?>
                                </div>
                        </div>
                </div>
        </div>
</div>
