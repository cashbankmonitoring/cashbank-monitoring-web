<div class="tab-pane fade" id="tab2_<?= $tab_id; ?>">
        <div class="row">
                <table class="table">
                        <thead>
                                <tr>
                                        <th>#</th>
                                        <th width="200">Kantor Tujuan</th>
                                        <th>Tanggal</th>
                                        <th>ASAL</th>
                                        <th>TUJUAN</th>
                                        <th>Jumlah</th>
                                        <th>STATUS</th>
                                </tr>
                        </thead>
                        <tbody id="tbl_list_TransMasuk">
                                <?php foreach ($transfer_masuk as $key => $value) { ?>
                                        <tr>
                                                <input name="TrcID" value="<?= $value['TrcID']; ?>" type="hidden" />
                                                <input name="LineID" value="<?= $value['LineID']; ?>" type="hidden" />
                                                <td><?= $key + 1; ?></td>
                                                <td><?= $value['OfficeNameFrom']; ?></td>
                                                <td><?= $value['DocDate']; ?></td>
                                                <td><?php echo $value['VirtualAccountFrom'] . ' - ';
                                                if($value['MLedgerIDFrom'] == 2){
                                                        echo 'KAS'; 
                                                }elseif($value['MLedgerIDFrom'] == 3){
                                                        echo $value['BankNameFrom'] . ' [' . $value['BankAccountFrom'] . ']' ;
                                                } 
                                                ?></td>
                                                <td><?php echo $value['VirtualAccountTo'] . ' - ';
                                                if($value['MLedgerIDTo'] == 2){
                                                        echo 'KAS'; 
                                                }elseif($value['MLedgerIDTo'] == 3){
                                                        echo $value['BankNameTo'] . ' [' . $value['BankAccountTo'] . ']' ;
                                                } ?></td>
                                                <td><?= $value['Amount3']; ?></td>
                                                <td><a href="#" style="color: green;" class="accept-link"><u>Terima</u></a><br /><a style="color: red;" href="#" class="reject-link"><u>Tolak</u></a></td>
                                        </tr>
                                <?php } ?>
                        </tbody>
                </table>
        </div>
</div>