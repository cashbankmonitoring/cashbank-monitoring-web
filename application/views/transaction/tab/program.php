<?php
$panel_code = $panel['C010_Code'];
$panel_id = $panel['C000_SysID'];
?>
<script src="<?= base_url(); ?>assets/js/jquery-3.1.1.min.js"></script>
<script src="<?= base_url(); ?>assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/global/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?= base_url(); ?>assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript">
        var subTrcTypes = [];
        var columns;
        var updatedRecords = {};

        $(function () {
                //modal
                get_modal('program', '#tbodyPrg');
                init_panel_<?= $panel_code; ?>();
                get_tr_<?= $panel_code; ?>();

                columns = [     { "data": "i" },
                                { "data": "program_name" },
                                { "data": "penyaluran" },
                                { "data": "DescriptionTo" },
                                { "data": "status" },
                                { "data": "action" },
                ];

                get_datatable("<?= $panel_code; ?>", columns);
        });

		function get_data_datatable(data, j){
			for(var i = 0; i < data.length; i ++){
                    var item = data[i];
                    var oldId = item['TrcID'] + '_' + item['LineID'];
                    var oldRecord = {};
                    oldRecord['data'] = item;
                    oldRecord['status'] = 0;
                    oldRecords[oldId] = oldRecord;

                    item['penyaluran'] = change_format_number(item['Amount1']);
                    item['program_name'] = get_program_name(item['SubLedger3IDTo']);
                    item['action'] = '<a class="edit-program btn btn-sm btn-primary" title="Edit" data-id="'+oldId+'" data-status="0"><i class="glyphicon glyphicon-pencil" /></a><a class="btn btn-sm btn-danger" title="Hapus" data-id="'+oldId+'" data-status="0" onclick="delete_data(\'#tbodyPrg\',\''+oldId+'\', 0);"><i class="glyphicon glyphicon-trash" /></a>';
                    item['status'] = '<span class="label label-success">Tersimpan</span>';
                    item['i'] = ++j;
                    
                    data[i] = item;
            }

            return data;
		}

		function get_program_name(id){
                var str = '<?= json_encode($projects); ?>';
                var val = jQuery.parseJSON(str);

                var item = '-';

                for (var a = 0; a < val.length; a++) {
                        if(val[a]['C000_SysID'] == id){
                                item = val[a]['C020_Descr'];
                        }
                }

                return item;
        }

        function init_panel_<?= $panel_code; ?>() {
                var strSubTrcTypes = '<?= json_encode($sub_trc_types); ?>';
                subTrcTypes = jQuery.parseJSON(strSubTrcTypes);
                $('[name="sub_trc"]').val(subTrcTypes[0].C000_SysID);
        }

        function get_tr_<?= $panel_code; ?>() {
                trTemplate = $('.tr-template-<?= $panel_code; ?>').first().clone();
                $('.tr-template-<?= $panel_code; ?>').remove();
                $('#tbodyPrg').append(trTemplate);
        }

        function get_balance(mledger, virtual) {
                $('[name="virtual"]').val(virtual);
                $.get( base_url+ $('#trc_link').val() +"/sum_balance?MLedgerID="+mledger+"&SubLedger2ID="+virtual).done(function( data ) {
                        var amount_update = 0;
                        var check_update = jQuery.isEmptyObject(updatedRecords);
                        if(check_update == false){
                            for(var key in updatedRecords){
                                var new_amount = 0;
                                if(updatedRecords[key].data.SubLedger2ID == virtual){
                                    amount_update = amount_update + Number(updatedRecords[key].data.Amount1);
                                }

                                new_amount = data - amount_update;    
                                $('.sisa_kas').html(change_format_number(new_amount));
                                $('[name="saldo"]').val(new_amount);
                            }
                        }else{
                            $('.sisa_kas').html(change_format_number(data));
                            $('[name="saldo"]').val(data);
                        }
                });
        }

        function check_kas(total, sisa_kas){
        	if(total < sisa_kas){
        		return true;
        	}else{
        		bootbox.alert("Total penyaluran terlalu besar... "+change_format_number(total)+"  karena sisa alokasi "+change_format_number(sisa_kas));
        		return false;
        	}
        }

        function get_item(data_form){
        	var item = {
                "TrcPanelID":<?= $panel_id; ?>,
                "SubTrcTypeID":data_form[3].value,
                "Amount1":data_form[5].value,
                "MLedgerID":data_form[1].value,
                "SubLedger2ID":data_form[2].value,
                "SubLedger3IDTo":data_form[4].value,
                "DescriptionTo":data_form[6].value,
                "penyaluran":data_form[5].value,
                "program_name":get_program_name(data_form[4].value),
                "i":'-'
            };

            return item;
        }

        function get_update_record(item, status){
        		var updatedRecord = {};
                updatedRecord['status'] = status;
                updatedRecord['data'] = item;

                return updatedRecord;
        }

        function get_delete_item(id){
                var item = {
                    "TrcPanelID":<?= $panel_id; ?>,
                    "SubTrcTypeID":oldRecords[id]['data']['SubTrcTypeID'],
                    "Amount1":oldRecords[id]['data']['Amount1'],
                    "MLedgerID":oldRecords[id]['data']['MLedgerID'],
                    "SubLedger2ID":oldRecords[id]['data']['SubLedger2ID'],
                    "SubLedger3IDTo":oldRecords[id]['data']['SubLedger3IDTo'],
                    "DescriptionTo":oldRecords[id]['data']['DescriptionTo'],
                    "penyaluran":oldRecords[id]['data']['penyaluran'],
                	"program_name":get_program_name(oldRecords[id]['data']['SubLedger3IDTo']),
                    "i":'-'
                };

            return item;
        }

        $(document).on('click','.edit-program', function(){
                var id = $(this).attr('data-id');
                var status = $(this).attr('data-status');
                var data_form = {};
                if(status == 0){
                        data_form = oldRecords[id]['data'];
                }else{
                        data_form = updatedRecords[id]['data'];
                }

                get_balance(2, data_form.SubLedger2ID);

                $('[name="program_id"]').val(id);
                $('select[name="program"]').val(data_form.SubLedger3IDTo);
                $('[name="penyaluran"]').val(data_form.penyaluran);
                $('[name="deskripsi"]').val(data_form.DescriptionTo);

        });

        $(document).on('keyup', '.change_saldo', function(){
                var penyaluran = $('[name="penyaluran"]').val().replace(/,/g, "");
                var saldo = $('[name="saldo"]').val();
                check_saldo(saldo, penyaluran);
                var new_saldo = saldo - penyaluran;
                $('.sisa_kas').html(change_format_number(new_saldo));
        });

        function check_saldo(saldo, jumlah){
            //Store the password field objects into variables ...
            var pass1 = document.getElementById('show_password');
            var pass2 = document.getElementById('confirm_password');
            //Store the Confimation Message Object ...
            var message = document.getElementById('confirmMessage');
            //Set the colors we will be using ...
            var goodColor = "#66cc66";
            var badColor = "#ff6666";
            //Compare the values in the password field 
            //and the confirmation field
            if(Number(saldo) >= Number(jumlah)){
                //The passwords match. 
                //Set the color to the good color and inform
                //the user that they have entered the correct password 
                // pass2.style.backgroundColor = goodColor;
                // message.style.color = goodColor;
                message.innerHTML = "";
                // return true;
            }else{
                //The passwords do not match.
                //Set the color to the bad color and
                //notify the user.
                // pass2.style.backgroundColor = badColor;
                message.style.color = badColor;
                message.innerHTML = "Saldo anda tidak cukup";
                // return false;
            }
        }
</script>
<div class="tab-pane fade active in" panel-code="<?= $panel_code; ?>" sub_trc_types='<?= json_encode($sub_trc_types); ?>'>
        <div class="control-btn">
	        <button class="btn btn-sm btn-default post-all" onclick="post_all_data();">Post Semua</button>
	        <button id="add-program" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Transaksi Program</button>
        </div>
        <div class="row">
                <div class="col-md-12 p-0">
                        <div class="panel">
                                <div class="panel-content">
                                        <!-- <button id="button-test" type="button">Click Me!</button> -->
                                        <table name="table-<?= $panel_code; ?>" class="table table-hover f-12">
                                                <thead>
                                                        <tr>
                                                                <th width="8%">#</th>
                                                                <th>Program</th>	
                                                                <th>Total Penyaluran</th>
                                                                <th>Keterangan</th>
                                                                <th>Status</th>
                                                                <th>Action</th>
                                                        </tr>
                                                </thead>
                                                <tbody id="tbodyPrg">
                                                        
                                                </tbody>
                                        </table>
                                </div>
                        </div>
                </div>
        </div>
</div>

<!-- Start Modal -->
<div class="modal fade" id="program-modal">
        <div class="modal-dialog">
          <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
                  <h4 class="modal-title"><strong id="title-type"></strong> Transaksi Program</h4>
                </div>
                <div class="modal-body p-t-0 p-b-0">
                        <div class="row">
                                <form class="col-md-12 form-horizontal" id="program-form">
                                         <div class="col-md-12">
                                                <input type="hidden" name="program_id">
                                                <input type="hidden" name="mledger" value="2">
                                                <input type="hidden" name="virtual">
                                                <input type="hidden" name="sub_trc">
                                                <div class="form-group">
                                                  <label class="col-md-4 control-label required">Program</label>
                                                  <div class="col-md-8">
                                                        <select class="form-control" name="program" required="" onchange="javascript: get_balance(2, $(this).find(':selected').attr('data-virtual'));">
                                                                <option value="">-- Pilih Program--</option>
                                                                <?php foreach($projects as $row){
                                                                        echo '<option value="'.$row["C000_SysID"].'" data-virtual="'.$row["C021_VirtualAccountID"].'">'.$row["C020_Descr"].'</option>';
                                                                } ?>
                                                        </select>
                                                  </div>
                                                </div>

                                                <div class="form-group">
                                                  <label class="col-md-4 control-label required">Total Penyaluran</label>
                                                  <div class="col-md-8">
                                                         <input type="text" name="penyaluran" required="" class="nomor form-control text-right change_saldo">
                                                         <p>Sisa Alokasi : <span class="sisa_kas change_saldo">0</span></p>
                                                         <span id="confirmMessage" class="confirmMessage"></span>
                                                  </div>
                                                </div>

                                                <div class="form-group">
												  <label class="col-md-4 control-label">Keterangan</label>
												  <div class="col-md-8">
													<textarea class="form-control" rows="4" name="deskripsi"></textarea>
												  </div>
												</div>
                                                <input type="hidden" name="saldo">
                                        </div>
                                </form>
                        </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default btn-embossed" onclick="cancel_data('program', 'Transaksi Program')">Batal</button>
                  <button type="button" class="btn btn-primary btn-embossed" onclick="add_data('program', '#tbodyPrg')">Tambahkan</button>
                </div>
          </div>
        </div>
</div>