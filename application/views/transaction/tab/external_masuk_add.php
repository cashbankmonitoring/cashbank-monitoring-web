<script>
        $(function () {
                $('.sub-trc.transfer-external-masuk-add').find('[name="SubLedger2ID"]').on('change', function () {
                        var parent = $(this).closest('.sub-trc');
                        var val = $(this).val();
                        if (val == 2) { // Mensos
                                $(parent).find('[name="SubLedger3ID"]').closest('.form-group').show();
                                $(parent).attr('sub_trc_type', '27');
                        } else {
                                $(parent).find('[name="SubLedger3ID"]').closest('.form-group').hide();
                                $(parent).attr('sub_trc_type', '26');
                        }
                });
        });
</script>
<div class="row line sub-trc transfer-external-masuk-add" <?= TRC_KEY_NAME_SUBTRCTYPE . '="26"'; ?> style="display: none;">
        <div class="row line">
                <div class="col-md-6">
                        <h3><strong>TRANSFER</strong> DARI</h3>
                        <form id="frm_transfer" class="form-horizontal">
                                <div class="col-md-12">
                                        <div class="form-group">
                                                <label class="col-sm-3 control-label">Tanggal*</label>
                                                <div class="col-sm-9">
                                                        <input type="text" name="C001_DateTransfer" class="datetimepicker form-control input-post-extra input-sm">
                                                        <label style="display:none;" class="error">Mohon diisi!</label>
                                                </div>
                                        </div>
                                        <div class="form-group">
                                                <label class="col-sm-3 control-label">Pengirim</label>
                                                <div class="col-sm-9">
                                                        <input type="text" name="C007_Name" class="form-control input-post-extra input-sm">
                                                        <label style="display:none;" class="error">Mohon diisi!</label>
                                                </div>
                                        </div>
                                        <div class="form-group">
                                                <label class="col-sm-3 control-label">Bank</label>
                                                <div class="col-sm-9">
                                                        <input type="text" name="C002_BankName" class="form-control input-post-extra input-sm">
                                                        <label style="display:none;" class="error">Mohon diisi!</label>
                                                </div>
                                        </div>
                                        <div class="form-group">
                                                <label class="col-sm-3 control-label">No. Rek.</label>
                                                <div class="col-sm-9">
                                                        <input type="text" name="C003_BankAccNumber" class="form-control input-post-extra input-sm">
                                                        <label style="display:none;" class="error">Mohon diisi!</label>
                                                </div>
                                        </div>
                                        <div class="form-group">
                                                <label class="col-sm-3 control-label">Atas Nama</label>
                                                <div class="col-sm-9">
                                                        <input type="text" name="C004_BankAccName" class="form-control input-post-extra input-sm">
                                                        <label style="display:none;" class="error">Mohon diisi!</label>
                                                </div>
                                        </div>
<!--                                        <div class="form-group">
                                                <label class="col-sm-3 control-label">Total Saldo</label>
                                                <div class="col-sm-9">
                                                        <input name="balance" type="text" value="0" class="form-control nomor input-sm text-right" readonly>
                                                </div>
                                        </div>-->
                                        <div class="form-group">
                                                <label class="col-sm-3 control-label">Jumlah</label>
                                                <div class="col-sm-9">
                                                        <input name="<?= TRC_INPUT_NAME_AMOUNT; ?>" type="text" seq_id="1" value="0" minplus="-1" class="form-control input-mutasi input-post input-sm nomor text-right">
                                                        <label style="display:none;" class="error">Input harus lebih besar dari nol!</label>
                                                </div>
                                        </div>
                                        <div class="form-group">
                                                <label class="col-sm-3 control-label">Keterangan</label>
                                                <div class="col-sm-9">
                                                        <textarea class="form-control input-post-extra" rows="3" name="C005_Descr" aria-required="true"></textarea>
                                                </div>
                                        </div>
                                </div>
                        </form>
                </div>
                <div class="col-md-6">
                        <?php if (FALSE) { ?>
                                <div class="row m-r-10" style="display: none">
                                        <div class="col-md-6">
                                                <h3 class="m-t-20"><strong>Tujuan</strong></h3>
                                        </div>
                                        <div class="col-md-6 m-t-10">
                                                <select id="tp_transfer" class="form-control">
                                                        <option value="">-- Select --</option>
                                                        <option value="1">Internal</option>
                                                        <option value="2">External</option>
                                                </select>
                                        </div>
                                </div>
                        <?php } ?>

                        <h3><strong>TRANSFER</strong> KE</h3>
                        <form class=" form-horizontal">
                                <div class="col-md-12 form-field">
                                        <div class="form-group">
                                                <label class="col-sm-3 control-label">Sumber</label>
                                                <div class="col-sm-9">
                                                        <select name="<?= TRC_INPUT_NAME_SUBLEDGER2ID; ?>" seq_id="1,2" class="form-control input-post input-get-balance">
                                                                <!--<option value="0">-- Select --</option>-->
                                                                <?php foreach ($virtual_accounts AS $virtual_account) { ?>
                                                                        <option value="<?= $virtual_account['C000_SysID']; ?>"><?= $virtual_account['C011_Descr']; ?></option>
                                                                <?php } ?>
                                                        </select>
                                                </div>
                                        </div>
                                        <div id="dvtrans_prog_asal" class="form-group div-sub-ledger-3" style="display:none">
                                                <label class="col-sm-3 control-label">Program</label>
                                                <div class="col-sm-9">
                                                        <select name="<?= TRC_INPUT_NAME_SUBLEDGER3ID; ?>" seq_id="2" onChange="myFunction()" class="form-control input-post sub-ledger-3">
                                                                <?php if (!count($projects)) { ?>
                                                                        <option value="0">-- Select --</option>
                                                                <?php } ?>
                                                                <?php foreach ($projects AS $project) { ?>
                                                                        <option value="<?= $project['C000_SysID']; ?>"><?= $project['C020_Descr']; ?></option>
                                                                <?php } ?>
                                                        </select>
                                                </div>
                                        </div>

                                        <div class="form-group" style="display:">
                                                <label class="col-sm-3 control-label">Kas / Bank</label>
                                                <div class="col-sm-9">
                                                        <select name="<?= TRC_INPUT_NAME_MLEDGER; ?>" seq_id="1" class="form-control m-ledger-mutasi m-ledger input-post input-get-balance">
                                                                <!--<option value="0">-- Select --</option>-->
                                                                <option value="2">KAS</option>
                                                                <option value="3">BANK</option>
                                                        </select>
                                                </div>
                                        </div>

                                        <div id="dvtrans_bank_asal" class="form-group div-bank-name" style="display: none">
                                                <label class="col-sm-3 control-label">Nama Bank</label>
                                                <div class="col-sm-9">
                                                        <select name="<?= TRC_INPUT_NAME_BANK_NAME; ?>" seq_id="1" class="form-control bank-name">
                                                                <option value="0">-- Select --</option>
                                                                <?php foreach ($banks AS $key => $bank) { ?>
                                                                        <option value="<?= $key; ?>"><?= $key; ?></option>
                                                                <?php } ?>
                                                        </select>
                                                        <label style="display:none;" class="error">Mohon dipilih!</label>
                                                </div>
                                        </div>
                                        <div id="dvtrans_bankacc_asal" class="form-group div-bank-account" style="display: none">
                                                <label class="col-sm-3 control-label">Nomor Rek.</label>
                                                <div class="col-sm-9">
                                                        <select name="<?= TRC_INPUT_NAME_SUBLEDGER1ID; ?>" seq_id="1" class="form-control bank-account input-post input-get-balance">
                                                                <option value="0">-- Select --</option>
                                                        </select>
                                                        <label style="display:none;" class="error">Mohon dipilih!</label>
                                                </div>
                                        </div>
                                </div>
                        </form>
                </div>	
        </div>

        <div class="row">
                <div class="col-md-8">
                </div>
                <div class="col-md-4">
                        <div class="panel">
                                <div class="panel-content">
                                        <button class="post-trans-keluar btn btn-embossed btn-primary btn-post" type="button">POST</button>
                                        <button class="cancel-trf-masuk btn btn-embossed btn-default m-b-10 m-r-0" type="reset">Cancel</button>

                                </div>
                        </div>
                </div>
        </div>
</div>