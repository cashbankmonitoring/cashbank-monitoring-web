<?php
$panel_code = $panel['C010_Code'];
$panel_id = $panel['C000_SysID'];
?>
<script src="<?= base_url(); ?>assets/js/jquery-3.1.1.min.js"></script>
<script src="<?= base_url(); ?>assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url(); ?>assets/global/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?= base_url(); ?>assets/js/select2.min.js"></script>
<!-- <link href="<?= base_url(); ?>assets/css/select2.min.css" rel="stylesheet"> -->
<script type="text/javascript">
        var subTrcTypes = [];
        var columns;
        var updatedRecords = {};

        $(function () {

            //modal
            get_modal('internal_keluar', '#tbodyIntKeluar');
            select2_office('#get_kantor', null, null);
            init_panel_<?= $panel_code; ?>();
            get_tr_<?= $panel_code; ?>();
            initValidatorStyle();

            columns = [     { "data": "i" },
                            { "data": "office_name" },
                            { "data": "kas_from" },
                            { "data": "kas_to" },
                            { "data": "jumlah" },
                            { "data": "status_transfer" },
                            { "data": "status" },
                            { "data": "action" },
            ];

            get_datatable("<?= $panel_code; ?>", columns);

        });

        function get_data_datatable(data, j){
                for(var i = 0; i < data.length; i ++){
                        var item = data[i];

                        var oldId = item['TrcID'] + '_' + item['LineID'];
                        var oldRecord = {};
                        oldRecord['data'] = item;
                        oldRecord['status'] = 0;
                        oldRecords[oldId] = oldRecord;

                        // item['office_name'] = 'test';

                        if(item['MLedgerID'] == 2){
                                item['kas_from'] = get_kas_name(item['MLedgerID']);
                        }else{
                                item['kas_from'] = get_bank_name(item['SubLedger1ID']);
                        }

                        if(item['MLedgerIDTo'] == 2){
                                item['kas_to'] = get_kas_name(item['MLedgerIDTo']);
                        }else{
                                item['kas_to'] = get_bank_name(item['SubLedger1IDTo']);
                        }

                        item['jumlah'] = change_format_number(item['Amount1']);

                        item['action'] = '-';
                        if(item['TransferStatus'] == 0){
                        	item['status_transfer'] = 'Money in Transfer';
                        	item['action'] = '<a class="edit-internal_keluar btn btn-sm btn-primary" title="Edit" data-id="'+oldId+'" data-status="0"><i class="glyphicon glyphicon-pencil" /></a><a class="btn btn-sm btn-danger" title="Hapus" data-id="'+oldId+'" data-status="0" onclick="delete_data(\'#tbodyIntKeluar\',\''+oldId+'\', 0);"><i class="glyphicon glyphicon-trash" /></a>';
                        }else if(item['TransferStatus'] == 1){
                        	item['status_transfer'] = 'Approved';
                        }else{
                        	item['status_transfer'] = 'Rejected';
                        }

                        
                        item['status'] = '<span class="label label-success">Tersimpan</span>';
                        item['i'] = ++j;
                        
                        data[i] = item;
                }

                return data;
        }

        function get_sub_trc(id){
                var strSubTrcTypes = '<?= json_encode($sub_trc_types); ?>';
                var item = '-';
                subTrcTypes = jQuery.parseJSON(strSubTrcTypes);

                for (var a = 0; a < subTrcTypes.length; a++) {
                        if(subTrcTypes[a]['C000_SysID'] == id){
                                item = subTrcTypes[a]['C011_Descr'];
                        }
                }

                return item;
        }

        function get_kas_name(id){
                var strKas = '<?= json_encode($ledger); ?>';
                var nama_kas = jQuery.parseJSON(strKas);

                var item = '-';

                for (var a = 0; a < nama_kas.length; a++) {
                        if(nama_kas[a]['SysID'] == id){
                                item = nama_kas[a]['Descr'];
                        }
                }

                return item;
        }

        function get_bank_name(id){
                var str = '<?= json_encode($banks_all); ?>';
                var val = jQuery.parseJSON(str);

                var item = '-';

                for (var a = 0; a < val.length; a++) {
                        if(val[a]['C000_SysID'] == id){
                                item = val[a]['C030_Descr']+' - '+val[a]['C010_BankAccNumber'];
                        }
                }

                return item;
        }

        function init_panel_<?= $panel_code; ?>() {
                var strSubTrcTypes = '<?= json_encode($sub_trc_types); ?>';
                subTrcTypes = jQuery.parseJSON(strSubTrcTypes);
                $('[name="sub_trc"]').val(subTrcTypes[0].C000_SysID);
        }

        function get_tr_<?= $panel_code; ?>() {
                trTemplate = $('.tr-template-<?= $panel_code; ?>').first().clone();
                $('.tr-template-<?= $panel_code; ?>').remove();
                $('#tbodyIntKeluar').append(trTemplate);
        }

        function get_bank(id){
                if(id == 3){
                      $('#data_bank').show();  
                      $('.sisa_kas').html(0);
                }else{
                      $('#data_bank').hide();
                      $('[name="bank"]').val("");
                      get_balance(2, 0, $('[name="virtual"]').val());
                }
        }

        function get_bank_to(id){
                if(id == 3){
                      $('#data_bank_to').show();  
                }else{
                      $('#data_bank_to').hide();
                      $('.bank_select2').val("");

                }
        }

        function get_balance(mledger, sublegder, virtual) {
                $.get( base_url + $('#trc_link').val() +"/sum_balance?MLedgerID="+mledger+"&SubLedger1ID="+sublegder+"&SubLedger2ID="+virtual).done(function( data ) {
                        var amount_update = 0;
                        var check_update = jQuery.isEmptyObject(updatedRecords);
                        if(check_update == false){
                            for(var key in updatedRecords){
                                var new_amount = 0;
                                var record_mledger = Number(updatedRecords[key].data.MLedgerID);
                                var record_sublegder = Number(updatedRecords[key].data.SubLedger1ID);
                                var record_virtual = Number(updatedRecords[key].data.SubLedger2ID);

                                mledger = Number(mledger);
                                sublegder = Number(sublegder);
                                virtual = Number(virtual);
                                if(record_mledger == mledger && record_sublegder == sublegder && record_virtual == virtual){
                                    amount_update = amount_update + Number(updatedRecords[key].data.Amount1.replace(/,/g, ""));
                                }

                                new_amount = data - amount_update;    
                                $('.sisa_kas').html(change_format_number(new_amount));
                                $('[name="saldo"]').val(new_amount);
                            }
                        }else{
                            $('.sisa_kas').html(change_format_number(data));
                            $('[name="saldo"]').val(data);
                        }
                });
        }

        function check_kas(total, sisa_kas){
        	if(total < sisa_kas){
        		return true;
        	}else{
        		bootbox.alert("Jumlah transfer terlalu besar... "+change_format_number(total)+"  karena saldo "+change_format_number(sisa_kas));
        		return false;
        	}
        }

        function get_item(data_form){
            var data = $('#get_kantor').select2('data');
        	var kas_from = '-';
        	var kas_to = '-';

        	if(data_form[2].value == 2){
            	kas_from = get_kas_name(data_form[2].value);
            }else{
                kas_from = get_bank_name(data_form[3].value);
            }

            if(data_form[7].value == 2){
                kas_to = get_kas_name(data_form[7].value);
            }else{
                // kas_to = get_bank_by_id(data_form[6].value, data_form[8].value);
                // get_bank_by_id(data_form[6].value, data_form[8].value, function(result){
                // 	kas_to = result
                // 	console.log(kas_to)

                // })
                kas_to = get_bank_name(data_form[8].value);
            }

            var item = {
                    "TrcPanelID":<?= $panel_id; ?>,
                    "SubTrcTypeID":data_form[1].value,
                    "Amount1":data_form[5].value,
                    "MLedgerID":data_form[2].value,
                    "SubLedger1ID":data_form[3].value,
                    "SubLedger2ID":data_form[4].value,
                    "MLedgerIDTo":data_form[7].value,
                    "SLedgerIDTo":data_form[6].value,
                    "SubLedger1IDTo":data_form[8].value,
                    "SubLedger2IDTo":data_form[4].value,
                    "office_name": data[0].text,
                    'kas_from': kas_from,
                    'kas_to': kas_to,
                    'status_transfer': 'Money in Transfer',
                    "jumlah":data_form[5].value,
                    "Description":data_form[9].value,
                    "i":'-'
            };

            return item;
        }

        function get_update_record(item, status){
                var updatedRecord = {};
                updatedRecord['status'] = status;
                updatedRecord['data'] = item;

                return updatedRecord;
        }

        function get_delete_item(id){
            var kas_from = '-';
        	var kas_to = '-';

        	if(oldRecords[id]['data']['MLedgerID'] == 2){
            	kas_from = get_kas_name(oldRecords[id]['data']['MLedgerID']);
            }else{
                kas_from = get_bank_by_id(oldRecords[id]['data']['SLedgerIDTo'], oldRecords[id]['data']['SubLedger1ID']);
            }

            if(oldRecords[id]['data']['MLedgerIDTo'] == 2){
                kas_to = get_kas_name(oldRecords[id]['data']['MLedgerIDTo']);
            }else{
                kas_to = get_bank_name(oldRecords[id]['data']['SubLedger1IDTo']);
            }

            var item = {
                    "TrcPanelID":<?= $panel_id; ?>,
                    "SubTrcTypeID":oldRecords[id]['data']['SubTrcTypeID'],
                    "Amount1":oldRecords[id]['data']['Amount1'],
                    "MLedgerID":oldRecords[id]['data']['MLedgerID'],
                    "SubLedger1ID":oldRecords[id]['data']['SubLedger1ID'],
                    "SubLedger2ID":oldRecords[id]['data']['SubLedger2ID'],
                    "SLedgerIDTo":oldRecords[id]['data']['SLedgerIDTo'],
                    "MLedgerIDTo":oldRecords[id]['data']['MLedgerIDTo'],
                    "SubLedger1IDTo":oldRecords[id]['data']['SubLedger1IDTo'],
                    "SubLedger2IDTo":oldRecords[id]['data']['SubLedger2IDTo'],
                    'office_name': oldRecords[id]['data']['office_name'],
                    'kas_from': kas_from,
                    'kas_to': kas_to,
                    'status_transfer': 'Money in Transfer',
                    "jumlah":oldRecords[id]['data']['jumlah'],
                    "Description":oldRecords[id]['data']['Description'],
                    "i":'-'
            };

            return item;
        }

        $(document).on('click','.edit-internal_keluar', function(){
                var id = $(this).attr('data-id');
                var status = $(this).attr('data-status');
                var data_form = {};
                if(status == 0){
                        data_form = oldRecords[id]['data'];
                }else{
                        data_form = updatedRecords[id]['data'];
                }

                $('[name="internal_keluar_id"]').val(id);
                $('select[name="kasbank"]').val(data_form.MLedgerID);

                if(data_form.MLedgerID == 2 || data_form.MLedgerID == 0){
                    $('#data_bank').hide();
                    $('select[name="bank"]').val(""); 
                }else{
                	$('#data_bank').show();  
                    $('select[name="bank"]').val(data_form.SubLedger1ID);
                }

                $('select[name="virtual"]').val(data_form.SubLedger2ID);
                get_balance(data_form.MLedgerID, data_form.SubLedger1ID, data_form.SubLedger2ID);
				$.post( base_url+"Offices/get_office_by_id/", {idx: data_form.SLedgerIDTo}).done(function( dataParent ) {

					var jsonParent = $.parseJSON(dataParent);
	                var s1 = $("#get_kantor").data('select2'); 
					s1.trigger('select', { 
					  data: {
							id: jsonParent.C000_SysID,
							text: jsonParent.C020_Descr
						} 
					});
				});

                // $('select[name="kantor"]').val(data_form.SLedgerIDTo).trigger('change');

                $('[name="penyaluran"]').val(data_form.jumlah);                
                $('select[name="kasbank_to"]').val(data_form.MLedgerIDTo); 

                setTimeout(function(){
	                if(data_form.MLedgerIDTo == 2 || data_form.MLedgerIDTo == 0){
	                    $('#data_bank_to').hide();
	                    $('select[name="bank_to"]').val("").trigger('change');; 
	                }else{
	                	$('#data_bank_to').show();  
	                    $('select[name="bank_to"]').val(data_form.SubLedger1IDTo).trigger('change');;
	                }
                },2000);

                $('[name="deskripsi"]').val(data_form.Description);
        });

        function initValidatorStyle(){
			$.validator.setDefaults({
			    highlight: function(element) {
			        $(element).closest('.form-group').addClass('has-error');
			    },
			    unhighlight: function(element) {
			        $(element).closest('.form-group').removeClass('has-error');
			    },
			    errorElement: 'span',
			    errorClass: 'help-block',
			    errorPlacement: function(error, element) {
			        if(element.parent('.input-group').length) {
			            error.insertAfter(element.parent());
			        }else if (element.hasClass('select2')) {     
		       			error.insertAfter(element.next('span'));
			        }else {
			            error.insertAfter(element);
			        }
			    }
			});
		}

        $(document).on('keyup', '.change_saldo', function(){
                var penyaluran = $('[name="penyaluran"]').val().replace(/,/g, "");
                var saldo = $('[name="saldo"]').val();
                check_saldo(saldo, penyaluran);
                var new_saldo = saldo - penyaluran;
                $('.sisa_kas').html(change_format_number(new_saldo));
        });

        function check_saldo(saldo, jumlah){
            //Store the password field objects into variables ...
            var pass1 = document.getElementById('show_password');
            var pass2 = document.getElementById('confirm_password');
            //Store the Confimation Message Object ...
            var message = document.getElementById('confirmMessage');
            //Set the colors we will be using ...
            var goodColor = "#66cc66";
            var badColor = "#ff6666";
            //Compare the values in the password field 
            //and the confirmation field
            if(Number(saldo) >= Number(jumlah)){
                //The passwords match. 
                //Set the color to the good color and inform
                //the user that they have entered the correct password 
                // pass2.style.backgroundColor = goodColor;
                // message.style.color = goodColor;
                message.innerHTML = "";
                // return true;
            }else{
                //The passwords do not match.
                //Set the color to the bad color and
                //notify the user.
                // pass2.style.backgroundColor = badColor;
                message.style.color = badColor;
                message.innerHTML = "Saldo anda tidak cukup";
                // return false;
            }
        }
</script>

<style type="text/css">
	.select2-dropdown {
	    background-color: white;
	    border: 1px solid #aaa;
	    border-radius: 4px;
	    box-sizing: border-box;
	    display: block;
	    position: absolute;
	    left: -100000px;
	    width: 100%;
	    z-index: 105100000;
	}
	.select2-container{
		width: 100% !important;
	}

	.select2-container--default .select2-selection--single{
		-webkit-box-shadow: none !important;
	    border: 1px solid #ECEDEE !important;
	    box-shadow: none !important;
	    color: #555555;
	    display: inline-block;
	    font-size: 13px;
	    height: auto;
	    line-height: normal;
	    padding: 3px 10px;
	    vertical-align: middle;
	    width: 100%;
	    -webkit-border-radius: 2px;
	    -moz-border-radius: 2px;
	    border-radius: 2px;
	    -webkit-transition: all 0.2s ease-out;
	    -moz-transition: all 0.2s ease-out;
	    -o-transition: all 0.2s ease-out;
	    -ms-transition: all 0.2s ease-out;
	    transition: all 0.2s ease-out;
	}

	.help-block{
	    color:#dd4b39;
	 }
	 
	.error .select2-selection {
	    border: 1px solid #a94442;
	    border-radius: 4px;
	}

</style>
<div class="tab-pane fade active in" panel-code="<?= $panel_code; ?>" sub_trc_types='<?= json_encode($sub_trc_types); ?>'>
		<div class="control-btn">
	        <button class="btn btn-sm btn-default post-all" onclick="post_all_data();">Post Semua</button>
	        <button id="add-internal_keluar" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Transfer Keluar</button>
        </div>
        <div class="row">
                <div class="col-md-12 p-0">
                        <div class="panel">
                                <div class="panel-content">
                                        <!-- <button id="button-test" type="button">Click Me!</button> -->
                                        <table name="table-<?= $panel_code; ?>" class="table table-hover f-12">
                                                <thead>
                                                        <tr>
                                                                <th width="8%">#</th>
                                                                <th>Kantor</th>
                                                                <th>Asal</th>
                                                                <th>Tujuan</th>
                                                                <th>Jumlah</th>
                                                                <th>Status Transfer</th>
                                                                <th>Status</th>
                                                                <th>Action</th>
                                                        </tr>
                                                </thead>
                                                <tbody id="tbodyIntKeluar">
                                                        
                                                </tbody>
                                        </table>
                                </div>
                        </div>
                </div>
        </div>
</div>

<!-- Start Modal -->
<div class="modal fade" id="internal_keluar-modal">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
                  <h4 class="modal-title"><strong id="title-type"></strong> Transfer Keluar</h4>
                </div>
                <div class="modal-body p-t-0 p-b-0">
                        <div class="row">
                                <form class="col-md-12 form-horizontal" id="internal_keluar-form">
                                        <div class="col-md-6">
                                        	<h3><b>Transfer</b> Dari</h3>
                                                <input type="hidden" name="internal_keluar_id">
                                                <input type="hidden" name="sub_trc">

                                                <div class="form-group">
                                                  <label class="col-md-4 control-label required">Kas / Bank</label>
                                                  <div class="col-md-8">
                                                        <select class="form-control" id="kasbank_val" name="kasbank" required="" onchange="javascript: get_bank($(this).val());" data-search="true">
                                                        <?php foreach($ledger as $row){
                                                                echo '<option value="'.$row["SysID"].'">'.$row["Descr"].'</option>';
                                                        } ?>
                                                        </select>
                                                  </div>
                                                </div>

                                                <div class="form-group" id="data_bank" style="display: none;">
                                                  <label class="col-md-4 control-label required">Bank</label>
                                                  <div class="col-md-8">
                                                        <select class="form-control" id="bank_val" name="bank" required="" data-search="true" onchange="javascript: get_balance(3, $(this).val(),  $('#virtual_val').val());">
                                                                <option value="">-- Pilih Akun Bank--</option>
                                                                <?php foreach($banks as $row){
                                                                        echo '<option value="'.$row["C000_SysID"].'">'.$row["C030_Descr"].' - '.$row["C010_BankAccNumber"].'</option>';
                                                                } ?>
                                                        </select>
                                                  </div>
                                                </div>

                                                <div class="form-group">
                                                  <label class="col-md-4 control-label required">Virtual Account</label>
                                                  <div class="col-md-8">
                                                        <select class="form-control" id="virtual_val" name="virtual" required="" data-search="true" onchange="javascript: get_balance($('#kasbank_val').val(), $('#bank_val').val(),  $(this).val());">
                                                        <?php foreach($virtual as $row){
                                                                echo '<option value="'.$row["C000_SysID"].'">'.$row["C011_Descr"].'</option>';
                                                        } ?>
                                                        </select>
                                                  </div>
                                                </div>

                                                <div class="form-group">
                                                  <label class="col-md-4 control-label required">Jumlah</label>
                                                  <div class="col-md-8">
                                                         <input type="text" name="penyaluran" required="" class="nomor form-control text-right change_saldo">
                                                         <p>Saldo : <span class="sisa_kas change_saldo">0</span></p>
                                                         <span id="confirmMessage" class="confirmMessage"></span>
                                                  </div>
                                                </div>
                                        </div>
                                        <div class="col-md-6">
                                        	<h3><b>Transfer</b> Ke</h3>
                                        		<div class="form-group">
                                                  <label class="col-md-4 control-label required">Kantor</label>
                                                  <div class="col-md-8">
                                                        <select class="form-control select2" id="get_kantor" name="kantor" required=""  onchange="javascript: select2_bank($(this).val());">
                                                        	<option value="">-- Pilih Kantor --</option>
                                                        </select>
                                                  </div>
                                                </div>

                                                <div class="form-group">
                                                  <label class="col-md-4 control-label required">Kas / Bank</label>
                                                  <div class="col-md-8">
                                                        <select class="form-control" name="kasbank_to" required="" onchange="javascript: get_bank_to($(this).val());" data-search="true">
                                                        <?php foreach($ledger as $row){
                                                                echo '<option value="'.$row["SysID"].'">'.$row["Descr"].'</option>';
                                                        } ?>
                                                        </select>
                                                  </div>
                                                </div>

                                                <div class="form-group" id="data_bank_to" style="display: none;">
                                                  <label class="col-md-4 control-label required">Bank</label>
                                                  <div class="col-md-8">
                                                        <select class="form-control bank_select2" name="bank_to" required="">
                                                                <option value="">-- Pilih Akun Bank--</option>

                                                        </select>
                                                  </div>
                                                </div>

                                                <div class="form-group">
												  <label class="col-md-4 control-label">Keterangan</label>
												  <div class="col-md-8">
													<textarea class="form-control" rows="4" name="deskripsi"></textarea>
												  </div>
												</div>
                                                <input type="hidden" name="saldo">
                                        </div>
                                </form>
                        </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default btn-embossed"onclick="cancel_data('internal_keluar', 'Transfer Keluar')">Batal</button>
                  <button type="button" class="btn btn-primary btn-embossed" onclick="add_data('internal_keluar', '#tbodyIntKeluar')">Tambahkan</button>
                </div>
          </div>
        </div>
</div>
<!-- End Modal -->
