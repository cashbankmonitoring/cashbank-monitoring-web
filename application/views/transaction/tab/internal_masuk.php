<?php
$panel_code = $panel['C010_Code'];
$panel_id = $panel['C000_SysID'];
?>
<script src="<?= base_url(); ?>assets/js/jquery-3.1.1.min.js"></script>
<script src="<?= base_url(); ?>assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/global/plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"></script>
<script src="<?= base_url(); ?>assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url(); ?>assets/global/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?= base_url(); ?>assets/js/select2.min.js"></script>
<!-- <link href="<?= base_url(); ?>assets/css/select2.min.css" rel="stylesheet"> -->
<script type="text/javascript">
        var subTrcTypes = [];
        var columns;
        var updatedRecords = {};
        var val = [];
        var modal_status = '';

        $(function () {

            //modal
            get_modal_masuk();
            init_panel_<?= $panel_code; ?>();
            get_tr_<?= $panel_code; ?>();

            columns = [     { "data": "i" },
                            { "data": "office_name" },
                            { "data": "bank_acc" },
                            { "data": "bank_acc_to" },
                            { "data": "Amount1" },
                            { "data": "aktual" },
            ];

            get_datatable("<?= $panel_code; ?>", columns, "internal_transfers/get_transfer_masuk");

            $("#flowcheckall").on('click', function () {
            	// alert('checked')
            	$('[name="mydata[]"]').prop('checked', this.checked);
		    });

        });

        function get_data_datatable(data, j){
                for(var i = 0; i < data.length; i ++){
                        var item = data[i];

                        var oldId = item['TrcID'] + '_' + item['LineID'];
                        var oldRecord = {};
                        oldRecord['data'] = item;
                        oldRecord['status'] = 0;
                        oldRecords[oldId] = oldRecord;

                        // item['office'] = 'test';

                        if(item['MLedgerID'] == 2){
                                item['kas_from'] = get_kas_name(item['MLedgerID']);
                        }else{
                                item['kas_from'] = get_bank_name(item['SubLedger1ID']);
                        }

                        if(item['MLedgerIDTo'] == 2){
                                item['kas_to'] = get_kas_name(item['MLedgerIDTo']);
                        }else{
                                item['kas_to'] = get_bank_name(item['SubLedger1IDTo']);
                        }

                        item['aktual'] = '<input type="text" name="aktual[]" class="form-control text-right" value="'+item['Amount1']+'">';
                        item['i'] = '<input type="checkbox" id="checkall" name="mydata[]" value="'+oldId+'" data-id="'+item['LineID']+'" data-trc_id="'+item['TrcID']+'">';
                        
                        data[i] = item;
                }

                return data;
        }

        function get_modal_masuk(){
	        var $title = $("#title-type"),
	        title = "";
	        $('.control-btn').on('click', '#approve', function(){
	            if($('input[name="mydata[]"]:checked').length > 0){
	            	$('#internal_masuk-modal').modal();
	                title = "Setujui";
	                $title.html(title);

	                val = [];
	                modal_status = 'accept';
		        	$('input[name="mydata[]"]:checked').each(function(i){
				    	val[i] = {
				    		'TrcID': $(this).attr('data-trc_id'), 
				    		'LineID': $(this).attr('data-id'), 
				    		'Amount': $('input[name="aktual[]"]').val(), 
				    		'Description': null
				    	};
				    }).get();
	            }else{
	            	bootbox.alert("Silahkan pilih salah satu record");
	            }
	        });

	        $('.control-btn').on('click', '#reject', function(){
	            if($('input[name="mydata[]"]:checked').length > 0){
	                $('#internal_masuk-modal').modal();
	                title = "Tolak";
	                $title.html(title);
                	$('#internal_masuk-form').trigger("reset");

	                val = [];
	                modal_status = 'reject';
		        	$('input[name="mydata[]"]:checked').each(function(i){
				    	val[i] = {
				    		'TrcID': $(this).attr('data-trc_id'), 
				    		'LineID': $(this).attr('data-id'), 
				    		'Amount': $('input[name="aktual[]"]').val(), 
				    		'Description': null
				    	};
				    }).get();
				}else{
	            	bootbox.alert("Silahkan pilih salah satu record");
	            }
	        });
		}

		function post_data(){
			var validator = $("#internal_masuk-form").validate();
		    if(validator.form()){
		        var data_form = JSON.parse(JSON.stringify($("#internal_masuk-form").serializeArray()));

				for (var i = 0; i < val.length; i++) {
					val[i].Description = data_form[0].value;
				}

				$.post( base_url + $('#trc_link').val() +"/"+modal_status, {data : val}).done(function( results ) {
		                // console.log(results)
		                if (results.status == "1"){
		                       location.reload();
		                }else{
		                        bootbox.alert("Terjadi kesalahan saat memasukan data");
		                }
		        });
			}
		}

        function get_sub_trc(id){
                var strSubTrcTypes = '<?= json_encode($sub_trc_types); ?>';
                var item = '-';
                subTrcTypes = jQuery.parseJSON(strSubTrcTypes);

                for (var a = 0; a < subTrcTypes.length; a++) {
                        if(subTrcTypes[a]['C000_SysID'] == id){
                                item = subTrcTypes[a]['C011_Descr'];
                        }
                }

                return item;
        }

        function get_kas_name(id){
                var strKas = '<?= json_encode($ledger); ?>';
                var nama_kas = jQuery.parseJSON(strKas);

                var item = '-';

                for (var a = 0; a < nama_kas.length; a++) {
                        if(nama_kas[a]['SysID'] == id){
                                item = nama_kas[a]['Descr'];
                        }
                }

                return item;
        }

        function get_bank_name(id){
                var str = '<?= json_encode($banks_all); ?>';
                var val = jQuery.parseJSON(str);

                var item = '-';

                for (var a = 0; a < val.length; a++) {
                        if(val[a]['C000_SysID'] == id){
                                item = val[a]['C030_Descr']+' - '+val[a]['C010_BankAccNumber'];
                        }
                }

                return item;
        }

        function init_panel_<?= $panel_code; ?>() {
                var strSubTrcTypes = '<?= json_encode($sub_trc_types); ?>';
                subTrcTypes = jQuery.parseJSON(strSubTrcTypes);
                $('[name="sub_trc"]').val(subTrcTypes[0].C000_SysID);
        }

        function get_tr_<?= $panel_code; ?>() {
                trTemplate = $('.tr-template-<?= $panel_code; ?>').first().clone();
                $('.tr-template-<?= $panel_code; ?>').remove();
                $('#tbodyIntKeluar').append(trTemplate);
        }
</script>

<div class="tab-pane fade active in" panel-code="<?= $panel_code; ?>" sub_trc_types='<?= json_encode($sub_trc_types); ?>'>
		<div class="control-btn">
            <button id="approve" class="btn btn-sm btn-success"><i class="fa fa-check"></i> Disetujui</button>
            <button id="reject" class="btn btn-sm btn-danger"><i class="fa fa-close"></i> Ditolak</button>
		</div>
        <div class="row">
                <div class="col-md-12 p-0">    
                        <div class="panel">
                                <div class="panel-content">
                                        <table name="table-<?= $panel_code; ?>" class="table table-hover f-12">
                                                <thead>
                                                        <tr>
                                                                <th class="check"><input type="checkbox" id="flowcheckall" name="check" value="" /></th>
                                                                <th>Kantor</th>
                                                                <th>Asal</th>
                                                                <th>Tujuan</th>
                                                                <th>Jumlah</th>
                                                                <th>Aktual</th>
                                                        </tr>
                                                </thead>
                                                <tbody id="tbodyIntKeluar">
                                                        
                                                </tbody>
                                        </table>
                                </div>
                        </div>
                </div>
        </div>
</div>

<!-- Start Modal -->
<div class="modal fade" id="internal_masuk-modal">
        <div class="modal-dialog modal-md">
          <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
                  <h4 class="modal-title"><strong id="title-type"></strong> Transfer Masuk</h4>
                </div>
                <div class="modal-body p-t-0 p-b-0">
                        <div class="row">
                                <form class="col-md-12 form-horizontal" id="internal_masuk-form">
                                        <div class="col-md-12">
                                                <div class="form-group">
												  <label class="col-md-4 control-label required">Keterangan</label>
												  <div class="col-md-8">
													<textarea class="form-control" rows="4" name="deskripsi" required=""></textarea>
												  </div>
												</div>
                                        </div>
                                </form>
                        </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default btn-embossed" onclick="cancel_data('internal_masuk', 'Transfer Masuk')">Batal</button>
                  <button type="button" class="btn btn-primary btn-embossed" onclick="post_data()">Update</button>
                </div>
          </div>
        </div>
</div>
<!-- End Modal -->
