<?php
$panel_code = $panel['C010_Code'];
?>
<script src="<?= base_url(); ?>assets/global/plugins/jquery/jquery-1.11.1.min.js"></script>
<script src="<?= base_url(); ?>assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/global/plugins/select2/select2.min.js"></script>
<script src="<?= base_url(); ?>assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url(); ?>assets/global/plugins/jquery-validation/jquery.validate.min.js"></script>
<script type="text/javascript">
        var subTrcTypes = [];
        $(document).ready(function () {
                //modal
                $('.tab-pane').on('click', '#add-transaction', function(){
                        $('#transaction-modal').modal();
                });
                
                initNomor();
                change_kas();
                init_panel_<?= $panel_code; ?>();
                get_tr_<?= $panel_code; ?>();

                $('[name="table-<?= $panel_code; ?>"]').DataTable({
                        "processing": true,
                        "serverSide": true,
                        "ajax": {
                                "url": "<?= base_url(); ?>common/get_doc_day/<?= $panel_code; ?>",
                                "type": "GET",
                                "dataSrc" : function (json) {
                                        var data = json['data'];
                                        var j = json['start'];
                                        recordsInPage = [];
                                        for(var i = 0; i < data.length; i ++){
                                                var item = data[i];
                                                
                                                item['kas'] = Number(item['C072_Amount1']) - Number(item['C073_Amount2']);
                                                item['utang'] = item['kas'] - Number(item['C073_Amount3']);
                                                item['i'] = ++j + ('<input type="hidden" name="old" value="'+JSON.stringify(item)+'">');
                                                
                                                recordsInPage.push(item);
                                                data[i] = item;
                                        }
//                                        data[data.length] = data[0];
                                        return data;
                                },
                                },
                        "columns": [
                                { "data": "i" },
                                { "data": "C001_SubTrcTypeID" },
                                { "data": "C210_MLedgerID" },
                                { "data": "C072_Amount1" },
                                { "data": "C073_Amount2" },
                                { "data": "C073_Amount3" },
                                { "data": "utang" },
                                { "data": "kas" },
                                { "data": "-" },
                                { "data": "" },
                        ],
                });
                
                $('#button-test').click(function(){
                        $('#tbodyPlg').prepend('<tr role="row"><td class="sorting_1">2<input type="hidden" name="old" value="{" c010_trctypeid":1020,"c011_month":1608,"c012_trcid":109,"c050_rev":1,"c000_lineid":1,"c010_trcpanelid":4,"c001_subtrctypeid":18,"c050_docdate":"2016-08-31","c070_currencyid":1,"c070_currencyidfrom":"\u0000\u0000\u0000\u0000","c070_currencyidto":"\u0000\u0000\u0000\u0000","c071_rate":1,"c071_ratefrom":0,"c071_rateto":0,"c072_amount1":100100000,"c073_amount2":100100010,"c073_amount3":10,"c200_sledgerid":1,"c200_sledgerobjectid":null,"c210_mledgerid":2,"c211_subledger1id":0,"c212_subledger2id":1,"c213_subledger3id":0,"c214_subledger4id":0,"c215_description":"","c220_mledgeridto":0,"c201_sledgeridto":0,"c222_subledger1idto":0,"c223_subledger2idto":0,"c224_subledger3idto":0,"c225_subledger4idto":0,"c226_descriptionto":"","isremoved":0,"kas":-10,"utang":-20}"=""></td><td>18</td><td>2</td><td>100100000</td><td>100100010</td><td>10</td><td>-20</td><td>-10</td><td>10</td><td>0</td></tr>');
//                        var oTable = $('[name="table-<?= $panel_code; ?>"]').dataTable();
//                        oTable.fnDraw();
                });
        });

        function initNomor() {
            $('.nomor').each(function (i, obj) {
                $(obj).val().replace(/[^\d]/, '');
                $(obj).val(numeral($(obj).val()).format('0,0'));
            });
        }

        function init_panel_<?= $panel_code; ?>() {
                var strSubTrcTypes = '<?= json_encode($sub_trc_types); ?>';
                subTrcTypes = jQuery.parseJSON(strSubTrcTypes);
                console.log(subTrcTypes);
        }

        function get_tr_<?= $panel_code; ?>() {
                trTemplate = $('.tr-template-<?= $panel_code; ?>').first().clone();
                $('.tr-template-<?= $panel_code; ?>').remove();
                $('#tbodyPlg').append(trTemplate);
        }

        function get_bank(id){
                if(id == 3){
                      $('#data_bank').show();  
                }else{
                      $('#data_bank').hide();  
                }
        }

        function change_kas(){
                $('.form-group').on('change', '.change_kas', function(){
                        // alert('test')
                        var masuk = $('[name="masuk"]').val();
                        var keluar = $('[name="keluar"]').val();
                        var pendapatan = $('[name="pendapatan"]').val();
                        var kas = masuk - keluar;
                        var utang = kas - pendapatan;
                        
                        $('[name="kas"]').val(kas);
                        $('[name="utang"]').val(utang);
                        // initNomor();
                });
        }
        
        $(document).on('click','.simpan', function(){
                var validator = $("#transaction-form").validate();
                if(validator.form()){
                        var data_form = JSON.parse(JSON.stringify($("#transaction-form").serializeArray()));
                        $('#tbodyPlg').prepend('<tr role="row"><td class="sorting_1">-<input type="hidden" name="new" value="{" c010_trctypeid":1020,"c011_month":1608,"c012_trcid":109,"c050_rev":1,"c000_lineid":1,"c010_trcpanelid":4,"c001_subtrctypeid":'+data_form[0].value+',"c050_docdate":"2016-08-31","c070_currencyid":1,"c070_currencyidfrom":"\u0000\u0000\u0000\u0000","c070_currencyidto":"\u0000\u0000\u0000\u0000","c071_rate":1,"c071_ratefrom":0,"c071_rateto":0,"c072_amount1":'+data_form[4].value+',"c073_amount2":'+data_form[5].value+',"c073_amount3":'+data_form[6].value+',"c200_sledgerid":1,"c200_sledgerobjectid":null,"c210_mledgerid":'+data_form[2].value+',"c211_subledger1id":0,"c212_subledger2id":1,"c213_subledger3id":0,"c214_subledger4id":0,"c215_description":"","c220_mledgeridto":0,"c201_sledgeridto":0,"c222_subledger1idto":0,"c223_subledger2idto":0,"c224_subledger3idto":0,"c225_subledger4idto":0,"c226_descriptionto":"","isremoved":0,"kas":-10,"utang":-20}"=""></td><td>'+data_form[1].value+'</td><td>'+data_form[2].value+'</td><td>'+data_form[4].value+'</td><td>'+data_form[5].value+'</td><td>'+data_form[6].value+'</td><td>'+data_form[7].value+'</td><td>'+data_form[8].value+'</td><td>New</td><td><a class="edit btn btn-sm btn-primary" title="Edit" data-id="'.$trow->C000_SysID.'"><i class="glyphicon glyphicon-pencil"></i></a>
                  <a class="delete-virtual btn btn-sm btn-danger" title="Hapus" data-id="'.$trow->C000_SysID.'")"><i class="glyphicon glyphicon-trash"></i> </a></td></tr>');
                }
        });

</script>
<div class="tab-pane fade active in" panel-code="<?= $panel_code; ?>" sub_trc_types='<?= json_encode($sub_trc_types); ?>'>
        <button id="add-transaction" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah Transaksi</button>
        <div class="row">
                <div class="col-md-12 p-0">
                        <div class="panel">
                                <div class="panel-content">
                                        <!-- <button id="button-test" type="button">Click Me!</button> -->
                                        <table name="table-<?= $panel_code; ?>" class="table table-hover f-12">
                                                <thead>
                                                        <tr>
                                                                <th width="8%">#</th>
                                                                <th>Transaksi</th>	
                                                                <th>Kas/Bank</th>
                                                                <th>Masuk</th>
                                                                <th>Keluar</th>
                                                                <th>Pendapatan</th>
                                                                <th>Kas</th>
                                                                <th>Utang</th>
                                                                <th>Status</th>
                                                                <th>Action</th>
                                                        </tr>
                                                </thead>
                                                <tbody id="tbodyPlg">
                                                        
                                                </tbody>
                                        </table>
                                </div>
                        </div>
                </div>
        </div>
</div>

<!-- Start Modal -->
<div class="modal fade" id="transaction-modal">
        <div class="modal-dialog">
          <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
                  <h4 class="modal-title"><strong id="title-type"></strong> Transaksi Pelanggan</h4>
                </div>
                <div class="modal-body p-t-0 p-b-0">
                        <div class="row">
                                <form class="col-md-12 form-horizontal" id="transaction-form">
                                        <div class="col-md-12">
                                                <input type="hidden" name="transaction_id">
                                                <div class="form-group">
                                                  <label class="col-md-4 control-label required">Transaksi</label>
                                                  <div class="col-md-8">
                                                        <select class="form-control" name="transaksi" required="" data-search="true">
                                                                <option value="">-- Pilih Transaksi--</option>
                                                                <?php foreach($sub_trc_types as $row){
                                                                        echo '<option value="'.$row["C000_SysID"].'">'.$row["C011_Descr"].'</option>';
                                                                } ?>
                                                        </select>
                                                  </div>
                                                </div>

                                                <div class="form-group">
                                                  <label class="col-md-4 control-label required">Kas / Bank</label>
                                                  <div class="col-md-8">
                                                        <select class="form-control" name="kasbank" required="" onchange="javascript: get_bank($(this).val());" data-search="true">
                                                        <?php foreach($ledger as $row){
                                                                echo '<option value="'.$row["SysID"].'">'.$row["Descr"].'</option>';
                                                        } ?>
                                                        </select>
                                                  </div>
                                                </div>

                                                <div class="form-group" id="data_bank" style="display: none;">
                                                  <label class="col-md-4 control-label required">Bank</label>
                                                  <div class="col-md-8">
                                                        <select class="form-control" name="bank" required="" data-search="true">
                                                                <option value="">-- Pilih Akun Bank--</option>
                                                                <?php foreach($banks as $row){
                                                                        echo '<option value="'.$row["C000_SysID"].'">'.$row["C030_Descr"].' - '.$row["C010_BankAccNumber"].'</option>';
                                                                } ?>
                                                        </select>
                                                  </div>
                                                </div>

                                                <div class="form-group">
                                                  <label class="col-md-4 control-label required">Masuk</label>
                                                  <div class="col-md-8">
                                                        <input type="text" name="masuk" required="" class="change_kas form-control text-right">
                                                  </div>
                                                </div>

                                                <div class="form-group">
                                                  <label class="col-md-4 control-label required">Keluar</label>
                                                  <div class="col-md-8">
                                                        <input type="text" name="keluar" required="" class="change_kas form-control text-right">
                                                  </div>
                                                </div>

                                                <div class="form-group">
                                                  <label class="col-md-4 control-label required">Pendapatan</label>
                                                  <div class="col-md-8">
                                                        <input type="text" name="pendapatan" required="" class="change_kas form-control text-right">
                                                  </div>
                                                </div>

                                                <div class="form-group">
                                                  <label class="col-md-4 control-label required">Kas</label>
                                                  <div class="col-md-8">
                                                        <input type="text" name="kas" required="" class="form-control text-right" readonly="">
                                                  </div>
                                                </div>

                                                <div class="form-group">
                                                  <label class="col-md-4 control-label required">Utang</label>
                                                  <div class="col-md-8">
                                                        <input type="text" name="utang" required="" class="form-control text-right" readonly="">
                                                  </div>
                                                </div>
                                        </div>
                                </form>
                        </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="keluar btn btn-default btn-embossed">Batal</button>
                  <button type="button" class="simpan btn btn-primary btn-embossed">Simpan</button>
                </div>
          </div>
        </div>
</div>
<!-- End Modal -->