<div class="tab-pane fade" id="tab2_<?= $tab_id; ?>">
        <div class="row">
                <div class="col-md-6 sub-trc" <?= TRC_KEY_NAME_SUBTRCTYPE . '="9"'; ?>>
                        <h3><strong>KAS MASUK LAIN LAIN</strong></h3>
                        <form class="form-horizontal line">
                                <div class="col-md-12 form-field">
                                        <div id="dv_int_kasbank_masuk m-ledger" class="form-group">
                                                <label class="col-sm-3 control-label">Kas / Bank :</label>
                                                <div class="col-sm-9">
                                                        <select name="<?= TRC_INPUT_NAME_MLEDGER; ?>" class="form-control m-ledger-mutasi m-ledger input-post">
                                                                <option value="2">KAS</option>
                                                                <option value="3">BANK</option>
                                                        </select>
                                                </div>
                                        </div>
                                        <div id="dv_int_bank_masuk" class="form-group div-bank-name" style="display:none">
                                                <label class="col-sm-3 control-label">Bank :</label>
                                                <div class="col-sm-9">
                                                        <select name="bank_name" class="form-control bank-name">
                                                                <option value="0">-- Select --</option>
                                                                <?php foreach ($banks AS $key => $bank) { ?>
                                                                        <option value="<?= $key; ?>"><?= $key; ?></option>
                                                                <?php } ?>
                                                        </select>
                                                        <label style="display:none;" class="error">Mohon dipilih!</label>
                                                </div>
                                        </div>
                                        <div id="dv_int_bankacc_masuk" class="form-group div-bank-account" style="display:none">
                                                <label class="col-sm-3 control-label">Nomor Rek. :</label>
                                                <div class="col-sm-9">
                                                        <select name="<?= TRC_INPUT_NAME_SUBLEDGER1ID; ?>" class="form-control bank-account input-post">
                                                                <option value="0">-- Select --</option>
                                                        </select>
                                                        <label style="display:none;" class="error">Mohon dipilih!</label>
                                                </div>
                                        </div>
                                        <div class="form-group">
                                                <label class="col-sm-3 control-label">Jumlah</label>
                                                <div class="col-sm-9">
                                                        <input name="<?= TRC_INPUT_NAME_AMOUNT; ?>" type="text" minplus="1" class="form-control input-mutasi input-sm nomor text-right input-post">
                                                        <label style="display:none;" class="error">Input harus lebih besar dari nol!</label>
                                                </div>
                                        </div>
                                        <div class="form-group">
                                                <label class="col-sm-3 control-label">Keterangan</label>
                                                <div class="col-sm-9">
                                                        <textarea class="form-control input-post" rows="3" name="<?= TRC_INPUT_NAME_DESCRIPTION; ?>" aria-required="true"></textarea>
                                                </div>
                                        </div>
                                </div>
                        </form>
                        <div class="row">
                                <div class="col-md-12">
                                        <div class="panel">
                                                <div class="panel-content">
                                                        <button class="post-pendapatan btn-post btn btn-embossed btn-primary" type="button">POST</button>
                                                        <button class="cancel cancel-trc btn btn-embossed btn-default m-b-10 m-r-0" type="reset">Cancel</button>

                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
                <div class="col-md-6 sub-trc" <?= TRC_KEY_NAME_SUBTRCTYPE . '="10"'; ?>>
                        <h3><strong>KAS KELUAR LAIN-LAIN</strong></h3>
                        <form class="form-horizontal line">
                                <div class="col-md-12 form-field">
                                        <input type="hidden" name="<?= TRC_INPUT_NAME_SUBLEDGER2ID; ?>" class="input-get-balance" value="1" />
                                        <div id="dv_int_kasbank_keluar" class="form-group">
                                                <label class="col-sm-3 control-label">Kas / Bank :</label>
                                                <div class="col-sm-9">
                                                        <select name="<?= TRC_INPUT_NAME_MLEDGER; ?>" class="form-control m-ledger-mutasi m-ledger input-get-balance input-post">
                                                                <option value="2">KAS</option>
                                                                <option value="3">BANK</option>
                                                        </select>
                                                </div>
                                        </div>
                                        <div id="dv_int_bank_keluar" class="form-group div-bank-name" style="display:none">
                                                <label class="col-sm-3 control-label">Bank :</label>
                                                <div class="col-sm-9">
                                                        <select name="bank_name" class="form-control bank-name">
                                                                <option value="0">-- Select --</option>
                                                                <?php foreach ($banks AS $key => $bank) { ?>
                                                                        <option value="<?= $key; ?>"><?= $key; ?></option>
                                                                <?php } ?>
                                                        </select>
                                                        <label style="display:none;" class="error">Mohon dipilih!</label>
                                                </div>
                                        </div>
                                        <div id="dv_int_bankacc_keluar" class="form-group div-bank-account" style="display:none">
                                                <label class="col-sm-3 control-label">Nomor Rek. :</label>
                                                <div class="col-sm-9">
                                                        <select name="<?= TRC_INPUT_NAME_SUBLEDGER1ID; ?>" class="form-control bank-account input-get-balance input-post">
                                                                <option value="0">-- Select --</option>
                                                        </select>
                                                        <label style="display:none;" class="error">Mohon dipilih!</label>
                                                </div>
                                        </div>
                                        <div class="form-group">
                                                <label class="col-sm-3 control-label">Saldo</label>
                                                <div class="col-sm-9">
                                                        <input name="balance" type="text" class="form-control input-sm nomor text-right output-balance" readonly>
                                                </div>
                                        </div>
                                        <div class="form-group">
                                                <label class="col-sm-3 control-label">Jumlah</label>
                                                <div class="col-sm-9">
                                                        <input name="<?= TRC_INPUT_NAME_AMOUNT; ?>" type="text" minplus="-1" class="form-control input-mutasi input-sm nomor text-right input-post">
                                                        <label style="display:none;" class="error">Input harus lebih besar dari nol dan kurang sama dengan saldo!</label>
                                                </div>
                                        </div>
                                        <div class="form-group">
                                                <label class="col-sm-3 control-label">Keterangan</label>
                                                <div class="col-sm-9">
                                                        <textarea class="form-control input-post" rows="3" name="<?= TRC_INPUT_NAME_DESCRIPTION; ?>" aria-required="true"></textarea>
                                                </div>
                                        </div>
                                </div>
                        </form>
                        <div class="row">
                                <div class="col-md-12">
                                        <div class="panel">
                                                <div class="panel-content">
                                                        <button class="post-belanja btn-post btn btn-embossed btn-primary" type="button">POST</button>
                                                        <button class="cancel cancel-trc btn btn-embossed btn-default m-b-10 m-r-0" type="reset">Cancel</button>

                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>	
</div>