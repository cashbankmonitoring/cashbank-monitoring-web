<?php
$panel_code = $panel['C010_Code'];
$panel_id = $panel['C000_SysID'];
?>
<script src="<?= base_url(); ?>assets/js/jquery-3.1.1.min.js"></script>
<script src="<?= base_url(); ?>assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/global/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?= base_url(); ?>assets/global/plugins/datatables/jquery.dataTables.min.js"></script>


<script type="text/javascript">
        var subTrcTypes = [];
        var columns;
        var updatedRecords = {};

        $(function () {
                //modal
                get_modal('pelanggan', '#tbodyPlg');
                // change_kas();
                init_panel_<?= $panel_code; ?>();
                get_tr_<?= $panel_code; ?>();
                
                columns = [     { "data": "i" },
                                { "data": "transaksi" },
                                { "data": "nama_kas" },
                                { "data": "masuk" },
                                { "data": "keluar" },
                                { "data": "pendapatan" },
                                { "data": "kas" },
                                { "data": "utang" },
                                { "data": "status" },
                                { "data": "action" },
                ];

                get_datatable("<?= $panel_code; ?>", columns);

        });

        function get_data_datatable(data, j){
                for(var i = 0; i < data.length; i ++){
                        var item = data[i];

                        var oldId = item['TrcID'] + '_' + item['LineID'];
                        var oldRecord = {};
                        oldRecord['data'] = item;
                        oldRecord['status'] = 0;
                        oldRecords[oldId] = oldRecord;
                        
                        if(item['MLedgerID'] == 2){
                                item['nama_kas'] = get_kas_name(item['MLedgerID']);
                        }else{
                                item['nama_kas'] = get_bank_name(item['SubLedger1ID']);
                        }

                        item['transaksi'] = get_sub_trc(item['SubTrcTypeID']);
                        var masuk = Number(item['Amount1']);
                        var kas = Number(item['Amount2']);
                        var keluar = Number(item['Amount1']) - kas;
                        var utang = Number(item['Amount3']);
                        var pendapatan = kas - utang;

                        item['masuk'] = change_format_number(masuk);
                        item['kas'] = change_format_number(kas);
                        item['keluar'] = change_format_number(keluar);
                        item['utang'] = change_format_number(utang);
                        item['pendapatan'] = change_format_number(pendapatan);
                        item['action'] = '<a class="edit-pelanggan btn btn-sm btn-primary" title="Edit" data-id="'+oldId+'" data-status="0"><i class="glyphicon glyphicon-pencil" /></a><a class="btn btn-sm btn-danger" title="Hapus" data-id="'+oldId+'" data-status="0" onclick="delete_data(\'#tbodyPlg\',\''+oldId+'\', 0);"><i class="glyphicon glyphicon-trash" /></a>';
                        item['status'] = '<span class="label label-success">Tersimpan</span>';
                        item['i'] = ++j;
                        
                        data[i] = item;
                }

                return data;
        }

        function get_sub_trc(id){
                var strSubTrcTypes = '<?= json_encode($sub_trc_types); ?>';
                var item = '-';
                subTrcTypes = jQuery.parseJSON(strSubTrcTypes);

                for (var a = 0; a < subTrcTypes.length; a++) {
                        if(subTrcTypes[a]['C000_SysID'] == id){
                                item = subTrcTypes[a]['C011_Descr'];
                        }
                }

                return item;
        }

        function get_kas_name(id){
                var strKas = '<?= json_encode($ledger); ?>';
                var nama_kas = jQuery.parseJSON(strKas);

                var item = '-';

                for (var a = 0; a < nama_kas.length; a++) {
                        if(nama_kas[a]['SysID'] == id){
                                item = nama_kas[a]['Descr'];
                        }
                }

                return item;
        }

        function get_bank_name(id){
                var str = '<?= json_encode($banks); ?>';
                var val = jQuery.parseJSON(str);

                var item = '-';

                for (var a = 0; a < val.length; a++) {
                        if(val[a]['C000_SysID'] == id){
                                item = val[a]['C030_Descr']+' - '+val[a]['C010_BankAccNumber'];
                        }
                }

                return item;
        }

        function init_panel_<?= $panel_code; ?>() {
                var strSubTrcTypes = '<?= json_encode($sub_trc_types); ?>';
                subTrcTypes = jQuery.parseJSON(strSubTrcTypes);
        }

        function get_tr_<?= $panel_code; ?>() {
                trTemplate = $('.tr-template-<?= $panel_code; ?>').first().clone();
                $('.tr-template-<?= $panel_code; ?>').remove();
                $('#tbodyPlg').append(trTemplate);
        }

        function get_bank(id){
                if(id == 3){
                    $('#data_bank').show();  
                }else{
                    $('#data_bank').hide();  
                    $('select[name="bank"]').val("");   
                }
        }

        // function change_kas(){
        //     $('.form-group').on('keyup', '.change_kas', function(){
        //         $('.format_number').each(function (i, obj) {
        // //                console.log(obj);
        //                 $(obj).val().replace(/[^\d]/, '');
        //                 $(obj).val(numeral($(obj).val()).format('0,0'));
        //         });
        //             var masuk = $('[name="masuk"]').val().replace(/,/g, "");
        //             var keluar = $('[name="keluar"]').val().replace(/,/g, "");
        //             var pendapatan = $('[name="pendapatan"]').val().replace(/,/g, "");
        //             var kas = masuk - keluar;
        //             var utang = kas - pendapatan;
                    
        //             $('[name="kas"]').val(kas);
        //             $('[name="utang"]').val(utang);
        //     });
        // }

        function get_item(data_form){
                var kas_name = '-';
                if(data_form[2].value == 2){
                        kas_name = get_kas_name(data_form[2].value);
                }else{
                        kas_name = get_bank_name(data_form[3].value);
                }

                var item = {
                        "TrcPanelID":<?= $panel_id; ?>,
                        "SubTrcTypeID":data_form[1].value,
                        "Amount1":data_form[4].value,
                        "Amount2":data_form[7].value,
                        "Amount3":data_form[8].value,
                        "MLedgerID":data_form[2].value,
                        "SubLedger1ID":data_form[3].value,
                        "transaksi": get_sub_trc(data_form[1].value),
                        "nama_kas": kas_name,
                        "kas":data_form[7].value,
                        "utang":data_form[8].value,
                        "pendapatan":data_form[6].value,
                        "keluar":data_form[5].value,
                        "masuk":data_form[4].value,
                        "i":'-'
                };

            return item;
        }

        function get_update_record(item, status){
                var updatedRecord = {};
                updatedRecord['status'] = status;
                updatedRecord['data'] = item;

                return updatedRecord;
        }

        function get_delete_item(id){
                var kas_name = '-';
                if(oldRecords[id]['data']['MLedgerID'] == 2){
                        kas_name = get_kas_name(oldRecords[id]['data']['MLedgerID']);
                }else{
                        kas_name = get_bank_name(oldRecords[id]['data']['SubLedger1ID']);
                }

                var item = {
                        "TrcPanelID":<?= $panel_id; ?>,
                        "SubTrcTypeID":oldRecords[id]['data']['SubTrcTypeID'],
                        "Amount1":oldRecords[id]['data']['Amount1'],
                        "Amount2":oldRecords[id]['data']['Amount2'],
                        "Amount3":oldRecords[id]['data']['Amount3'],
//                        "Amount3":oldRecords[id]['data']['kas'] - oldRecords[id]['data']['utang'],
                        "MLedgerID":oldRecords[id]['data']['MLedgerID'],
                        "SubLedger1ID":oldRecords[id]['data']['SubLedger1ID'],
                        "transaksi": get_sub_trc(oldRecords[id]['data']['SubTrcTypeID']),
                        'nama_kas': kas_name,
                        "masuk":oldRecords[id]['data']['masuk'],
                        "keluar":oldRecords[id]['data']['keluar'],
                        "pendapatan":oldRecords[id]['data']['pendapatan'],
                        "kas":oldRecords[id]['data']['kas'],
                        "utang":oldRecords[id]['data']['utang'],
                        "i":'-'
                };

            return item;
        }

        $(document).on('click','.edit-pelanggan', function(){
                var id = $(this).attr('data-id');
                var status = $(this).attr('data-status');
                var data_form = {};
                if(status == 0){
                        data_form = oldRecords[id]['data'];
                }else{
                        data_form = updatedRecords[id]['data'];
                }

                $('[name="pelanggan_id"]').val(id);
                $('select[name="transaksi"]').val(data_form.SubTrcTypeID);
                $('select[name="kasbank"]').val(data_form.MLedgerID);

                if(data_form.MLedgerID == 2 || data_form.MLedgerID == 0){
                    $('#data_bank').hide();
                    $('select[name="bank"]').val("");   
                }else{
                        $('#data_bank').show();  
                    $('select[name="bank"]').val(data_form.SubLedger1ID);     
                }

                $('[name="masuk"]').val(data_form.masuk);
                $('[name="keluar"]').val(data_form.keluar);
//                var pendapatan = data_form.kas - data_form.utang;
                $('[name="pendapatan"]').val(data_form.pendapatan);
                $('[name="kas"]').val(data_form.kas);
                $('[name="utang"]').val(data_form.utang);

        });

        $(document).on('keyup', '.change_kas', function(){
                var masuk = $('[name="masuk"]').val().replace(/,/g, "");
                var keluar = $('[name="keluar"]').val().replace(/,/g, "");
                var pendapatan = $('[name="pendapatan"]').val().replace(/,/g, "");
                var kas = masuk - keluar;
                var utang = kas - pendapatan;
                
                $('[name="kas"]').val(kas);
                $('[name="utang"]').val(utang);
                initNomor();
        });
</script>
<div class="tab-pane fade active in" panel-code="<?= $panel_code; ?>" sub_trc_types='<?= json_encode($sub_trc_types); ?>'>
        <div class="control-btn">
                <button class="btn btn-sm btn-default post-all" onclick="post_all_data();">Post Semua</button>
                <button id="add-pelanggan" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Transaksi Pelanggan</button>
        </div>
        <div class="row">
                <div class="col-md-12 p-0">
                        <div class="panel">
                                <div class="panel-content">
                                        <!-- <button id="button-test" type="button">Click Me!</button> -->
                                        <table name="table-<?= $panel_code; ?>" class="table table-hover f-12">
                                                <thead>
                                                        <tr>
                                                                <th width="8%">#</th>
                                                                <th>Transaksi</th>	
                                                                <th>Kas/Bank</th>
                                                                <th>Masuk</th>
                                                                <th>Keluar</th>
                                                                <th>Pendapatan</th>
                                                                <th>Kas</th>
                                                                <th>Utang</th>
                                                                <th>Status</th>
                                                                <th>Action</th>
                                                        </tr>
                                                </thead>
                                                <tbody id="tbodyPlg">
                                                        
                                                </tbody>
                                        </table>
                                </div>
                        </div>
                </div>
        </div>
</div>

<!-- Start Modal -->
<div class="modal fade" id="pelanggan-modal">
        <div class="modal-dialog">
          <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
                  <h4 class="modal-title"><strong id="title-type"></strong> Transaksi Pelanggan</h4>
                </div>
                <div class="modal-body p-t-0 p-b-0">
                        <div class="row">
                                <form class="col-md-12 form-horizontal" id="pelanggan-form">
                                        <div class="col-md-12">
                                                <input type="hidden" name="pelanggan_id">
                                                <div class="form-group">
                                                  <label class="col-md-4 control-label required">Transaksi</label>
                                                  <div class="col-md-8">
                                                        <select class="form-control" name="transaksi" required="">
                                                                <option value="">-- Pilih Transaksi--</option>
                                                                <?php foreach($sub_trc_types as $row){
                                                                        echo '<option value="'.$row["C000_SysID"].'">'.$row["C011_Descr"].'</option>';
                                                                } ?>
                                                        </select>
                                                  </div>
                                                </div>

                                                <div class="form-group">
                                                  <label class="col-md-4 control-label required">Kas / Bank</label>
                                                  <div class="col-md-8">
                                                        <select class="form-control" name="kasbank" required="" onchange="javascript: get_bank($(this).val());" data-search="true">
                                                        <?php foreach($ledger as $row){
                                                                echo '<option value="'.$row["SysID"].'">'.$row["Descr"].'</option>';
                                                        } ?>
                                                        </select>
                                                  </div>
                                                </div>

                                                <div class="form-group" id="data_bank" style="display: none;">
                                                  <label class="col-md-4 control-label required">Bank</label>
                                                  <div class="col-md-8">
                                                        <select class="form-control" name="bank" required="" data-search="true">
                                                                <option value="">-- Pilih Akun Bank--</option>
                                                                <?php foreach($banks as $row){
                                                                        echo '<option value="'.$row["C000_SysID"].'">'.$row["C030_Descr"].' - '.$row["C010_BankAccNumber"].'</option>';
                                                                } ?>
                                                        </select>
                                                  </div>
                                                </div>

                                                <div class="form-group">
                                                  <label class="col-md-4 control-label required">Masuk</label>
                                                  <div class="col-md-8">
                                                        <input type="text" name="masuk" required="" class="change_kas nomor form-control text-right">
                                                  </div>
                                                </div>

                                                <div class="form-group">
                                                  <label class="col-md-4 control-label required">Keluar</label>
                                                  <div class="col-md-8">
                                                        <input type="text" name="keluar" required="" class="change_kas nomor form-control text-right">
                                                  </div>
                                                </div>

                                                <div class="form-group">
                                                  <label class="col-md-4 control-label required">Pendapatan</label>
                                                  <div class="col-md-8">
                                                        <input type="text" name="pendapatan" required="" class="change_kas nomor form-control text-right">
                                                  </div>
                                                </div>

                                                <div class="form-group">
                                                  <label class="col-md-4 control-label required">Kas</label>
                                                  <div class="col-md-8">
                                                        <input type="text" name="kas" required="" class="form-control nomor text-right" readonly="">
                                                  </div>
                                                </div>

                                                <div class="form-group">
                                                  <label class="col-md-4 control-label required">Utang</label>
                                                  <div class="col-md-8">
                                                        <input type="text" name="utang" required="" class="form-control nomor text-right" readonly="">
                                                  </div>
                                                </div>
                                        </div>
                                </form>
                        </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default btn-embossed"onclick="cancel_data('pelanggan', 'Transaksi Pelanggan')">Batal</button>
                  <button type="button" class="btn btn-primary btn-embossed" onclick="add_data('pelanggan', '#tbodyPlg')">Tambahkan</button>
                </div>
          </div>
        </div>
</div>
<!-- End Modal -->
