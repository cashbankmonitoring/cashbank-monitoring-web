<?php
require_once(APPPATH . 'models/Base_model.php');
class Internal_Transfer_Status extends Base_Model{
        function __construct() {
                parent::__construct();
                $this->TABLE = "T026_InternalTransferStatus";
        }
        
        function generate_doc_day(&$rekaps) {
                $where = "(TrcID = {$rekaps[0]['TrcID']} AND  LineID = {$rekaps[0]['LineID']})";
                for ($i = 1; $i < count($rekaps); $i++) {
                        $rekap = $rekaps[$i];

                        $where .= " OR ";
                        $where .= "(TrcID = {$rekap['TrcID']} AND  LineID = {$rekap['LineID']})";
                }

                $internal_transfer_list = $this->get($where);
                $internal_transfer_map = array();
                foreach ($internal_transfer_list as $internal_transfer) {
                        $key = $internal_transfer['TrcID'] . '_' . $internal_transfer['LineID'];
                        $tmp_internal_transfer['TransferStatus'] = 
                                $internal_transfer['Status'] ?: 0;

                        $internal_transfer_map[$key] = $tmp_internal_transfer;
                }

                foreach ($rekaps as $key => $rekap) {
                        $key1 = $rekap['TrcID'] . '_' . $rekap['LineID'];
                        // print_r($key1);
                        if(isset($internal_transfer_map[$key1])){
                                $internal_transfer = $internal_transfer_map[$key1];
                                $rekap = array_merge($rekap, $internal_transfer);
                        }
                        
                        $rekaps[$key] = $rekap;
                }

        }
}
