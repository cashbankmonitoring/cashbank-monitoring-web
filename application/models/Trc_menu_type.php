<?php

require_once(APPPATH . 'models/Base_model.php');

class Trc_menu_type extends Base_Model {

        function __construct() {
                parent::__construct();
                $this->TABLE = "T024_1TrcType";
        }

        function get($where = null) {
                $this->db->select('*');
                $this->db->from('T024_1TrcType');
                // $this->db->join('T010_Partner', 'T010_Partner.SysID = T023_UserGroup.C030_PartnerID', 'left');
                if ($where != NULL) {
                        $this->db->where($where);
                }
                $this->db->order_by("C012_LineID", "ASC");
                return $this->db->get();
        }

        function get_panel($where = null) {
                $this->db->select('*');
                $this->db->from('T024_2TrcPanel');
                // $this->db->join('T010_Partner', 'T010_Partner.SysID = T023_UserGroup.C030_PartnerID', 'left');
                if ($where != NULL) {
                        $this->db->where($where);
                }
                return $this->db->get();
        }

        function get_panel_menu($where = null, $order = null) {
                $this->db->select('
				T024_2TrcPanel.*,
				T024_1TrcType.C013_Label AS menu_label,
				T024_1TrcType.C016_ParentID AS menu_parent,
			');
                $this->db->from('T024_2TrcPanel');
                $this->db->join('T024_1TrcType', 'T024_1TrcType.C000_SysID = T024_2TrcPanel.C010_TrcTypeID', 'left');
                if ($where != NULL) {
                        $this->db->where($where);
                }

                if ($order != NULL) {
                        $this->db->order_by($order, 'ASC');
                }

                return $this->db->get();
        }

        function insert($data) {
                // $data['C050_UserPasswd'] = $this->get_hash($data['C030_UserName'], $data['C050_UserPasswd']);
                $query = $this->db->insert('T024_1TrcType', $data);
                return $query;
        }

        function insert_panel($data) {
                $query = $this->db->insert('T024_TrcPanelType', $data);
                return $query;
        }

        function insert_panel_master($data) {
                $query = $this->db->insert('T024_2TrcPanel', $data);
                return $query;
        }

        function insert_sub_panel($data) {
                $query = $this->db->insert('T024_SubTrcPanelType', $data);
                return $query;
        }

        function get_last_id() {
                $this->db->select("C000_SysID");
                $this->db->from("T024_1TrcType");
                $this->db->limit("1");
                $this->db->order_by("C000_SysID", "DESC");
                $query = $this->db->get()->row_array();
                return $query['C000_SysID'];
        }

        function get_last_panel_id() {
                $this->db->select("C000_SysID");
                $this->db->from("T024_2TrcPanel");
                $this->db->limit("1");
                $this->db->order_by("C000_SysID", "DESC");
                $query = $this->db->get()->row_array();
                return $query['C000_SysID'];
        }

        function get_trc_sub_panel($where = null) {
                $this->db->select("T024_SubTrcPanelType.*");
                $this->db->from("T024_SubTrcPanelType");
                // $this->db->join('T024_1TrcType', 'T024_1TrcType.C000_SysID = T024_TrcPanelType.TrcTypeID', 'left');
                if ($where != NULL) {
                        $this->db->where($where);
                }
                return $this->db->get();
        }

        function get_trc_panel($where = null) {
                $this->db->select("T024_TrcPanelType.*");
                $this->db->from("T024_TrcPanelType");
                $this->db->join('T024_1TrcType', 'T024_1TrcType.C000_SysID = T024_TrcPanelType.TrcTypeID', 'left');
                if ($where != NULL) {
                        $this->db->where($where);
                }
                return $this->db->get();
        }

        function get_sub_panel($where = null) {
                $this->db->select('*');
                $this->db->from('T024_3SubTrcType');
                // $this->db->join('T010_Partner', 'T010_Partner.SysID = T023_UserGroup.C030_PartnerID', 'left');
                if ($where != NULL) {
                        $this->db->where($where);
                }
                return $this->db->get();
        }

        function delete_panel_master($id) {
                $this->db->where('C000_SysID', $id);
                return $this->db->delete('T024_2TrcPanel');
        }

        function delete_sub_trc_panel($where) {
                $this->db->where($where);
                return $this->db->delete('T024_SubTrcPanelType');
        }

        function delete_panel($where) {
                $this->db->where($where);
                return $this->db->delete('T024_TrcPanelType');
        }

        function update($id, $data) {
                // if($data['password'] != NULL){
                // 	$data['password'] = $this->get_hash($data['username'], $data['password']);
                // }
                $this->db->where('C000_SysID', $id);
                return $this->db->update('T024_1TrcType', $data);
        }

        function update_panels($id, $data) {
                // if($data['password'] != NULL){
                // 	$data['password'] = $this->get_hash($data['username'], $data['password']);
                // }
                $this->db->where('C000_SysID', $id);
                return $this->db->update('T024_2TrcPanel', $data);
        }

        function delete($id) {
                $this->db->where('C000_SysID', $id);
                return $this->db->delete('T024_1TrcType');
        }

        function delete_by_type($id) {
                $this->db->where('TrcTypeID', $id);
                return $this->db->delete('T024_TrcPanelType');
        }

}
