<?php

require_once(APPPATH . 'models/Base_model.php');

class T024_Sub_Trc_Type extends Base_Model {
        
        function __construct() {
                parent::__construct();
                $this->TABLE = "T024_3SubTrcType";
        }
        
        function get_by_id($id){
                return $this->get_one(array(
                    'C000_SysID' => $id,
                ));
        }
}

