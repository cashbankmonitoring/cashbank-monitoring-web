<?php
require_once(APPPATH . 'models/Base_model.php');
class Partner extends Base_Model {
	function __construct() {
		parent::__construct();
		$this->TABLE = "T010_Partner";
	}
	
	function get($where = NULL){
		$this->db->select('*');
		$this->db->from('T010_Partner');
		if($where != NULL){
			$this->db->where($where);
		}
		$this->db->order_by('SysID','ASC');
		return $this->db->get();
	}
	
	function add($data){
		$query = $this->db->insert('T010_Partner', $data);
		// $this->db->insert();
		return $query;
	}
	
	function edit($id, $data){
		$this->db->where('SysID', $id);
		$this->db->update('T010_Partner', $data);
		return $id;
	}
	
	function delete($id){
		$this->db->where('SysID', $id);
		return $this->db->delete('T010_Partner');
	}
	function get_data($criteria='',$order='',$order_by='',$limit='',$start='',$offset=0){
		$this->db->select('*');
		$this->db->from('T010_Partner');
		if($criteria != ''){
			$this->db->where($criteria);
		}
		if($criteria!='')
		$this->db->where($criteria);
		// if($order!='')
		// $this->db->order_by($order,$order_by);
		if($limit!='')
		$this->db->limit($limit,$start);

		$this->db->order_by('SysID','ASC');
		return $this->db->get();

	}
	
}