<?php

require_once(APPPATH . 'models/Base_model.php');

class Day_Delta extends Base_Model {

        function __construct() {
                parent::__construct();
                $this->TABLE = "T610_DayDelta";
        }
        
        function add_amount($where, $amount_difference = 0){
                $where['SubLedger1ID'] = isset($where['SubLedger1ID']) ? $where['SubLedger1ID'] : 0;
                $where['SubLedger2ID'] = isset($where['SubLedger2ID']) ? $where['SubLedger2ID'] : 0;
                $where['SubLedger3ID'] = isset($where['SubLedger3ID']) ? $where['SubLedger3ID'] : 0;
                $where['SubLedger4ID'] = isset($where['SubLedger4ID']) ? $where['SubLedger4ID'] : 0;
                $where['datePeriod'] = $this->get_curr_date();
                
                $old = $this->get_one($where);
                if($old){
                        $amount = $old['Amount'] + $amount_difference;
                        $this->update($where, array(
                            'Amount' => $amount,
                        ));
                }else{
                        $data = $where;
                        $data['Amount'] = $amount_difference;
                        $this->insert($data);
                }
        }
        
        function update_amount($where, $amount = 0){
                $this->load->model('day_pos');
                
                $where['SubLedger1ID'] = isset($where['SubLedger1ID']) ? $where['SubLedger1ID'] : 0;
                $where['SubLedger2ID'] = isset($where['SubLedger2ID']) ? $where['SubLedger2ID'] : 0;
                $where['SubLedger3ID'] = isset($where['SubLedger3ID']) ? $where['SubLedger3ID'] : 0;
                $where['SubLedger4ID'] = isset($where['SubLedger4ID']) ? $where['SubLedger4ID'] : 0;
                $where['datePeriod'] = $this->get_curr_date();
                
                $where_day_pos = $where;
                unset($where_day_pos['datePeriod']);
                $day_pos = $this->day_pos->get_one($where_day_pos);
                
                $prev_amount = $day_pos ? $day_pos['Amount'] : 0;
                $delta_amount = $amount - $prev_amount;
                
                $old = $this->get_one($where);
                if($old){
                        $this->update($where, array(
                            'Amount' => $delta_amount,
                        ));
                }else{
                        $data = $where;
                        $data['Amount'] = $delta_amount;
                        $this->insert($data);
                }
        }

}
