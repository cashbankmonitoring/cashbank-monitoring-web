<?php
class Debt extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function get($where=null, $like=null){
		$this->db->select('*');
		$this->db->from('T030_MLedger');

		if($where != null){
			$this->db->where($where);
		}

		if($like != null){
			$this->db->like('Code', $like);
		}
		return $this->db->get();	
	}
	
}