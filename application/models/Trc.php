<?php

require_once(APPPATH . 'models/Base_model.php');

class Trc extends Base_Model {

        function __construct() {
                parent::__construct();
                $this->TABLE = "T610_Trc";
        }

        function get_latest($where) {
                $this->db->select('*');
                $this->db->from($this->TABLE);
                $this->db->where($where);
                $this->db->limit(1);
                $this->db->order_by('C015_DatePos DESC, C016_TimePost DESC');

                $q = $this->db->get();

                if ($q->num_rows() > 0) {
                        return $q->row_array();
                }

                return NULL;
        }

        function get_intransfer($criteria = "") {
                $this->db->select_sum("AmountInTrc");
                $this->db->from($this->TABLE);
                $this->db->where($criteria);
                $q = $this->db->get();
                if ($q->num_rows() > 0) {
                        return $q->row_array();
                }
                return NULL;
        }

        function get_saldo($criteria = "") {
                $this->db->select_sum("AmountTrc");
                $this->db->from($this->TABLE);
                $this->db->where($criteria);
                $this->db->group_by("SubLedger1ID");
                //  GROUP BY
                // // dbo.T610_Trc.SubLedger1ID");
                $q = $this->db->get();
                if ($q->num_rows() > 0) {
                        return $q->row_array();
                }
                return NULL;
        }

        function get_all_latest() {
                $this->db->select('*');
                $this->db->from($this->TABLE);
                $this->db->order_by('C015_DatePos DESC, C016_TimePost DESC');

                $m_trc_s_all = $this->db->get()->result_array();
                $map_trc_s = array();
                $m_trc_s = array();
                foreach ($m_trc_s_all as $key => $m_trc) {
                        $key1 = $m_trc['SLedgerID']
                                . '-'
                                . $m_trc['MLedgerID']
                                . '-'
                                . $m_trc['SubLedger1ID']
                                . '-'
                                . $m_trc['SubLedger2ID']
                                . '-'
                                . $m_trc['SubLedger3ID'];
                        if (!isset($map_trc_s[$key1])) {
                                $map_trc_s[$key1] = 1;
                                $m_trc_s[] = $m_trc;
                        }
                }

                return $m_trc_s;
        }

        function save_from_rekap($crud_action, $header, $rekap = NULL /* new rekap */, $old_rekap = NULL) {
                $CI = & get_instance();

                $this->load->model('map_to_ledger');
                $this->load->model('sub_trc_type');

                $CI->load->model('office');
                $this->office = $CI->office;

                $CI->load->model('t510_rekap');
                $this->t510_rekap = $CI->t510_rekap;

                $CI->load->model('m_ledger');
                $this->m_ledger = $CI->m_ledger;

                $CI->load->model('day_delta');
                $this->day_delta = $CI->day_delta;

                $CI->load->model('day_pos');
                $this->day_pos = $CI->day_pos;

                if ($crud_action == CRUD_INSERT) {
                        $map_to_ledgers = $this->get_map_to_ledger_list(
                                $rekap['C001_SubTrcTypeID']);
                        foreach ($map_to_ledgers AS $key => $map_to_ledger) {
                                $this->insert_from_rekap($header, $rekap, $map_to_ledger);
                        }
                } elseif ($crud_action == CRUD_UPDATE || $crud_action == CRUD_DELETE) {
                        $old_map_to_ledgers = $this->get_map_to_ledger_list(
                                $old_rekap['C001_SubTrcTypeID']);
                        foreach ($old_map_to_ledgers AS $key => $map_to_ledger) {
                                $this->delete_from_rekap($header, $old_rekap, $map_to_ledger);
                        }
                        if ($crud_action == CRUD_UPDATE) {
                                $new_map_to_ledgers = $this->get_map_to_ledger_list(
                                        $rekap['C001_SubTrcTypeID']);
                                foreach ($new_map_to_ledgers AS $key => $map_to_ledger) {
                                        $this->insert_from_rekap($header, $rekap, $map_to_ledger);
                                }
                        }
                }
        }

        private function insert_from_rekap($header, $rekap, $map_to_ledger) {
                $trc = $this->generate_trc_data($rekap, $header, $map_to_ledger);

                $this->insert($trc);
//print_r($trc);
                // Update day pos
                $where_columns_balance = array(
                    'MLedgerID',
                    'SLedgerID',
                    'SubLedger1ID',
                    'SubLedger2ID',
                    'SubLedger3ID',
                    'SubLedger4ID',
                );
                $where_balance = array();
                foreach ($where_columns_balance as $key => $where_column_balance) {
                        $where_balance[$where_column_balance] = $trc[$where_column_balance];
                }
                $amount_difference = $trc['AmountInTrc'] - $trc['AmountOutTrc'];

                $this->day_delta->add_amount($where_balance, $amount_difference);
        }

        private function delete_from_rekap($header, $old_rekap, $map_to_ledger) {
                $old_rekap['C072_Amount1'] = -1 * $old_rekap['C072_Amount1'];
                $old_rekap['C073_Amount2'] = -1 * $old_rekap['C073_Amount2'];
                $old_rekap['C073_Amount3'] = -1 * $old_rekap['C073_Amount3'];
                $old_rekap['IsRemoved'] = 1;

                $this->insert_from_rekap($header, $old_rekap, $map_to_ledger);

                $this->update(
                        array(// WHERE
                    'C000_TrcID' => $old_rekap['C012_TrcID'],
                    'C012_LineID' => $old_rekap['C000_LineID'],
                    'C013_MapperID' => $map_to_ledger['C000_SysID'],
                        ), array(// DATA
                    'IsRemoved' => 1,
                        )
                );
        }

        private function generate_trc_data($rekap, $header, $map_to_ledger) {
                $trc['C010_TrcTypeID'] = $rekap['C010_TrcTypeID'];
                $trc['C011_Month'] = $rekap['C011_Month'];
                $trc['C000_TrcID'] = $rekap['C012_TrcID'];
                $trc['C050_Rev'] = $rekap['C050_Rev'];
                $trc['C012_LineID'] = $rekap['C000_LineID'];
                $trc['C001_SubTrcTypeID'] = $rekap['C001_SubTrcTypeID'];
                $trc['IsRemoved'] = $rekap['IsRemoved'];

                // Fill date
                $trc['C014_MonthPostIdx'] = $this->get_curr_month();
                $trc['YearIdx'] = $this->trc->get_curr_year();
                $trc['MonthIdx'] = $this->trc->get_curr_month();
                $trc['C015_DatePos'] = $this->trc->get_curr_date();
                $trc['C016_TimePost'] = $this->trc->get_curr_time();
                $trc['C050_AccountDate'] = $this->get_curr_datetime();

                // Fill data from header
                $trc['C050_DocNum'] = $header['DocNum'];
                $trc['C050_DocDate'] = $header['DocDate'];

                // Fill data from t510_rekap object
                $columns_from_rekap = array(
                    'MLedgerID',
                    'SLedgerID',
                    'SubLedger1ID',
                    'SubLedger2ID',
                    'SubLedger3ID',
                    'SubLedger4ID',
                    'Description',
                );
                $rekap_columns = array();
                foreach ($columns_from_rekap as $key => $val) {
                        $rekap_column = $this->t510_rekap->get_column_name_by_suffix($val . $map_to_ledger['MLedgerSetIdx']);
                        $trc[$val] = $rekap[$rekap_column];

                        $rekap_columns[] = $rekap_column;
                }

                if (!$trc['SLedgerID']) {
                        $trc['SLedgerID'] = $rekap['C200_SLedgerID'];
                }

                $m_ledger = $this->m_ledger->get_by_id($trc['MLedgerID']);

                // Generate sledger
                $trc['SLedgerID'] = $this->get_sledger_id($trc['SLedgerID'], $m_ledger['SGTypeID']);

                // Fill amount data
                $amount_field = $map_to_ledger['AmountField'];
                $amount = $rekap[$amount_field] * /* ($m_ledger['isHutang'] ? -1 : 1) * */ $map_to_ledger['C020_PlusMinus'];
//                $trc['MinPlus'] = ($m_ledger['isHutang'] ? -1 : 1) * ($rekap[$amount_field] >= 0 ? 1 : -1);
                $trc['AmountInTrc'] = 0;
                $trc['AmountOutTrc'] = 0;
                if ($amount > 0) {
                        $trc['AmountInTrc'] = $amount;
                } elseif ($amount < 0) {
                        $trc['AmountOutTrc'] = abs($amount);
                }
                $where_columns_balance = array(
                    'MLedgerID',
                    'SLedgerID',
                    'SubLedger1ID',
                    'SubLedger2ID',
                    'SubLedger3ID',
                    'SubLedger4ID',
                );
                $where_balance = array();
                foreach ($where_columns_balance as $key => $where_column_balance) {
                        $where_balance[$where_column_balance] = $trc[$where_column_balance];
                }
                $balance = $this->get_balance($where_balance) + $amount;
                $trc['AmountTrc'] = abs($balance);
                $trc['MinPlus'] = $balance >= 0 ? 1 : -1;

                // Fill data from map_to_ledger
                $trc['C013_MapperID'] = $map_to_ledger['C000_SysID'];
                if ($map_to_ledger['C021_MLedgerID']) {
                        $trc['MLedgerID'] = $map_to_ledger['C021_MLedgerID'];
                }
//                print_r($trc);die();
                return $trc;
        }

        private function get_sledger_id($office_id, $sg_type_id = 0) {
                if (!$sg_type_id) {
                        return $office_id;
                }

                if ($sg_type_id == 1) {
                        if (!isset($this->office_pusat_id)) {
                                $office_pusat = $this->office->get_pusat($office_id);
                                $this->office_pusat_id = $office_pusat ? $office_pusat['C000_SysID'] : $office_id;
                        }

                        return $this->office_pusat_id;
                }
                if ($sg_type_id >= 2) {
                        $arg = 'SELECT T020_SettlementGroup.*, T020_SGMember.OfficeID AS OfficeMemberID'
                                . ' FROM T020_SettlementGroup '
                                . ' LEFT JOIN T020_SGMember '
                                . ' ON T020_SettlementGroup.SysID = T020_SGMember.GroupID '
                                . ' WHERE T020_SettlementGroup.TypeID = ' . $sg_type_id
                                . ' AND ('
                                . ' T020_SettlementGroup.OfficeID = ' . $office_id
                                . ' OR '
                                . ' T020_SGMember.OfficeID = ' . $office_id
                                . ' ) ';
                        $query = $this->db->query($arg);

                        return $query->num_rows() > 0 ? $query->row_array()['OfficeID'] : $office_id;
                }
        }

        private function get_balance($where) {
                unset($where['datePeriod']);
                $day_pos = $this->day_pos->get_one($where);

                $where['datePeriod'] = $this->get_curr_date();
                $day_delta = $this->day_delta->get_one($where);

                $balance_pos = $day_pos ? $day_pos['Amount'] : 0;
                $balance_delta = $day_delta ? $day_delta['Amount'] : 0;

                $balance = $balance_pos + $balance_delta;

                return $balance;
        }

        private function get_map_to_ledger_list($SubTrcTypeID) {
                $code = $this->sub_trc_type->get_code($SubTrcTypeID);
                $map_to_ledgers = $this->map_to_ledger->get(array(
                    'C011_SubTrcTypeCode' => $code,
                ));
                
                return $map_to_ledgers;
        }

}
