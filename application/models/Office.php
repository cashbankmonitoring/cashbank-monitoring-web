<?php

require_once(APPPATH . 'models/Base_model.php');

class Office extends Base_Model {
	function __construct() {
		parent::__construct();
                $this->TABLE = "T020_Office";
	}
	
	function get($criteria='',$order='',$order_by='',$limit='',$start='',$offset=0){
		$this->db->select('*');
		$this->db->from('T020_Office');
		if($criteria != ''){
			$this->db->where($criteria);
		}
		if($limit!='')
		$this->db->limit($limit,$start);

		if($order != ''){
			$this->db->order_by($order);
		}else{
			$this->db->order_by('C000_SysID','ASC');
		}
		return $this->db->get();

	}

	function get_parent_only($criteria=''){
		$this->db->select('C000_SysID, C001_ParentID');
		$this->db->from('T020_Office');
		if($criteria != ''){
			$this->db->where($criteria);
		}

		return $this->db->get();

	}
	function get_form_day_delta($where = null){
		$this->db->select("SLedgerID");
		$this->db->from('T610_DayDelta');
		if($where != null){
			$this->db->where($where);
		}
		$this->db->limit(1);
		return $this->db->get();
	}

	function add($data){
		$query = $this->db->insert('T020_Office', $data);
		return $query;
	}
	
	function update($id, $data){
		$this->db->where('C000_SysID', $id);
		$this->db->update('T020_Office', $data);
		return $id;
	}
	
	function delete($id){
		$this->db->where('C000_SysID', $id);
		return $this->db->delete('T020_Office');
	}

	function get_bank_office($where = null){
		$this->db->select('*');
		$this->db->from('T022_KBAccount');
		$this->db->join('T020_Office', 'T022_KBAccount.C001_OfficeID = T020_Office.C000_SysID', 'left');
		if($where != null){
			$this->db->where($where);
		}
		$this->db->order_by('T022_KBAccount.C020_Name','ASC');

		return $this->db->get();	
	}
	function search($q, $where = null){
		$this->db->select('*, C000_SysID as id');
		$this->db->from('T020_Office');

		if($where != null){
            $this->db->where($where);
		}
                $this->db->like('C020_Descr', $q);
		$this->db->order_by('C020_Descr','ASC');
                
		return $this->db->get()->result_array();

	}
        
        function get_pusat($child_id){
                return $this->get_parent($child_id, 1);
        }
        
        function get_parent($child_id, $parent_level_id = NULL){
                if($parent_level_id !== NULL && $parent_level_id < 1){
                        $parent_level_id = 1;
                }
                
                $this->db->select('*');
		$this->db->from('T020_Office');
                $this->db->where("C000_SysID", $child_id);
                $q_child = $this->db->get();
                
                $child = NULL;
                if($q_child->num_rows() > 0){
                        $child = $q_child->row_array();
                }else{
                        return NULL;
                }
                
                if($parent_level_id == NULL){
                        if($child['C012_Level'] == 1){
                                return $this->get_parent($child['C000_SysID'], 1);
                        }else{
                                return $this->get_parent($child['C001_ParentID'], $child['C012_Level']-1);
                        }
                        
                }
                
                if($child['C012_Level'] == $parent_level_id){
                        return $child;
                }elseif($parent_level_id < $child['C012_Level']){
                        return $this->get_parent($child['C001_ParentID'], $parent_level_id);
                }else{
                        return NULL;
                }
        }
        
        function get_kprk_by_pusat($pusat_office_id, $keywords = ""){
                $q_select_regional_ids = "SELECT C000_SysID FROM T020_Office WHERE C001_ParentID = " . $pusat_office_id;
                
                $q_select_kprk = "SELECT * FROM T020_Office WHERE "
                        . " C001_ParentID IN (" . $q_select_regional_ids . ")";
                if($keywords){
                        $q_select_kprk .= " AND C020_Descr LIKE '%".$keywords."%'";
                }
                
                return $this->db->query($q_select_kprk)->result_array();
        }
        
        function get_childs_by_parent($parent_office_id, $keywords = ""){
                $q = "SELECT * FROM T020_Office WHERE C001_ParentID = " . $parent_office_id;
                
                if($keywords){
                        $q .= " AND C020_Descr LIKE '%".$keywords."%'";
                }
                
                return $this->db->query($q)->result_array();
        }

    function add_export($data){
    	$check = $this->get_one(array('C010_Code' => $data['C010_Code']));
    	$query = 1;
    	if(!$check){
			$query = $this->db->insert('T020_Office', $data);
    	}
		return $query;
	}
        
}