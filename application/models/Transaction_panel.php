<?php

require_once(APPPATH . 'models/Base_model.php');

class Transaction_panel extends Base_Model {

        function __construct() {
                parent::__construct();
                $this->TABLE = "T024_2TrcPanel";
        }

        function get_panel($where = null) {
                $this->db->select('T024_2TrcPanel.*');
                $this->db->from('T024_2TrcPanel');
                
                if ($where != null) {
                        $this->db->where($where);
                }

                return $this->db->get();
        }
}
