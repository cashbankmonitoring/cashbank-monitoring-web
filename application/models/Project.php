<?php

require_once(APPPATH . 'models/Base_model.php');

class Project extends Base_Model {
        
	function __construct() {
                parent::__construct();
                $this->TABLE = "T005_Project";
        }
	
        function get_transaksi_program($office_id){
                $this->db->select($this->TABLE . '.*, T027_ProgramAlocation.C030_Alokasi AS Alokasi, T610_DayPos.Amount AS Amount');
		$this->db->from($this->TABLE);
                $this->db->join('T027_ProgramAlocation', '(T027_ProgramAlocation.C020_ProjectID = '.$this->TABLE.'.C000_SysID'
                        .' AND T027_ProgramAlocation.C010_OfficeID = '.$office_id.')', 'left');
                $this->db->join('T610_DayPos', 
                        '(T610_DayPos.SubLedger3ID = '.$this->TABLE.'.C000_SysID'
                        .' AND T610_DayPos.SLedgerID = '.$office_id
                        .' AND T610_DayPos.MLedgerID = 19'
                        .' AND T610_DayPos.SubLedger2ID = 2)', 'left');
                return $this->db->get()->result_array();
        }
        
	function get($where = null){
		$this->db->select('*');
		$this->db->from('T005_Project');
		$this->db->join('T010_Partner', 'T010_Partner.SysID = T005_Project.C030_PartnerID', 'left');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}
	
	function add($data) {
		// $data['C050_UserPasswd'] = $this->get_hash($data['C030_UserName'], $data['C050_UserPasswd']);
		$query = $this->db->insert('T005_Project', $data);
		return $query;
	}
	
	function update($id,$data) {
		// if($data['password'] != NULL){
		// 	$data['password'] = $this->get_hash($data['username'], $data['password']);
		// }
		$this->db->where('C000_SysID',$id);
		return $this->db->update('T005_Project', $data);
	}

	function delete($id){
		$this->db->where('C000_SysID',$id);
		return $this->db->delete('T005_Project');
	}


	function get_data($criteria='',$order='',$order_by='',$limit='',$start='',$offset=0){
		$this->db->select('T005_Project.*,T010_Partner.*');
		$this->db->from('T005_Project');
		
		if($criteria != ''){
			$this->db->where($criteria);
		}
		if($criteria!='')
		$this->db->where($criteria);
		// if($order!='')
		// $this->db->order_by($order,$order_by);

		$this->db->join('T010_Partner', 'T010_Partner.SysID = T005_Project.C030_PartnerID', 'left');
		if($limit!='')
		$this->db->limit($limit,$start);
		//$this->db->order_by('T023_User.C000_SysID','ASC');
		return $this->db->get();

	}
}
