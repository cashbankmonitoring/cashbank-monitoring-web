<?php

require_once(APPPATH . 'models/Base_model.php');

class Map_To_Ledger extends Base_Model {

        function __construct() {
                parent::__construct();
                $this->TABLE = "T024_4MapToLedger";
        }

        function get_by_sub_trc_type_id($sub_trc_type_id) {
                $this->db->select($this->TABLE . '.*, '
                        . 'CAST(CASE 
                  WHEN SUBSTRING("T030_MLedger"."Code", 1, 2) = \'20\'  
                     THEN 1 
                  ELSE 0 END AS bit) as "isHutang", '
                        . 'T030_MLedger.Code, T030_MLedger.Descr, T030_MLedger.MAcctLevel, T030_MLedger.TypeML');
                $this->db->from($this->TABLE);
                $this->db->join('T030_MLedger', 'T030_MLedger.SysID = ' . $this->TABLE . '.C021_MLedgerID', 'LEFT');
                $this->db->where($this->TABLE . '.C011_SubTrcTypeID', $sub_trc_type_id);
                $result = $this->db->get()->result_array();

                return $result;
        }

}
