<?php

require_once(APPPATH . 'models/Base_model.php');

class M_Ledger extends Base_Model {

        function __construct() {
                parent::__construct();
                $this->TABLE = "T030_MLedger";
        }

        function get_by_id($id) {
                $this->db->select('*, CAST(CASE 
                  WHEN SUBSTRING("T030_MLedger"."Code", 1, 2) = \'20\'  
                     THEN 1 
                  ELSE 0 END AS bit) as "isHutang"');
                $this->db->from($this->TABLE);
                $this->db->where('SysID', $id);
                $q = $this->db->get();

                if ($q->num_rows() > 0) {
                        return $q->row_array();
                }
                return NULL;
        }

        function get_by_codes($codes) {
                $this->db->select('*');
                $this->db->from($this->TABLE);
                $this->db->where_in('Code', $codes);
                return $this->db->get()->result_array();
        }

}
