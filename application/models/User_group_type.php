<?php

require_once(APPPATH . 'models/Base_model.php');

class User_group_type extends Base_Model {

        function __construct() {
                parent::__construct();
                $this->TABLE = "T023_UserGroupType";
        }


        function get($where = null, $order = null){
		$this->db->select('*');
		$this->db->from('T023_UserGroupType');
		// $this->db->join('T010_Partner', 'T010_Partner.SysID = T023_UserGroupType.C030_PartnerID', 'left');
		if($where != NULL){
			$this->db->where($where);
		}

		if($order != NULL){
			$this->db->order_by($order, 'DESC');
		}
		return $this->db->get();
		}
		
		function add($data) {
			// $data['C050_UserPasswd'] = $this->get_hash($data['C030_UserName'], $data['C050_UserPasswd']);
			$query = $this->db->insert('T023_UserGroupType', $data);
			return $query;
		}
		
		function update($id,$data) {
			// if($data['password'] != NULL){
			// 	$data['password'] = $this->get_hash($data['username'], $data['password']);
			// }
			$this->db->where('SysID',$id);
			return $this->db->update('T023_UserGroupType', $data);
		}

		function delete($id){
			$this->db->where('SysID',$id);
			return $this->db->delete('T023_UserGroupType');
		}
		function get_last_id(){
			$this->db->select("SysID");
			$this->db->from("T023_UserGroupType");
			$this->db->limit("1");
			$this->db->order_by("SysID","DESC");
			$query = $this->db->get()->row_array();
			return $query['SysID'];
		}
	
	function add_export($data){
    	$check = $this->get_one(array('UserGroupTypeName' => $data['UserGroupTypeName']));
    	$query = 1;
    	if(!$check){
			$query = $this->db->insert('T023_UserGroupType', $data);
    	}
		return $query;
	}
}
