<?php
class Transaction extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function get($select=null, $where=null, $group_by=null, $sum=null){
		if($select != null){
			$this->db->select($select);
		}else{
			$this->db->select('*');
		}
		
		if($sum != null){
			$this->db->select_sum($sum);
		}

		$this->db->from('T610_Trc');

		if($where != null){
			$this->db->where($where);
		}

		if($group_by != null){
			$this->db->group_by($group_by);
		}

		return $this->db->get();	
	}

	function add($data){
		$this->db->insert('T610_Trc',$data);
		return 1;
	}
	
}