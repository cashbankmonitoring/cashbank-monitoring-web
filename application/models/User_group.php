<?php

require_once(APPPATH . 'models/Base_model.php');

class User_Group extends Base_Model {

        function __construct() {
                parent::__construct();
                $this->TABLE = "T023_UserGroup";
        }

        function get_panel($where = NULL, $order = NULL) {
                $this->db->distinct();
                $this->db->select('T024_2TrcPanel.C010_TrcTypeID');
                $this->db->from('T023_UserGroup');
                $this->db->join('T024_2TrcPanel', 'T024_2TrcPanel.C000_SysID = T023_UserGroup.TrcPanelID', 'left');
                // $this->db->join('T024_1TrcType', 'T024_1TrcType.C000_SysID = T024_2TrcPanel.C010_TrcTypeID', 'left');

                if ($where) {
                        $this->db->where($where);
                }

                if ($order) {
                        $this->db->order_by($order);
                }

                // $this->db->group_by('T024_2TrcPanel.C010_TrcTypeID');

                return $this->db->get();
        }

        function get_trc_type($where = NULL, $order = NULL) {
                $this->db->select('T023_UserGroup.*, T024_1TrcType.*');
                $this->db->from('T023_UserGroup');
                $this->db->join('T024_2TrcPanel', 'T024_2TrcPanel.C000_SysID = T023_UserGroup.TrcPanelID', 'left');
                $this->db->join('T024_1TrcType', 'T024_1TrcType.C000_SysID = T024_2TrcPanel.C010_TrcTypeID', 'left');

                if ($where) {
                        $this->db->where($where);
                }

                if ($order) {
                        $this->db->order_by($order);
                }
                return $this->db->get();
        }

        function get_trc_type_all($where = NULL) {
                $this->db->select('C000_SysID AS TrcTypeID, *');
                $this->db->from('T024_1TrcType');

                if ($where) {
                        $this->db->where($where);
                }

                $this->db->order_by('C012_LineID', 'ASC');
                return $this->db->get();
        }

        function get($where = null, $order = null) {
                $this->db->select('*');
                $this->db->from('T023_UserGroup');
                // $this->db->join('T010_Partner', 'T010_Partner.SysID = T023_UserGroup.C030_PartnerID', 'left');
                if ($where != NULL) {
                        $this->db->where($where);
                }

                if ($order != NULL) {
                        $this->db->order_by($order, 'asc');
                }
               
                return $this->db->get();
        }

        function add($data) {
                // $data['C050_UserPasswd'] = $this->get_hash($data['C030_UserName'], $data['C050_UserPasswd']);
                $query = $this->db->insert('T023_UserGroup', $data);
                return $query;
        }

        function update($id, $data) {
                // if($data['password'] != NULL){
                // 	$data['password'] = $this->get_hash($data['username'], $data['password']);
                // }
                $this->db->where('SysID', $id);
                return $this->db->update('T023_UserGroup', $data);
        }

        function delete($where) {
                $this->db->where($where);
                return $this->db->delete('T023_UserGroup');
        }

        function deleteByType($id) {
                $this->db->where('UserGroupTypeID', $id);
                return $this->db->delete('T023_UserGroup');
        }

        function get_trc_panel($UserGroupTypeID, $TrcTypeCode) {
                $this->db->select('T024_2TrcPanel.*');
                $this->db->from($this->TABLE);
                $this->db->join('T024_1TrcType', 'T024_1TrcType.C000_SysID = T023_UserGroup.TrcTypeID', 'LEFT');
                $this->db->join('T024_2TrcPanel', 'T024_2TrcPanel.C000_SysID = T023_UserGroup.TrcPanelID', 'LEFT');

                $this->db->where($this->TABLE . '.UserGroupTypeID', $UserGroupTypeID);
                $this->db->where('T024_1TrcType.C010_Code', $TrcTypeCode);
                $this->db->order_by('T024_2TrcPanel.C013_LineID', 'ASC');

                return $this->db->get()->result_array();
        }

}
