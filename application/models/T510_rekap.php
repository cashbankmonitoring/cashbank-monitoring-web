<?php

require_once(APPPATH . 'models/Base_model.php');

class T510_Rekap extends Base_Model {

        function __construct() {
                parent::__construct();
                $this->TABLE = "T510_Rekap";
        }

        function get_rekap_delta($where = null) {
                $this->db->select("T510_Rekap.*, T610_DayDelta.Amount, ROW_NUMBER() OVER (ORDER BY T510_Rekap.C200_SLedgerID) AS RowNum");
                $this->db->from('T510_Rekap');
                $this->db->join('T610_DayDelta', 'T610_DayDelta.SLedgerID = T510_Rekap.C200_SLedgerID AND T610_DayDelta.SubLedger1ID = T510_Rekap.C211_SubLedger1ID AND T610_DayDelta.SubLedger2ID = T510_Rekap.C212_SubLedger2ID AND T610_DayDelta.MLedgerID = T510_Rekap.C210_MLedgerID', 'left');

                if ($where != null) {
                        $this->db->where($where);
                }

                $this->db->order_by('RowNum', 'desc');

                return $this->db->get();
        }

        function add($data) {
                $this->db->insert('T510_Rekap', $data);
                return 1;
        }

        function get_q510_rekap_kas_bank($where = null, $or_where = null) {
                $this->db->select('*');
                $this->db->from('Q510_RekapKasBank');
                if ($where != null) {
                        $this->db->where($where);
                }

                if ($or_where != null) {
                        $this->db->or_where($or_where);
                }
                return $this->db->get();
        }

        function get_map_to_ledger($where = null) {
                $this->db->select('*');
                $this->db->from('T024_4MapToLedger');
                if ($where != null) {
                        $this->db->where($where);
                }
                return $this->db->get();
        }

        function get_histori_transfer_internal($office_id, $is_transfer_masuk = TRUE) {
                // Query untuk mendapatkan money in transfer dengan tujuan $office_id
                $this->db->select($this->TABLE . '.*,'
                        . 'T030_2VirtualAccount.C011_Descr AS VirtualAccountTo, '
                        . 'T022_KBAccount.C010_BankAccNumber AS BankAccountTo, '
                        . 'T022_KBAccount.C020_Name AS BankNameTo, '
                        . 'T022_KBAccount.C030_Descr AS BankDescrTo');
                $this->db->from($this->TABLE);
                $this->db->join('T022_KBAccount', 'T022_KBAccount.C000_SysID = ' . $this->TABLE . '.C222_SubLedger1IDTo', 'LEFT');
                $this->db->join('T030_2VirtualAccount', 'T030_2VirtualAccount.C000_SysID = ' . $this->TABLE . '.C223_SubLedger2IDTo', 'LEFT');
                $this->db->where('C210_MLedgerID', 10);
                if ($is_transfer_masuk) {
                        $this->db->where('C201_SLedgerIDTo', $office_id);
                        $this->db->where('C214_SubLedger4ID', 0); // PENDING TRANSFER
                } else {
                        $this->db->where('C200_SLedgerID', $office_id);
                }
                $this->db->order_by('C012_TrcID', 'DESC');
                $this->db->order_by('C000_LineID', 'DESC');

                $m_rekap_list = $this->db->get()->result_array();

                if (!$m_rekap_list) {
                        return array();
                }

                $TrcIDs = array();
                foreach ($m_rekap_list as $key => $m_rekap) {
                        $TrcIDs[] = $m_rekap['C012_TrcID'];
                }

                // Query untuk mendapatkan pengirim dengan transaksi id $TrcIDs
                $this->db->select($this->TABLE . '.*, '
                        . 'T030_2VirtualAccount.C011_Descr AS VirtualAccountFrom, '
                        . 'T020_Office.C020_Descr AS OfficeNameFrom, '
                        . 'T022_KBAccount.C010_BankAccNumber AS BankAccountFrom, '
                        . 'T022_KBAccount.C020_Name AS BankNameFrom, '
                        . 'T022_KBAccount.C030_Descr AS BankDescrFrom');
                $this->db->from($this->TABLE);
                $this->db->join('T020_Office', 'T020_Office.C000_SysID = ' . $this->TABLE . '.C200_SLedgerID', 'LEFT');
                $this->db->join('T022_KBAccount', 'T022_KBAccount.C000_SysID = ' . $this->TABLE . '.C211_SubLedger1ID', 'LEFT');
                $this->db->join('T030_2VirtualAccount', 'T030_2VirtualAccount.C000_SysID = ' . $this->TABLE . '.C212_SubLedger2ID', 'LEFT');
                $this->db->where($this->TABLE . '.C210_MLedgerID !=', 10);
                $this->db->where_in($this->TABLE . '.C012_TrcID', $TrcIDs);
                $this->db->order_by('C012_TrcID', 'DESC');
                $this->db->order_by('C000_LineID', 'DESC');

                $m_rekap_from_list = $this->db->get()->result_array();

                foreach ($m_rekap_list as $key => $m_rekap) {
                        $m_rekap_from = $m_rekap_from_list[$key];

                        $m_rekap['MLedgerIDFrom'] = $m_rekap_from['C210_MLedgerID'];
                        $m_rekap['SLedgerIDFrom'] = $m_rekap_from['C200_SLedgerID'];
                        $m_rekap['SubLedger1IDFrom'] = $m_rekap_from['C211_SubLedger1ID'];
                        $m_rekap['SubLedger2IDFrom'] = $m_rekap_from['C212_SubLedger2ID'];
                        $m_rekap['SubLedger3IDFrom'] = $m_rekap_from['C213_SubLedger3ID'];
                        $m_rekap['SubLedger4IDFrom'] = $m_rekap_from['C214_SubLedger4ID'];

                        $m_rekap['OfficeNameFrom'] = $m_rekap_from['OfficeNameFrom'];
                        $m_rekap['BankAccountFrom'] = $m_rekap_from['BankAccountFrom'];
                        $m_rekap['BankNameFrom'] = $m_rekap_from['BankNameFrom'];
                        $m_rekap['BankDescrFrom'] = $m_rekap_from['BankDescrFrom'];
                        $m_rekap['VirtualAccountFrom'] = $m_rekap_from['VirtualAccountFrom'];

                        // Algoritma untuk menghilangkan prefix, misal C210_MLedgerID jadi MLedgerID
                        foreach ($m_rekap AS $key1 => $val) {
                                $key_parts = explode('_', $key1);
                                $new_key1 = count($key_parts) > 1 ? $key_parts[1] : $key_parts[0];
                                $m_rekap[$new_key1] = $m_rekap[$key1];
                                if ($new_key1 != $key1) {
                                        unset($m_rekap[$key1]);
                                }
                        }

                        $m_rekap_list[$key] = $m_rekap;
                }

                return $m_rekap_list;
        }

        function get_transfer_masuk_internal($office_id) {
                // Query untuk mendapatkan money in transfer dengan tujuan $office_id
                $this->db->select($this->TABLE . '.*,'
                        . 'T030_2VirtualAccount.C011_Descr AS VirtualAccountTo, '
                        . 'T022_KBAccount.C010_BankAccNumber AS BankAccountTo, '
                        . 'T022_KBAccount.C020_Name AS BankNameTo, '
                        . 'T022_KBAccount.C030_Descr AS BankDescrTo');
                $this->db->from($this->TABLE);
                $this->db->join('T022_KBAccount', 'T022_KBAccount.C000_SysID = ' . $this->TABLE . '.C222_SubLedger1IDTo', 'LEFT');
                $this->db->join('T030_2VirtualAccount', 'T030_2VirtualAccount.C000_SysID = ' . $this->TABLE . '.C223_SubLedger2IDTo', 'LEFT');
                $this->db->where('C210_MLedgerID', 10);
                $this->db->where('C201_SLedgerIDTo', $office_id);
                $this->db->where('C214_SubLedger4ID', 0);
                $this->db->order_by('C012_TrcID', 'DESC');
                $this->db->order_by('C000_LineID', 'DESC');

                $m_rekap_list = $this->db->get()->result_array();

                if (!$m_rekap_list) {
                        return array();
                }

                $TrcIDs = array();
                foreach ($m_rekap_list as $key => $m_rekap) {
                        $TrcIDs[] = $m_rekap['C012_TrcID'];
                }

                // Query untuk mendapatkan pengirim dengan transaksi id $TrcIDs
                $this->db->select($this->TABLE . '.*, '
                        . 'T030_2VirtualAccount.C011_Descr AS VirtualAccountFrom, '
                        . 'T020_Office.C020_Descr AS OfficeNameFrom, '
                        . 'T022_KBAccount.C010_BankAccNumber AS BankAccountFrom, '
                        . 'T022_KBAccount.C020_Name AS BankNameFrom, '
                        . 'T022_KBAccount.C030_Descr AS BankDescrFrom');
                $this->db->from($this->TABLE);
                $this->db->join('T020_Office', 'T020_Office.C000_SysID = ' . $this->TABLE . '.C200_SLedgerID', 'LEFT');
                $this->db->join('T022_KBAccount', 'T022_KBAccount.C000_SysID = ' . $this->TABLE . '.C211_SubLedger1ID', 'LEFT');
                $this->db->join('T030_2VirtualAccount', 'T030_2VirtualAccount.C000_SysID = ' . $this->TABLE . '.C212_SubLedger2ID', 'LEFT');
                $this->db->where($this->TABLE . '.C210_MLedgerID !=', 10);
                $this->db->where_in($this->TABLE . '.C012_TrcID', $TrcIDs);
                $this->db->order_by('C012_TrcID', 'DESC');
                $this->db->order_by('C000_LineID', 'DESC');

                $m_rekap_from_list = $this->db->get()->result_array();

                foreach ($m_rekap_list as $key => $m_rekap) {
                        $m_rekap_from = $m_rekap_from_list[$key];

                        $m_rekap['MLedgerIDFrom'] = $m_rekap_from['C210_MLedgerID'];
                        $m_rekap['SLedgerIDFrom'] = $m_rekap_from['C200_SLedgerID'];
                        $m_rekap['SubLedger1IDFrom'] = $m_rekap_from['C211_SubLedger1ID'];
                        $m_rekap['SubLedger2IDFrom'] = $m_rekap_from['C212_SubLedger2ID'];
                        $m_rekap['SubLedger3IDFrom'] = $m_rekap_from['C213_SubLedger3ID'];
                        $m_rekap['SubLedger4IDFrom'] = $m_rekap_from['C214_SubLedger4ID'];

                        $m_rekap['OfficeNameFrom'] = $m_rekap_from['OfficeNameFrom'];
                        $m_rekap['BankAccountFrom'] = $m_rekap_from['BankAccountFrom'];
                        $m_rekap['BankNameFrom'] = $m_rekap_from['BankNameFrom'];
                        $m_rekap['BankDescrFrom'] = $m_rekap_from['BankDescrFrom'];
                        $m_rekap['VirtualAccountFrom'] = $m_rekap_from['VirtualAccountFrom'];

                        // Algoritma untuk menghilangkan prefix, misal C210_MLedgerID jadi MLedgerID
                        foreach ($m_rekap AS $key1 => $val) {
                                $key_parts = explode('_', $key1);
                                $new_key1 = count($key_parts) > 1 ? $key_parts[1] : $key_parts[0];
                                $m_rekap[$new_key1] = $m_rekap[$key1];
                                if ($new_key1 != $key1) {
                                        unset($m_rekap[$key1]);
                                }
                        }

                        $m_rekap_list[$key] = $m_rekap;
                }

                return $m_rekap_list;
        }

        function get_intransfer_balance($where = "") {
                $this->db->select_sum("C073_Amount3");
                $this->db->from($this->TABLE);

                $where['C214_SubLedger4ID'] = 0;
                $where['C210_MLedgerID'] = 10;
                $this->db->where($where);

                $q = $this->db->get();
                $data = $q->row_array();

                return 0 + $data['C073_Amount3'];
        }

        function get_mledger($where) {
                $this->db->select("
                        C010_TrcTypeID as TrcTypeID,
                        C011_Month as Month,
                        C012_TrcID as TrcID,
                        C050_Rev as Rev,
                        C000_LineID as LineID,
                        C010_TrcPanelID as TrcPanelID,
                        C001_SubTrcTypeID as SubTrcTypeID,
                        C050_DocDate as DocDate,
                        C070_CurrencyID as CurrencyID,
                        C070_CurrencyIDFrom as CurrencyIDFrom,
                        C070_CurrencyIDTo as CurrencyIDTo,
                        C071_Rate as Rate,
                        C071_RateFrom as RateFrom,
                        C071_RateTo as RateTo,
                        C072_Amount1 as Amount1,
                        C073_Amount2 as Amount2,
                        C073_Amount3 as Amount3,
                        C200_SLedgerID as SLedgerID,
                        C200_SLedgerObjectID as SLedgerObjectID,
                        C210_MLedgerID as MLedgerID,
                        C211_SubLedger1ID as SubLedger1ID,
                        C212_SubLedger2ID as SubLedger2ID,
                        C213_SubLedger3ID as SubLedger3ID,
                        C214_SubLedger4ID as SubLedger4ID,
                        C215_Description as Description,
                        C220_MLedgerIDTo as MLedgerIDTo,
                        C201_SLedgerIDTo as SLedgerIDTo,
                        C222_SubLedger1IDTo as SubLedger1IDTo,
                        C223_SubLedger2IDTo as SubLedger2IDTo,
                        C224_SubLedger3IDTo as SubLedger3IDTo,
                        C225_SubLedger4IDTo as SubLedger4IDTo,
                        C226_DescriptionTo as DescriptionTo,
                        IsRemoved,
                        T030_MLedger.Descr
                ");
                $this->db->from('T510_Rekap');
                $this->db->join('T030_MLedger', 'T030_MLedger.SysID = T510_Rekap.C210_MLedgerID', 'left');

                if ($where != null) {
                        $this->db->where($where);
                }

                $this->db->order_by('T510_Rekap.C050_DocDate', 'DESC');

                return $this->db->get();
        }

        function save_from_field($crud_action, $header, $record) {
                $this->load->model('rekap_to_field');
                $this->load->model('rekap_default_value');
                $this->load->model('trc');

                $rekap = NULL;
                $old_rekap = NULL;
                
                if ($crud_action == CRUD_INSERT) {
                        $rekap = $this->insert_from_field($record);
                } elseif ($crud_action == CRUD_UPDATE || $crud_action == CRUD_DELETE) {
                        $old_rekap = $this->get_one(array(
                            'C012_TrcID' => $record['TrcID'],
                            'C000_LineID' => $record['LineID'],
                            'IsRemoved' => 0,
                        ));
                        
                        if ($crud_action == CRUD_UPDATE) {
                                $rekap = $this->update_from_field($record, $old_rekap);
                        } elseif ($crud_action == CRUD_DELETE) {
                                $this->delete_from_field($record);
                        }
                }
                
                $this->trc->save_from_rekap($crud_action, $header, $rekap, $old_rekap);
        }

        private function insert_from_field($record) {
                $rekap = $this->generate_rekap_data($record);
                
                $this->insert($rekap);
                
                $rekap = $this->get_one(array(
                    'C012_TrcID' => $rekap['C012_TrcID'],
                    'C000_LineID' => $rekap['C000_LineID'],
                    'IsRemoved' => 0,
                ));
                
                return $rekap;
        }

        private function update_from_field($record, $old_rekap) {
                $rekap_to_field = $this->rekap_to_field->get_one(array(
                    'C001_SubTrcTypeID' => $record['SubTrcTypeID'],
                ));
                
                unset($rekap_to_field['C001_SubTrcTypeID']);
                // Check changed value
                $changed_rekap = array();
                
                foreach ($rekap_to_field as $key => $value) {
                        if (!$value || ($record[$value] == $old_rekap[$key])) {
                                continue;
                        }
                        $changed_rekap[$key] = $record[$value];
                }

                if($record['SubTrcTypeID'] != $old_rekap['C001_SubTrcTypeID']){
                        $changed_rekap['C001_SubTrcTypeID'] = $record['SubTrcTypeID'];
                }
                
                if (!$changed_rekap) {
                        return $old_rekap;
                }

                $this->delete_from_field($old_rekap);
                
                $record['C050_Rev'] = $old_rekap['C050_Rev'] + 1; 
                return $this->insert_from_field($record);
        }

        private function delete_from_field($record) {
                $this->update(
                        array(// WHERE
                    'C012_TrcID' => $record['TrcID'],
                    'C000_LineID' => $record['LineID'],
                        ), array(// DATA
                    'IsRemoved' => 1,
                        )
                );
        }

        private function generate_rekap_data($record) {
                $rekap = array(
                    'C010_TrcTypeID' => $record['TrcTypeID'],
                    'C011_Month' => $record['Month'],
                    'C012_TrcID' => $record['TrcID'],
                    'C050_Rev' => isset($record['Rev']) ? $record['Rev'] : 1,
                    'C000_LineID' => $record['LineID'],
                    'C050_DocDate' => $record['DocDate'],
                    'C010_TrcPanelID' => $record['TrcPanelID'],
                    'C001_SubTrcTypeID' => $record['SubTrcTypeID'],
                    'C070_CurrencyID' => 1,
                    'C071_Rate' => 1,
                    'C071_RateFrom' => 1,
                    'C071_RateTo' => 1,
                    'C200_SLedgerID' => $record['SLedgerID'],
                );
                
                $rekap_to_field = $this->rekap_to_field->get_one(array(
                    'C001_SubTrcTypeID' => $record['SubTrcTypeID'],
                ));
                
                unset($rekap_to_field['C001_SubTrcTypeID']);
                foreach ($rekap_to_field as $key => $value) {
                        if (!$value) {
                                continue;
                        }
                        $rekap[$key] = $record[$value];
                }

                $rekap_default_value = $this->rekap_default_value->get_one(array(
                    'C001_SubTrcTypeID' => $record['SubTrcTypeID'],
                ));
                unset($rekap_default_value['C001_SubTrcTypeID']);
                foreach ($rekap_default_value as $key => $value) {
                        if (isset($rekap[$key])) {
                                continue;
                        }
                        $rekap[$key] = $value;
                }

                if ($rekap['C210_MLedgerID'] != 3) { // BANK
                        $rekap['C211_SubLedger1ID'] = 0;
                }

                if ($rekap['C220_MLedgerIDTo'] != 3) { // BANK
                        $rekap['C222_SubLedger1IDTo'] = 0;
                }
                
                return $rekap;
        }
        
        function count_doc_day($office_id, $panel_id){
                $this->db->select("COUNT(1) AS 'count'");
                $this->db->from($this->TABLE);
                $this->db->where('C200_SLedgerID', $office_id);
                $this->db->where('IsRemoved', 0);
                $this->db->where('C010_TrcPanelID', $panel_id);
                $this->db->where('C050_DocDate', $this->get_curr_date());
                
                return $this->db->get()->row_array()['count'];
                
        }
        
        function get_doc_day($office_id, $panel_id, $limit = NULL, $offset = 0){
                $select = $this->get_select_doc_day();
                
                $this->db->select($select);
                $this->db->from($this->TABLE);
                $this->db->where('C200_SLedgerID', $office_id);
                $this->db->where('IsRemoved', 0);
                $this->db->where('C010_TrcPanelID', $panel_id);
                $this->db->where('C050_DocDate', $this->get_curr_date());
                if($limit){
                        $this->db->limit($limit);
                }
                
                $this->db->offset($offset);
                $this->db->order_by('C012_TrcID', 'DESC');
                $this->db->order_by('C000_LineID', 'ASC');
                
                $elmnts = $this->db->get()->result_array();
                
                $page['total'] = $this->count_doc_day($office_id, $panel_id);
                $page['count'] = count($elmnts);
                $page['limit'] = $limit;
                $page['offset'] = $offset;
                $page['data'] = $elmnts;
                
                return $page;
        }

        function count_doc_day_rekap($where){
                $this->db->select("COUNT(1) AS 'count'");
                $this->db->from($this->TABLE);
                $this->db->where('IsRemoved', 0);
                if($where){
                        $this->db->where($where);
                }
                
                return $this->db->get()->row_array()['count'];
                
        }

        function get_doc_day_rekap($where, $limit = NULL, $offset = 0){
                $select = $this->get_select_doc_day();
                
                $this->db->select($select);
                $this->db->select('
                    T030_MLedger.Descr AS DescrTransaction,
                    Mledger2.Descr AS DescrKasBank,
                    MledgerTo.Descr AS DescrKasBankTo,
                    T022_KBAccount.C030_Descr AS BankName,
                    T022_KBAccount.C010_BankAccNumber AS BankAccount,
                    KBAccountTo.C030_Descr AS BankNameTo,
                    KBAccountTo.C010_BankAccNumber AS BankAccountTo,
                    T020_Office.C020_Descr AS DescrKantor,
                    OfficeTo.C020_Descr AS DescrKantorTo,
                    T026_InternalTransferStatus.status AS TransferStatus,
                    T026_ExternalTransfer.C003_Name AS ExtName,
                    T026_ExternalTransfer.C005_BankAccName AS ExtBankAccName,
                    T026_ExternalTransfer.C006_BankName AS ExtBankName,
                    T026_ExternalTransfer.C007_BankAccNumber AS ExtBankAccNumber,
                ');
                $this->db->from($this->TABLE);
                $this->db->where('IsRemoved', 0);
                if($where){
                        $this->db->where($where);
                }

                if($limit){
                        $this->db->limit($limit);
                }
                
                $this->db->offset($offset);
                $this->db->order_by('C012_TrcID', 'DESC');
                $this->db->order_by('C000_LineID', 'ASC');
                
                $this->db->join('T030_MLedger', 'T030_MLedger.SysID = '.$this->TABLE.'.C001_SubTrcTypeID', 'left');
                $this->db->join('T030_MLedger AS Mledger2', 'Mledger2.SysID = '.$this->TABLE.'.C210_MLedgerID', 'left');
                $this->db->join('T030_MLedger AS MledgerTo', 'MledgerTo.SysID = '.$this->TABLE.'.C220_MLedgerIDTo', 'left');
                $this->db->join('T022_KBAccount', 'T022_KBAccount.C000_SysID = '.$this->TABLE.'.C211_SubLedger1ID', 'left');
                $this->db->join('T022_KBAccount AS KBAccountTo', 'KBAccountTo.C000_SysID = '.$this->TABLE.'.C222_SubLedger1IDTo', 'left');
                $this->db->join('T020_Office', 'T020_Office.C000_SysID = '.$this->TABLE.'.C200_SLedgerID', 'left');
                $this->db->join('T020_Office AS OfficeTo', 'OfficeTo.C000_SysID = '.$this->TABLE.'.C201_SLedgerIDTo', 'left');
                $this->db->join('T026_InternalTransferStatus', 'T026_InternalTransferStatus.TrcID = '.$this->TABLE.'.C012_TrcID', 'left');
                $this->db->join('T026_ExternalTransfer', 'T026_ExternalTransfer.C000_TrcID = '.$this->TABLE.'.C012_TrcID', 'left');
                
                $elmnts = $this->db->get()->result_array();
                
                $page['total'] = $this->count_doc_day_rekap($where);
                $page['count'] = count($elmnts);
                $page['limit'] = $limit;
                $page['offset'] = $offset;
                $page['data'] = $elmnts;
                
                return $page;
        }
        
        function count_transfer_masuk($office_id){
                $this->db->select("COUNT(1) AS 'count'");
                $this->db->from($this->TABLE);
                $this->db->join('T026_InternalTransferStatus', 
                        $this->TABLE . '.C012_TrcID = T026_InternalTransferStatus.TrcID AND '
                        . $this->TABLE . '.C000_LineID = T026_InternalTransferStatus.LineID', 'LEFT');
                $where = "C201_SLedgerIDTo = {$office_id} AND IsRemoved = 0 AND C001_SubTrcTypeID = 14 AND "
                . "(T026_InternalTransferStatus.Status = 0 OR T026_InternalTransferStatus.Status IS NULL)";
                $this->db->where($where);
                
                return $this->db->get()->row_array()['count'];
                
        }
        
        function get_transfer_masuk($office_id, $limit = NULL, $offset = 0){
                $select = $this->get_select_doc_day();
                
                $this->db->select($select . 
                        ', T026_InternalTransferStatus.Status, '
                        . 'T026_InternalTransferStatus.CreatedAt, '
                        . 'T026_InternalTransferStatus.UpdatedAt');
                $this->db->from($this->TABLE);
                $this->db->join('T026_InternalTransferStatus', 
                        $this->TABLE . '.C012_TrcID = T026_InternalTransferStatus.TrcID AND '
                        . $this->TABLE . '.C000_LineID = T026_InternalTransferStatus.LineID', 'LEFT');
                
                $where = "C001_SubTrcTypeID = 14 AND C201_SLedgerIDTo = {$office_id} AND IsRemoved = 0 AND "
                . "(T026_InternalTransferStatus.Status = 0 OR T026_InternalTransferStatus.Status IS NULL)";
                $this->db->where($where);
                
                if($limit){
                        $this->db->limit($limit);
                }
                
                $this->db->offset($offset);
                $this->db->order_by('C012_TrcID', 'DESC');
                $this->db->order_by('C000_LineID', 'ASC');
                
                $elmnts = $this->db->get()->result_array();
                
                $page['total'] = $this->count_transfer_masuk($office_id);
                $page['count'] = count($elmnts);
                $page['limit'] = $limit;
                $page['offset'] = $offset;
                $page['data'] = $elmnts;
                
                return $page;
        }

        private function get_select_doc_day(){
                $select = 'C010_TrcTypeID AS TrcTypeID,
                        C012_TrcID AS TrcID,
                        C000_LineID AS LineID,
                        C010_TrcPanelID AS TrcPanelID,
                        C050_DocDate AS DocDate,
                        C001_SubTrcTypeID AS SubTrcTypeID,
                        C071_RateTo AS RateTo,
                        C072_Amount1 AS Amount1,
                        C073_Amount2 AS Amount2,
                        C073_Amount3 AS Amount3,
                        C200_SLedgerID AS SLedgerID,
                        C210_MLedgerID AS MLedgerID,
                        C211_SubLedger1ID AS SubLedger1ID,
                        C212_SubLedger2ID AS SubLedger2ID,
                        C213_SubLedger3ID AS SubLedger3ID,
                        C214_SubLedger4ID AS SubLedger4ID,
                        C215_Description AS Description,
                        C220_MLedgerIDTo AS MLedgerIDTo,
                        C201_SLedgerIDTo AS SLedgerIDTo,
                        C222_SubLedger1IDTo AS SubLedger1IDTo,
                        C223_SubLedger2IDTo AS SubLedger2IDTo,
                        C224_SubLedger3IDTo AS SubLedger3IDTo,
                        C225_SubLedger4IDTo AS SubLedger4IDTo,
                        C226_DescriptionTo AS DescriptionTo
                ';
                
                return $select;
        }
}