<?php
require_once(APPPATH . 'models/Base_model.php');
class KBAccount extends Base_Model {
	function __construct() {
		parent::__construct();
		$this->TABLE = "T022_KBAccount";
	}
	
	function get($where = NULL, $order = NULL){
		$this->db->select('*');
		$this->db->from('T022_KBAccount');
		if($where != NULL){
			$this->db->where($where);
		}
		if($order){
			$this->db->order_by($order);
		}else{
			$this->db->order_by('C000_SysID','ASC');
		}
		return $this->db->get();
	}
	
	function add($data){
		$query = $this->db->insert('T022_KBAccount', $data);
		return $query;
	}
	
	function edit($id, $data){
		$this->db->where('C000_SysID', $id);
		$this->db->update('T022_KBAccount', $data);
		return $id;
	}
	
	function delete($id){
		$this->db->where('C000_SysID', $id);
		return $this->db->delete('T022_KBAccount');
	}

	function get_data($criteria='',$order='',$order_by='',$limit='',$start='',$offset=0){
		$this->db->select('T022_KBAccount.C000_SysID as kodeBank, T022_KBAccount.C001_OfficeID, T022_KBAccount.C020_Name, T022_KBAccount.C010_BankAccNumber, T022_KBAccount.C030_Descr ,T020_Office.C020_Descr');
		$this->db->from('T022_KBAccount');
		$this->db->join('T020_Office', 'T020_Office.C000_SysID = T022_KBAccount.C001_OfficeID', 'left');
		if($criteria!='')
		$this->db->where($criteria);
		// if($order!='')
		// $this->db->order_by($order,$order_by);
		if($limit!='')
		$this->db->limit($limit,$start);

		$this->db->order_by('T022_KBAccount.C000_SysID','ASC');
		return $this->db->get();

	}

	function get_bank_begin_balance($where=null){
		$this->db->select('T022_KBAccount.*, T510_Rekap.*');
		$this->db->from('T022_KBAccount');
		$this->db->join('T510_Rekap', 'T510_Rekap.C200_SLedgerID = T022_KBAccount.C001_OfficeID AND T510_Rekap.C211_SubLedger1ID = T022_KBAccount.C000_SysID', 'left');
		// $this->db->join('T610_DayDelta', 'T610_DayDelta.SLedgerID = T510_Rekap.C200_SLedgerID', 'left');
		// $this->db->join('T030_2VirtualAccount', 'T030_2VirtualAccount.C000_SysID = T510_Rekap.C212_SubLedger2ID', 'left');
		if($where != NULL){
			$this->db->where($where);
		}

		return $this->db->get();
	}
        
        function get_bank_name($office_id){
                $this->db->select('C020_Name as name, C030_Descr as description');
		$this->db->from('T022_KBAccount');
                $this->db->where(array(
                    'C001_OfficeID' => $office_id
                ));
		$this->db->order_by('C020_Name','ASC');
                
                $tmp = $this->db->get()->result_array();
                $bank_names = array();
                $result = array();
                
                foreach($tmp AS $value){
                        $name = $value['name'];
                        if(!isset($bank_names[$name])){
                                $bank_names[$name] = 1;
                                $result[] = $value;
                        }
                        
                }
                
		return $result;
        }
        
        function get_bank_account($office_id, $bank_name){
                $this->db->select('C000_SysID AS id, C010_BankAccNumber AS account');
		$this->db->from('T022_KBAccount');
                $this->db->where(array(
                    'C001_OfficeID' => $office_id,
                    'C020_Name' => $bank_name,
                ));
		$this->db->order_by('C010_BankAccNumber','ASC');
                
                $result = $this->db->get()->result_array();
                
		return $result;
        }

	
}