<?php

require_once(APPPATH . 'models/Base_model.php');

class Virtual_Account extends Base_Model {
	function __construct() {
		parent::__construct();
                $this->TABLE = 'T030_2VirtualAccount';
	}

	function add($data){
		return $this->db->insert($this->TABLE, $data);
	}
	
	function edit($id, $data){
		$this->db->where('C000_SysID', $id);
		$this->db->update($this->TABLE, $data);
		return $id;
	}

	function delete($id){
		$this->db->where('C000_SysID', $id);
		return $this->db->delete($this->TABLE);
	}
}