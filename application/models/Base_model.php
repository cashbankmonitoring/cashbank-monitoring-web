<?php

/**
 * Description of mi_model
 *
 * @author Fikran
 * 
 * MI_Model ~> Mobilus Interactive Model
 */
class Base_Model extends CI_Model {

        public $TABLE = "";
        private $COLUMNS = array();
        private $map_columns = array();
        private $curr_yearmonth;
        private $curr_month;
        private $curr_year;
        private $curr_datetime;
        private $curr_date;
        private $curr_time;

        function __construct() {
                parent::__construct();
        }

        public function get_curr_yearmonth() {
                if (!$this->curr_yearmonth) {
                        $this->curr_yearmonth = date('ym');
                }
                return $this->curr_yearmonth;
        }

        public function get_curr_month() {
                if (!$this->curr_month) {
                        $this->curr_month = date('m');
                }
                return $this->curr_month;
        }

        public function get_curr_year() {
                if (!$this->curr_year) {
                        $this->curr_year = date('y');
                }
                return $this->curr_year;
        }

        public function get_curr_datetime() {
                if (!$this->curr_datetime) {
                        $this->curr_datetime = date('Y-m-d H:i:s');
                }
                return $this->curr_datetime;
        }

        public function get_curr_date() {
                if (!$this->curr_date) {
                        $this->curr_date = date('Y-m-d');
                }
                return $this->curr_date;
        }

        public function get_curr_time() {
                if (!$this->curr_time) {
                        $this->curr_time = date('H:i:s');
                }
                return $this->curr_time;
        }

        /* public function get_column_id(){
          if($this->COLUMN_ID){
          return $this->COLUMN_ID;
          }

          $q = $this->db->query("SELECT [COLUMN_NAME]
          FROM [INFORMATION_SCHEMA].[COLUMNS]
          WHERE TABLE_NAME='{$this->TABLE}' "
          . " AND ([COLUMN_NAME] = 'C000_SysID' "
          . " OR  [COLUMN_NAME] = 'SysID')"
          );
          $columns_meta = $q->result_array();
          var_dump($columns_meta);die();
          if($columns_meta){
          $this->COLUMN_ID = $columns_meta[0]["COLUMN_NAME"];

          return $this->COLUMN_ID;
          }

          return "";
          } */

        public function reset_cache_columns() {
                $this->COLUMNS = array();
        }

        public function get_columns() {
                if ($this->COLUMNS) {
                        return $this->COLUMNS;
                }

                $q = $this->db->query("SELECT [COLUMN_NAME]
                        FROM [INFORMATION_SCHEMA].[COLUMNS]
                        WHERE TABLE_NAME='{$this->TABLE}'");
                $columns_meta = $q->result_array();

                $columns = array();
                foreach ($columns_meta as $column) {
                        $columns[] = $column["COLUMN_NAME"];
                }

                $this->COLUMNS = $columns;

                return $columns;
        }

        protected function get_serialized_data($data) {
                $columns = $this->get_columns();
                $serialized_data = array();
                foreach ($columns as $column) {
                        if (isset($data[$column])) {
                                $serialized_data[$column] = $data[$column];
                        }
                }

                return $serialized_data;
        }

        public function insert($data) {
                $serialized_data = $this->get_serialized_data($data);
                if ($serialized_data) {
                        $this->db->insert($this->TABLE, $serialized_data);
                }
        }

        public function update($where = array(), $data) {
                $serialized_data = $this->get_serialized_data($data);
                if ($serialized_data) {
                        if ($where) {
                                $this->db->where($where);
                        }
                        $this->db->update($this->TABLE, $serialized_data);
                }
        }

        public function delete($where) {
                if (!$where) {
                        return 0;
                }
                $this->db->where($where);
                $this->db->delete($this->TABLE);

                return 1;
        }

        public function delete_all() {
                $this->db->empty_table($this->TABLE);
        }

        // $where_clause is string. ex: id = ?, name = ?
        // $where_args is array, value of ?, ex: array(1, 'fikran')
        // $order_by is string. ex: `name` ASC, `id` DESC
        public function find(
        $where_clause = NULL, $where_args = NULL, $order_by = NULL, $group_by = NULL, $limit = 0, $offset = 0) {
                $arg = "SELECT * FROM {$this->TABLE} ";
                if ($where_clause) {
                        $arg .= " WHERE {$where_clause} ";
                }
                if ($group_by) {
                        $arg .= " GROUP BY {$group_by} ";
                }
                if ($order_by) {
                        $arg .= " ORDER BY {$order_by} ";
                }
                if ($limit) {
                        $arg .= " LIMIT {$limit} ";
                        $arg .= " OFFSET {$offset} ";
                }
                $query = $this->db->query($arg, $where_args);
                return $query->result_array();
        }

        public function first($where_clause = NULL, $where_args = NULL, $order_by = NULL) {
                $items = $this->find(
                        $where_clause, $where_args, $order_by, NULL, 1, 0
                );

                if ($items) {
                        return $items[0];
                }

                return NULL;
        }

        public function find_all() {
                return $this->find();
        }

        public function find_with_query($query_clause = NULL, $query_args = NULL) {
                if (!$query_clause) {
                        return array();
                }

                $query = $this->db->query($query_clause, $query_args);
                return $query->result_array();
        }

        private function _get($where = NULL) {
                $this->db->select('*');
                $this->db->from($this->TABLE);
                if ($where) {
                        $this->db->where($where);
                }
                return $this->db->get();
        }

        function get($where = NULL) {
                return $this->_get($where)->result_array();
        }

        function get_one($where = NULL) {
                $q = $this->_get($where);

                if ($q->num_rows() > 0) {
                        return $q->row_array();
                }

                return NULL;
        }

        function get_new_id($select){
            $this->db->select($select);
            $this->db->from($this->TABLE);
            $this->db->limit(1);
            $this->db->order_by($select,'DESC');
            
            $q = $this->db->get()->row_array();
            
            $new_id = $q[$select]+1;
            return $new_id;
        } 

        function get_sys_id($options = array()) {
                $where['Opsi_1'] = isset($options['Opsi_1']) ? (int) $options['Opsi_1'] : 0;
                $where['Opsi_2'] = isset($options['Opsi_2']) ? (int) $options['Opsi_2'] : 0;
                $where['YearMonth'] = isset($options['YearMonth']) ? (int) $options['YearMonth'] : 0;

                $this->db->query('sp01_GetNewSysID_2 ?,?,?,?,?', array(
                    '@TableName' => $this->TABLE,
                    '@Opsi_1' => $where['Opsi_1'],
                    '@Opsi_2' => $where['Opsi_2'],
                    '@YearMonth' => $where['YearMonth'],
                    '@NewSysID' => 0,
                ));

                $this->db->select('CurrentSysID');
                $this->db->from('T_000_Counter');
                $this->db->where(array(
                    'TableName' => $this->TABLE,
                    'Opsi_1' => $where['Opsi_1'],
                    'Opsi_2' => $where['Opsi_2'],
                    'YearMonth' => $where['YearMonth'],
                ));

                $q = $this->db->get();

                if ($q->num_rows() > 0) {
                        $row = $q->row_array();
                        return $row['CurrentSysID'];
                }
                return 1;
        }

        function get_column_name_by_suffix($suffix) {
                $columns = $this->get_columns();

                if (!$this->map_columns) {
                        foreach ($columns as $key => $column) {
                                $a_column = explode('_', $column);
                                $key_column = $a_column[count($a_column) - 1];

                                $this->map_columns[strtolower($key_column)] = $column;
                        }
                }

                return isset($this->map_columns[strtolower($suffix)]) ? $this->map_columns[strtolower($suffix)] : '';
        }

        function remove_suffix(&$row) {
                $columns = $this->get_columns();

                foreach ($columns as $old_column_key) {
                        if (!isset($row[$old_column_key])) {
                                continue;
                        }
                        $column_parts = explode('_', $old_column_key);
                        $new_column_key = $column_parts[count($column_parts) - 1];

                        $row[$new_column_key] = $row[$old_column_key];
                        unset($row[$old_column_key]);
                }
        }

}
