<?php
require_once(APPPATH . 'models/Base_model.php');
class User extends Base_Model {
	function __construct() {
		parent::__construct();
		$this->TABLE = "T023_User";
	}
	
	function get($where = null){
		$this->db->select('*');
		$this->db->from('T023_User');
		$this->db->join('T020_Office', 'T020_Office.C000_SysID = T023_User.C002_OfficeID', 'left');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}
	
	function get_hash($password) {
		return md5($password);
	}

	function add($data) {
		$data['C050_UserPasswd'] = $this->get_hash($data['C050_UserPasswd'], $data['C050_UserPasswd']);
		$query = $this->db->insert('T023_User', $data);
		return $query;
	}
	function check_password($where=null){
		$this->db->select('C000_SysID');
		$this->db->from('T023_User');
		$this->db->where($where);
		$query = $this->db->get();
		return $query->num_rows();

	}
	function reset_password($id){
		$this->db->where('C000_SysID',$id);
		$data['C050_UserPasswd'] = md5("123456");
		return $this->db->update('T023_User', $data);
	}
	function update($id,$data) {
		// if($data['password'] != NULL){
		// 	$data['password'] = $this->get_hash($data['username'], $data['password']);
		// }
		$this->db->where('C000_SysID',$id);
		return $this->db->update('T023_User', $data);
	}

	function activate($ids) {
		$data = array(
			'status' => USER_ACTIVE,
		);
		
		foreach($ids as $id) {
			$this->db->or_where('user_id', $id);
		}
		
		$this->db->update('user', $data);
	}

	function suspend($ids) {
		$data = array(
			'status' => USER_NOT_ACTIVE,
		);
		
		foreach($ids as $id) {
			$this->db->or_where('user_id', $id);
		}
		
		$this->db->update('user', $data);
	}

	function auth($username, $password, $where = NULL) {
		$this->db->select('*');
		$this->db->from('T023_User');
		$this->db->where(array(
			'T023_User.C030_UserName' => $username,
			'T023_User.C050_UserPasswd' => $this->get_hash($password)
		));
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get()->row_array();
	}

	function delete($id){
		$this->db->where('C000_SysID',$id);
		return $this->db->delete('T023_User');
	}

	function set_user_password(){
		$users = $this->get()->result_array();
		foreach($users as $user){
			$this->edit($user['user_id'],array('username' => $user['username'], 'password' => 'testing'));
		}
	}

	function get_user($where){
		$this->db->select('*');
		$this->db->from('T023_User');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}

	function get_data($criteria='',$order='',$order_by='',$limit='',$start='',$offset=0){
		$this->db->select('T023_User.C000_SysID as C000_SysID,
		T023_User.C002_OfficeID,
		T023_User.C010_Code,
		T023_User.C020_Descr,
		T023_User.C030_UserName,
		T023_User.C040_UserFullname,
		T023_User.C050_UserPasswd,
		T023_User.C060_UserEmail,
		T023_User.C070_UserState,
		T023_User.C080_machineID,
		T023_User.C110_UserGroupType,
		T023_User.C120_IsBeginBalance,
		T023_User.C130_IsReadOnly,
		T020_Office.C012_Level,
		ROW_NUMBER() OVER (ORDER BY T023_User.C002_OfficeID) AS RowNum,
		T020_Office.C010_Code, T020_Office.C020_Descr');
		$this->db->from('T023_User');
		
		
		// if($order!='')
		// $this->db->order_by($order,$order_by);

		$this->db->join('T020_Office', 'T020_Office.C000_SysID = T023_User.C002_OfficeID', 'left');
		if($criteria!='')
		$this->db->where($criteria);
		// if($limit!='')
		// $this->db->limit($limit,$start);
		$this->db->order_by('T023_User.C000_SysID','ASC');
		return $this->db->get();

	}
	function cek_kode($where){
		$this->db->select('*');
		$this->db->from('T023_User');
		$this->db->where($where);
		$this->db->limit(1);
		$query = $this->db->get();
		$query = $query->num_rows();
		return $query;
	}
	function get_max(){
		$this->db->select('C000_SysID');
		$this->db->from('T023_User');
		$this->db->order_by('C000_SysID','DESC');
		$this->db->limit('1');
		return $this->db->get();
	}
}
