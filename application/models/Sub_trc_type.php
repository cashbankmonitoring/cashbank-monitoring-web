<?php

require_once(APPPATH . 'models/Base_model.php');

class Sub_Trc_Type extends Base_Model {
	function __construct() {
		parent::__construct();
                $this->TABLE = 'T024_3SubTrcType';
	}
        
        function get_code($id){
                $SubTrcType = $this->sub_trc_type->get_one(array(
                    'C000_SysID' => $id,
                ));
                
                return $SubTrcType ? $SubTrcType['C010_Code'] : NULL;
        }
}