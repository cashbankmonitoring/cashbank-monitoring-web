<?php

require_once(APPPATH . 'models/Base_model.php');

class T500_Rekap extends Base_Model {

        function __construct() {
                parent::__construct();
                $this->TABLE = "T500_Rekap";
        }

        function add($data) {
                $this->db->insert('T500_Rekap', $data);
                return $this->db->insert_id();
        }

        function get_q500_rekap($where = null) {
                $this->db->select('*');
                $this->db->from('Q500_Rekap');
                if ($where != null) {
                        $this->db->where($where);
                }
                return $this->db->get();
        }

}
