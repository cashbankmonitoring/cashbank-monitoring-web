<?php
require_once(APPPATH . 'models/Base_model.php');
class Program_alocation extends Base_Model {
	function __construct() {
		parent::__construct();
		$this->TABLE = "T027_ProgramAlocation";
	}
	
	function get($where = NULL){
		$this->db->select('*');
		$this->db->from('T027_ProgramAlocation');
		if($where != NULL){
			$this->db->where($where);
		}
		$this->db->order_by('C000_SysID','ASC');
		return $this->db->get();
	}
	
	function add($data){
		// $this->db->set($data);
		
		// $query = $this->db->insert();
		return $this->db->insert('T027_ProgramAlocation', $data);
	}
	
	function edit($id, $data){
		$this->db->where('C000_SysID', $id);
		$this->db->update('T027_ProgramAlocation', $data);
		return $id;
	}
	
	function delete($id){
		$this->db->where('C000_SysID', $id);
		return $this->db->delete('T027_ProgramAlocation');
	}

	function get_program_alocation_program($where = null){
		$this->db->select('*');
		$this->db->from('T027_ProgramAlocation');
		$this->db->join('T005_Project', 'T027_ProgramAlocation.C010_OfficeID = T005_Project.C000_SysID');
		if($where != null){
			$this->db->where($where);
		}

		return $this->db->get();	
	}
	function get_data($criteria='',$order='',$order_by='',$limit='',$start='',$offset=0){
		$this->db->select('T027_ProgramAlocation.C000_SysID,
			T027_ProgramAlocation.C010_OfficeID,
			T020_Office.C001_ParentID,
			T020_Office.C012_Level,
			T020_Office.C010_Code,
			T020_Office.C020_Descr AS off_name,
			T027_ProgramAlocation.C020_ProjectID,
			T005_Project.C020_Descr AS prog_name,
			T027_ProgramAlocation.C030_Alokasi,
			T027_ProgramAlocation.C040_StartDate,
			T027_ProgramAlocation.C050_DueDate,
			T027_ProgramAlocation.C060_Descr,
			T005_Project.C050_IsClose');
		$this->db->from('T027_ProgramAlocation');
		
		if($criteria != ''){
			$this->db->where($criteria);
		}
		// if($order!='')
		// $this->db->order_by($order,$order_by);

		$this->db->join('T020_Office', 'T020_Office.C000_SysID = T027_ProgramAlocation.C010_OfficeID');
		$this->db->join('T005_Project', 'T005_Project.C000_SysID = T027_ProgramAlocation.C020_ProjectID');
		if($limit!='')
		$this->db->limit($limit,$start);
		//$this->db->order_by('T023_User.C000_SysID','ASC');
		return $this->db->get();

	}
	function get_program_by_off($criteria=''){
		$this->db->select('dbo.T027_ProgramAlocation.C010_OfficeID,
		dbo.T027_ProgramAlocation.C020_ProjectID,
		dbo.T005_Project.C020_Descr,
		SUM(dbo.T027_ProgramAlocation.C030_Alokasi) AS alokasi');
		$this->db->from('T027_ProgramAlocation');

		$this->db->join('T005_Project', 'T005_Project.C000_SysID = T027_ProgramAlocation.C020_ProjectID');
		if($criteria!='')
		$this->db->where($criteria);
		$this->db->group_by('dbo.T027_ProgramAlocation.C010_OfficeID');
		$this->db->group_by('dbo.T027_ProgramAlocation.C020_ProjectID');
		$this->db->group_by('dbo.T005_Project.C020_Descr');
		//$this->db->order_by('T023_User.C000_SysID','ASC');
		return $this->db->get();
	}
	function get_alocation_value($criteria=''){
	// $rslt = mssql_query("
		// SELECT
		// dbo.T027_ProgramAlocation.C020_ProjectID,
		// Sum(dbo.T027_ProgramAlocation.C030_Alokasi) AS tot_alok

		// FROM
		// dbo.T027_ProgramAlocation
		// INNER JOIN dbo.T020_Office ON dbo.T027_ProgramAlocation.C010_OfficeID = dbo.T020_Office.C000_SysID
		// WHERE
		// dbo.T027_ProgramAlocation.C020_ProjectID = ".$prg_id."
		// GROUP BY
		// dbo.T027_ProgramAlocation.C020_ProjectID
		// ");	}
		$this->db->select('
			T027_ProgramAlocation.C020_ProjectID,
			Sum(T027_ProgramAlocation.C030_Alokasi) AS tot_alok
		');
		$this->db->from('T027_ProgramAlocation');
		$this->db->join('T005_Project', 'T005_Project.C000_SysID = T027_ProgramAlocation.C020_ProjectID');
		if($criteria!='')
		$this->db->where($criteria);
		$this->db->group_by('dbo.T027_ProgramAlocation.C020_ProjectID');
		return $this->db->get();
	}
}