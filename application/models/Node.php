<?php

require_once(APPPATH . 'models/Base_model.php');

class Node extends Base_Model {
        
        function __construct() {
                parent::__construct();
                $this->TABLE = "T500_Node";
        }
        
        function get($where=null){
        	$this->db->select('*');
			$this->db->from('T500_Node');
			if($where != null){
				$this->db->where($where);
			}
			return $this->db->get();
        }

        function add($data){
        	$this->db->insert('T500_Node',$data);
                return $this->db->insert_id();
        }
}

