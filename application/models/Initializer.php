<?php

require_once(APPPATH . 'models/Base_model.php');

class Initializer extends Base_Model {

        function __construct() {
                parent::__construct();
//                $this->TABLE = "T030_MLedger";
        }

        function init_table($table_name) {
                $this->TABLE = $table_name;
        }

        function turn_on_identity_insert() {
                $q = 'SET IDENTITY_INSERT ' . $this->TABLE . ' ON';
                return $this->db->query($q);
        }

        function turn_off_identity_insert() {
                $q = 'SET IDENTITY_INSERT ' . $this->TABLE . ' OFF';
                return $this->db->query($q);
        }

        function insert($data) {
                $db_debug = $this->db->db_debug; //save setting
                $this->db->db_debug = FALSE; //disable debugging for queries

                $this->db->trans_start();

                $has_turn_on_identity_insert = $this->turn_on_identity_insert();
                
                parent::insert($data);
                
                $this->turn_off_identity_insert();
                
                $this->db->trans_complete();

                echo $this->TABLE . ' -> ';

                if (!$has_turn_on_identity_insert) {
//                        $this->db->trans_start();
                        parent::insert($data);
                        echo '!has_turn_on_identity_insert';
//                        $this->db->trans_complete();
                } else {
                        echo 'has_turn_on_identity_insert';
                }

                echo '<br/>';

                $this->db->db_debug = $db_debug; //restore setting
        }

}
