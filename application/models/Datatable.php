<?php

require_once(APPPATH . 'models/Base_model.php');

class Datatable extends Base_Model {
    function __construct() {
        parent::__construct();
        $this->TABLE;
    }

	private function _get_datatables_query($table, $column_search, $column_order, $order){

		if($table == 'T022_KBAccount'){
			$this->db->select('
				T022_KBAccount.C000_SysID as id, 
				T022_KBAccount.C001_OfficeID, 
				T022_KBAccount.C020_Name as name, 
				T022_KBAccount.C010_BankAccNumber as account_number, 
				T022_KBAccount.C030_Descr as descr,
				T020_Office.C020_Descr as office'
			);

			$this->db->join('T020_Office', 'T020_Office.C000_SysID = T022_KBAccount.C001_OfficeID', 'left');
		}else if($table == 'T023_User'){
			$this->db->select('
				T023_User.C000_SysID as id,
				T023_User.C002_OfficeID as office_id,
				T023_User.C030_UserName as username,
				T023_User.C040_UserFullname as name,
				T023_User.C060_UserEmail as email,
				T023_User.C070_UserState as status,
				T020_Office.C012_Level as level,
				T020_Office.C010_Code as code, 
				T020_Office.C020_Descr as desc');

			$this->db->join('T020_Office', 'T020_Office.C000_SysID = T023_User.C002_OfficeID', 'left');
		}else if($table == 'T005_Project'){
			$this->db->select('T005_Project.*,T010_Partner.*');
			$this->db->join('T010_Partner', 'T010_Partner.SysID = T005_Project.C030_PartnerID', 'left');
		}else if($table == 'T027_ProgramAlocation'){
            $this->db->select('
                T027_ProgramAlocation.C000_SysID as id,
                T027_ProgramAlocation.C010_OfficeID,
                T020_Office.C001_ParentID,
                T020_Office.C012_Level,
                T020_Office.C010_Code,
                T020_Office.C020_Descr as office,
                T027_ProgramAlocation.C020_ProjectID,
                T005_Project.C020_Descr as name,
                T027_ProgramAlocation.C030_Alokasi as alokasi,
                T027_ProgramAlocation.C040_StartDate,
                T027_ProgramAlocation.C050_DueDate,
                T027_ProgramAlocation.C060_Descr as desc,
                T005_Project.C050_IsClose');

            $this->db->join('T020_Office', 'T020_Office.C000_SysID = T027_ProgramAlocation.C010_OfficeID');
            $this->db->join('T005_Project', 'T005_Project.C000_SysID = T027_ProgramAlocation.C020_ProjectID');
        }
		
		$this->db->from($table);
 
        $i = 0;
     
        foreach ($column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($order))
        {
            $order = $order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables($table, $column_search, $column_order, $order){
        $this->_get_datatables_query($table, $column_search, $column_order, $order);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered($table, $column_search, $column_order, $order){
        $this->_get_datatables_query($table, $column_search, $column_order, $order);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all($table){
        $this->db->from($table);
        return $this->db->count_all_results();
    }
}

