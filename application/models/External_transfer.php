<?php

require_once(APPPATH . 'models/Base_model.php');

class External_Transfer extends Base_Model {

        function __construct() {
                parent::__construct();
                $this->TABLE = "T026_ExternalTransfer";
        }

        function get_trf_masuk($office_id) {
                $this->db->select($this->TABLE . '.*, '
                        . 'T022_KBAccount.C010_BankAccNumber AS BankAccTo,'
                        . 'T022_KBAccount.C020_Name AS BankNameTo,'
                        . 'T022_KBAccount.C030_Descr AS BankDescrTo,'
                        . 'T030_2VirtualAccount.C011_Descr AS VirtualAccount,'
                        . 'T005_Project.C020_Descr AS ProgramTo,'
                        . 'T030_MLedger.Descr AS KasBankTo');
                $this->db->from($this->TABLE);
                $this->db->join('T022_KBAccount', $this->TABLE . '.C211_SubLedger1ID = T022_KBAccount.C000_SysID', 'LEFT');
                $this->db->join('T030_2VirtualAccount', $this->TABLE . '.C212_SubLedger2ID = T030_2VirtualAccount.C000_SysID', 'LEFT');
                $this->db->join('T005_Project', $this->TABLE . '.C213_SubLedger3ID = T005_Project.C000_SysID', 'LEFT');
                $this->db->join('T030_MLedger', $this->TABLE . '.C210_MLedgerID = T030_MLedger.SysID', 'LEFT');
                $this->db->where($this->TABLE . '.C200_SLedgerID', $office_id);
                $this->db->where($this->TABLE . '.C006_IsTrfMasuk', 1);
                $this->db->order_by($this->TABLE . '.C001_DateTransfer', 'DESC');
                $this->db->order_by($this->TABLE . '.C000_SysID', 'DESC');

                return $this->db->get()->result_array();
        }

        function generate_doc_day(&$rekaps) {
                $where = "(C000_TrcID = {$rekaps[0]['TrcID']} AND  C001_LineID = {$rekaps[0]['LineID']})";
                for ($i = 1; $i < count($rekaps); $i++) {
                        $rekap = $rekaps[$i];

                        $where .= " OR ";
                        $where .= "(C000_TrcID = {$rekap['TrcID']} AND  C001_LineID = {$rekap['LineID']})";
                }

                $external_transfer_list = $this->get($where);
                $external_transfer_map = array();
                foreach ($external_transfer_list as $external_transfer) {
                        $this->remove_suffix($external_transfer);
                        
                        $key = $external_transfer['TrcID'] . '_' . $external_transfer['LineID'];

                        $tmp_external_transfer = $external_transfer;
                        unset($tmp_external_transfer['TrcID']);
                        unset($tmp_external_transfer['LineID']);

                        $external_transfer_map[$key] = $tmp_external_transfer;
                }

                $empty_external_transfer = array();
                
                foreach ($rekaps as $key => $rekap) {
                        $key1 = $rekap['TrcID'] . '_' . $rekap['LineID'];
                        if(isset($external_transfer_map[$key1])){
                                $external_transfer = $external_transfer_map[$key1];
                                $rekap = array_merge($rekap, $external_transfer);
                        }else{
                                if(!$empty_external_transfer){
                                        $columns = $this->get_columns();
                                        foreach ($columns as $column) {
                                                $dummy_external_transfer[$column] = NULL;
                                        }
                                        $this->remove_suffix($dummy_external_transfer);
                                }
                                
                                $rekap = array_merge($rekap, $dummy_external_transfer);
                        }
                        
                        $rekaps[$key] = $rekap;
                }
        }

}
