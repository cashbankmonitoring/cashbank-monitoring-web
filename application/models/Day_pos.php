<?php

require_once(APPPATH . 'models/Base_model.php');

class Day_Pos extends Base_Model {
        function __construct() {
                parent::__construct();
                $this->TABLE = "T610_DayPos";
        }
        
        function sum_balance($where){
                $this->load->model('day_delta');
                
                $balance = 0;
                
                $where['datePeriod'] = $this->day_delta->get_curr_date();
                
                $this->db->select_sum('Amount');
		$this->db->from($this->day_delta->TABLE);
                $this->db->where($where);
		$q = $this->db->get();
                if($q->num_rows() > 0){
                        $data = $q->row_array();
                        $balance += $data['Amount'];
                }
                
                
                $where['datePeriod < '] = $where['datePeriod'];
                unset($where['datePeriod']);
                
                $this->db->select_sum('Amount');
		$this->db->from($this->TABLE);
                $this->db->where($where);
		$q = $this->db->get();
                if($q->num_rows() > 0){
                        $data = $q->row_array();
                        $balance += $data['Amount'];
                }
                
                return $balance;
        }
        
        function get_balance($where) {
                $this->load->model('day_delta');
                
                $result = array();
                
                $where['datePeriod'] = $this->day_delta->get_curr_date();
                $day_delta_list = $this->day_delta->get($where);
                $day_delta_map = array();
                foreach ($day_delta_list as $val) {
                        $tmp = $val;
                        unset($tmp['datePeriod']);
                        unset($tmp['Amount']);
                        $key = implode('_', $tmp);
                        
                        $day_delta_map[$key] = $val;
                }
                
                $where['datePeriod < '] = $where['datePeriod'];
                unset($where['datePeriod']);
                $day_pos_list = $this->get($where);
                $day_pos_map = array();
                foreach ($day_pos_list as $val) {
                        $tmp = $val;
                        unset($tmp['datePeriod']);
                        unset($tmp['Amount']);
                        $key = implode('_', $tmp);
                        
                        if(isset($day_delta_map[$key])){
                                $day_delta = $day_delta_map[$key];
                                $val['Amount'] += $day_delta['Amount'];
                        }
                        
                        unset($val['datePeriod']);
                        $day_pos_map[$key] = $val;
                        $result[] = $val;
                }
                
                foreach ($day_delta_map as $key => $val) {
                        if(!isset($day_pos_map[$key])){
                                unset($val['datePeriod']);
                                $result[] = $val;
                        }
                }
                
                return $result;
        }
}