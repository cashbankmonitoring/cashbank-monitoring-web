<?php

require_once('common.php');
if (!defined('BASEPATH'))
        exit('No direct script access allowed');

class Transactions extends Common {

        function __construct() {
                parent::__construct("transactions");

                $this->meta = array();
                $this->scripts = array('transaction', 'post.transaction');
                $this->styles = array();
                $this->title = "Transactions";

                $this->check_node = $this->get_doc_header(
                        $this->user_data['C002_OfficeID'], TRC_TYPE_TRANSACTION, date('Y-m-d')
                );

                if ($this->check_node) {
                        $this->data_header = $this->check_node;
                } else {
                        $this->new_header(TRC_TYPE_TRANSACTION, 1, $this->check_node, "transactions");
                }

                $this->load->model(array(
                    'office',
                    'program_alocation',
                    'project',
                    'transaction',
                    'Debt',
                    'node',
                    't500_rekap',
                    't510_rekap',
                    'kbaccount',
                    'trc_type',
                    't024_sub_trc_type',
                    'trc',
                    'day_delta',
                    'external_transfer',
                    'virtual_account',
                    'm_ledger',
                    'map_to_ledger',
                ));
        }

        public function index() {
                $data['banks'] = $this->kbaccount->get(array('T022_KBAccount.C001_OfficeID' => $this->user_data['C002_OfficeID']))->result_array();
                $banks = array();
                foreach ($data['banks'] AS $bank_account) {
                        $banks[$bank_account['C020_Name']][] = $bank_account;
                }
                ksort($banks);
                $data['banks'] = $banks;

                $header = $this->data_header;
                //get kasbank awal
                $where = array('SLedgerID' => $header['C045_OfficeID']);
                $m_latest_trc = $this->trc->get_latest($where);
                if ($m_latest_trc['AmountTrc'] != null) {
                        $data['kasbank_awal'] = $m_latest_trc['AmountTrc'];
                } else {
                        $data['kasbank_awal'] = 0;
                }

                //get kasbank in transfer
                $getIntrans = $this->trc->get_intransfer($criteria = " SLedgerID =  " . $header['C045_OfficeID'] . " AND AmountOutTrc = 0 AND MLedgerID = 1");
                if ($getIntrans['AmountInTrc'] != null) {
                        $data['kasbank_intransfer'] = $getIntrans['AmountInTrc'];
                } else {
                        $data['kasbank_intransfer'] = 0;
                }

                $data['kasbank_mutasi'] = 0;
                $data['projects'] = $this->project->get()->result_array();
                $data['alocations'] = $this->program_alocation->
                                get_program_alocation_program(array(
                                    'T027_ProgramAlocation.C010_OfficeID' => $this->user_data['C002_OfficeID']
                                ))->result_array();
                $data['value_program'] = $this->transaction->
                                get('SubLedger1ID', array(
                                    'C010_TrcTypeID' => TRC_TYPE_TRANSACTION,
                                    'SubLedger2ID' => 2, 'MLedgerID' => 1,
                                    'SubLedger1ID' => $this->user_data['C002_OfficeID'],
                                    'SLedgerID' => 1,
                                        ), 'SubLedger1ID', 'AmountTrc'
                                )->result_array();

                $data['debts'] = $this->Debt->get(array('IsDetail' => 1), '20.')->result_array();
                $data['virtual_accounts'] = $this->virtual_account->find_all();

                $tab_ids = unserialize(TRC_TAB_IDS);
                $tabs = array();
                foreach ($tab_ids AS $tab_id) {
                        $dt_tab = $data;
                        if ($tab_id == TRC_TAB_PELANGGAN) {
                                $dt_tab['operational_types'] = $this->t024_sub_trc_type->get(array('C010_TrcPanelID' => 4, 'C010_Code !=' => 'Pelanggan'));
                        } else if ($tab_id == TRC_TAB_PROGRAM) {
                                $dt_tab['projects'] = $this->project->find_all();
                        } else if ($tab_id == TRC_TAB_TRANSFER_MASUK) {
                                $dt_tab['transfer_masuk'] = $this->t510_rekap->get_transfer_masuk_internal($header['C045_OfficeID']);
                                $data['count_transfer_masuk'] = count($dt_tab['transfer_masuk']);
                        } else if ($tab_id == TRC_TAB_TRANSFER_KELUAR) {
                                $dt_tab['tmpl_int_trans'] = $this->load->view('transaction/tmpl/tmpl_int_trans', array('virtual_accounts' => $data['virtual_accounts']), TRUE);
                                $dt_tab['tmpl_ext_trans'] = $this->load->view('transaction/tmpl/tmpl_ext_trans', '', TRUE);
                        }

                        $tabs[] = $this->load->view('transaction/tab/tab_' . $tab_id, $dt_tab, TRUE);
                }
                $data['tabs'] = $tabs;

                $this->load->view("transaction/index", $data);
        }

        function search_office() {
                $this->layout = FALSE;
                $result = array('total_count' => 410979, 'incomplete_results' => false, 'items' => "");

                $lvl = $this->input->get('lvl');
                $q = $this->input->get('q');

                $result['items'] = $this->office->search($lvl, $q);

                header('Content-type: application/json');
                echo json_encode($result);
        }

        function tmpl_addtr_plg() {
                echo $this->load->view("transaction/tmpl/tmpl_addtr_plg", '', TRUE);
        }

        function save_t500_rekap() {
                echo json_encode($this->data_header);
                die();
        }

        function save_trc_pelanggan() {
                $this->layout = FALSE;

                $header = $this->get_header();

                $transactions = $this->input->post('transactions');
                if ($this->is_trc_pelanggan_valid($transactions)) {
                        echo 0;
                }

                foreach ($transactions as $key => $trc) {
                        $where = array(
                            'MLedgerID' => $trc['jns_bayar'],
                            'SLedgerID' => $header['C045_OfficeID'],
                            'SubLedger2ID' => 1, //Operation
                        );
                        if ($where['MLedgerID'] == 3) { //bank
                                $where['SubLedger1ID'] = $trc['rek_bank'];
                        }
                        $m_latest_trc = $this->trc->get_latest($where);

                        // balance
                        $prev_balance = 0;
                        if ($m_latest_trc) {
                                $prev_balance = $m_latest_trc['AmountTrc'];
                        }
                        $delta_balance = $trc['amount_in'] - $trc['amount_out'];
                        $balance = $prev_balance + $delta_balance;

                        $C211_SubLedger1ID = 0; //rek bank id
                        if ($trc['jns_bayar'] == 3) { // if bank
                                $C211_SubLedger1ID = $trc['rek_bank'];
                        }

                        $m_rekap = array(
                            'C010_TrcTypeID' => $header['C010_TrcTypeID'],
                            'C011_Month' => $header['C011_Month'],
                            'C012_TrcID' => $header['C000_SysID'],
                            'C050_Rev' => 1, // Sementara, karena belum ada draft
                            'C000_LineID' => $trc['line_id'],
                            'C001_SubTrcTypeID' => $trc['type'],
                            'C070_CurrencyID' => 1,
                            'C071_Rate' => 1,
                            'C071_RateFrom' => 0,
                            'C071_RateTo' => 0,
                            'C072_Amount1' => $prev_balance,
                            'C073_Amount2' => $balance,
                            'C073_Amount3' => $delta_balance,
                            'C210_MLedgerID' => $trc['jns_bayar'], // 2. kas or 3. bank
                            'C200_SLedgerID' => $header['C045_OfficeID'],
                            'C211_SubLedger1ID' => $C211_SubLedger1ID,
                            'C212_SubLedger2ID' => 1, //Operation
                        );

                        $this->t510_rekap->insert($m_rekap);

                        $m_trc = array();
                        foreach ($this->trc->get_columns() AS $column) {
                                if (isset($m_rekap[$column])) { // copy value if have same key/column
                                        $m_trc[$column] = $m_rekap[$column];
                                }
                        }
                        $m_trc['MLedgerID'] = $m_rekap['C210_MLedgerID'];
                        $m_trc['SLedgerID'] = $m_rekap['C200_SLedgerID'];
                        $m_trc['SubLedger1ID'] = $m_rekap['C211_SubLedger1ID'];
                        $m_trc['SubLedger2ID'] = $m_rekap['C212_SubLedger2ID'];

                        $m_trc['C000_TrcID'] = $m_rekap['C012_TrcID'];
                        $m_trc['C012_LineID'] = $m_rekap['C000_LineID'];
                        $m_trc['C014_MonthPostIdx'] = $this->trc->get_curr_month();
                        $m_trc['YearIdx'] = $this->trc->get_curr_year();
                        $m_trc['MonthIdx'] = $this->trc->get_curr_month();
                        $m_trc['C015_DatePos'] = $this->trc->get_curr_date();
                        $m_trc['C016_TimePost'] = $this->trc->get_curr_time();
                        $m_trc['C050_DocNum'] = $header['C050_DocNum'];
                        $m_trc['C050_DocDate'] = $header['C050_DocDate'];
                        $m_trc['C050_AccountDate'] = $this->trc->get_curr_datetime();
                        $m_trc['Currency'] = 'IDR';
                        $m_trc['Rate'] = 1;

                        // 1. add one row for adjusting balance in table T610_Trc
                        $m_trc['MinPlus'] = $balance >= 0 ? 1 : -1;
                        $m_trc['AmountTrc'] = abs($balance);
                        $m_trc['AmountInTrc'] = abs($trc['amount_in']);
                        $m_trc['AmountOutTrc'] = abs($trc['amount_out']);

                        $this->trc->insert($m_trc);

                        // 1.a. add one row for adjusting balance in table T610_DayDelta
                        $m_day_delta['MLedgerID'] = $m_trc['MLedgerID'];
                        $m_day_delta['SLedgerID'] = $m_trc['SLedgerID'];
                        $m_day_delta['SubLedger1ID'] = $m_trc['SubLedger1ID'];
                        $m_day_delta['SubLedger2ID'] = $m_trc['SubLedger2ID'];
                        $m_day_delta['datePeriod'] = $this->trc->get_curr_date();

                        $this->day_delta->update($m_day_delta, $delta_balance);

                        // 2. add one row for adjusting "hutang" in table T610_Trc
                        $delta_hutang = $delta_balance - $trc['amount_service'];
                        if (abs($delta_hutang) > 0) {
                                $where = array(//re-use variable
                                    'MLedgerID' => $m_trc['C001_SubTrcTypeID'], // jenis hutang
                                    'SLedgerID' => $header['C045_OfficeID'],
                                    'SubLedger2ID' => 1, // 1. Operation, karena di function ini semuanya operation
                                );
                                $m_latest_trc = $this->trc->get_latest($where); // get latest hutang

                                $prev_hutang = $m_latest_trc ? ($m_latest_trc['AmountTrc'] * $m_latest_trc['MinPlus'] ) : 0;
                                $delta_hutang = $delta_balance - $trc['amount_service'];
                                $hutang = $prev_hutang + $delta_hutang;

                                $m_trc['MLedgerID'] = $m_trc['C001_SubTrcTypeID'];
                                unset($m_trc['C001_SubTrcTypeID']);
                                $m_trc['SubLedger1ID'] = 0; // di hutang, tidak ada account bank yang ter-relasi
                                $m_trc['MinPlus'] = $hutang <= 0 ? 1 : -1;
                                $m_trc['AmountTrc'] = abs($hutang);
                                if ($delta_hutang >= 0) {
                                        $m_trc['AmountInTrc'] = abs($delta_hutang);
                                        $m_trc['AmountOutTrc'] = 0;
                                } else {
                                        $m_trc['AmountInTrc'] = 0;
                                        $m_trc['AmountOutTrc'] = abs($delta_hutang);
                                }

                                $this->trc->insert($m_trc);

                                // 2.a. add one row for adjusting balance in table T610_DayDelta
                                $m_day_delta['MLedgerID'] = $m_trc['MLedgerID'];
                                $m_day_delta['SubLedger1ID'] = 0;

                                $this->day_delta->update($m_day_delta, $delta_hutang);
                        }


                        // 3. add one row for adjusting "pendapatan" in table T610_Trc
                        if (abs($trc['amount_service']) > 0) {
                                $where = array(//re-use variable
                                    'MLedgerID' => 9, // pendapatan
                                    'SLedgerID' => $header['C045_OfficeID'],
                                    'SubLedger2ID' => 1, // 1. Operation, karena di function ini semuanya operation
                                );
                                $m_latest_trc = $this->trc->get_latest($where); // get latest pendapatan

                                $prev_pendapatan = $m_latest_trc ? ($m_latest_trc['AmountTrc'] * $m_latest_trc['MinPlus'] ) : 0;
                                $pendapatan = $prev_pendapatan + $trc['amount_service'];

                                $m_trc['MLedgerID'] = 9; // pendapatan
                                unset($m_trc['C001_SubTrcTypeID']);
                                $m_trc['SubLedger1ID'] = 0; // di pendapatan, tidak ada account bank yang ter-relasi
                                $m_trc['MinPlus'] = $pendapatan >= 0 ? 1 : -1;
                                $m_trc['AmountTrc'] = abs($pendapatan);
                                if ($trc['amount_service'] >= 0) {
                                        $m_trc['AmountInTrc'] = abs($trc['amount_service']);
                                        $m_trc['AmountOutTrc'] = 0;
                                } else {
                                        $m_trc['AmountInTrc'] = 0;
                                        $m_trc['AmountOutTrc'] = abs($trc['amount_service']);
                                }

                                $this->trc->insert($m_trc);

                                // 2.a. add one row for adjusting balance in table T610_DayDelta
                                $m_day_delta['MLedgerID'] = $m_trc['MLedgerID'];
                                $this->day_delta->update($m_day_delta, $trc['amount_service']);
                        }
                }

                echo 1;
        }

        function save_internal_operasional() {
                $this->layout = FALSE;

                $header = $this->get_header();

                $kas_bank = $this->input->post('kas_bank');
                $bank_account = $this->input->post('bank_account') ? : 0;
                $delta_amount = $this->input->post('amount');
                $is_amount_in = $this->input->post('is_amount_in');
                $desc = $this->input->post('desc');

                if (!$delta_amount) {
                        echo -1;
                        die();
                }
                if (!$is_amount_in) {
                        $delta_amount = $delta_amount * -1;
                }

                $where = array(
                    'MLedgerID' => $kas_bank,
                    'SLedgerID' => $header['C045_OfficeID'],
                    'SubLedger2ID' => 1, //Operation
                );
                if ($where['MLedgerID'] == 3) { //bank
                        $where['SubLedger1ID'] = $bank_account;
                }
                $m_latest_trc = $this->trc->get_latest($where);

                $prev_amount = isset($m_latest_trc['AmountTrc']) ? $m_latest_trc['AmountTrc'] : 0;
                $amount = $prev_amount + $delta_amount;

                $m_rekap = array(
                    'C010_TrcTypeID' => $header['C010_TrcTypeID'],
                    'C011_Month' => $header['C011_Month'],
                    'C012_TrcID' => $header['C000_SysID'],
                    'C050_Rev' => 1, // Sementara, karena belum ada draft
                    'C000_LineID' => 1,
                    'C001_SubTrcTypeID' => 0,
                    'C070_CurrencyID' => 1,
                    'C071_Rate' => 1,
                    'C071_RateFrom' => 0,
                    'C071_RateTo' => 0,
                    'C072_Amount1' => $prev_amount,
                    'C073_Amount2' => $amount,
                    'C073_Amount3' => $delta_amount,
                    'C210_MLedgerID' => $kas_bank, // 2. kas or 3. bank
                    'C200_SLedgerID' => $header['C045_OfficeID'],
                    'C211_SubLedger1ID' => $bank_account,
                    'C212_SubLedger2ID' => 1, //Operation
                );

                $this->t510_rekap->insert($m_rekap);

                $m_trc = array();
                foreach ($this->trc->get_columns() AS $column) {
                        if (isset($m_rekap[$column])) { // copy value if have same key/column
                                $m_trc[$column] = $m_rekap[$column];
                        }
                }
                $m_trc['MLedgerID'] = $m_rekap['C210_MLedgerID'];
                $m_trc['SLedgerID'] = $m_rekap['C200_SLedgerID'];
                $m_trc['SubLedger1ID'] = $m_rekap['C211_SubLedger1ID'];
                $m_trc['SubLedger2ID'] = $m_rekap['C212_SubLedger2ID'];

                $m_trc['C000_TrcID'] = $m_rekap['C012_TrcID'];
                $m_trc['C012_LineID'] = $m_rekap['C000_LineID'];
                $m_trc['C014_MonthPostIdx'] = $this->trc->get_curr_month();
                $m_trc['YearIdx'] = $this->trc->get_curr_year();
                $m_trc['MonthIdx'] = $this->trc->get_curr_month();
                $m_trc['C015_DatePos'] = $this->trc->get_curr_date();
                $m_trc['C016_TimePost'] = $this->trc->get_curr_time();
                $m_trc['C050_DocNum'] = $header['C050_DocNum'];
                $m_trc['C050_DocDate'] = $header['C050_DocDate'];
                $m_trc['C050_AccountDate'] = $this->trc->get_curr_datetime();
                $m_trc['Currency'] = 'IDR';
                $m_trc['Rate'] = 1;
                $m_trc['Description'] = $desc;

                // add one row for adjusting balance in table T610_Trc
                $m_trc['MinPlus'] = $delta_amount >= 0 ? 1 : -1;
                $m_trc['AmountTrc'] = abs($amount);
                $m_trc['AmountInTrc'] = 0;
                $m_trc['AmountOutTrc'] = 0;
                if ($delta_amount >= 0) {
                        $m_trc['AmountInTrc'] = $delta_amount;
                } else {
                        $m_trc['AmountOutTrc'] = $delta_amount;
                }

                $this->trc->insert($m_trc);

                // add one row for adjusting balance in table T610_DayDelta
                $m_day_delta['MLedgerID'] = $m_trc['MLedgerID'];
                $m_day_delta['SLedgerID'] = $m_trc['SLedgerID'];
                $m_day_delta['SubLedger1ID'] = $m_trc['SubLedger1ID'];
                $m_day_delta['SubLedger2ID'] = $m_trc['SubLedger2ID'];
                $m_day_delta['datePeriod'] = $this->trc->get_curr_date();

                $this->day_delta->update($m_day_delta, $delta_amount);

                echo 1;
        }

        function save_transaksi_program() {
                $this->layout = FALSE;

                $header = $this->get_header();

                $transactions = $this->input->post('transactions');

                foreach ($transactions as $key => $trc) {
                        if (!$trc['penyaluran']) {
                                continue;
                        }

                        $where = array(
                            'MLedgerID' => 2, // KAS
                            'SLedgerID' => $header['C045_OfficeID'],
                            'C001_SubTrcTypeID' => $trc['program_id'],
                            'SubLedger2ID' => 2, // MENSOS
                        );

                        $m_latest_trc = $this->trc->get_latest($where);

                        $prev_balance = 0;
                        if ($m_latest_trc) {
                                $prev_balance = $m_latest_trc['AmountTrc'];
                        } else {
                                $project = $this->project->get_one(array(
                                    'C000_SysID' => $trc['program_id'],
                                ));
                                $prev_balance = $project ? $project['C040_Value'] : 0;
                        }

                        $delta_balance = $trc['penyaluran'];
                        $balance = $prev_balance - $delta_balance;

                        $m_rekap = array(
                            'C010_TrcTypeID' => $header['C010_TrcTypeID'],
                            'C011_Month' => $header['C011_Month'],
                            'C012_TrcID' => $header['C000_SysID'],
                            'C050_Rev' => 1, // Sementara, karena belum ada draft
                            'C000_LineID' => 1,
                            'C001_SubTrcTypeID' => $trc['program_id'],
                            'C070_CurrencyID' => 1,
                            'C071_Rate' => 1,
                            'C071_RateFrom' => 0,
                            'C071_RateTo' => 0,
                            'C072_Amount1' => $prev_balance,
                            'C073_Amount2' => $balance,
                            'C073_Amount3' => -abs($delta_balance),
                            'C210_MLedgerID' => 2, // KAS
                            'C200_SLedgerID' => $header['C045_OfficeID'],
                            'C211_SubLedger1ID' => 0, // account bank
                            'C212_SubLedger2ID' => 2, // MENSOS
                        );

                        $this->t510_rekap->insert($m_rekap);

                        $m_trc = array();
                        foreach ($this->trc->get_columns() AS $column) {
                                if (isset($m_rekap[$column])) { // copy value if have same key/column
                                        $m_trc[$column] = $m_rekap[$column];
                                }
                        }
                        $m_trc['MLedgerID'] = $m_rekap['C210_MLedgerID'];
                        $m_trc['SLedgerID'] = $m_rekap['C200_SLedgerID'];
                        $m_trc['SubLedger1ID'] = $m_rekap['C211_SubLedger1ID'];
                        $m_trc['SubLedger2ID'] = $m_rekap['C212_SubLedger2ID'];

                        $m_trc['C000_TrcID'] = $m_rekap['C012_TrcID'];
                        $m_trc['C012_LineID'] = $m_rekap['C000_LineID'];
                        $m_trc['C014_MonthPostIdx'] = $this->trc->get_curr_month();
                        $m_trc['YearIdx'] = $this->trc->get_curr_year();
                        $m_trc['MonthIdx'] = $this->trc->get_curr_month();
                        $m_trc['C015_DatePos'] = $this->trc->get_curr_date();
                        $m_trc['C016_TimePost'] = $this->trc->get_curr_time();
                        $m_trc['C050_DocNum'] = $header['C050_DocNum'];
                        $m_trc['C050_DocDate'] = $header['C050_DocDate'];
                        $m_trc['C050_AccountDate'] = $this->trc->get_curr_datetime();
                        $m_trc['Currency'] = 'IDR';
                        $m_trc['Rate'] = 1;

                        // 1. add one row for adjusting balance in table T610_Trc
                        $m_trc['MinPlus'] = -1;
                        $m_trc['AmountTrc'] = abs($balance);
                        $m_trc['AmountInTrc'] = 0;
                        $m_trc['AmountOutTrc'] = abs($delta_balance);

                        $this->trc->insert($m_trc);

                        // 1.a. add one row for adjusting balance in table T610_DayDelta
                        $m_day_delta['MLedgerID'] = $m_trc['MLedgerID'];
                        $m_day_delta['SLedgerID'] = $m_trc['SLedgerID'];
                        $m_day_delta['SubLedger1ID'] = $m_trc['SubLedger1ID'];
                        $m_day_delta['SubLedger2ID'] = $m_trc['SubLedger2ID'];
                        $m_day_delta['datePeriod'] = $this->trc->get_curr_date();

                        $this->day_delta->update($m_day_delta, -abs($delta_balance));

                        // 2. add one row for adjusting "hutang" in table T610_Trc
                        $where = array(//re-use variable
                            'MLedgerID' => 5, // Hutang Salur Dana
                            'SLedgerID' => $header['C045_OfficeID'],
                            'SubLedger1ID' => $trc['program_id'],
                            'SubLedger2ID' => 2, // MENSOS
                        );
                        $m_latest_trc = $this->trc->get_latest($where); // get latest hutang

                        $prev_hutang = $m_latest_trc ? $m_latest_trc['AmountTrc'] : 0;
                        $delta_hutang = abs($trc['penyaluran']);
                        $hutang = $prev_hutang + $delta_hutang;

                        $m_trc['MLedgerID'] = 5; // Hutang Salur Dana
                        unset($m_trc['C001_SubTrcTypeID']);
                        $m_trc['SubLedger1ID'] = 0; // di hutang, tidak ada account bank yang ter-relasi
                        $m_trc['MinPlus'] = -1;
                        $m_trc['AmountTrc'] = abs($hutang);
                        $m_trc['AmountInTrc'] = abs($delta_hutang);
                        $m_trc['AmountOutTrc'] = 0;

                        $this->trc->insert($m_trc);

                        // 2.a. add one row for adjusting balance in table T610_DayDelta
                        $m_day_delta['MLedgerID'] = $m_trc['MLedgerID'];
                        $m_day_delta['SubLedger1ID'] = 0;

                        $this->day_delta->update($m_day_delta, $delta_hutang * -1);
                }

                echo 1;
        }

        function save_transfer_kasbank() {
                $this->layout = FALSE;

                $header = $this->get_header();

                $trf_from = $this->input->post('trf_from');
                $trf_to = $this->input->post('trf_to');
                $amount = abs($this->input->post('amount'));
                $desc = $this->input->post('description');

                $m_rekap_from = $this->save_rekap_trf_kasbank_from($header, $trf_from, $amount);
                $this->save_trc_trf_kasbank_from($header, $m_rekap_from, $desc);

                $m_rekap_to = $this->save_rekap_trf_kasbank_to($header, $trf_to, $amount);
                $this->save_trc_trf_kasbank_to($header, $m_rekap_to, $desc);

                echo 1;
        }

        private function save_rekap_trf_kasbank_from($header, $trf_from, $amount) {
                $where_from = array(
                    'MLedgerID' => $trf_from['kas_bank'], // KAS
                    'SLedgerID' => $header['C045_OfficeID'],
                    'SubLedger2ID' => $trf_from['sumber'], // OPERATION | MENSOS
                );
                $SubTrcTypeID = 0;
                if ($trf_from['sumber'] == 2) { //MENSOS
                        $where_from['C001_SubTrcTypeID'] = $trf_from['program_id'];
                        $SubTrcTypeID = $where_from['C001_SubTrcTypeID'];
                }

                $SubLedger1ID = 0;
                if ($trf_from['kas_bank'] == 3) { // if BANK
                        $where_from['SubLedger1ID'] = $trf_from['bank_account'];
                        $SubLedger1ID = $where_from['SubLedger1ID'];
                }

                $m_latest_trc_from = $this->trc->get_latest($where_from);
                $prev_balance_from = $m_latest_trc_from ? $m_latest_trc_from['AmountTrc'] : 0;
                if ($prev_balance_from - $amount < 0) {
                        echo -1;
                        die();
                }

                $delta_balance_from = -$amount;
                $balance_from = $prev_balance_from + $delta_balance_from;

                $m_rekap_from = array(
                    'C010_TrcTypeID' => $header['C010_TrcTypeID'],
                    'C011_Month' => $header['C011_Month'],
                    'C012_TrcID' => $header['C000_SysID'],
                    'C050_Rev' => 1, // Sementara, karena belum ada draft
                    'C000_LineID' => 1,
                    'C001_SubTrcTypeID' => $SubTrcTypeID,
                    'C070_CurrencyID' => 1,
                    'C071_Rate' => 1,
                    'C071_RateFrom' => 0,
                    'C071_RateTo' => 0,
                    'C072_Amount1' => $prev_balance_from,
                    'C073_Amount2' => $balance_from,
                    'C073_Amount3' => $delta_balance_from,
                    'C210_MLedgerID' => $trf_from['kas_bank'], // KAS | BANK
                    'C200_SLedgerID' => $header['C045_OfficeID'],
                    'C211_SubLedger1ID' => $SubLedger1ID, // account bank
                    'C212_SubLedger2ID' => $trf_from['sumber'], // MENSOS
                );
                $this->t510_rekap->insert($m_rekap_from);

                return $m_rekap_from;
        }

        private function save_trc_trf_kasbank_from($header, $m_rekap, $desc) {
                $m_trc = array();
                foreach ($this->trc->get_columns() AS $column) {
                        if (isset($m_rekap[$column])) { // copy value if have same key/column
                                $m_trc[$column] = $m_rekap[$column];
                        }
                }
                $m_trc['MLedgerID'] = $m_rekap['C210_MLedgerID'];
                $m_trc['SLedgerID'] = $m_rekap['C200_SLedgerID'];
                $m_trc['SubLedger1ID'] = $m_rekap['C211_SubLedger1ID'];
                $m_trc['SubLedger2ID'] = $m_rekap['C212_SubLedger2ID'];

                $m_trc['C000_TrcID'] = $m_rekap['C012_TrcID'];
                $m_trc['C012_LineID'] = $m_rekap['C000_LineID'];
                $m_trc['C014_MonthPostIdx'] = $this->trc->get_curr_month();
                $m_trc['YearIdx'] = $this->trc->get_curr_year();
                $m_trc['MonthIdx'] = $this->trc->get_curr_month();
                $m_trc['C015_DatePos'] = $this->trc->get_curr_date();
                $m_trc['C016_TimePost'] = $this->trc->get_curr_time();
                $m_trc['C050_DocNum'] = $header['C050_DocNum'];
                $m_trc['C050_DocDate'] = $header['C050_DocDate'];
                $m_trc['C050_AccountDate'] = $this->trc->get_curr_datetime();
                $m_trc['Currency'] = 'IDR';
                $m_trc['Rate'] = 1;
                $m_trc['Description'] = $desc;

                // add one row for adjusting balance in table T610_Trc 
                $m_trc['MinPlus'] = -1;
                $m_trc['AmountTrc'] = $m_rekap['C073_Amount2'];
                $m_trc['AmountInTrc'] = 0;
                $m_trc['AmountOutTrc'] = abs($m_rekap['C073_Amount3']);

                $this->trc->insert($m_trc);

                // add one row for adjusting balance in table T610_DayDelta
                $m_day_delta['MLedgerID'] = $m_trc['MLedgerID'];
                $m_day_delta['SLedgerID'] = $m_trc['SLedgerID'];
                $m_day_delta['SubLedger1ID'] = $m_trc['SubLedger1ID'];
                $m_day_delta['SubLedger2ID'] = $m_trc['SubLedger2ID'];
                $m_day_delta['datePeriod'] = $this->trc->get_curr_date();

                $this->day_delta->update($m_day_delta, -abs($m_rekap['C073_Amount2']));
        }

        private function save_trc_trf_kasbank_to($header, $m_rekap, $desc) {
                $m_trc = array();
                foreach ($this->trc->get_columns() AS $column) {
                        if (isset($m_rekap[$column])) { // copy value if have same key/column
                                $m_trc[$column] = $m_rekap[$column];
                        }
                }
                $m_trc['MLedgerID'] = $m_rekap['C210_MLedgerID'];
                $m_trc['SLedgerID'] = $m_rekap['C200_SLedgerID'];
                $m_trc['SubLedger1ID'] = $m_rekap['C211_SubLedger1ID'];
                $m_trc['SubLedger2ID'] = $m_rekap['C212_SubLedger2ID'];

                $m_trc['C000_TrcID'] = $m_rekap['C012_TrcID'];
                $m_trc['C012_LineID'] = $m_rekap['C000_LineID'];
                $m_trc['C014_MonthPostIdx'] = $this->trc->get_curr_month();
                $m_trc['YearIdx'] = $this->trc->get_curr_year();
                $m_trc['MonthIdx'] = $this->trc->get_curr_month();
                $m_trc['C015_DatePos'] = $this->trc->get_curr_date();
                $m_trc['C016_TimePost'] = $this->trc->get_curr_time();
                $m_trc['C050_DocNum'] = $header['C050_DocNum'];
                $m_trc['C050_DocDate'] = $header['C050_DocDate'];
                $m_trc['C050_AccountDate'] = $this->trc->get_curr_datetime();
                $m_trc['Currency'] = 'IDR';
                $m_trc['Rate'] = 1;
                $m_trc['Description'] = $desc;

                // add one row for adjusting balance in table T610_Trc 
                $m_trc['MinPlus'] = 1;
                $m_trc['AmountTrc'] = $m_rekap['C073_Amount2'];
                $m_trc['AmountInTrc'] = abs($m_rekap['C073_Amount3']);
                $m_trc['AmountOutTrc'] = 0;

                $this->trc->insert($m_trc);

                // add one row for adjusting balance in table T610_DayDelta
                $m_day_delta['MLedgerID'] = $m_trc['MLedgerID'];
                $m_day_delta['SLedgerID'] = $m_trc['SLedgerID'];
                $m_day_delta['SubLedger1ID'] = $m_trc['SubLedger1ID'];
                $m_day_delta['SubLedger2ID'] = $m_trc['SubLedger2ID'];
                $m_day_delta['datePeriod'] = $this->trc->get_curr_date();

                $this->day_delta->update($m_day_delta, abs($m_rekap['C073_Amount2']));
        }

        private function save_rekap_trf_kasbank_to($header, $trf_to, $amount) {
                $where_to = array(
                    'MLedgerID' => $trf_to['kas_bank'], // KAS
                    'SLedgerID' => $header['C045_OfficeID'],
                    'SubLedger2ID' => $trf_to['sumber'], // OPERATION | MENSOS
                );
                $SubTrcTypeID = 0;
                if ($trf_to['sumber'] == 2) { //MENSOS
                        $where_to['C001_SubTrcTypeID'] = $trf_to['program_id'];
                        $SubTrcTypeID = $where_to['C001_SubTrcTypeID'];
                }

                $SubLedger1ID = 0;
                if ($trf_to['kas_bank'] == 3) { // if BANK
                        $where_to['SubLedger1ID'] = $trf_to['bank_account'];
                        $SubLedger1ID = $where_to['SubLedger1ID'];
                }

                $m_latest_trc_to = $this->trc->get_latest($where_to);
                $prev_balance_to = $m_latest_trc_to ? $m_latest_trc_to['AmountTrc'] : 0;
                $delta_balance_to = $amount;
                $balance_to = $prev_balance_to + $delta_balance_to;

                $m_rekap_to = array(
                    'C010_TrcTypeID' => $header['C010_TrcTypeID'],
                    'C011_Month' => $header['C011_Month'],
                    'C012_TrcID' => $header['C000_SysID'],
                    'C050_Rev' => 1, // Sementara, karena belum ada draft
                    'C000_LineID' => 1,
                    'C001_SubTrcTypeID' => $SubTrcTypeID,
                    'C070_CurrencyID' => 1,
                    'C071_Rate' => 1,
                    'C071_RateFrom' => 0,
                    'C071_RateTo' => 0,
                    'C072_Amount1' => $prev_balance_to,
                    'C073_Amount2' => $balance_to,
                    'C073_Amount3' => $delta_balance_to,
                    'C210_MLedgerID' => $trf_to['kas_bank'], // KAS | BANK
                    'C200_SLedgerID' => $header['C045_OfficeID'],
                    'C211_SubLedger1ID' => $SubLedger1ID, // account bank
                    'C212_SubLedger2ID' => $trf_to['sumber'], // MENSOS
                );
                $this->t510_rekap->insert($m_rekap_to);

                return $m_rekap_to;
        }

        function is_trc_pelanggan_valid($transactions) {
                if (!$transactions) {
                        return false;
                }

                foreach ($transactions AS $key => $trc) {
                        foreach ($trc AS $key_trc => $value_trc) {
                                if ($key_trc == 'rek_bank') {
                                        continue;
                                }

                                if ($value_trc < 1) {
                                        return 0;
                                }
                        }

                        if ($trc['jns_bayar'] == 3 && $trc['rek_bank'] < 1) {
                                return 0;
                        }
                }

                return true;
        }

        function transfer_kas_bank() {
                $this->layout = FALSE;
                // [0]dt_trans.push(asal_smb);
                // [1]dt_trans.push(asal_prog);
                // [2]dt_trans.push(asal_jns);
                // [3]dt_trans.push(asal_rek);
                // [4]dt_trans.push(tuju_smb);
                // [5]dt_trans.push(tuju_prog);
                // [6]dt_trans.push(tuju_jns);
                // [7]dt_trans.push(tuju_rek);
                // [8]dt_trans.push(vol_trans);
                // [9]dt_trans.push(ket_trans);
                $officeId = $this->user_data['C002_OfficeID'];
                $dt_trans = $_POST["dt_trans"];
                $head_ins = $_POST["head_ins"];

                echo"sukses";
        }

        // function transfer_keluar() {
        //         $this->layout = FALSE;
        //         //    [0] dt_trans.push(asal_smb);
        //         //    [1] dt_trans.push(asal_prog);
        //         //    [2] dt_trans.push(asal_jns);
        //         //    [3] dt_trans.push(asal_rek);
        //         //    [4] dt_trans.push(tuju_off);
        //         //    [5] dt_trans.push(tuju_smb);
        //         //    [6] dt_trans.push(tuju_prog);
        //         //    [7] dt_trans.push(tuju_jns);
        //         //    [8] dt_trans.push(tuju_rek);
        //         //    [9] dt_trans.push(vol_trans);
        //         $officeId = $this->user_data['C002_OfficeID'];
        //         $dt_trans = $_POST["dt_trans"];
        //         $head_ins = $_POST["head_ins"];
        //         $tjn = $_POST["tjn"];
        //         echo"sukses";
        // }


        function get_transfer_masuk() {
                
        }

        function transfer_keluar() {
                $this->layout = FALSE;

                $header = $this->get_header();
                $trf_from = $_POST["dt_trans"];
                $tjn = $_POST["tjn"];
                $trf_from['bank_account'] = $trf_from[3];
                $trf_from['source'] = $trf_from[0];
                $trf_from['program_id'] = $trf_from[1];
                $trf_from['kas_bank'] = $trf_from[2];
                //$trf_from = $this->input->post('trf_from');
                $delta_amount = abs($trf_from[9]) * -1;

                $where = array(
                    'MLedgerID' => $trf_from['kas_bank'],
                    'SLedgerID' => $header['C045_OfficeID'],
                    'SubLedger2ID' => $trf_from['source'], //Operation
                );
                $SubLedger1ID = 0;
                if ($where['MLedgerID'] == 3) { //bank
                        $where['SubLedger1ID'] = $trf_from['bank_account'];
                        $SubLedger1ID = $trf_from['bank_account'];
                }
                $m_latest_trc = $this->trc->get_latest($where);

                $prev_amount = isset($m_latest_trc['AmountTrc']) ? $m_latest_trc['AmountTrc'] : 0;
                $amount = $prev_amount + $delta_amount;

                $m_rekap = array(
                    'C010_TrcTypeID' => $header['C010_TrcTypeID'],
                    'C011_Month' => $header['C011_Month'],
                    'C012_TrcID' => $header['C000_SysID'],
                    'C050_Rev' => 1, // Sementara, karena belum ada draft
                    'C000_LineID' => 1,
                    'C001_SubTrcTypeID' => $trf_from['program_id'],
                    'C070_CurrencyID' => 1,
                    'C071_Rate' => 1,
                    'C071_RateFrom' => 0,
                    'C071_RateTo' => 0,
                    'C072_Amount1' => $prev_amount,
                    'C073_Amount2' => $amount,
                    'C073_Amount3' => $delta_amount,
                    'C210_MLedgerID' => $trf_from['kas_bank'], // 2. kas or 3. bank
                    'C200_SLedgerID' => $header['C045_OfficeID'],
                    'C211_SubLedger1ID' => $SubLedger1ID,
                    'C212_SubLedger2ID' => $trf_from['source'], //Operation
                );

                $this->t510_rekap->insert($m_rekap);
                // dt_trans.push(asal_smb);
                // dt_trans.push(asal_prog);
                // dt_trans.push(asal_jns);
                // dt_trans.push(asal_rek);
                // dt_trans.push(tuju_tgl);
                // dt_trans.push(tuju_bank);
                // dt_trans.push(tuju_rek);
                // dt_trans.push(tuju_nama);
                // dt_trans.push(tuju_ket);
                // dt_trans.push(vol_trans);
                // $trf_to = $this->input->post('trf_to');


                $tujuan = $this->input->post('tjn');
                $m_external_transfer = NULL;
                if ($tujuan != 1) {
                        $trf_to['date'] = $trf_from[4];
                        $trf_to['bank_name'] = $trf_from[5];
                        $trf_to['bank_account'] = $trf_from[6];
                        $trf_to['bank_account_name'] = $trf_from[7];
                        $trf_to['desc'] = $trf_from[8];
                        $m_external_transfer = array(
                            "C001_DateTransfer" => $trf_to['date'],
                            "C002_BankName" => $trf_to['bank_name'],
                            "C003_BankAccNumber" => $trf_to['bank_account'],
                            "C004_BankAccName" => $trf_to['bank_account_name'],
                            "C005_Descr" => $trf_to['desc'],
                        );

                        $this->external_transfer->insert($m_external_transfer);
                        $m_external_transfer = $this->external_transfer->get_one($m_external_transfer);
                } else {
                        
                }

                $m_trc = array();
                foreach ($this->trc->get_columns() AS $column) {
                        if (isset($m_rekap[$column])) { // copy value if have same key/column
                                $m_trc[$column] = $m_rekap[$column];
                        }
                }
                $m_trc['MLedgerID'] = $m_rekap['C210_MLedgerID'];
                $m_trc['SLedgerID'] = $m_rekap['C200_SLedgerID'];
                $m_trc['SubLedger1ID'] = $m_rekap['C211_SubLedger1ID'];
                $m_trc['SubLedger2ID'] = $m_rekap['C212_SubLedger2ID'];
                if ($m_external_transfer) {
                        $m_trc['SubLedger3ID'] = $m_external_transfer['C000_SysID'];
                }
                $m_trc['C000_TrcID'] = $m_rekap['C012_TrcID'];
                $m_trc['C012_LineID'] = $m_rekap['C000_LineID'];
                $m_trc['C014_MonthPostIdx'] = $this->trc->get_curr_month();
                $m_trc['YearIdx'] = $this->trc->get_curr_year();
                $m_trc['MonthIdx'] = $this->trc->get_curr_month();
                $m_trc['C015_DatePos'] = $this->trc->get_curr_date();
                $m_trc['C016_TimePost'] = $this->trc->get_curr_time();
                $m_trc['C050_DocNum'] = $header['C050_DocNum'];
                $m_trc['C050_DocDate'] = $header['C050_DocDate'];
                $m_trc['C050_AccountDate'] = $this->trc->get_curr_datetime();
                $m_trc['Currency'] = 'IDR';
                $m_trc['Rate'] = 1;

                $m_trc['MinPlus'] = -1;
                $m_trc['AmountTrc'] = $amount;
                $m_trc['AmountInTrc'] = 0;
                $m_trc['AmountOutTrc'] = abs($delta_amount);

                $this->trc->insert($m_trc);

                // add one row for adjusting balance in table T610_DayDelta
                $m_day_delta['MLedgerID'] = $m_trc['MLedgerID'];
                $m_day_delta['SLedgerID'] = $m_trc['SLedgerID'];
                $m_day_delta['SubLedger1ID'] = $m_trc['SubLedger1ID'];
                $m_day_delta['SubLedger2ID'] = $m_trc['SubLedger2ID'];
                $m_day_delta['datePeriod'] = $this->trc->get_curr_date();

                $this->day_delta->update($m_day_delta, $delta_amount);

                //         //    [4] dt_trans.push(tuju_off);
                //         //    [5] dt_trans.push(tuju_smb);
                //         //    [6] dt_trans.push(tuju_prog);
                //         //    [7] dt_trans.push(tuju_jns);
                //         //    [8] dt_trans.push(tuju_rek);
                //         //    [9] dt_trans.push(vol_trans);

                if ($tujuan == 1) {
                        $trf_to['office_id'] = $trf_from[4];
                        $trf_to['sumber'] = $trf_from[5];
                        $trf_to['kas_bank'] = $trf_from[7];
                        $trf_to['bank_account'] = $trf_from[8];

                        $m_trc['MLedgerID'] = 10;
                        $m_trc['SLedgerID'] = $trf_to['office_id'];
                        $m_trc['SubLedger2ID'] = $trf_to['sumber'];
                        $m_trc['SubLedger3ID'] = $trf_to['kas_bank'];
                        if ($m_trc['SubLedger3ID'] == 3) { //bank
                                $m_trc['SubLedger1ID'] = $trf_to['bank_account'];
                        }
                        $m_trc['MinPlus'] = 1;
                        $m_trc['AmountTrc'] = 0;
                        $m_trc['AmountInTrc'] = abs($delta_amount);
                        $m_trc['AmountOutTrc'] = 0;

                        $this->trc->insert($m_trc);
                }

                echo 1;
        }

        function accept_transfer() {
                $this->layout = FALSE;

                $header = $this->get_header();

                $trc_id = $this->input->post("trc_id");
                $trc = $this->trc->get_one(array(
                    'C000_TrcID' => $trc_id,
                    'MLedgerID' => 10,
                ));

                $where = array(
                    'MLedgerID' => $trc['SubLedger3ID'],
                    'SLedgerID' => $header['C045_OfficeID'],
                    'SubLedger2ID' => $trc['source'], //Operation
                );
                $SubLedger1ID = 0;
                if ($where['MLedgerID'] == 3) { //bank
                        $where['SubLedger1ID'] = $trc['SubLedger1ID'];
                        $SubLedger1ID = $trc['SubLedger1ID'];
                }
                $m_latest_trc = $this->trc->get_latest($where);

                $m_rekap = array(
                    'C010_TrcTypeID' => $header['C010_TrcTypeID'],
                    'C011_Month' => $header['C011_Month'],
                    'C012_TrcID' => $header['C000_SysID'],
                    'C050_Rev' => 1, // Sementara, karena belum ada draft
                    'C000_LineID' => 1,
                    'C001_SubTrcTypeID' => $trc['C001_SubTrcTypeID'],
                    'C070_CurrencyID' => 1,
                    'C071_Rate' => 1,
                    'C071_RateFrom' => 0,
                    'C071_RateTo' => 0,
                    'C072_Amount1' => $m_latest_trc['AmountTrc'],
                    'C073_Amount2' => $m_latest_trc['AmountTrc'] + $trc['AmountInTrc'],
                    'C073_Amount3' => $trc['AmountInTrc'],
                    'C210_MLedgerID' => $trc['SubLedger3ID'], // 2. kas or 3. bank
                    'C200_SLedgerID' => $header['C045_OfficeID'],
                    'C211_SubLedger1ID' => $trc['SubLedger1ID'],
                    'C212_SubLedger2ID' => $trc['SubLedger2ID'], //Operation
                );

                $this->t510_rekap->insert($m_rekap);

                $m_trc = array();
                foreach ($this->trc->get_columns() AS $column) {
                        if (isset($m_rekap[$column])) { // copy value if have same key/column
                                $m_trc[$column] = $m_rekap[$column];
                        }
                }
                $m_trc['MLedgerID'] = $m_rekap['C210_MLedgerID'];
                $m_trc['SLedgerID'] = $m_rekap['C200_SLedgerID'];
                $m_trc['SubLedger1ID'] = $m_rekap['C211_SubLedger1ID'];
                $m_trc['SubLedger2ID'] = $m_rekap['C212_SubLedger2ID'];

                $m_trc['C000_TrcID'] = $m_rekap['C012_TrcID'];
                $m_trc['C012_LineID'] = $m_rekap['C000_LineID'];
                $m_trc['C014_MonthPostIdx'] = $this->trc->get_curr_month();
                $m_trc['YearIdx'] = $this->trc->get_curr_year();
                $m_trc['MonthIdx'] = $this->trc->get_curr_month();
                $m_trc['C015_DatePos'] = $this->trc->get_curr_date();
                $m_trc['C016_TimePost'] = $this->trc->get_curr_time();
                $m_trc['C050_DocNum'] = $header['C050_DocNum'];
                $m_trc['C050_DocDate'] = $header['C050_DocDate'];
                $m_trc['C050_AccountDate'] = $this->trc->get_curr_datetime();
                $m_trc['Currency'] = 'IDR';
                $m_trc['Rate'] = 1;

                // add one row for adjusting balance in table T610_Trc 
                $m_trc['MinPlus'] = 1;
                $m_trc['AmountTrc'] = $m_latest_trc['AmountTrc'] + $trc['AmountInTrc'];
                $m_trc['AmountInTrc'] = $trc['AmountInTrc'];
                $m_trc['AmountOutTrc'] = 0;

                $this->trc->insert($m_trc);

                // add one row for adjusting balance in table T610_DayDelta
                $m_day_delta['MLedgerID'] = $m_trc['MLedgerID'];
                $m_day_delta['SLedgerID'] = $m_trc['SLedgerID'];
                $m_day_delta['SubLedger1ID'] = $m_trc['SubLedger1ID'];
                $m_day_delta['SubLedger2ID'] = $m_trc['SubLedger2ID'];
                $m_day_delta['datePeriod'] = $this->trc->get_curr_date();

                $this->day_delta->update($m_day_delta, $trc['AmountInTrc']);

                $this->trc->delete($trc);

                echo 1;
        }

        function remove_transfer() {
                $this->layout = FALSE;

                $trc_id = $this->input->post("trc_id");
                $trf = $this->trc->get_one(array(
                    'C000_TrcID' => $trc_id,
                    'MLedgerID' => 10,
                ));
                $this->trc->delete($trf);

                echo 1;
        }

        function get_header() {
                $check_node = $this->check_node;
                if ($check_node) {
                        $rev = $this->check_node['C050_Rev'] + 1;
                } else {
                        $rev = 1;
                }

                return $this->create_header(TRC_TYPE_TRANSACTION, $rev, $check_node);
        }

        function get_saldo() {
                $this->layout = false;
                $sl = $_POST["sl"];
                $sl2 = $_POST["sl2"];
                $sl3 = $_POST["sl3"];
                $sl4 = $_POST["sl4"];
                $officeId = $this->user_data['C002_OfficeID'];
                $row = $this->trc->get_saldo($criteria = "( MLedgerID = 1 OR MLedgerID = 9) AND SubLedger1ID = " . $officeId . " AND SLedgerID = " . $sl . " AND SubLedger2ID = " . $sl2 . " AND SubLedger3ID = " . $sl3 . " AND SubLedger4ID = " . $sl4 . "");
                $val = $row["AmountTrc"];

                echo $val;
        }

        private function save_rekap() {
                $where = array(
                    'MLedgerID' => $trc['jns_bayar'],
                    'SLedgerID' => $header['C045_OfficeID'],
                    'SubLedger2ID' => 1, //Operation
                );
                if ($where['MLedgerID'] == 3) { //bank
                        $where['SubLedger1ID'] = $trc['rek_bank'];
                }
                $m_latest_trc = $this->trc->get_latest($where);

                // balance
                $prev_balance = 0;
                if ($m_latest_trc) {
                        $prev_balance = $m_latest_trc['AmountTrc'];
                }
                $delta_balance = $trc['amount_in'] - $trc['amount_out'];
                $balance = $prev_balance + $delta_balance;

                $C211_SubLedger1ID = 0; //rek bank id
                if ($trc['jns_bayar'] == 3) { // if bank
                        $C211_SubLedger1ID = $trc['rek_bank'];
                }

                $m_rekap = array(
                    'C010_TrcTypeID' => $header['C010_TrcTypeID'],
                    'C011_Month' => $header['C011_Month'],
                    'C012_TrcID' => $header['C000_SysID'],
                    'C050_Rev' => 1, // Sementara, karena belum ada draft
                    'C000_LineID' => $trc['line_id'],
                    'C001_SubTrcTypeID' => $trc['type'],
                    'C070_CurrencyID' => 1,
                    'C071_Rate' => 1,
                    'C071_RateFrom' => 0,
                    'C071_RateTo' => 0,
                    'C072_Amount1' => $prev_balance,
                    'C073_Amount2' => $balance,
                    'C073_Amount3' => $delta_balance,
                    'C210_MLedgerID' => $trc['jns_bayar'], // 2. kas or 3. bank
                    'C200_SLedgerID' => $header['C045_OfficeID'],
                    'C211_SubLedger1ID' => $C211_SubLedger1ID,
                    'C212_SubLedger2ID' => 1, //Operation
                );

                $this->t510_rekap->insert($m_rekap);
        }

        /*
          function validate_row_input($row, $keys){
          foreach($keys AS $key){
          $key_arr = explode("_", $key);
          $minkey = $key_arr[0];
          $suffix = isset($key_arr[1]) ? $key_arr[0] : '';
          if($minkey == TRC_INPUT_NAME_MLEDGER
          && $row[$key] == 3
          && (!isset($row[TRC_INPUT_NAME_SUBLEDGER1ID . $suffix])
          || !$row[TRC_INPUT_NAME_SUBLEDGER1ID . $suffix])
          ){ //bank
          echo -1;die();
          }

          if($minkey == TRC_INPUT_NAME_SUBLEDGER2ID
          && $row[$key] == 2 // mensos
          && (!isset($row[TRC_INPUT_NAME_SUBLEDGER3ID . $suffix])
          || !$row[TRC_INPUT_NAME_SUBLEDGER3ID . $suffix])
          ){
          echo -1;die();
          }
          }
          }
         */

        function save_trc_plg() {
                $this->layout = FALSE;

                $header = $this->get_header();

                $rows = $this->input->post('data');

                foreach ($rows AS $row) {

                        $kas = array(
                            'MLedgerID' => $row['MLedgerID'],
                            'SubTrcTypeID' => $row['MLedgerID_1'],
                            'SubLedger1ID' => $row['SubLedger1ID'],
                            'SubLedger2ID' => 1, //Operation
                            'Amount' => $row['Amount'],
                            'LineID' => $row['LineID'],
                        );

                        $m_rekap = $this->store_to_rekap($header, $kas);
                        $this->store_to_rekap($header, $kas, $m_rekap);

                        $hutang = array(
                            'MLedgerID' => $row['MLedgerID_1'],
                            'SubTrcTypeID' => $row['MLedgerID_1'],
                            'SubLedger1ID' => 0,
                            'SubLedger2ID' => 1, //Operation
                            'Amount' => $row['Amount_1'],
                            'LineID' => $row['LineID'],
                        );

                        $m_rekap = $this->store_to_rekap($header, $hutang);
                        $this->store_to_rekap($header, $hutang, $m_rekap, TRUE);
                }

                echo 1;
        }

        private function store_to_rekap($header, $map_to_ledger, $trc) {
                $SubLedger1ID = 0; //rek bank id

                $where = array(
                    'MLedgerID' => $trc['MLedgerID'],
                    'SLedgerID' => $trc['SLedgerID'],
                    'SubLedger2ID' => $trc['SubLedger2ID'], //Operation / Mensos
                );

                if ($where['MLedgerID'] == 3) { //bank
                        $where['SubLedger1ID'] = $trc['SubLedger1ID'];
                        $SubLedger1ID = $trc['SubLedger1ID'];
                }
                $m_latest_trc = $this->trc->get_latest($where);

                // balance
                $prev_balance = 0;
                if ($m_latest_trc) {
                        $prev_balance = $m_latest_trc['AmountTrc'];
                }

                $delta_balance = $trc['Amount'] * ($map_to_ledger['isHutang'] ? -1 : 1) * $map_to_ledger['C020_PlusMinus'];
                $balance = $prev_balance + $delta_balance;

//                echo $map_to_ledger['isHutang'];

                $m_rekap = array(
                    'C010_TrcTypeID' => $header['C010_TrcTypeID'],
                    'C011_Month' => $header['C011_Month'],
                    'C012_TrcID' => $header['C000_SysID'],
                    'C050_Rev' => 1, // Sementara, karena belum ada draft
                    'C000_LineID' => isset($trc['LineID']) ? $trc['LineID'] : $trc['LineID'],
                    'C050_DocDate' => $header['C050_DocDate'],
                    'C010_TrcPanelID' => $map_to_ledger['C010_TrcPanelID'],
                    'C001_SubTrcTypeID' => $trc['SubTrcTypeID'],
                    'C070_CurrencyID' => 1,
                    'C071_Rate' => 1,
                    'C071_RateFrom' => 0,
                    'C071_RateTo' => 0,
                    'C072_Amount1' => $prev_balance,
                    'C073_Amount2' => $balance,
                    'C073_Amount3' => $delta_balance,
                    'C210_MLedgerID' => $trc['MLedgerID'], // 2. kas or 3. bank
                    'C200_SLedgerID' => $this->get_s_ledger_id($trc['SLedgerID'], $map_to_ledger['IsKPRK_Pusat']),
                    'C211_SubLedger1ID' => $SubLedger1ID,
                    'C212_SubLedger2ID' => isset($trc['SubLedger2ID']) ? $trc['SubLedger2ID'] : 0,
                    'C213_SubLedger3ID' => isset($trc['SubLedger3ID']) ? $trc['SubLedger3ID'] : 0, // Mensos program
                    'C214_SubLedger4ID' => isset($trc['SubLedger4ID']) ? $trc['SubLedger4ID'] : 0,
                    'C220_MLedgerIDTo' => isset($trc['MLedgerIDTo']) ? $trc['MLedgerIDTo'] : 0,
                    'C201_SLedgerIDTo' => isset($trc['SLedgerIDTo']) ? $trc['SLedgerIDTo'] : 0,
                    'C222_SubLedger1IDTo' => isset($trc['SubLedger1IDTo']) ? $trc['SubLedger1IDTo'] : 0,
                    'C223_SubLedger2IDTo' => isset($trc['SubLedger2IDTo']) ? $trc['SubLedger2IDTo'] : 0,
                    'C224_SubLedger3IDTo' => isset($trc['SubLedger3IDTo']) ? $trc['SubLedger3IDTo'] : 0,
                    'C225_SubLedger4IDTo' => isset($trc['SubLedger4IDTo']) ? $trc['SubLedger4IDTo'] : 0,
                );

                $this->t510_rekap->insert($m_rekap);

//                echo json_encode($m_rekap);
//                die();

                return $m_rekap;
        }

        private function store_to_trc($header, $map_to_ledger, $trc, $m_rekap) {
                $m_trc = array();
                foreach ($this->trc->get_columns() AS $column) {
                        if (isset($m_rekap[$column])) { // copy value if have same key/column
                                $m_trc[$column] = $m_rekap[$column];
                        }
                }
                $m_trc['MLedgerID'] = $m_rekap['C210_MLedgerID'];
                $m_trc['SLedgerID'] = $m_rekap['C200_SLedgerID'];
                $m_trc['SubLedger1ID'] = $m_rekap['C211_SubLedger1ID'];
                $m_trc['SubLedger2ID'] = $m_rekap['C212_SubLedger2ID'];

                $m_trc['C000_TrcID'] = $m_rekap['C012_TrcID'];
                $m_trc['C012_LineID'] = $m_rekap['C000_LineID'];
                $m_trc['C014_MonthPostIdx'] = $this->trc->get_curr_month();
                $m_trc['C013_MapperID'] = $map_to_ledger['C000_SysID'];
                $m_trc['YearIdx'] = $this->trc->get_curr_year();
                $m_trc['MonthIdx'] = $this->trc->get_curr_month();
                $m_trc['C015_DatePos'] = $this->trc->get_curr_date();
                $m_trc['C016_TimePost'] = $this->trc->get_curr_time();
                $m_trc['C050_DocNum'] = $header['C050_DocNum'];
                $m_trc['C050_DocDate'] = $header['C050_DocDate'];
                $m_trc['C050_AccountDate'] = $this->trc->get_curr_datetime();
                $m_trc['Currency'] = 'IDR';
                $m_trc['Rate'] = 1;
                $m_trc['Description'] = isset($trc['Description']) ? $trc['Description'] : "";

                $m_trc['MinPlus'] = ($map_to_ledger['isHutang'] ? -1 : 1) * ($m_rekap['C073_Amount3'] >= 0 ? 1 : -1);
                $m_trc['AmountTrc'] = $m_rekap['C073_Amount2'];

                $m_trc['AmountInTrc'] = 0;
                $m_trc['AmountOutTrc'] = 0;

                if ($m_rekap['C073_Amount3'] >= 0) {
                        $m_trc['AmountInTrc'] = abs($m_rekap['C073_Amount3']);
                } else {
                        $m_trc['AmountOutTrc'] = abs($m_rekap['C073_Amount3']);
                }

                $this->trc->insert($m_trc);

                // add one row for adjusting balance in table T610_DayDelta
                $m_day_delta['MLedgerID'] = $m_trc['MLedgerID'];
                $m_day_delta['SLedgerID'] = $m_trc['SLedgerID'];
                $m_day_delta['SubLedger1ID'] = $m_trc['SubLedger1ID'];
                $m_day_delta['SubLedger2ID'] = $m_trc['SubLedger2ID'];
                $m_day_delta['datePeriod'] = $this->trc->get_curr_date();

                $this->day_delta->update($m_day_delta, $m_rekap['C073_Amount2']);
        }

        function get_balance() {
                $this->layout = false;

                $data = $this->input->post('data');
                $where = array(
                    'MLedgerID' => $data[TRC_INPUT_NAME_MLEDGER],
                    'SLedgerID' => $this->user_data['C002_OfficeID'],
                    'SubLedger2ID' => $data[TRC_INPUT_NAME_SUBLEDGER2ID], //Operation/mensos
                );
                if ($where['MLedgerID'] == 3) { //bank
                        $where['SubLedger1ID'] = $data[TRC_INPUT_NAME_SUBLEDGER1ID];
                }
                $m_latest_trc = $this->trc->get_latest($where);
                if ($m_latest_trc) {
                        echo $m_latest_trc['AmountTrc'];
                        die();
                }
                echo 0;
        }

        function get_cities() {
                $this->layout = FALSE;
                $data = array(
                    array(
                        'id' => 1,
                        'name' => 'Toronto'
                    ),
                    array(
                        'id' => 2,
                        'name' => 'Toronti'
                    ),
                );

                echo json_encode($data);
        }

        function get_office() {
                $this->layout = FALSE;

                $q = $this->input->get('query') ? : '';
                $office_level = $this->input->get('office_level') ? : 4;

                $result = $this->office->search($office_level, $q);

                $offices = array();
                foreach ($result AS $office) {
                        $offices[] = array(
                            'id' => $office['C000_SysID'],
                            'name' => $office['C020_Descr'],
                        );
                }
                echo json_encode($offices);
        }

        function get_bank_name() {
                $this->layout = FALSE;

                $office_id = $this->input->get('office_id') ? : 0;

                $result = $this->kbaccount->get_bank_name($office_id);

                echo json_encode($result);
        }

        function get_bank_account() {
                $this->layout = FALSE;

                $office_id = $this->input->get('office_id') ? : 0;
                $bank_name = $this->input->get('bank_name') ? : '';

                $result = $this->kbaccount->get_bank_account($office_id, $bank_name);

                echo json_encode($result);
        }

        private function _save($data) {
                $header = $data['header'];
                $sub_trc_type_id = $data['sub_trc_type_id'];
                $lines = $data['lines'];
                $extra = isset($data['extra']) ? $data['extra'] : null;

                $sub_trc_type = $this->t024_sub_trc_type->get_by_id($sub_trc_type_id);
                $map_to_ledgers = $this->map_to_ledger->get_by_sub_trc_type_id($sub_trc_type_id);

                foreach ($lines AS $line_id => $line) {
                        $amount_seq_id_1 = 0;
                        foreach ($map_to_ledgers AS $map_to_ledger) {
                                $trc = $line[$map_to_ledger['C001_SeqIdx']];

                                // get map_to_ledger
                                if ($map_to_ledger['C001_SeqIdx'] == 1) {
                                        $amount_seq_id_1 = $trc['Amount'];
                                } else if (!isset($trc['Amount']) || !$trc['Amount']) {
                                        $trc['Amount'] = $amount_seq_id_1;
                                }

                                // get m_ledger
                                $m_ledger_id = $map_to_ledger['C021_MLedgerID'];
                                if (!$m_ledger_id && isset($trc['MLedgerID']) && $trc['MLedgerID']) {
                                        $m_ledger_id = $trc['MLedgerID'];
                                }
                                $trc['MLedgerID'] = $m_ledger_id;
                                $m_ledger = $this->m_ledger->get_by_id($m_ledger_id);

                                // merge $map_to_ledger and $m_ledger
                                $map_to_ledger = array_merge($map_to_ledger, $m_ledger);
                                $map_to_ledger['C010_TrcPanelID'] = $sub_trc_type['C010_TrcPanelID'];

                                $trc['LineID'] = $line_id;
                                if (!isset($trc['SubLedger2ID'])) {
                                        $trc['SubLedger2ID'] = 0;
                                }
                                if (($sub_trc_type['C010_TrcPanelID'] == 4 || $sub_trc_type['C010_TrcPanelID'] == 6) && !$map_to_ledger['isHutang']) {

                                        $trc['SubLedger2ID'] = 1;
                                }
                                $trc['SubTrcTypeID'] = $map_to_ledger['C011_SubTrcTypeID'];

                                if ($sub_trc_type_id == 8) {
                                        $trc['SubLedger2ID'] = 2;
                                        $this->save_penyaluran_program($trc);
                                } else if ($sub_trc_type_id == 17) {
                                        $trc['C222_SubLedger1IDTo'] = $this->save_external_bank_account($extra[$line_id][$map_to_ledger['C001_SeqIdx']]);
                                }

                                $trc['SLedgerID'] = $header['C045_OfficeID'];

                                $m_rekap = $this->store_to_rekap($header, $map_to_ledger, $trc);
                                $this->store_to_trc($header, $map_to_ledger, $trc, $m_rekap);
                        }
                }
        }

        function save() {
                $this->layout = FALSE;

                $data = array(
                    'header' => $this->get_header(),
                    'sub_trc_type_id' => $this->input->post('sub_trc_type_id'),
                    'lines' => $this->input->post('data'),
                    'extra' => $this->input->post('extra'),
                );

                $this->_save($data);

                echo 1;
        }

        function accept_internal_trf() {
                $this->layout = FALSE;

                $trc_id = $this->input->post('trc_id');
                $line_id = $this->input->post('line_id');
                $received_amount = $this->input->post('received_amount');

                $header = $this->get_header();

                $m_rekap_from = $this->t510_rekap->get_one(array(
                    'C012_TrcID' => $trc_id,
                    'C000_LineID' => $line_id,
                    'C210_MLedgerID' => 10, // Money intransfer
                ));
                if ($m_rekap_from['C201_SLedgerIDTo'] != $header['C045_OfficeID']) {
                        echo 0;
                        die();
                }

                $trc_from = array();
                foreach ($m_rekap_from AS $key => $val) {
                        $key_parts = explode('_', $key);
                        $new_key = count($key_parts) > 1 ? $key_parts[1] : $key_parts[0];
                        $trc_from[$new_key] = $val;
                }
                $trc_from['Amount'] = $received_amount ? : $trc_from['Amount3'];
                $trc_from['LineID'] = 1;
                $trc_from['SubLedger4ID'] = 1;

                $trc_to = array(
                    'MLedgerID' => $trc_from['MLedgerIDTo'],
                    'SLedgerID' => $trc_from['SLedgerIDTo'],
                    'SubLedger1ID' => $trc_from['SubLedger1IDTo'],
                    'SubLedger2ID' => $trc_from['SubLedger2IDTo'],
                    'SubLedger3ID' => $trc_from['SubLedger3IDTo'],
                    'LineID' => $trc_from['LineID'],
                );

                $lines[1][1] = $trc_from;
                $lines[1][2] = $trc_to;

                $this->t510_rekap->update(
                        array(
                    'C012_TrcID' => $trc_id,
                    'C000_LineID' => $line_id,
                    'C210_MLedgerID' => 10, // Money intransfer
                        ), array(
                    'C214_SubLedger4ID' => 1,
                ));

                $data = array(
                    'header' => $header,
                    'sub_trc_type_id' => 12,
                    'lines' => $lines,
                    'extra' => null,
                );

                $this->_save($data);

                echo 1;
        }

        function reject_internal_trf() {
                $this->layout = FALSE;

                $trc_id = $this->input->post('trc_id');
                $line_id = $this->input->post('line_id');
//                $received_amount = $this->input->post('received_amount');

                $header = $this->get_header();

                $m_rekap_from = $this->t510_rekap->get_one(array(
                    'C012_TrcID' => $trc_id,
                    'C000_LineID' => $line_id,
                    'C210_MLedgerID' => 10, // Money intransfer
                ));
                if ($m_rekap_from['C201_SLedgerIDTo'] != $header['C045_OfficeID']) {
                        echo 0;
                        die();
                }

                $trc_from = array();
                foreach ($m_rekap_from AS $key => $val) {
                        $key_parts = explode('_', $key);
                        $new_key = count($key_parts) > 1 ? $key_parts[1] : $key_parts[0];
                        $trc_from[$new_key] = $val;
                }
                $trc_from['Amount'] = 0;
                $trc_from['LineID'] = 1;
                $trc_from['SubLedger4ID'] = 2;

                $lines[1][1] = $trc_from;

                $this->t510_rekap->update(
                        array(
                    'C012_TrcID' => $trc_id,
                    'C000_LineID' => $line_id,
                    'C210_MLedgerID' => 10, // Money intransfer
                        ), array(
                    'C214_SubLedger4ID' => 2,
                ));

                $data = array(
                    'header' => $header,
                    'sub_trc_type_id' => 13,
                    'lines' => $lines,
                    'extra' => null,
                );

                $this->_save($data);

                echo 1;
        }

        private function save_external_bank_account($bank_account) {
                $DateTransfer = date('Y-m-d H:i:s', $bank_account['C001_DateTransfer']);
                $bank_account['C001_DateTransfer'] = $DateTransfer;

                $this->external_transfer->insert($bank_account);
                $bank_account = $this->external_transfer->get_one($bank_account);

                return $bank_account['C000_SysID'];
        }

        private function save_penyaluran_program($trc) {
                if ($trc['SubLedger2ID'] != 2 || !isset($trc['SubLedger3ID']) || !$trc['SubLedger3ID']) {
                        return;
                }

                $old_program = $this->project->get_one(array(
                    'C000_SysID' => $trc['SubLedger3ID'],
                ));

                if (!$old_program) {
                        return;
                }

                $balance = $old_program['C040_Value'] - $trc['Amount'];

                $this->project->update($trc['SubLedger3ID'], array(
                    'C040_Value' => $balance,
                ));
        }

        private function get_parent() {
                $this->layout = FALSE;

                echo json_encode($this->office->get_parent(2));
                die();
        }

        private function get_s_ledger_id($office_id, $IsKPRK_Pusat) {
                if ($IsKPRK_Pusat == 1) {
                        return $office_id;
                } else if ($IsKPRK_Pusat == 2) {
                        $office = $this->office->get_parent($office_id);
                        return $office['C000_SysID'];
                } else {
                        $office = $this->office->get_parent($office_id, 1);
                        return $office['C000_SysID'];
                }
        }

        function get_transfer_masuk_internal() {
                $this->layout = false;

                $data = $this->t510_rekap->get_transfer_masuk_internal(37);

                echo json_encode($data);
                die();
        }

}