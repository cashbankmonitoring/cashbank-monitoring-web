function copyrightPos(){
    var windowHeight = $(window).height();
    if(windowHeight < 700) {
        $('.account-copyright').css('margin-top', 40);
    }
    else {
        $('.account-copyright').css('position', '').css('margin-top', '');
    }
}

$(window).resize(function() {
    copyrightPos();
});

$(function() {

    copyrightPos();
    if($('body').data('page') == 'login'){

        /* Show / Hide Password Recover Form */
        $('#password').on('click', function(e) {
            e.preventDefault();
            $('.form-signin').slideUp(300, function() {
                $('.form-password').slideDown(300);
            });
        });
        $('#login').on('click', function(e) {
            e.preventDefault();
            $('.form-password').slideUp(300, function() {
                $('.form-signin').slideDown(300);
            });
        });
        
        $('#submit-form').click(function(e) {
            e.preventDefault();
            var l = Ladda.create(this);
            l.start();
    //         setTimeout(function() {
				// //console.log($('#frm_login').validate({success: "valid"}));
    //             var uname = $('[name="username"]').val();
				// var upass = $('[name="password"]').val();
				// $.post( "php/get_data.php", { opr : 'cek_login', uname : uname, upass : md5(upass)}).done(function( data ) {
				// 	if (data != ""){
				// 		if (data == 0){
				// 			$('#new-pass').data('uname',uname);
				// 			$('#new-pass').modal('show');
				// 		}else if (data == 2){
				// 			$('#pass-error').show();
				// 		} else {
				// 			$('#frm_login').submit();
				// 		}
				// 	}		
				// })
				
				// //$('#frm_login').submit();
				// //window.location.href = "php/check_login.php";
				// l.stop();
    //         }, 2000);
        });
		
		$('.post-new-pass').click(function(e) {
			var usrname = $('#new-pass').data('uname');
			var newpass = $('[name="new_pass"]').val();
			
			if ($('[name="new_pass"]').val() == $('[name="konf_new_pass"]').val()){
				$.post( "php/post_data.php", { opr : 'set_new_pass', usrname : usrname, newpass : md5(newpass)}).done(function( data ) {
					if (data != ""){
						if (data == 1){
							$('#new-pass').modal('hide');
							alert('Password berhasil diupdate....');
							setTimeout(function() {
								window.location.href = "user-login.php";
							},500);
						}
					}		
				})	
			} else {
				alert('Konfirmasi password tidak sama....');
			}
		});
		
		
		
		$('[name="email-reset"]').keyup(function(){
			$('#email-salah').fadeOut();
		});
		
		$('#username, [name="password"]').keypress(function(e) {
			if(e.which == 13) {
				$('#submit-form').click();
			}
		});
		
		
		
		$('#submit-reset-password').click(function(e) {
            //e.preventDefault();
            var l = Ladda.create(this);
            l.start();
            setTimeout(function() {
                if ($('[name="email-reset"]').val() != ""){
					$.post( "php/check_login.php", { opr:"email-reset", email: $('[name="email-reset"]').val()}).done(function( data ) {
						if (data == "1"){
							//location.reload();
							$('.form-password').html('Link untuk reset password sudah dikirim ke email..');
							setTimeout(function() {
								location.reload();
							},3000);
							//console.log(data);
							l.stop();
						} else if (data == "2"){
							$('#email-salah').fadeIn();
							$('#email-salah').html('Email tidak terdaftar...');
							l.stop();
						}
						
					});
				}
				//$('#frm_login').submit();
				//window.location.href = "php/check_login.php";
            }, 3000);
			
        });
		
		
        $.backstretch(["assets/global/images/gallery/login.jpg"],
        {
            fade: 600,
            duration: 4000
        });


        /***** DEMO CONTENT, CAN BE REMOVED ******/
        $("#account-builder").on('mouseenter', function() {
            TweenMax.to($(this), 0.35, {
                css: {
                    height: 130,
                    width: 500,
                    left: '37%',
                    'border-bottom-left-radius': 0,
                    'border-top-right-radius': 0,
                    '-moz-border-bottom-left-radius': 0,
                    '-moz-border-top-right-radius': 0,
                    '-webkit-border-bottom-left-radius': 0,
                    '-webkit-border-top-right-radius': 0
                },
                ease: Circ.easeInOut
            });
        });
        $("#account-builder").on('mouseleave', function() {
            TweenMax.to($(this), 0.35, {
                css: {
                    height: 44,
                    width: 250,
                    left: '44%',
                    'border-bottom-left-radius': 20,
                    'border-top-right-radius': 20
                },
                ease: Circ.easeInOut
            });
        });
        /* Hide / Show Social Connect */
        $('#social-cb').change(function() {
            if ($(this).is(":checked")) {
                $('.social-btn').slideDown(function() {
                    $('body').removeClass('no-social');
                });
            }
            else {
                $('.social-btn').slideUp(function() {
                    $('body').addClass('no-social');
                });
            }
        });
        /* Hide / Show Background Image */
        $('#image-cb').change(function() {
            if ($(this).is(":checked")) {
                $.backstretch(["../assets/global/images/gallery/login.jpg"], {
                    fade: 600,
                    duration: 4000
                });
                $('#slide-cb').attr('checked', false);
            }
            else $.backstretch("destroy");
        });
        /* Add / Remove Slide Image */
        $('#slide-cb').change(function() {
            if ($(this).is(":checked")) {
                $.backstretch(["../assets/global/images/gallery/login4.jpg", "../assets/global/images/gallery/login3.jpg", "../assets/global/images/gallery/login2.jpg", "../assets/global/images/gallery/login.jpg"], {
                    fade: 600,
                    duration: 4000
                });
                $('#image-cb').attr('checked', false);
            }
            else {
                $.backstretch("destroy");
            }
        });
        /* Hide / Show User Image */
        $('#user-cb').change(function() {
            if ($(this).is(":checked")) {
                TweenMax.to($('.user-img'), 0.3, {
                    opacity: 0,
                    ease: Circ.easeInOut
                });
            }
            else {
                TweenMax.to($('.user-img'), 0.3, {
                    opacity: 1,
                    ease: Circ.easeInOut
                });
            }
        });

    }
    if($('body').data('page')== 'signup'){

        var form = $(".form-signup");
        $.backstretch(["../assets/global/images/gallery/login.jpg"],
        {
            fade: 600,
            duration: 4000
        });
        $("#account-builder").on('mouseenter', function() {
            TweenMax.to($(this), 0.35, {
                css: {
                    height: 130,
                    width: 500,
                    left: '37%',
                    'border-bottom-left-radius': 0,
                    'border-top-right-radius': 0,
                    '-moz-border-bottom-left-radius': 0,
                    '-moz-border-top-right-radius': 0,
                    '-webkit-border-bottom-left-radius': 0,
                    '-webkit-border-top-right-radius': 0
                },
                ease: Circ.easeInOut
            });
        });
        $("#account-builder").on('mouseleave', function() {
            TweenMax.to($(this), 0.35, {
                css: {
                    height: 44,
                    width: 250,
                    left: '44%',
                    'border-bottom-left-radius': 20,
                    'border-top-right-radius': 20
                },
                ease: Circ.easeInOut
            });
        });
        /* Hide / Show Social Connect */
        $('#social-cb').change(function() {
            if ($(this).is(":checked")) {
                $('.social-btn').slideDown(function() {
                    $('body').removeClass('no-social');
                });
            }
            else {
                $('.social-btn').slideUp(function() {
                    $('body').addClass('no-social');
                });
            }
        });
        /* Hide / Show Background Image */
        $('#image-cb').change(function() {
            if ($(this).is(":checked")) {
                $.backstretch(["../assets/global/images/gallery/login.jpg"], {
                    fade: 600,
                    duration: 4000
                });
                $('#slide-cb').attr('checked', false);
            }
            else $.backstretch("destroy");
        });
        /* Add / Remove Slide Image */
        $('#slide-cb').change(function() {
            if ($(this).is(":checked")) {
                $.backstretch(["../assets/global/images/gallery/login4.jpg", "../assets/global/images/gallery/login3.jpg", "../assets/global/images/gallery/login2.jpg", "../assets/global/images/gallery/login.jpg"], {
                    fade: 600,
                    duration: 4000
                });
                $('#image-cb').attr('checked', false);
            }
            else {
                $.backstretch("destroy");
            }
        });
        /* Hide / Show User Image */
        $('#user-cb').change(function() {
            if ($(this).is(":checked")) {
                TweenMax.to($('.user-img'), 0.3, {
                    opacity: 0,
                    ease: Circ.easeInOut
                });
            }
            else {
                TweenMax.to($('.user-img'), 0.3, {
                    opacity: 1,
                    ease: Circ.easeInOut
                });
            }
        });
        $('#submit-form').click(function(e) {
            form.validate({
                rules: {
                    firstname:
                    {
                        required: true,
                        minlength: 3,
                    },
                    lastname:
                    {
                        required: true,
                        minlength: 4,
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true,
                        minlength: 6,
                        maxlength: 16
                    },
                    password2: {
                        required: true,
                        minlength: 6,
                        maxlength: 16,
                        equalTo: '#password'
                    },
                    terms: {
                        required: true
                    }
                },
                messages: {
                    firstname: {
                        required: 'Enter your first name',
                        minlength: 'Enter at least 3 characters or more'
                    },
                    lastname: {
                        required: 'Enter your last name',
                        minlength: 'Enter at least 3 characters or more'
                    },
                    email: {
                        required: 'Enter email address',
                        email: 'Enter a valid email address'
                    },
                    password: {
                        required: 'Write your password',
                        minlength: 'Minimum 6 characters',
                        maxlength: 'Maximum 16 characters'
                    },
                    password2: {
                        required: 'Write your password',
                        minlength: 'Minimum 6 characters',
                        maxlength: 'Maximum 16 characters',
                        equalTo: 'Password don\'t match'
                    },
                    terms: {
                        required: 'You must agree with terms'
                    }
                },
                errorPlacement: function(error, element) {
                    if (element.is(":radio") || element.is(":checkbox")) {
                        element.closest('.option-group').after(error);
                    }
                    else {
                        error.insertAfter(element);
                    }
                }
            });
            e.preventDefault();
            if (form.valid()) {
                $(this).addClass('ladda-button');
                var l = Ladda.create(this);
                l.start();
                setTimeout(function() {
                    window.location.href = "php/check_login.php";
                }, 2000);
            }
        });

    }
    
});