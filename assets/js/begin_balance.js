function header_submit(type){
	var idx = $('.nav-primary').find('.active').attr('tab-idx');
	if(idx == 1){
		if(type == 'submit'){
			$("#form-bank").submit();
		}else{
			document.getElementById("form-bank").reset();
		}
	}else if(idx == 2){
		if(type == 'submit'){
			$("#form-kas").submit();
		}else{
			document.getElementById("form-kas").reset();
		}
	}else if(idx == 3){
		if(type == 'submit'){
			$("#form-hutang").submit();
		}else{
			document.getElementById("form-hutang").reset();
		}
	}else if(idx == 4){
		if(type == 'submit'){
			$("#form-hutang-alokasi").submit();
		}else{
			document.getElementById("form-hutang-alokasi").reset();
		}
	}
}

function sum(idx, type, panel, set_total){
	// var count_bank = $('input[name="count_bank"]').val();
	// alert(idx);
	var system = $('input[name="system_'+panel+'_'+type+'['+idx+']"]').val().replace(/,/g, "");
	var aktual = $('input[name="aktual_'+panel+'_'+type+'['+idx+']"]').val().replace(/,/g, "");

    var result = aktual - system;
	$('input[name="adjustment_'+panel+'_'+type+'['+idx+']"]').val(result);
	
	if(set_total){
		sum_total(idx, panel);
	}else{
		initNomor();
	}
}

function sum_total(idx, panel){
	var ak_1 = $('input[name="aktual_'+panel+'_1['+idx+']"]').val().replace(/,/g, "");
	var ak_2 = $('input[name="aktual_'+panel+'_2['+idx+']"]').val().replace(/,/g, "");

	var ak_total = parseInt(ak_1) + parseInt(ak_2);
	$('input[name="aktual_'+panel+'_total['+idx+']"]').val(ak_total);

	var adj_1 = $('input[name="adjustment_'+panel+'_1['+idx+']"]').val().replace(/,/g, "");
	var adj_2 = $('input[name="adjustment_'+panel+'_2['+idx+']"]').val().replace(/,/g, "");

	var adj_total = parseInt(adj_1) + parseInt(adj_2);

	$('input[name="adjustment_'+panel+'_total['+idx+']"]').val(adj_total);
	initNomor();
}