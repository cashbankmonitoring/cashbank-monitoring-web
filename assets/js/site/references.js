$(document).ready(function(){
	initValidatorStyle();
});

function datatable(id, url){
	//datatables
    table = $('#'+id).dataTable({ 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": base_url+url,
            "type": "POST"
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
 
    });
}

function reload_table(){
    table.ajax.reload(null,false); //reload datatable ajax 
}

function get_modal(modal_name, regional, kprk, office){
	var $title = $("#title-type"),
		title = "";
	$('.control-btn').on('click', '#add-'+modal_name, function(){
		$('#'+modal_name+'-modal').modal();
		title = "Tambah";
		$title.html(title);

		$('#'+modal_name+'-form').trigger("reset");
		$(':input').val('');
		if(modal_name == 'office'){
			$('#show-regional').hide();
			$('#show-kprk').hide();
		}

		if(office != null){
			$(office).attr('readonly', false);
			get_office_data(office);
		}
		
	});

	$('#lst_'+modal_name).on('click','.edit-'+modal_name, function(){
		var id = $(this).attr('data-id');
		$('#'+modal_name+'-modal').modal();
		title = "Ubah";
		$title.html(title);
	});
}

function get_office_data(id, level, parent){
        var url = "references/search_office2";
        if(parent != null){
                url = "references/search_office2/"+level+"/"+parent;
        }else if(level != null){
                url = "references/search_office2/"+level;
        }

        $(id).select2({
        placeholder: '--- Pilih kantor ---',
        minimumInputLength: 1,
        ajax: {
                  url: base_url + url,
                  dataType: 'json',
                  delay: 250,
      //             data: function (term) {
		 			// return {
		 			// 	q: term, // search term
		 			// };
		 		 //  },
                  processResults: function (data) {
                    return {
                        results: $.map(data, function(obj) {
                                return { id: obj.id, text: obj.C020_Descr };
                            })
                      // results: data
                    };
                  },
                  cache: true
                }
        });
}

function initValidatorStyle(){
	$.validator.setDefaults({
	    highlight: function(element) {
	        $(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
	    errorElement: 'span',
	    errorClass: 'help-block',
	    errorPlacement: function(error, element) {
	        if(element.parent('.input-group').length) {
	            error.insertAfter(element.parent());
	        }else if (element.hasClass('select2')) {     
       			error.insertAfter(element.next('span'));
	        }else {
	            error.insertAfter(element);
	        }
	    }
	});
}

// function get_office_data(id, level, parent){
// 	var url = base_url + "references/search_office/?lvl=" + 2
// 	if(parent != null){
// 		url = base_url + "references/search_office/"+level+"/"+parent+"?lvl=" + 2
// 	}else if(level != null){
// 		url = base_url + "references/search_office/"+level+"?lvl=" + 2
// 	}
// 	$(id).select2({
// 		 placeholder: "Cari Kantor",
// 		 minimumInputLength: 1,
// 		 ajax: {// instead of writing the function to execute the request we use Select2's convenient helper
// 		 url: url,
// 		 dataType: 'json',
// 		 quietMillis: 250,
// 		 data: function (term, page) {
// 		 return {
// 		 q: term, // search term
// 		 };
// 		 },
// 		 results: function (data, page) { // parse the results into the format expected by Select2.
// 		 // since we are using custom formatting functions we do not need to alter the remote JSON data
// 		 return {results: data.items};
// 		 },
// 		 cache: true
// 		 },
// 		 initSelection: function (element, callback) {
// 		 // the input tag has a value attribute preloaded that points to a preselected repository's id
// 		 // this function resolves that id attribute to an object that select2 can render
// 		 // using its formatResult renderer - that way the repository name is shown preselected
// 		 var id = $(element).val();
// 			 if (id !== "") {
// 				 $.ajax(base_url + "references/search_office?" + id, {
// 				 dataType: "json"
// 				 }).done(function (data) {
// 				 callback(data);
// 				 });
// 			 }
// 		 },
// 		 formatResult: repoFormatResult, // omitted for brevity, see the source of this page
// 		 formatSelection: repoFormatSelection, // omitted for brevity, see the source of this page
// 		 dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
// 		 escapeMarkup: function (m) {
// 		 return m;
// 		 } // we do not want to escape markup since we are displaying html in results
//  		// });
//     });

// 	function repoFormatResult(repo) {
//         var markup = '<div class="row">' +
//                 '<div class="col-md-12">' +
//                 '<div class="row">' +
//                 '<div class="col-md-12">[' + repo.C010_Code + '] ' + repo.C020_Descr + '</div>' +
//                 '</div>';
//         if (repo.C020_Descr) {
//                 //markup += '<div>' + repo.C020_Descr + '</div>';
//         }
//         markup += '</div></div>';
//         return markup;
// 	}

// 	function repoFormatSelection(repo) {
// 	        return repo.C020_Descr;
// 	}
// }

function post_data(form_name, url){
	var validator = $("#"+form_name+"-form").validate();
	if(validator.form()){
		var data_form = $("#"+form_name+"-form").serialize();
		$.post( base_url+url, data_form).done(function( results ) {
			if (results == "1"){
				location.reload();
			}else{
				bootbox.alert("Terjadi kesalahan saat memasukan data");
			}
		});
	}
}

function cancel_post(modal_name, message){
		bootbox.confirm({ 
		    size: 'medium',
		    message: "Anda yakin ingin membatalkan data form "+message+" ini?", 
		    callback: function(result){ 
			    if (result) {
					$('#'+modal_name+'-modal').modal('hide');
				}else{

				}
			}
		});
		// bootbox.confirm("Anda yakin ingin membatalkan data form "+message+" ini?", function(result){
		// 	if (result) {
		// 		$('#'+modal_name+'-modal').modal('hide');
		// 	}else{

		// 	}
		// });
}

$(document).on('keyup','input',function(event ){
		var inp = $(this);
		if ($(this).hasClass('nomor')){
			$(this).val($(this).val().replace(/[^\d]/,''));
			$(this).val(numeral($(this).val()).format('0,0'));
		}
	});

function repoFormatResult(repo) {
        var markup = '<div class="row">' +
                '<div class="col-md-12">' +
                '<div class="row">' +
                '<div class="col-md-12">[' + repo.C010_Code + '] ' + repo.C020_Descr + '</div>' +
                '</div>';
        if (repo.C020_Descr) {
                //markup += '<div>' + repo.C020_Descr + '</div>';
        }
        markup += '</div></div>';
        return markup;
}

function repoFormatSelection(repo) {
        return repo.C020_Descr;
}