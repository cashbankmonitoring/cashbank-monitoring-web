$(document).ready(function(){
	$('.prg-tgl').datepicker({ dateFormat: 'dd-mm-yy' });
	datatable('table', 'Projects/get_ajax_data/');
	get_modal('program', null, null, null);
	$("#1060").collapse('toggle');

	$('.control-btn').on('click', '#import-program', function(){
		$('#program-import-modal').modal();
	});
});

$(document).on('click','.edit-program',function(){
	$('#program-form').trigger("reset");
	var idx = $(this).attr('data-id');
	$.post( base_url+"Projects/get_program_by_id/", {idx: idx}).done(function( data ) {
		var json = $.parseJSON(data);

		$('[name="program_id"]').val(json.C000_SysID);
		$('select[name="partner"]').val(json.C030_PartnerID);
		$('[name="nama"]').val(json.C020_Descr);
		$('select[name="virtual"]').val(json.C021_VirtualAccountID);
		$('[name="tgl_mulai"]').val(json.C060_DateStart);
		$('[name="tgl_selesai"]').val(json.C070_DateFinished);
		$('[name="nilai"]').val(json.C040_Value);
		$('select[name="currency"]').val(json.C080_CurrencyID);
		$('select[name="status"]').val(json.C050_IsClose);
	});
});

$(document).on('click','.delete-program',function(){
	var idx = $(this).attr('data-id');
	bootbox.confirm('Hapus data program..?', function(result) {
	    if (result) {
	        $.post( base_url+"Projects/delete_program", { ktr_id : idx}).done(function( data ) {
			if (data == '1'){
				location.reload();
			} else {
				bootbox.alert('Kesalahan delete data...');
			}
		});
	    } else {
	    }
	});
});