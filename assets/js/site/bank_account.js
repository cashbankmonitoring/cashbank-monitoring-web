$(document).ready(function(){
	datatable('table', 'Bank_accounts/get_ajax_data/');
	get_modal('bank', null, null, '#get_kantor');

	$("#1060").collapse('toggle');

	$('.control-btn').on('click', '#import-bank', function(){
		$('#bank-import-modal').modal();
	});
});

$(document).on('click','.delete-bank',function(){
	var idx = $(this).attr('data-id');
	bootbox.confirm("Hapus data bank?", function(result) {
	    if (result) {
	        $.post( base_url+"Bank_accounts/check_delete_bank", {bank_id : idx}).done(function( data ) {
				if(data == '1'){
					$.post( base_url+"Bank_accounts/delete_bankacc", { opr:"post-edit-kantor", ktr_id : idx}).done(function( data ) {
						if (data == '1'){
							// $('#edit-kantor').modal('hide');
							var pgact = $('#pg_bankacc').find('.active').html();
							if(pgact == undefined){
								pgact = 1;
							} 
							
							$('#searchBank').removeAttr('value');
								
							// getlist_bankacc($('#get_idoff_bankacc').val(),pgact);
							location.reload();
						} else {
							bootbox.alert('Kesalahan delete data...');
						}
					});
				}else{
					bootbox.alert("Data tidak terhapus karena telah digunakan untuk transaksi.");
				}
			});
	    } else {

	    }
	});
});

$(document).on('click','.edit-bank',function(){
	$('#bank-form').trigger("reset");
	var idx = $(this).attr('data-id');
	$.post( base_url+"Bank_accounts/get_bankacc_by_id/", {idx: idx}).done(function( data ) {
		var json = $.parseJSON(data);

		var officeID = json.C001_OfficeID;
		$.post( base_url+"references/get_office_by_id/", {idx: officeID}).done(function( dataParent ) {
			var jsonParent = $.parseJSON(dataParent);
			$('#get_kantor').attr('readonly', false);
			get_office_data('#get_kantor');
			
			if(jsonParent){
				var s1 = $("#get_kantor").data('select2'); 
				s1.trigger('select', { 
				  data: {
						id: jsonParent.C000_SysID,
						text: jsonParent.C020_Descr
					} 
				}); 
			}else{
				$('select[name="kantor_id"]').val();
			}
		});

		$('[name="bank_id"]').val(json.C000_SysID);
		$('[name="bank"]').val(json.C020_Name);
		$('[name="no_rek"]').val(json.C010_BankAccNumber);
		$('[name="deskripsi"]').val(json.C030_Descr);
	});
});