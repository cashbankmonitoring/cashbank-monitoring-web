$(document).ready(function(){
	datatable('table', 'Partners/get_ajax_data/');
	get_modal('partner', null, null, '#get_kantor');
	$("#1060").collapse('toggle');

	$('.control-btn').on('click', '#import-partner', function(){
		$('#partner-import-modal').modal();
	});
});

$(document).on('click','.edit-partner',function(){
	$('#partner-form').trigger("reset");
	var idx = $(this).attr('data-id');
	$.post( base_url+"Partners/get_partner_by_id/", {idx: idx}).done(function( data ) {
		var json = $.parseJSON(data);

		$('[name="partner_id"]').val(json.SysID);
		$('[name="kode"]').val(json.PartnerCode);
		$('[name="nama"]').val(json.PartnerName);
	});
});

$(document).on('click','.delete-partner',function(){
	var idx = $(this).attr('data-id');
	bootbox.confirm('Hapus data mitra..?', function(result) {
	    if (result) {
	        $.post( base_url+"Partners/delete_partner", { id : idx}).done(function( data ) {
			if (data == '1'){
				location.reload();
			} else {
				bootbox.alert('Kesalahan delete data...');
			}
		});
	    } else {

	    }
	});
});