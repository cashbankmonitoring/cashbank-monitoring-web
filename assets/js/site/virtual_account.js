$(document).ready(function(){
	datatable('table', 'Virtual_accounts/get_ajax_data/');
	get_modal('virtual', null, null, null);
	$("#1060").collapse('toggle');

	$('.control-btn').on('click', '#import-virtual', function(){
		$('#virtual-import-modal').modal();
	});
});

$(document).on('click','.edit-virtual',function(){
	$('#virtual-form').trigger("reset");
	var idx = $(this).attr('data-id');
	$.post( base_url+"virtual_accounts/get_virtual_by_id/", {idx: idx}).done(function( data ) {
		var json = $.parseJSON(data);

		$('[name="virtual_id"]').val(json.C000_SysID);
		$('[name="kode"]').val(json.C010_Code);
		$('[name="nama"]').val(json.C011_Descr);
	});
});

$(document).on('click','.delete-virtual',function(){
	var idx = $(this).attr('data-id');
	bootbox.confirm("Hapus data virtual account?", function(result) {
	    if (result) {
	        $.post( base_url+"virtual_accounts/check_delete_virtual", {virtual_id : idx}).done(function( data ) {
				if(data == '1'){
					$.post( base_url+"virtual_accounts/delete_virtual", {id : idx}).done(function( data ) {
						if (data == '1'){
							location.reload();
						} else {
							bootbox.alert('Kesalahan delete data...');
						}
					});
				}else{
					bootbox.alert("Data tidak terhapus karena telah digunakan untuk transaksi.");
				}
			});
	        
	    } else {

	    }
	});
});
