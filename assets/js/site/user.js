$(document).ready(function(){
	datatable('table', 'Users/get_ajax_data/');
	get_modal('user', null, null, '#get_kantor');
	
	$("#1060").collapse('toggle');

	$('.control-btn').on('click', '#import-user', function(){
		$('#user-import-modal').modal();
	});
});

function checkPass(){
    //Store the password field objects into variables ...
    var pass1 = document.getElementById('show_password');
    var pass2 = document.getElementById('confirm_password');
    //Store the Confimation Message Object ...
    var message = document.getElementById('confirmMessage');
    //Set the colors we will be using ...
    var goodColor = "#66cc66";
    var badColor = "#ff6666";
    //Compare the values in the password field 
    //and the confirmation field
    if(pass1.value == pass2.value){
        //The passwords match. 
        //Set the color to the good color and inform
        //the user that they have entered the correct password 
        // pass2.style.backgroundColor = goodColor;
        // message.style.color = goodColor;
        message.innerHTML = ""
    }else{
        //The passwords do not match.
        //Set the color to the bad color and
        //notify the user.
        // pass2.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = "Password yang di input tidak sesuai!"
    }
}  

function post_data_user(){
	$('.simpan').attr('disabled', true);
	if($('#is_begin_balance').attr("checked")){
		$('[name="is_begin_balance"]').val(0);
	}else{
		$('[name="is_begin_balance"]').val(1);
	}

	if($('#is_read_only').attr("checked")){
		$('[name="is_read_only"]').val(1);
	}else{
		$('[name="is_read_only"]').val(0);
	}

	var validator = $("#user-form").validate();
	if(validator.form()){
		$.post( base_url+"Users/check_user_exist/", {kode : $('#user_name').val()}).done(function( data ) {
			var data_form = $('#user-form').serialize();
			if($('[name="user_id"]').val() != 0 && data == 1){
				data = 0;	
			}
			if(String(data) == '0'){
				// return 1;
				$.post( base_url+"Users/save_user", data_form).done(function( results ) {
					if (results == "1"){
						location.reload();
					}else{
						bootbox.alert("Terjadi kesalahan saat memasukan data");
						$('.simpan').attr('disabled', false);
					}
				});
			}else{
				bootbox.alert("Username sudah terdaftar");
				$('.simpan').attr('disabled', false);
			}
		});
	}else{
		$('.simpan').attr('disabled', false);
	}
}

$(document).on('click','.edit-user',function(){
	$('#field_password').hide();
	$('#field_confirm_password').hide();
	$('#user-form').trigger("reset");
	var idx = $(this).attr('data-id');
	$.post( base_url+"Users/get_user_by_id/", {idx: idx}).done(function( data ) {
		var json = $.parseJSON(data);
		var officeID = json.C002_OfficeID;

		if(officeID == 0){
			$('#get_kantor').attr('readonly', false);
			get_office_data('#get_kantor');
		}else{
			$.post( base_url+"references/get_office_by_id/", {idx: officeID}).done(function( dataParent ) {
				var jsonParent = $.parseJSON(dataParent);
				$('#get_kantor').attr('readonly', false);
				// $('[name="kantor_id"]').val(jsonParent.C000_SysID);
				get_office_data('#get_kantor');
				var s1 = $("#get_kantor").data('select2'); 
				s1.trigger('select', { 
				  data: {
						id: jsonParent.C000_SysID,
						text: jsonParent.C020_Descr
					} 
				}); 
			});
		}

		$('[name="user_id"]').val(json.C000_SysID);
		$('[name="username"]').val(json.C030_UserName);
		$('[name="nama"]').val(json.C040_UserFullname);
		$('[name="email"]').val(json.C060_UserEmail);
		$('[name="komputer_id"]').val(json.C080_machineID);
		$('[name="deskripsi"]').val(json.C020_Descr);
		$('select[name="status"]').val(json.C070_UserState);
		$('select[name="user_type"]').val(json.C110_UserGroupType);
		$('[name="is_begin_balance"]').val(json.C120_IsBeginBalance);
		$('[name="is_read_only"]').val(json.C130_IsReadOnly);

		if(json.C120_IsBeginBalance == 0){
			$('#is_begin_balance').attr("checked", true);
		}else{
			$('#is_begin_balance').attr("checked", false);
		}
			console.log(json)

		if(json.C130_IsReadOnly == 1){
			console.log('yes')
			$('#is_read_only').attr("checked", true);
		}else{
			$('#is_read_only').attr("checked", false);
		}
	});
});

$(document).on('click','#is_begin_balance',function(){
	if($(this).attr("checked")){
		$("#is_begin_balance").attr("checked", false);
		$('[name="is_begin_balance"]').val(1);
	}else{
		$("#is_begin_balance").attr("checked", true);
		$('[name="is_begin_balance"]').val(0);
	}
});

$(document).on('click','#is_read_only',function(){
	if($(this).attr("checked")){
		$("#is_read_only").attr("checked", false);
		$('[name="is_read_only"]').val(0);
	}else{
		$("#is_read_only").attr("checked", true);
		$('[name="is_read_only"]').val(1);
	}
});

$(document).on('click','.reset-password',function(){
	var idx = $(this).attr('data-id');
	var tr = $(this).closest('tr');
	$('#reset-pass').modal('show');
	$('#reset-pass').data('idx',idx);
});

$(document).on('click','.confirm-rst-pass',function(){
	var usr_id = $('#reset-pass').data('idx');
	var pass = $('[name="admin_pass"]').val();
	$('#reset-pass').modal('hide');
	$.post( base_url+"Users/search_pass", { user_id: usr_id, pass : pass}).done(function( data ) {
		if (parseInt(data) == 1){
			$('#reset-pass').modal('hide');
			alert('Password telah direset...!');
		} else {
			alert('Password admin salah ...');
		}
	});
});

$(document).on('click','.delete-user',function(){
	var idx = $(this).attr('data-id');
	var office_idx = $(this).attr('data-office-id');
	bootbox.confirm("Hapus data User?", function(result){
		if (result) {
			$.post( base_url+"Users/delete_user", {id : idx}).done(function( data ) {
				if (data == '1'){
					location.reload();
				} else {
					bootbox.alert('Kesalahan delete data...');
				}
			});
		}else{
		}
	});
});