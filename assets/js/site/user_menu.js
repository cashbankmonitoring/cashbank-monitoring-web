$(document).ready(function(){
	// if ($('.reference').attr('idx') == "1"){
	// 	getlist_office("");
	// }else if($('.reference').attr('idx') == "2"){
	// 	getlist_bankacc("","");
	// }else{
		
	$("#1060").collapse('toggle');
		// getlist_user("");
	// }
});
$(document).on('click','.btn-frm-user-menu',function(){
	$('#frm_add_user_menu').fadeIn();
});
$(document).on('click','.cancel-add-user-menu',function(){
	$('#frm_add_user_menu').fadeOut();
	
});
$(document).on('click','.edit-user-type',function(){
	
		$('#form-user-menu').trigger("reset");
		var idx = $(this).closest('tr').attr('idx');
		var tr = $(this).closest('tr');
		$('#edit-user-menu').data('idx',idx);
		// var prt = $(tr).find('td').eq(1).attr('idx');
		// $("#prg_partner").val(prt).trigger("change");
		// $('[name="nm_prg"]').val($(tr).find('td').eq(2).html());
		
		// $( ".prg-tgl" ).datepicker({ dateFormat: 'dd-mm-yy' });
		
		$.post( base_url+"User_menu/get_trc_by_id/", { opr : 'add_office', idx: idx}).done(function( data1 ) {
			
			var json = $.parseJSON(data1)
			// alert(json[0].C010_Code);
			$('[name="code_edit"]').val(json[0].C010_Code);
			$('[name="lineID_edit"]').val(json[0].C012_LineID);
			$('[name="label_edit"]').val(json[0].C013_Label);
			$('[name="link_edit"]').val(json[0].C014_Link);
			$('[name="icon_edit"]').val(json[0].C015_Icon);
			$('[name="desc_edit"]').val(json[0].C011_Descr);
			$('[name="parent_edit"]').val(String(json[0].C016_ParentID));
			$.post( base_url+"User_menu/get_panel_by_trc_id/", { opr : 'add_office', idx: idx}).done(function( data ) {
			var json = $.parseJSON(data)
			var values = [];

			for (var i = 0; i < json.length; i++) {
				console.log(json[i].TrcPanelID);
				values.push(String(json[i].TrcPanelID));
				// $("#user-list [value=" + json[i].TrcTypeID + "]").attr("checked", "checked");
			}
			console.log(values);
			$("#user-list").find('[value=' + values.join('], [value=') + ']').prop("checked", true);
			// console.log(json.length);		
		});
	});
});
		// $('[name="prg_val"]').val($(tr).find('td').eq(5).html());
	// });

$(document).on('click','.btn-add-user-menu',function(){
var dt_ins = [];
dt_ins.push($('[name="code"]').val());
dt_ins.push($('[name="lineID"]').val());
dt_ins.push($('[name="label"]').val());
dt_ins.push($('[name="link"]').val());
dt_ins.push($('[name="icon"]').val());
dt_ins.push($('[name="parent"]').val());
dt_ins.push($('[name="desc"]').val());
// dt_ins.push($('[name="code"]').val());
var arr = $("input[name='trcType']:checked").getCheckboxVal()
if(arr.length <= 0){
	var arr = 0;
}
console.log(arr);
// console.log($('form').serialize());	
var validator = $( "#user-menu-frm" ).validate();
		
if(validator.form()){	
$.post( base_url+"user_menu/post/", { opr : 'add_office', dt_ins : dt_ins, trcType : arr}).done(function( data ) {
		if (data == "0"){
			// getlist_office("");
			location.reload();
			$('#frm_add_office').fadeOut();
		}		
	});
}
});
jQuery.fn.getCheckboxVal = function(){
	var vals = [];
	var i = 0;
	this.each(function(){
		vals[i++] = jQuery(this).val();
	});
	return vals;
}
$(document).on('click','.post-upd-user-menu',function(){
		var usr_id = $('#edit-user-menu').data('idx');
		var dt_ins = [];
		// if ($('#lev_ktr').val() == "1"){
		// 	dt_ins.push("0");
		// } else {
		// 	dt_ins.push($('#parent_ktr').val());
		// }
		// dt_ins.push($('#lev_ktr').val());
		// dt_ins.push($('[name="name_edit"]').val());
		// dt_ins.push($('[name="desc_edit"]').val());
		dt_ins.push($('[name="code_edit"]').val());
		dt_ins.push($('[name="lineID_edit"]').val());
		dt_ins.push($('[name="label_edit"]').val());
		dt_ins.push($('[name="link_edit"]').val());
		dt_ins.push($('[name="icon_edit"]').val());
		dt_ins.push($('[name="parent_edit"]').val());
		dt_ins.push($('[name="desc_edit"]').val());
		// dt_ins.push($('[name="koor_x"]').val());
		// dt_ins.push($('[name="koor_y"]').val());
		
		var arr = $("input[name='trcTypeEd']:checked").getCheckboxVal()
		if(arr.length <= 0){
			var arr = 0;
		}
		bootbox.confirm("Update data menu?", function(result){
			if (result) {
				$.post( base_url+"User_menu/update", { opr:"post-edit-kantor", usr_id : usr_id, dt_ins : dt_ins, arr:arr}).done(function( data ) {
					if (data == '1'){
						$('#edit-kantor').modal('hide');
						var pgact = $('#pg_office').find('.active').html();
						if(pgact == undefined){
							pgact = 1;
						} 
						$('#search').removeAttr('value');
						
						// getlist_office(pgact);
						location.reload();
					} else {
						bootbox.alert('Kesalahan update data...');
					}
				});
			}else{
			}
		});
	});
$(document).on('click','.delete-user-menu',function(){
		var idx = $(this).closest('tr').attr('idx');
		
		bootbox.confirm("Delete user group type?", function(result){
			if (result) {
				$.post( base_url+"User_menu/delete", { opr:"post-edit-kantor", usr_id : idx}).done(function( data ) {
					if (data == '1'){
						// $('#edit-kantor').modal('hide');
						var pgact = $('#pg_office').find('.active').html();
						$('#search').removeAttr('value');
						
						if(pgact == undefined){
							pgact = 1;
						} 
						// getlist_office(pgact);
						location.reload();
					} else {
						bootbox.alert('Kesalahan delete data...');
					}
				});
			}else{
			}
		});
	});