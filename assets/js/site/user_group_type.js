$(document).ready(function(){
	get_modal();
	$("#1060").collapse('toggle');
});

function get_modal(){
	var $title = $("#title-type"),
		title = "";
	$('.control-btn').on('click', '#add-group', function(){
		$('#group-modal').modal();
		title = "Tambah";
		$title.html(title);

		$('#group-form').trigger("reset");
	});

	$('#lst_group').on('click','.edit-group', function(){
		var id = $(this).attr('data-id');
		$('#group-modal').modal();
		title = "Ubah";
		$title.html(title);
	});
}

function post_data(){
	var id = $('[name="group_id"]').val();
	var dt_ins = [];
	dt_ins.push($('[name="nama"]').val());
	dt_ins.push($('[name="deskripsi"]').val());

	var arr = $("input[name='trcType']:checked").getCheckboxVal()
	if(arr.length <= 0){
		var arr = 0;
	}
	var validator = $("#group-form").validate();
	if(validator.form()){
		if(id != 0){
			$.post( base_url+"User_group_types/update", {usr_id : id, dt_ins : dt_ins, arr:arr}).done(function( results ) {
				if (results == "1"){
					location.reload();
				}else{
					bootbox.alert("Terjadi kesalahan saat memasukan data");
				}
			});
		}else{
			$.post( base_url+"references/post_group_type/", { dt_ins : dt_ins, trcType : arr}).done(function( results ) {
				if (results == "1"){
					location.reload();
				}else{
					bootbox.alert("Terjadi kesalahan saat memasukan data");
				}
			});
		}
		
	}
}

jQuery.fn.getCheckboxVal = function(){
	var vals = [];
	var i = 0;
	this.each(function(){
		vals[i++] = jQuery(this).val();
	});
	return vals;
}

$(document).on('click','.edit-group',function(){
	$('#group-form').trigger("reset");
	var idx = $(this).closest('tr').attr('idx');
	var tr = $(this).closest('tr');
	$('#edit-user-group-types').data('idx',idx);

	$('[name="group_id"]').val(idx);
	$('[name="nama"]').val($(tr).find('td').eq(1).html());
	$('[name="deskripsi"]').val($(tr).find('td').eq(2).html());
	$.post( base_url+"User_group_types/get_trc_type_by_group/", { opr : 'add_office', idx: idx}).done(function( data ) {
		var json = $.parseJSON(data)
		var values = [];

		for (var i = 0; i < json.length; i++) {
			// console.log(json[i].TrcTypeID);
			values.push(String(json[i].TrcPanelID));
			// $("#user-list [value=" + json[i].TrcTypeID + "]").attr("checked", "checked");
		}
		$("#user-list").find('[value=' + values.join('], [value=') + ']').prop("checked", true);
		// console.log(json.length);		
	});
});

$(document).on('click','.post-upd-user-edit',function(){
		var usr_id = $('#edit-user-group-types').data('idx');
		var dt_ins = [];
		// if ($('#lev_ktr').val() == "1"){
		// 	dt_ins.push("0");
		// } else {
		// 	dt_ins.push($('#parent_ktr').val());
		// }
		// dt_ins.push($('#lev_ktr').val());
		dt_ins.push($('[name="name_edit"]').val());
		dt_ins.push($('[name="desc_edit"]').val());
		// dt_ins.push($('[name="koor_x"]').val());
		// dt_ins.push($('[name="koor_y"]').val());
		
		var arr = $("input[name='trcTypeEd']:checked").getCheckboxVal()
		if(arr.length <= 0){
			var arr = 0;
		}
		bootbox.confirm("Update data kantor?", function(result){
			if (result) {
				$.post( base_url+"User_group_types/update", { opr:"post-edit-kantor", usr_id : usr_id, dt_ins : dt_ins, arr:arr}).done(function( data ) {
					if (data == '1'){
						$('#edit-kantor').modal('hide');
						var pgact = $('#pg_office').find('.active').html();
						if(pgact == undefined){
							pgact = 1;
						} 
						$('#search').removeAttr('value');
						
						// getlist_office(pgact);
						location.reload();
					} else {
						bootbox.alert('Kesalahan update data...');
					}
				});
			}else{
			}
		});
	});
$(document).on('click','.delete-user-type',function(){
		var idx = $(this).closest('tr').attr('idx');
		
		bootbox.confirm("Delete user group type?", function(result){
			if (result) {
				$.post( base_url+"User_group_types/delete", { opr:"post-edit-kantor", usr_id : idx}).done(function( data ) {
					if (data == '1'){
						// $('#edit-kantor').modal('hide');
						var pgact = $('#pg_office').find('.active').html();
						$('#search').removeAttr('value');
						
						if(pgact == undefined){
							pgact = 1;
						} 
						// getlist_office(pgact);
						location.reload();
					} else {
						bootbox.alert('Kesalahan delete data...');
					}
				});
			}else{
			}
		});
	});