$(document).ready(function(){
	datatable('table', 'Offices/get_ajax_data/');
	get_modal('office', null, null, null);
	change_select_level_office();

	$("#1060").collapse('toggle');

	$('.control-btn').on('click', '#import-office', function(){
		$('#office-import-modal').modal();
	});
});

function change_select_level_office(){
	$(document).on('change','#change-level',function(){
		if (parseInt($('#change-level').val()) > 2 && parseInt($('#change-level').val()) != 5){
			if (parseInt($('#change-level').val()) == 3){
				$('#show-regional').show();
				$('#show-kprk').hide();
			} else {
				$('#show-regional').show();
				$('#show-kprk').show();
			}
			$('#get_regional').attr('readonly', false);
			get_office_data('#get_regional', 2);
		
		} else {
			$('#show-kprk').hide();
			$('#show-regional').hide();
		}
		
	});

	$(document).on('change','#get_regional',function(){
		$('#get_kprk').val("").trigger('change');
		var id_regional = $('#get_regional').val();
		$('#get_kprk').attr('readonly', false);

		get_office_data('#get_kprk', 3, id_regional);
	});
}

function post_data_office(){
	var validator = $("#office-form").validate();
	if(validator.form()){
		$.post( base_url+"Offices/check_code/", {code_office : $('#kode_kantor').val()}).done(function( data ) {
			var data_form = $('#office-form').serialize();
			if($('[name="kantor_id"]').val() != 0 && data == 1){
				data = 0;	
			}
			if(String(data) == '0'){
				// return 1;
				$.post( base_url+"Offices/save_office", data_form).done(function( results ) {
					if (results == "1"){
						location.reload();
					}else{
						bootbox.alert("Terjadi kesalahan saat memasukan data");
					}
				});
			}else{
				bootbox.alert("Kode kantor sudah terdaftar");
			}
		});
	}
}

$(document).on('click','.edit-office',function(){
	$('#show-regional').hide();
	$('#show-kprk').hide();
	$('#office-form').trigger("reset");
	var idx = $(this).attr('data-id');
	$.post( base_url+"Offices/get_office_by_id/", {idx: idx}).done(function( data ) {
		var json = $.parseJSON(data);
		$('[name="kantor_id"]').val(json.C000_SysID);
		$('select[name="level"]').val(json.C012_Level);

		var parentID = json.C001_ParentID;		
		if(parentID != 0 && json.C012_Level != 1 && json.C012_Level != 2){
			$.post( base_url+"Offices/get_office_by_id/", {idx: parentID}).done(function( dataParent ) {
				var jsonParent = $.parseJSON(dataParent);
				var levelID = jsonParent.C012_Level;		
				if(levelID == 2){
					$('#show-regional').show();
					$('#get_regional').attr('readonly', false);
					get_office_data('#get_regional', 2);
					var s1 = $("#get_regional").data('select2'); 
					setTimeout(function(){
						s1.trigger('select', { 
						  data: {
								id: jsonParent.C000_SysID,
								text: jsonParent.C020_Descr
							} 
						}); 
					},1000);
				}else{
					$('#show-kprk').show();
					get_office_data('#get_kprk', 3, jsonParent.C001_ParentID);
					$('#get_kprk').attr('readonly', false);
	    			var s1 = $("#get_kprk").data('select2'); 
					setTimeout(function(){
						s1.trigger('select', { 
						  data: {
								id: jsonParent.C000_SysID,
								text: jsonParent.C020_Descr
							} 
						}); 
					},1000);

					$.post( base_url+"references/get_office_by_id/", {idx: jsonParent.C001_ParentID}).done(function( dataParent2 ) {
						var jsonParent2 = $.parseJSON(dataParent2);
						$('#get_regional').attr('readonly', false);
						get_office_data('#get_regional', 2);

						$('#show-regional').show();
						var s2 = $("#get_regional").data('select2'); 
						s2.trigger('select', { 
						  data: {
								id: jsonParent2.C000_SysID,
								text: jsonParent2.C020_Descr
							} 
						});
					});
				}
			});
		}

		$('[name="kode"]').val(json.C010_Code);
		$('[name="deskripsi"]').val(json.C020_Descr);
		$('[name="koor_x"]').val(json.C100_Long);
		$('[name="koor_y"]').val(json.C101_Lat);
	});
});

$(document).on('click','.delete-office',function(){
	var idx = $(this).attr('data-id');
	bootbox.confirm("Hapus data kantor?", function(result){
		if (result) {
			$.post( base_url+"Offices/check_delete_office", {id : idx}).done(function( data ) {
				if(data == '1'){
					$.post( base_url+"Offices/delete_office", {id : idx}).done(function( data ) {
						if (data == '1'){
							location.reload();
						} else {
							bootbox.alert('Kesalahan delete data...');
						}
					});
				}else{
					bootbox.alert("Data tidak terhapus karena digunakan sebagai parent office atau telah digunakan untuk transaksi.");
				}
			});
		}else{
		}
	});
});