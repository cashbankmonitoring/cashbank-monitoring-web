$(document).ready(function(){
	datatable('table', 'Program_alocations/get_ajax_data/');
	get_modal('alokasi', null, null, '#get_kantor');
	
	$("#1060").collapse('toggle');
	$('.control-btn').on('click', '#import-alokasi', function(){
		$('#alokasi-import-modal').modal();
	});
});

function get_sisa_alokasi(prg_id){
	$.post(base_url+"Program_alocations/get_sisa_alokasi/", { opr:"get_sisa_alokasi", prg_id : prg_id}).done(function( data ) {
		$('#sisa-alokasi').html(data);
	});
}

function post_data_alocation(){
	var sisa_alok = $('#sisa-alokasi').html().replace(/,/g , "");
	var tot_alok = parseFloat($('[name="alokasi"]').val().replace(/,/g , ""));

	if ( tot_alok > sisa_alok){
		bootbox.alert('Nilai alokasi terlalu besar...'+tot_alok+"  karena sisa alokasi"+sisa_alok);
		return false;
	}

	var validator = $("#alokasi-form").validate();
	if(validator.form()){
		var data_form = $('#alokasi-form').serialize();
		$.post( base_url+"Program_alocations/save_allocations", data_form).done(function( results ) {
			if (results == "1"){
				location.reload();
			}else{
				bootbox.alert("Terjadi kesalahan saat memasukan data");
			}
		});
	}
}

$(document).on('click','.edit-alokasi',function(){
	$('#alokasi-form').trigger("reset");
	var idx = $(this).attr('data-id');
	$.post( base_url+"Program_alocations/get_alokasi_by_id/", {idx: idx}).done(function( data ) {
		var json = $.parseJSON(data);

		var officeID = json.C010_OfficeID;
		$.post( base_url+"references/get_office_by_id/", {idx: officeID}).done(function( dataParent ) {
			var jsonParent = $.parseJSON(dataParent);
			$('#get_kantor').attr('readonly', false);
			get_office_data('#get_kantor');
			
			if(jsonParent){
				var s1 = $("#get_kantor").data('select2'); 
				s1.trigger('select', { 
				  data: {
						id: jsonParent.C000_SysID,
						text: jsonParent.C020_Descr
					} 
				});
			}else{
				$('[name="kantor_id"]').val();
			}
		});

		$('[name="alokasi_id"]').val(json.C000_SysID);
		$('select[name="program"]').val(json.C020_ProjectID);
		get_sisa_alokasi(json.C020_ProjectID);
		$('[name="alokasi"]').val(json.C030_Alokasi);
		$('[name="deskripsi"]').val(json.C060_Descr);
	});
});

$(document).on('click','.delete-alokasi',function(){
	var idx = $(this).attr('data-id');
	bootbox.confirm("Delete data alokasi program..?", function(result) {
	    if (result) {
	        $.post( base_url+"Program_alocations/delete_alokasi/", {id : idx}).done(function( data ) {
				$('#edit-alokasi').modal('hide');
				location.reload();
		});
	    } else {

	    }
	});
});
