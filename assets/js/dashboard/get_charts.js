$(document).ready(function(){
	// var url = "http://code.highcharts.com/highcharts.js";
	var url = base_url+"assets/js/highcharts.js";
	var url2 = base_url+"assets/js/highcharts-3d.js";
	$.when(
	    $.getScript(url),
	    $.getScript(url2),
	    $.Deferred(function( deferred ){
	        $( deferred.resolve );
	    })
	).done(function( script, textStatus ) {
	    console.log( textStatus );
	  })
	  .fail(function( jqxhr, settings, exception ) {
	    $( "div.log" ).text( "Triggered ajaxError handler." );
	});
	setTimeout(function() {
	    get_cashbank_chart();    
    }, 1000);
    // option_chart();
	get_chart_by_level();
});

function option_chart(){
	highcharts.setOptions({
		lang: {
			thousandsSep: ','
		}
	});
}

function get_cashbank_chart(){
	$.post(base_url+"dashboard/get_cashbank", {}, function(data) {
		
		var n = data.total_kasbank;
		// var n = 0;
		// for (var i = 0; i < data.length; i++) {
		// 	n = n + data[i].y
		// }
		var total_kas = n.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
		// concole.log(data);

		$('#cashbank_chart').highcharts({
	        chart: {
	            type: 'pie',
	            options3d: {
	                enabled: true,
	                alpha: 45,
	                beta: 0
	            }
	        },
	        title: {
	            text: 'Total Cash : '+ total_kas +' IDR'
	        },
	        tooltip: {
	            pointFormat: '{series.name}: <b>{point.y:,.0f}</b> IDR'
	        },
	        plotOptions: {
	            pie: {
	            	size: 200,
	                allowPointSelect: true,
	                cursor: 'pointer',
	                depth: 35,
	                dataLabels: {
	                    enabled: true,
	                    format: '{point.name} <br> <b>{point.y:,.0f}</b> IDR'
	                }
	            }
	        },
	        series: [{
	            type: 'pie',
	            name: 'Positions',
	            data: data.kasbank
	        }]
	    });
	},"json");
}

function get_chart_by_level(){
	$.post(base_url+"dashboard/get_level_by", {}, function(data) {
		$('#chart_regional').highcharts({
			chart: {
	            type: 'column'
	        },
	        title: {
	            text: 'Total Cash Bank Per '+data.user_level
	        },
	        subtitle: {
	            text: ''
	        },
	        xAxis: {
	            categories: data.type,
	            crosshair: true
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: 'IDR'
	            }
	        },
	        tooltip: {
	            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	                '<td style="padding:0"><b>{point.y}</b> IDR</td></tr>',
	            footerFormat: '</table>',
	            shared: true,
	            useHTML: true
	        },
	        plotOptions: {
	            column: {
	                pointPadding: 0.2,
	                borderWidth: 0
	            }
	        },
	        series: data.content
	    });
	},"json");
}
