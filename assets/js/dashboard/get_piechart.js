$(document).ready(function(){
	setTimeout(function() {
	    get_cashbank_chart();    
    }, 1000);
    option_chart();
});

function option_chart(){
	Highcharts.setOptions({
		lang: {
			thousandsSep: ','
		}
	});
}

function get_cashbank_chart(){
	$.post(base_url+"dashboard/get_cashbank", {}, function(data) {
		
		var n = data.total_kasbank;
		// var n = 0;
		// for (var i = 0; i < data.length; i++) {
		// 	n = n + data[i].y
		// }
		var total_kas = n.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
		// concole.log(data);

		$('#cashbank_chart').highcharts({
	        chart: {
	            type: 'pie',
	            options3d: {
	                enabled: true,
	                alpha: 45,
	                beta: 0
	            }
	        },
	        title: {
	            text: 'Total Cash : '+ total_kas +' IDR'
	        },
	        tooltip: {
	            pointFormat: '{series.name}: <b>{point.y:,.0f}</b> IDR'
	        },
	        plotOptions: {
	            pie: {
	            	size: 200,
	                allowPointSelect: true,
	                cursor: 'pointer',
	                depth: 35,
	                dataLabels: {
	                    enabled: true,
	                    format: '{point.name} <br> <b>{point.y:,.0f}</b> IDR'
	                }
	            }
	        },
	        series: [{
	            type: 'pie',
	            name: 'Positions',
	            data: data.kasbank
	        }]
	    });
	},"json");
}