$(document).ready(function(){
    option_chart();
	get_chart_by_level();
});

function option_chart(){
	Highcharts.setOptions({
		lang: {
			thousandsSep: ','
		}
	});
}

function get_chart_by_level(){
	$.post(base_url+"dashboard/get_level_by", {}, function(data) {
		$('#chart_regional').highcharts({
			chart: {
	            type: 'column'
	        },
	        title: {
	            text: 'Total Cash Bank Per '+data.user_level
	        },
	        subtitle: {
	            text: ''
	        },
	        xAxis: {
	            categories: data.type,
	            crosshair: true
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: 'IDR'
	            }
	        },
	        tooltip: {
	            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	                '<td style="padding:0"><b>{point.y}</b> IDR</td></tr>',
	            footerFormat: '</table>',
	            shared: true,
	            useHTML: true
	        },
	        plotOptions: {
	            column: {
	                pointPadding: 0.2,
	                borderWidth: 0
	            }
	        },
	        series: data.content
	    });
	},"json");
}
