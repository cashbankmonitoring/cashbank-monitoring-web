$(document).ready(function() {
   $('input[type="radio"]').click(function() {
       if($(this).attr('id') == 'date_range') {
       		$('#date_range').prop("checked", true);	
            $('#per_date').hide();           
            $('#per_date_range').show();           
       }else {
       		$('#today').prop("checked", true);
            $('#per_date').show();           
            $('#per_date_range').hide();  
       }
   });

   var dates = $("#from, #to").datepicker({
	    // defaultDate: "+1w",
	    onSelect: function(selectedDate) {
	        var option = this.id == "from" ? "minDate" : "maxDate",
	            instance = $(this).data("datepicker"),
	            date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
	        dates.not(this).datepicker("option", option, date);
	    }
	});

   var validator = $( "#form-rekap" ).validate({
			rules: {
               date: { 
                 required: true,
               },
               date_from: { 
                 required: true,
               },
               date_to: { 
                 required: true,
               }
            }
		});
});

// function get_datatable(panelCode, columns, url){
//   if(url == undefined){
//           url = "Recap_transactions/get_rekap/"+panelCode;
//   }else{
//           url = url;
//   }

//   $('[name="table-'+panelCode+'"]').DataTable({
//           "bSort": false,
//           "searching": false,
//           "processing": true,
//           "serverSide": true,
//           "ajax": {
//                   "url": base_url+url,
//                   "type": "GET",
//                   "dataSrc" : function (json) {

//                           var data = json['data'];
//                           var j = json['start'];

//                           data = get_data_datatable(paneldata, j);
                          
//                           return data;
//                   },
//                   },
//           "columns": columns,
//   });
// }

function change_format_number(number){
    return numeral(number).format('0,0');
}