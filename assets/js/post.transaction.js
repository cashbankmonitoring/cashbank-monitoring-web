/*
 $(document).on('click', '.btn-submit-trcplg', function () {
 var jml_row = $('#tbl_listTrc').find('tr');
 var transactions = [];
 $.each(jml_row, function (i, obj) {
 var trc = {};
 trc['line_id'] = i + 1;
 trc['type'] = $(obj).find('[name="operation_type"]').val();
 trc['jns_bayar'] = $(obj).find('[name="jns_bayar"]').val();
 if(trc['jns_bayar'] == 3){
 trc['rek_bank'] = $(obj).find('[name="rek_bank"]').val();
 }
 trc['amount_in'] = $(obj).find('[name="AmountInTrc"]').val().replace(/,/g, "");
 trc['amount_out'] = $(obj).find('[name="AmountOutTrc"]').val().replace(/,/g, "");
 trc['amount_service'] = $(obj).find('[name="AmountService"]').val().replace(/,/g, "");
 
 transactions.push(trc);
 });
 
 var is_valid = is_trc_pelanggan_valid(transactions);
 if(!is_valid){
 alert('Data belum diisi semua!');
 return;    
 }
 
 blockUI($('#trc_dv'));
 $.post(base_url+"transactions/save_trc_pelanggan", {
 transactions: transactions
 }).done(function (data) {
 console.log(data);
 if (parseInt(data) > 0) {
 
 bootbox.alert("Data berhasil dimasukan!", function() {
 // Example.show("Hello world callback");
 
 location.reload();
 });
 }else{
 bootbox.alert("Data gagal dimasukan!", function() {
 // Example.show("Hello world callback");
 
 unblockUI($('#trc_dv'));
 // location.reload();
 });
 }
 });
 });
 */

$(document).on('click', '.accept-link', function (e) {
        e.preventDefault();

        var parent = $(this).closest('tr');

        var trc_id = $(parent).find('[name="TrcID"]').val();
        var line_id = $(parent).find('[name="LineID"]').val();
        var received_amount = 0;

        bootbox.confirm("Apakah Anda yakin?", function (result) {
                if (!result) {
                        return;
                }
                // blockUI($(parent));
                $.blockUI(parent);
                $.post(base_url + "transactions/accept_internal_trf?trc_type_code=" + trc_type_code, {
                        trc_type_code: trc_type_code,
                        trc_id: trc_id,
                        line_id: line_id,
                        received_amount: received_amount,
                }).done(function (data) {
                        console.log(data);
                        if (parseInt(data) > 0) {
                                $(parent).remove();
                        } else {
                                bootbox.alert("Data gagal dimasukan!", function () {
//                                unblockUI($(parent));
                                        $.unblockUI($(parent));
                                });
                        }
                });
        });

});

$(document).on('click', '.reject-link', function (e) {
        e.preventDefault();

        var parent = $(this).closest('tr');

        var trc_id = $(parent).find('[name="TrcID"]').val();
        var line_id = $(parent).find('[name="LineID"]').val();
        var received_amount = 0;

//        blockUI($(parent));
        $.blockUI(parent);
        $.post(base_url + "transactions/reject_internal_trf?trc_type_code=" + trc_type_code, {
                trc_id: trc_id,
                line_id: line_id,
                received_amount: received_amount,
        }).done(function (data) {
                console.log(data);
                if (parseInt(data) > 0) {
                        $(parent).remove();
                } else {
                        bootbox.alert("Data gagal dimasukan!", function () {
//                                unblockUI($(parent));
                                $.unblockUI($(parent));
                        });
                }
        });
});

$(document).on('click', '.btn-post', function () {
        var parent = $(this).closest('.sub-trc');
        var sub_trc_type = parent.attr('sub_trc_type');

        var lines = parent.find('.line');

        if ($(lines).length == 0) {
                bootbox.alert("Mohon masukkan data!");
                return;
        }

        var transactions = {};
        var extra = {};

        var isValid = validate_form(sub_trc_type, lines);
        if (!isValid) {
                $.unblockUI();
                return;
        }

        bootbox.confirm("Apakah Anda yakin data yang Anda masukan sudah benar", function (result) {
                if (!result) {
                        return;
                }

                $.each(lines, function (i, obj) {
                        var rows = get_input_post_in_a_line(obj, 'input-post');
                        var rows_extra = get_input_post_in_a_line(obj, 'input-post-extra');
                        var line_id = i + 1;

                        transactions[line_id] = rows;
                        extra[line_id] = rows_extra;
                });

                console.log(JSON.stringify(transactions));
//              console.log(extra);
//return;
                $.blockUI();
                $.post(base_url + "transactions/save?trc_type_code=" + trc_type_code, {
                        sub_trc_type_id: sub_trc_type,
                        data: transactions,
                        extra: extra
                }).done(function (data) {
                        console.log(data);
                        $.unblockUI();
                        if (parseInt(data) > 0) {
                                bootbox.alert("Data berhasil dimasukan!", function () {
                                        location.reload();
                                });
                        } else {
                                bootbox.alert("Data gagal dimasukan!", function () {

                                });
                        }
                });
        });

});

function get_input_post_in_a_line(line, class_name) {
        var rows = {};
        $.each($(line).find('.' + class_name), function (i, obj) {
                $.each($(obj), function (i2, obj2) {
                        var name = $(obj2).attr('name');
                        if (name == null) {
                                return;
                        }

                        var seq_ids = $(obj2).attr('seq_id');
                        if (seq_ids == null) {
                                seq_ids = "1";
                        }

                        var seq_ids = seq_ids.split(",");

                        for (var j = 0; j < seq_ids.length; j++) {
                                var seq_id = seq_ids[j];
                                if (rows[seq_id] == null) {
                                        rows[seq_id] = {};
                                }
                                var row = rows[seq_id];

                                row[name] = $(obj2).val();
                                if ($(obj2).hasClass('nomor')) {
                                        row[name] = row[name].replace(/,/g, "");
                                } else if ($(obj2).hasClass('datetimepicker')) {
                                        row[name] = moment(row[name], "MM/DD/YYYY H:mm").unix();
                                }
                        }


                });
        });

        return rows;
}

function validate_form(sub_trc_type, lines) {
        if (sub_trc_type == '18') {
                return validate_trc_pelanggan(lines);
        } else if (sub_trc_type == '8') {
                return validate_trc_program(lines);
        } else if (sub_trc_type == '9' || sub_trc_type == '10') {
                return validate_trc_internal_operasional(sub_trc_type, lines);
        } else if (sub_trc_type == '11') {
                return validate_transfer_kas_bank(lines);
        } else if (sub_trc_type == '14' || sub_trc_type == '24') {
                return validate_transfer_internal(lines);
        } else if (sub_trc_type == '17'
                || sub_trc_type == '25'
                || sub_trc_type == '26'
                || sub_trc_type == '27') {
                return validate_transfer_external(lines);
        }

        return false;
}

function validate_trc_pelanggan(lines) {
        var isValid = true;
        $.each(lines, function (i, obj) {
                isValid = isValid & validate_trc_pelanggan_in_a_line(obj);
        });

        return isValid;
}

function validate_trc_program(lines) {
        var isValid = true;

        var total_penyaluran = 0;
        $.each(lines, function (i, line) {
                total_penyaluran += parseInt($(line).find('[name="Amount"]').val().replace(/,/g, ""));
                total_penyaluran = Math.abs(total_penyaluran);
        });

        var isValid = total_penyaluran > 0;

        if (!isValid) {
                bootbox.alert('Isi salah satu "Total Penyaluran"!');
        }

        $.each(lines, function (i, line) {
//                var penyaluran = parseInt($(line).find('[name="Amount"]').val().replace(/,/g, ""));
                var sisa_kas = parseInt($(line).find('[name="sisa_kas_prg"]').val().replace(/,/g, ""));

                if (sisa_kas < 0) {
                        isValid = false;
                        $(line).find('.error').show();
                }

        });

        return isValid;
}

function validate_transfer_external(lines) {
        var isValid = validate_transfer_kas_bank(lines);

        $.each(lines, function (i, line) {
                $.each($(line).find('.input-post-extra:not([name="C005_Descr"])'), function (j, element) {
                        var val = $(element).val();
                        if (val == "") {
                                isValid = false;
                                $(element).parent().find('.error').show();
                        } else {
                                $(element).parent().find('.error').hide();
                        }
                });
                //input-post-extra

                var date = $(line).find('[name="C001_DateTransfer"]').val();

                var date = moment(date, "MM/DD/YYYY H:mm").unix();

                console.log(date);
        });



        return isValid;
}

function validate_transfer_internal(lines) {
        var isValid = validate_transfer_kas_bank(lines);

        $.each(lines, function (i, line) {
                console.log('validate_transfer_internal');

                $.each($(line).find('[name="MLedgerIDTo"]'), function (j, element) {
                        var seq_id = $(element).attr('seq_id');
                        var mledgerid = $(element).val();
                        if (mledgerid == '3') {
                                var bank_name_id = $(line).find('select[name="bank_name"][seq_id="' + seq_id + '"]').val();
                                console.log('validate_transfer_kas_bank' + bank_name_id);
                                if (bank_name_id == '0') {
                                        isValid = false;
                                        $(line).find('select[name="bank_name"][seq_id="' + seq_id + '"]').parent().find('.error').show();
                                } else {
                                        $(line).find('select[name="bank_name"][seq_id="' + seq_id + '"]').parent().find('.error').hide();
                                }

                                var bank_account_id = $(line).find('select[name="SubLedger1IDTo"][seq_id="' + seq_id + '"]').val();
                                if (bank_account_id == '0') {
                                        isValid = false;
                                        $(line).find('select[name="SubLedger1IDTo"][seq_id="' + seq_id + '"]').parent().find('.error').show();
                                } else {
                                        $(line).find('select[name="SubLedger1IDTo"][seq_id="' + seq_id + '"]').parent().find('.error').hide();
                                }
                        }
                });

                var sledgerid = $(line).find('[name="SLedgerIDTo"][seq_id="2"]').val();
                if (sledgerid == '0') {
                        isValid = false;
                        $(line).find('[name="SLedgerIDTo"][seq_id="2"]').parent().find('.error').show();
                } else {
                        $(line).find('[name="SLedgerIDTo"][seq_id="2"]').parent().find('.error').hide();
                }


        });

        return isValid;
}

function validate_transfer_kas_bank(lines) {
        var isValid = true;

        $.each(lines, function (i, line) {
                $.each($(line).find('[name="MLedgerID"]'), function (j, element) {
                        var seq_id = $(element).attr('seq_id');
                        var mledgerid = $(element).val();
                        if (mledgerid == '3') {
                                var bank_name_id = $(line).find('select[name="bank_name"][seq_id="' + seq_id + '"]').val();
                                console.log('validate_transfer_kas_bank' + bank_name_id);
                                if (bank_name_id == '0') {
                                        isValid = false;
                                        $(line).find('select[name="bank_name"][seq_id="' + seq_id + '"]').parent().find('.error').show();
                                } else {
                                        $(line).find('select[name="bank_name"][seq_id="' + seq_id + '"]').parent().find('.error').hide();
                                }

                                var bank_account_id = $(line).find('select[name="SubLedger1ID"][seq_id="' + seq_id + '"]').val();
                                if (bank_account_id == '0') {
                                        isValid = false;
                                        $(line).find('select[name="SubLedger1ID"][seq_id="' + seq_id + '"]').parent().find('.error').show();
                                } else {
                                        $(line).find('select[name="SubLedger1ID"][seq_id="' + seq_id + '"]').parent().find('.error').hide();
                                }
                        }
                });

                var amount = Number($(line).find('[name="Amount"]').val().replace(/,/g, ""));
                var balance = $(balanceElmnt).val();
                var balanceElmnt = $(line).find('[name="balance"]');
                
                if (balance != null) {
                        var balance = Number($(balanceElmnt).val().replace(/,/g, ""));

                        if (amount > balance || amount == 0) {
                                isValid = false;
                                $(line).find('[name="Amount"]').parent().find('.error').show();
                        } else {
                                $(line).find('[name="Amount"]').parent().find('.error').hide();
                        }
                } else {
                        if (amount == 0) {
                                isValid = false;
                                $(line).find('[name="Amount"]').parent().find('.error').show();
                        } else {
                                $(line).find('[name="Amount"]').parent().find('.error').hide();
                        }
                }



        });

        return isValid;
}

function validate_trc_internal_operasional(sub_trc_type, lines) {
        var isValid = true;

        $.each(lines, function (i, line) {
                if ($(line).find('[name="MLedgerID"]').val() == 3) {
                        $.each($(line).find('select:not([name="MLedgerID"])'), function (j, element) {
                                var val = $(element).val();
                                if (val == '0') {
                                        isValid = false;
                                        $(element).parent().find('.error').show();
                                } else {
                                        $(element).parent().find('.error').hide();
                                }
                        });
                }

                var val = Number(($(line).find('[name="Amount"]').val().replace(/,/g, "")));
                if (val == 0) {
                        isValid = false;
                        $(line).find('[name="Amount"]').parent().find('.error').show();
                } else if (sub_trc_type == '10') {
                        var balance = Number(($(line).find('[name="balance"]').val().replace(/,/g, "")));

                        if (val > balance) {
                                console.log(val);
                                isValid = false;
                                $(line).find('[name="Amount"]').parent().find('.error').show();
                        } else {
                                $(line).find('[name="Amount"]').parent().find('.error').hide();
                        }
                } else {
                        $(line).find('[name="Amount"]').parent().find('.error').hide();
                }


        });

        return isValid;
}

function validate_trc_pelanggan_in_a_line(line) {
        console.log('validate_trc_pelanggan_in_a_line');
        var isValid = true;
        $.each($(line).find('select'), function () {
                var value = $(this).val();
                var name = $(this).attr('name');
                console.log('[name="MLedgerID"][seq_id="1,2"] > ' + $(line).find('[name="MLedgerID"][seq_id="1,2"]').val());
                console.log('name > ' + name);
                console.log('value > ' + value);
                if ($(line).find('[name="MLedgerID"][seq_id="1,2"]').val() == '2'
                        && name == 'SubLedger1ID') {
                        console.log('susu');
                        $(this).closest('td').find('.error').hide();
                } else if (value == '0') {
                        isValid = false;
                        $(this).closest('td').find('.error').show();
                } else {
                        $(this).closest('td').find('.error').hide();
                }

        });

        var el_amount_in = $(line).find('#AmountInTrc');
        var el_amount_out = $(line).find('#AmountOutTrc');
        if (el_amount_in.val() == '0' && el_amount_out.val() == '0') {
                isValid = false;
                el_amount_in.closest('td').find('.error').show();
                el_amount_out.closest('td').find('.error').show();
        } else {
                el_amount_in.closest('td').find('.error').hide();
                el_amount_out.closest('td').find('.error').hide();
        }

        return isValid;
}

/*
 function is_trc_pelanggan_valid(transactions) {
 if (transactions.length === 0) {
 return false;
 }
 
 for (var i in transactions) {
 var trc = transactions[i];
 for (var key_trc in trc) {
 if (key_trc == 'rek_bank'
 || key_trc == 'amount_in'
 || key_trc == 'amount_out'
 || key_trc == 'amount_service') {
 continue;
 }
 
 var val = parseInt(trc[key_trc]);
 if (val < 1) {
 return false;
 }
 }
 if (parseInt(trc['jns_bayar']) === 3 && parseInt(trc['rek_bank']) < 1) {
 return false;
 }
 if (parseInt(trc['amount_in']) === 0 && parseInt(trc['amount_out']) === 0 && parseInt(trc['amount_service']) === 0) {
 return false;
 }
 }
 
 return true;
 }
 */
/*
 $(document).on('click', '.btn-submit-trcproj', function () {
 //VALIDASI
 var jml_row = $('#tbl_listTrcProj').find('tr');
 var transactions = [];
 
 $.each(jml_row, function (i, obj) {
 if (parseFloat($(obj).find('[name="AmountOutTrc"]').val().replace(/,/g, "")) > parseFloat($(obj).find('[name="sisa_kas_prg"]').val().replace(/,/g, ""))) {
 alert('Kas program tidak mencukupi...!');
 return false;
 }
 
 var trc = {};
 trc['program_id'] = $(obj).find('[name="projectID"]').val();
 trc['penyaluran'] = $(obj).find('[name="AmountOutTrc"]').val();
 
 transactions.push(trc);
 });
 
 
 blockUI($('#trc_dv'));
 $.post(base_url + "transactions/save_transaksi_program", {
 transactions: transactions,
 }).done(function (data) {
 // console.log(data);
 if (parseInt(data) > 0) {
 
 bootbox.alert("Data berhasil dimasukan!", function () {
 // Example.show("Hello world callback");
 
 location.reload();
 });
 } else {
 bootbox.alert("Data gagal dimasukan!", function () {
 // Example.show("Hello world callback");
 
 unblockUI($('#trc_dv'));
 // location.reload();
 });
 }
 });
 
 
 });
 */

/*
 $(document).on('click', '.post-pendapatan', function () {
 post_internal('masuk');
 });
 
 $(document).on('click', '.post-belanja', function () {
 post_internal('keluar');
 });
 
 
 function post_internal(jenis) {
 
 blockUI($('#trc_dv'));
 var kas_bank = $('#sel_int_kasbank_' + jenis).val();
 var bank_account = $('#sel_int_bankacc_' + jenis).val();
 var amount = $('[name="jml_' + jenis + '"]').val().replace(/,/g, "");
 var desc = $('[name="ket_' + jenis + '"]').val().replace(/,/g, "");
 var is_amount_in = jenis == 'masuk' ? 1 : 0;
 console.log(amount);
 //        blockUI($('#trc_dv'));
 $.post(base_url + "transactions/save_internal_operasional", {
 kas_bank: kas_bank,
 bank_account: bank_account,
 amount: amount,
 is_amount_in: is_amount_in,
 desc: desc
 }).done(function (data) {
 // console.log(data);
 if (parseInt(data) > 0) {
 
 bootbox.alert("Data berhasil dimasukan!", function () {
 // Example.show("Hello world callback");
 
 location.reload();
 });
 } else {
 bootbox.alert("Data gagal dimasukan!", function () {
 // Example.show("Hello world callback");
 
 unblockUI($('#trc_dv'));
 // location.reload();
 });
 }
 });
 }
 */


//function post_internal(jenis,kasbank,bankacc){
//		var head_ins = [];
//		var dt_ins = [];
//		head_ins.push('10020');
//		head_ins.push($('[name="C050_DocNum"]').val());
//		head_ins.push($('#trc_dv').attr('off_id'));
//		
//		dt_ins.push(kasbank);
//		dt_ins.push(bankacc);
//		dt_ins.push($('[name="jml_'+jenis+'"]').val().replace(/,/g , ""));
//		dt_ins.push($('[name="ket_'+jenis+'"]').val());
//		
//		blockUI($('#trc_dv'));
//		$.post( "php/post_data.php", { opr:"post-internal", dt_ins : dt_ins, head_ins : head_ins, jenis : jenis}).done(function( data ) {
//			if (parseInt(data) > 0){
//				unblockUI($('#trc_dv'));
//				alert('Transaksi berhasil...!');
//				location.reload();
//				//$('#tbl_listTrc').html('');
//			};
//		});
//
//}
/*
 $(document).on('click', '.post-trans-keluar', function () {
 
 if (parseFloat($('#jml_trans_keluar').val().replace(/,/g, "")) > parseFloat($('[name="val-total"]').val().replace(/,/g, ""))) {
 // alert();
 bootbox.alert('Jumlah transfer lebih besar dari saldo..!');
 return false;
 }
 
 
 var jml_row = $('#tbl_listTrc').find('tr');
 var dt_trans = [];
 var head_ins = [];
 head_ins.push('1020');
 head_ins.push($('[name="C050_DocNum"]').val());
 head_ins.push($('#trc_dv').attr('off_id'));
 
 var tjn = $('#tp_transfer').val();
 
 if (tjn == "") {
 bootbox.alert("Mohon isi semua field terlebih dahulu!");
 return false;
 } else if (tjn == "1") {
 // var asal_off = $('#trc_dv').attr('off_id');
 var asal_smb = $('[name="trans_jenis_asal"]').val();
 var asal_prog = $('#seltrans_prog_asal').val();
 var asal_jns = $('[name="trans_kasbank_asal"]').val();
 
 var asal_rek = $('#seltrans_bankacc_asal').val();
 
 var tuju_off = $('#get_idoff_tujuan').val();
 var tuju_smb = $('[name="trans_jenis_tujuan"]').val();
 var tuju_prog = $('#seltrans_prog_tujuan').val();
 var tuju_jns = $('[name="trans_kasbank_tujuan"]').val();
 var tuju_rek = $('#seltrans_bankacc_tujuan').val();
 
 // if (asal_jns == ""){ 
 //         bootbox.alert("Mohon isi semua field terlebih dahulu!");
 //         return false;
 // }
 var vol_trans = $('#jml_trans_keluar').val().replace(/,/g, "");
 
 //------------------------------Validasi------------------------------
 // if(asal_jns == 2){
 //         if(asal_smb == "" ||  tuju_smb=="" || tuju_prog == "" || tuju_jns=="" || tuju_rek==""){
 //                 bootbox.alert("Mohon isi semua field terlebih dahulu!");
 //                 return false;
 //         }
 // }else if(asal_jns == 3){
 //         if(asal_smb == "" || asal_jns=="" || asal_rek==0 || $('#seltrans_bank_asal').val() == 0 || asal_rek==undefined || tuju_smb=="" || tuju_prog == "" || tuju_jns=="" || tuju_rek==""){
 //                 bootbox.alert("Mohon isi semua field terlebih dahulu!");
 //                 return false;
 //         }
 // }else{
 //      bootbox.alert("Mohon isi semua field terlebih dahulu!");
 //         return false;   
 // }
 if (asal_jns == 2) {
 asal_rek = 0;
 }
 if (tuju_jns == 2) {
 tuju_rek = 0;
 }
 
 if (asal_smb == 1) {
 asal_prog = 0;
 }
 if (tuju_smb == 1) {
 tuju_prog = 0;
 }
 
 
 // dt_trans.push(asal_off);
 dt_trans.push(asal_smb);
 dt_trans.push(asal_prog);
 dt_trans.push(asal_jns);
 dt_trans.push(asal_rek);
 dt_trans.push(tuju_off);
 dt_trans.push(tuju_smb);
 dt_trans.push(tuju_prog);
 dt_trans.push(tuju_jns);
 dt_trans.push(tuju_rek);
 dt_trans.push(vol_trans);
 } else {
 var asal_off = $('#trc_dv').attr('off_id');
 var asal_smb = $('[name="trans_jenis_asal"]').val();
 var asal_prog = $('#seltrans_prog_asal').val();
 var asal_jns = $('[name="trans_kasbank_asal"]').val();
 var asal_rek = $('#seltrans_bankacc_asal').val();
 
 var tuju_tgl = $('[name="extgl_transfer"]').val();
 var tuju_bank = $('[name="exbank_transfer"]').val();
 var tuju_rek = $('[name="exrek_transfer"]').val();
 var tuju_nama = $('[name="exnm_transfer"]').val();
 var tuju_ket = $('[name="exket_transfer"]').val();
 
 var vol_trans = $('#jml_trans_keluar').val();
 var vol_trans = $('#jml_trans_keluar').val().replace(/,/g, "");
 
 //------------------------------Validasi------------------------------
 // if(asal_jns == 2){
 //         if(asal_smb == "" ||  tuju_smb=="" || asal_rek==0 || asal_prog == "" || tuju_tgl=="" || tuju_rek=="" || tuju_bank=="" || tuju_nama=="" || tuju_ket==""){
 //                 bootbox.alert("Mohon isi semua field terlebih dahulu!");
 //                 return false;
 //         }
 // }else if(asal_jns == 3){
 //         if(asal_smb == "" || asal_jns=="" || asal_rek==0 || tuju_smb=="" || tuju_prog == "" || tuju_tgl=="" || tuju_rek=="" || tuju_bank=="" || tuju_nama=="" || tuju_ket==""){
 //                 bootbox.alert("Mohon isi semua field terlebih dahulu!");
 //                 return false;
 //         }
 // }else{
 //      bootbox.alert("Mohon isi semua field terlebih dahulu!");
 //         return false;   
 // }
 // dt_trans.push(asal_off);
 dt_trans.push(asal_smb);
 dt_trans.push(asal_prog);
 dt_trans.push(asal_jns);
 dt_trans.push(asal_rek);
 dt_trans.push(tuju_tgl);
 dt_trans.push(tuju_bank);
 dt_trans.push(tuju_rek);
 dt_trans.push(tuju_nama);
 dt_trans.push(tuju_ket);
 dt_trans.push(vol_trans);
 
 
 }
 
 
 //console.log(dt_trans);
 
 
 $.post(base_url + "transactions/transfer_keluar", {opr: "trans-keluar", tjn: tjn, dt_trans: dt_trans, head_ins: head_ins}).done(function (data) {
 // var d = data.replace(/./g , "");
 // alert(d);
 if (parseInt(data) > 0) {
 
 bootbox.alert("Data berhasil dimasukan!", function () {
 // Example.show("Hello world callback");
 
 location.reload();
 });
 } else {
 bootbox.alert("Data gagal dimasukan!", function () {
 // Example.show("Hello world callback");
 
 unblockUI($('#trc_dv'));
 // location.reload();
 });
 }
 });
 
 
 });
 */
/*
 function get_transfer_kasbank(jenis) {
 var trf = {};
 trf['sumber'] = $('[name="tariksetor_jenis_' + jenis + '"]').val();
 trf['kas_bank'] = $('[name="tariksetor_kasbank_' + jenis + '"]').val();
 trf['program_id'] = $('#tariksetor_kasbank_' + jenis).val();
 trf['bank_account'] = $('#seltariksetor_bankacc_' + jenis).val();
 
 return trf;
 }
 
 $(document).on('click', '.post-tariksetor', function () {
 var trf_from = get_transfer_kasbank('asal');
 var trf_to = get_transfer_kasbank('ke');
 var amount = $('#jml_tariksetor').val().replace(/,/g, "");
 var desc = $('[name="ket_transfer"]').val();
 
 blockUI($('#trc_dv'));
 $.post(base_url + "transactions/save_transfer_kasbank", {
 trf_from: trf_from,
 trf_to: trf_to,
 amount: amount,
 desc: desc
 }).done(function (data) {
 // console.log(data);
 bootbox.alert();
 if (parseInt(data) > 0) {
 bootbox.alert("Data berhasil dimasukan!", function () {
 // Example.show("Hello world callback");
 
 location.reload();
 });
 }
 
 // location.reload();
 unblockUI($('#trc_dv'));
 });
 
 var dt_trans = [];
 var head_ins = [];
 head_ins.push('1020');
 head_ins.push($('[name="C050_DocNum"]').val());
 head_ins.push($('#trc_dv').attr('off_id'));
 
 // var id_off = $('#trc_dv').attr('off_id');
 
 var asal_smb = $('[name="tariksetor_jenis_asal"]').val();
 var asal_prog = $('#seltariksetor_prog_asal').val();
 var asal_jns = $('[name="tariksetor_kasbank_asal"]').val();
 var asal_rek = $('#seltariksetor_bankacc_asal').val();
 
 var tuju_smb = $('[name="tariksetor_jenis_ke"]').val();
 var tuju_prog = $('#setariksetor_prog_ke').val();
 var tuju_jns = $('[name="tariksetor_kasbank_ke"]').val();
 var tuju_rek = $('#seltariksetor_bankacc_ke').val();
 
 var ket_trans = $('[name="ket_transfer"]').val();
 var vol_trans = $('#jml_tariksetor').val().replace(/,/g , "");;
 //------------------------------Validasi------------------------------
 if(asal_jns == 2){
 if(asal_smb == "" ||  asal_prog == "" || tuju_smb=="" || tuju_prog==""){
 bootbox.alert("Mohon isi semua field terlebih dahulu!");
 return false;
 }
 }else if(asal_jns == 3){
 if(asal_smb == "" ||  asal_prog == "" || asal_jns=="" || asal_rek==0 || tuju_smb=="" || tuju_prog == ""){
 bootbox.alert("Mohon isi semua field terlebih dahulu!");
 return false;
 }
 }else{
 bootbox.alert("Mohon isi semua field terlebih dahulu!");
 return false;   
 }
 
 if(tuju_jns == 2){
 if(ket_trans == ""){
 bootbox.alert("Mohon isi semua field terlebih dahulu!");
 return false; 
 }
 }else if(tuju_jns == 3){
 if(ket_trans == "" || tuju_rek==0){
 bootbox.alert("Mohon isi semua field terlebih dahulu!");
 return false; 
 }
 }else{
 bootbox.alert("Mohon isi semua field terlebih dahulu!");
 return false;   
 }
 
 if (asal_jns == 2){ asal_rek = 0; }
 if (tuju_jns == 2){ tuju_rek = 0; }
 
 if (asal_smb == 1){ asal_prog = 0; }
 if (tuju_smb == 1){ tuju_prog = 0; }
 
 
 // dt_trans.push(id_off);
 dt_trans.push(asal_smb);
 dt_trans.push(asal_prog);
 dt_trans.push(asal_jns);
 dt_trans.push(asal_rek);
 dt_trans.push(tuju_smb);
 dt_trans.push(tuju_prog);
 dt_trans.push(tuju_jns);
 dt_trans.push(tuju_rek);
 dt_trans.push(vol_trans);
 dt_trans.push(ket_trans);
 var ins_ok = true;
 if (dt_trans[3] == dt_trans[7]){
 if (dt_trans[3] == 2){
 bootbox.alert('Tidak bisa dilakukan transfer dari KAS ke KAS');
 ins_ok = false;
 } 
 if (dt_trans[3] == 3){
 if (dt_trans[4] == dt_trans[8]){
 bootbox.alert('Tidak bisa dilakukan transfer dari BANK ke BANK dalam satu rekening');
 ins_ok = false;
 }
 }
 }
 
 if (vol_trans > $('[name="val-total-asal"]').val().replace(/,/g , "")){
 // alert('Saldo tidak mencukupi....');
 bootbox.alert('Saldo tidak mencukupi....');
 return false;
 } 
 
 
 
 if (ins_ok) {
 $.post( base_url+"transactions/transfer_kas_bank", { opr:"post-tariksetor", dt_trans : dt_trans, head_ins : head_ins }).done(function( data ) {
 //console.log(data);
 if (data== ".sukses"){
 location.reload();
 //$('#tbl_listTrc').html('');
 };
 });
 }
 
 });*/
