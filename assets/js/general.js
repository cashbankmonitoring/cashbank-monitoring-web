$(document).ready(function(){
	$(window).scroll(function(){
	  var sticky = $('.sticky'),
	      scroll = $(window).scrollTop();

	  if (scroll >= 200) sticky.addClass('fixed');
	  else sticky.removeClass('fixed');
	});

	initNomor();
});

function initNomor() {
    $('.nomor').each(function (i, obj) {
        $(obj).val().replace(/[^\d]/, '');
        $(obj).val(numeral($(obj).val()).format('0,0'));
    });
}

//Add Button Transaksi Pelanggan
$(document).on('click','.addrow-trc',function(){
	var html_add = '<tr>'+
					'<td>1</td>'+
					'<td>'+
						'<select name="" class="form-control">'+
							'<option value="0">-- Select --</option>0'+
						'</select>'+
					'</td>'+
					'<td>'+
						'<div class="jns-bayar" style="display:block">'+
							'<select name="jns_bayar" class="sel-jns-bayar form-control">'+
								'<option value="0">-- Select --</option>'+
								'<option value="2">KAS</option>'+
								'<option value="3">BANK</option>'+
							'</select>'+
						'</div>'+
						'<div class="input-group m-t-5" style="display:none">'+
						  '<select name="rek_bank" class="form-control" >'+
							'<option value="0">-- Select Bank Acc --</option>0'+
						  '</select>'+
						  '<span class="close-bankacc input-group-addon bg-red p-10" style="cursor:pointer"><i class="icons-office-52"></i></span>'+
						'</div>'+
					'</td>'+
					'<td><input type="text" name="AmountInTrc" class="form-control input-sm nomor trc text-right" value="0"></td>'+
					'<td><input type="text" name="AmountOutTrc" class="form-control input-sm nomor trc text-right" value="0"></td>'+
					'<td><input type="text" name="AmountService" class="form-control input-sm nomor trc text-right" value="0"></td>'+
					'<td><input type="text" name="AmountKas" class="form-control input-sm nomor trc text-right" value="0" readonly></td>'+
					'<td><input type="text" name="AmountTrc" class="form-control input-sm nomor text-right" value="0" readonly></td>'+
					'<td><a href="javascript:;" class="remrow-trc btn btn-sm btn-danger"><i class="fa fa-remove"></i></a></td>'+
					'</tr>';
	$('#tbl_listTrc').append(html_add);
});

//Remove Button Transaksi Pelanggan
$(document).on('click','.remrow-trc',function(){
	if (confirm('Delete transaksi...?')){
		$(this).closest('tr').remove();
	}
});

//Add Button Transaksi Program
$(document).on('click','.addrow-trcproj',function(){
	var html_add = '<tr>'+
		'<td>1</td>'+
		'<td>'+
			'<select id="selsal_program" name="SubLedger4ID" class="C010_TrcTypeID form-control">'+
				'<option value="">-- Select --</option>'+
			'</select>'+
		'</td>'+
		'<td><input type="text" name="sisa_kas_prg" value="0" class="form-control input-sm nomor text-right" readonly></td>'+
		'<td><input type="text" name="AmountOutTrc" value="0" class="form-control input-sm nomor text-right"></td>'+
		'<td><a href="javascript:;" class="remrow-trc btn btn-sm btn-danger"><i class="fa fa-remove"></i></a></td>'+
	'</tr>';
	$('#tbl_listTrcProj').append(html_add);
});

//Remove Button Transaksi Program
$(document).on('click','.remrow-trc',function(){
	if (confirm('Delete transaksi...?')){
		$(this).closest('tr').remove();
	}
});
