var banks2;
var operational_types;
var projects;
var trc_type_code;
var office_id;
var office_level;
var recordsInPage = [];
var updatedRecords = {};
var oldRecords = {};
var trTemplate;

$(function () {
        initElements();
        initNomor();
        initTabPosition();
        init_transfer_internal_keluar();
        init_header();
        setInterval(calc_trcplg(), 100);

        init_balance();

        setInterval(function () {

                var row = $('#list_rek_bank').find('tr');
                if (row.length > 0) {
                        $.each(row, function (a, b) {
                                if ($(b).find('[name="nilai_saatini_opr"]').length) {
                                        var x = $(b).find('[name="nilai_saatini_opr"]').val().replace(/,/g, "");
                                        var y = $(b).find('[name="nilai_aktual_opr"]').val().replace(/,/g, "");
                                        if (x != "" && y != "") {
                                                var q = parseFloat(y) - parseFloat(x);
                                                $(b).find('[name="nilai_koreksi_opr"]').val(numeral(q).format('0,0'));
                                        }
                                }
                                if ($(b).find('[name="nilai_saatini_prog"]').length) {
                                        var x = $(b).find('[name="nilai_saatini_prog"]').val().replace(/,/g, "");
                                        var y = $(b).find('[name="nilai_aktual_prog"]').val().replace(/,/g, "");
                                        if (x != "" && y != "") {
                                                var q = parseFloat(y) - parseFloat(x);
                                                $(b).find('[name="nilai_koreksi_prog"]').val(numeral(q).format('0,0'));
                                        }
                                }

                        });
                }

                var row_kas = $('#list_kas').find('tr');
                if (row_kas.length > 0) {
                        $.each(row_kas, function (a, b) {
                                if ($(b).find('[name="nilai_saatini"]').length) {
                                        var x = $(b).find('[name="nilai_saatini"]').val().replace(/,/g, "");
                                        var y = $(b).find('[name="nilai_aktual"]').val().replace(/,/g, "");
                                        if (x != "" && y != "") {
                                                var q = parseFloat(y) - parseFloat(x);
                                                $(b).find('[name="nilai_koreksi"]').val(numeral(q).format('0,0'));
                                        }
                                }

                        });
                }

                var row_hut = $('#list_hutang_pusat').find('tr');
                if (row_hut.length > 0) {
                        $.each(row_hut, function (a, b) {
                                if ($(b).find('[name="nilai_saatini"]').length) {
                                        var x = $(b).find('[name="nilai_saatini"]').val().replace(/,/g, "");
                                        var y = $(b).find('[name="nilai_aktual"]').val().replace(/,/g, "");
                                        if (x != "" && y != "") {
                                                var q = parseFloat(y) - parseFloat(x);
                                                $(b).find('[name="nilai_koreksi"]').val(numeral(q).format('0,0'));
                                        }
                                }

                        });
                }

                var row_prg = $('#list_hutprojpusat').find('tr');
                if (row_prg.length > 0) {
                        $.each(row_prg, function (a, b) {
                                if ($(b).find('[name="nilai_saatini"]').length) {
                                        var x = $(b).find('[name="nilai_saatini"]').val().replace(/,/g, "");
                                        var y = $(b).find('[name="nilai_aktual"]').val().replace(/,/g, "");
                                        if (x != "" && y != "") {
                                                var q = parseFloat(y) - parseFloat(x);
                                                $(b).find('[name="nilai_koreksi"]').val(numeral(q).format('0,0'));
                                        }
                                }

                        });
                }
        }, 100);

        get_first_panel_view();
});

function get_datatable(panelCode, columns, url){
        if(url == undefined){
                url = "common_transaction/get_doc_day/"+panelCode;
        }else{
                url = url;
        }

        $('[name="table-'+panelCode+'"]').DataTable({
                "bSort": false,
                "searching": false,
                "processing": true,
                "serverSide": true,
                "ajax": {
                        "url": base_url+url,
                        "type": "GET",
                        "dataSrc" : function (json) {

                                var data = json['data'];
                                var j = json['start'];
                                oldRecords = {};

                                data = get_data_datatable(data, j);
                                
                                for(var i = 0; i < data.length; i ++){
                                        var oldId = data[i]['TrcID'] + '_' + data[i]['LineID'];
                                        if(updatedRecords[oldId] != undefined){
                                                if(updatedRecords[oldId]['status'] == 3 || updatedRecords[oldId]['status'] == 2){
                                                        data[i] = updatedRecords[oldId]['data'];
                                                        var index = data.indexOf(data[i]);
                                                        if (index > -1) {
                                                            data.splice(index, 1);
                                                        }
                                                }
                                        }
                                }
                                
                                var new_j = json['start'];

                                if(new_j == 0){
                                        if(updatedRecords != undefined){
                                                for(var key in updatedRecords){
                                                        var updatedRecord = updatedRecords[key];
                                                        var new_item = {};
                                                        new_item = updatedRecord['data'];
                                                        data.splice(0, 0, new_item);
                                                }
                                        }
                                        
                                }
                                return data;
                        },
                        },
                "columns": columns,
        });
}

function to_tr(item){
        var tr = '<tr role="row">';
        for(var i = 0; i < columns.length; i ++){
                tr += '<td';
                if(i == 0){
                        tr += ' class="sorting_1"';
                }
                tr += '>';
                
                var column = columns[i];
                if(item[column['data']] != '-'){
                        tr += item[column['data']];
                }else{
                        tr +=  '-';
                }

                
                tr += '</td>';
        }
        tr += '</tr>';
        return tr;
}

function get_modal(name, table){
        var $title = $("#title-type"),
        title = "";
        $('.tab-pane').on('click', '#add-'+name, function(){
                $('#'+name+'-modal').modal();
                title = "Tambah";
                $title.html(title);

                $('#'+name+'-form').trigger("reset");
                $('[name="'+name+'_id"]').val('');

                if(name == 'pelanggan' || name == 'operasional_masuk'){
                        $('#data_bank').hide();
                }else if(name == 'program'){
                        $('.sisa_kas').html(0);
                }else if(name == 'operasional_keluar' || name == 'external_masuk' || name == 'external_keluar'){
                        $('#data_bank').hide();
                        get_balance(2, 0, 1);
                }else if(name == 'internal_kasbank'){
                        $('#data_bank').hide();
                        $('#data_bank_to').hide();
                        // get_balance(2, 0, 1);
                }else if(name == 'internal_keluar'){
                        $('#data_bank').hide();
                        $('#data_bank_to').hide();
                        $('#get_kantor').empty();
                        $(".bank_select2").empty();
                        get_balance(2, 0, 1);
                }

        });

        $(table).on('click','.edit-'+name, function(){
                var id = $(this).attr('data-id');
                $('#'+name+'-modal').modal();
                title = "Ubah";
                $title.html(title);
        });
}

function add_data(name, table){
        var validator = $("#"+name+"-form").validate();
        if(validator.form()){
                var data_form = JSON.parse(JSON.stringify($("#"+name+"-form").serializeArray()));
                var check_data = true;
                
                console.log(data_form)
                if(name == 'program' || name == 'operasional_keluar' || name == 'internal_kasbank' || name == 'internal_keluar' || name == 'external_keluar'){
                        var total = Number(data_form[5].value.replace(/,/g, ""));
                        if(name == 'operasional_keluar'){
                                total = Number(data_form[4].value.replace(/,/g, ""));
                        }

                        var sisa_kas =  Number($('.sisa_kas').html().replace(/,/g, ""));
                        // console.log(total+' dan '+sisa_kas)
                        check_data = check_saldo_return(sisa_kas, total);     
                }
                
                if(check_data){
                        var number = generate_updated_record_key();
                
                        if(data_form[0].value != ''){ 
                                number = data_form[0].value
                                $('a[data-id="'+number+'"]').parent().parent().remove();
                                var item = get_item(data_form);
                                var status = 0;

                                if(oldRecords[number] != undefined){
                                        item['action'] = '<a class="btn btn-sm btn-success" title="Undo" data-id="'+number+'" data-status="2" onclick="undo_data(\''+table+'\',\''+number+'\', 2);"><i class="glyphicon glyphicon-repeat" /></a><a class="edit-'+name+' btn btn-sm btn-primary" title="Edit" data-id="'+number+'" data-status="2"><i class="glyphicon glyphicon-pencil" /></a><a class="btn btn-sm btn-danger" title="Hapus" data-id="'+number+'" data-status="2" onclick="delete_data(\''+table+'\',\''+number+'\', 2);"><i class="glyphicon glyphicon-trash" /></a>';
                                        item['status'] = '<span class="label label-warning">Ubah</span>';    
                                        status = 2;
                                }else{
                                        item['action'] = '<a class="edit-'+name+' btn btn-sm btn-primary" title="Edit" data-id="'+number+'" data-status="1"><i class="glyphicon glyphicon-pencil" /></a><a class="delete-'+name+' btn btn-sm btn-danger" title="Hapus" data-id="'+number+'" data-status="1" onclick="delete_data(\''+table+'\',\''+number+'\', 1);"><i class="glyphicon glyphicon-trash" /></a>';
                                        item['status'] = '<span class="label label-primary">Baru</span>';   
                                        status = 1;
                                }

                                $(table).prepend(to_tr(item));
                                
                                updatedRecord = get_update_record(item, status);
                                updatedRecords[number] = updatedRecord;
                        }else{
                                var item = get_item(data_form);

                                item['action'] = '<a class="edit-'+name+' btn btn-sm btn-primary" title="Edit" data-id="'+number+'" data-status="1"><i class="glyphicon glyphicon-pencil" /></a><a class="delete-'+name+' btn btn-sm btn-danger" title="Hapus" data-id="'+number+'" data-status="1" onclick="delete_data(\''+table+'\',\''+number+'\', 1);"><i class="glyphicon glyphicon-trash" /></a>';
                                item['status'] = '<span class="label label-primary">Baru</span>';
                                $(table).prepend(to_tr(item));

                                updatedRecord = get_update_record(item, 1);
                                updatedRecords[number] = updatedRecord;
                        }

                        $('#'+name+'-modal').modal('hide');

                }else{
                        
                }       
        }  
}

function cancel_data(modal_name, message){
        bootbox.confirm({ 
            size: 'medium',
            message: "Anda yakin ingin membatalkan data form "+message+" ini?", 
            callback: function(result){ 
                    if (result) {
                                $('#'+modal_name+'-modal').modal('hide');
                        }else{

                        }
                }
        });
}

function delete_data(table, id, status){
        bootbox.confirm({ 
            size: 'medium',
            message: "Anda yakin ingin menghapus transaksi ini?", 
            callback: function(result){ 
                    if (result) {
                                $('a[data-id="'+id+'"]').parent().parent().remove();
                                
                                if(status == 1){
                                        delete updatedRecords[id];
                                }else{
                                        var item = get_delete_item(id);
                                        item['action'] = '<a class="btn btn-sm btn-success" title="Undo" data-id="'+id+'" data-status="3" onclick="undo_data(\''+table+'\',\''+id+'\', 3);"><i class="glyphicon glyphicon-repeat" /></a>';
                                        item['status'] = '<span class="label label-danger">Hapus</span>';
                                        $(table).prepend(to_tr(item));
                                        updatedRecord = get_update_record(item, 3);
                                        updatedRecords[id] = updatedRecord;     
                                }
                                
                                
                        }else{

                        }
                }
        });
}

function undo_data(table, id, status){
        $('a[data-id="'+id+'"]').parent().parent().remove();
        $(table).prepend(to_tr(oldRecords[id]['data']));

        if(oldRecords[id]['status'] == 0){
                delete updatedRecords[id];
        }else{
                var updatedRecord = {};
                updatedRecord = oldRecords[id];
                updatedRecords[id] = updatedRecord;
        }
}



function post_all_data(){
    var check_update = jQuery.isEmptyObject(updatedRecords);
    $('.post-all').prop('disabled', true);
    $('.post-all').html("Sedang di Posting...");

    for(var key in updatedRecords){
        if(typeof updatedRecords[key].data.Amount1 === 'string'){
            if(updatedRecords[key].data.Amount1 != undefined){
                updatedRecords[key].data.Amount1 = Number(updatedRecords[key].data.Amount1.replace(/,/g, ""));
            }
        }

        if(typeof updatedRecords[key].data.Amount2 === 'string'){
            if(updatedRecords[key].data.Amount2 != undefined){
                updatedRecords[key].data.Amount2 = Number(updatedRecords[key].data.Amount2.replace(/,/g, ""));
            }
        }

        if(typeof updatedRecords[key].data.Amount3 === 'string'){
            if(updatedRecords[key].data.Amount2 != undefined){
                updatedRecords[key].data.Amount3 = Number(updatedRecords[key].data.Amount3.replace(/,/g, ""));
            }
        }
    }

    if(check_update == false){    
        $.post( base_url + $('#trc_link').val() +"/save", {data : updatedRecords}).done(function( results ) {
        // $.post( base_url +"common_transaction/save", {data : updatedRecords}).done(function( results ) {
                if (results == "1"){
                       location.reload();
                }else{
                        bootbox.alert("Terjadi kesalahan saat memasukan data");
                        $('.post-all').prop('disabled', false);
                        $('.post-all').html("Post Semua");
                }
        });
    }else{
        bootbox.alert("Tidak ada data yang di posting");
        $('.post-all').prop('disabled', false);
        $('.post-all').html("Post Semua"); 
    }
    
}

function select2_office(id, level, parent){
        $('#get_kantor').empty();

        var url = "references/search_office2";
        if(parent != null){
                url = "references/search_office2/"+level+"/"+parent;
        }else if(level != null){
                url = "references/search_office2/"+level;
        }

        $(id).select2({
        placeholder: '--- Select Item ---',
        minimumInputLength: 1,
        ajax: {
                  url: base_url + url,
                  dataType: 'json',
                  delay: 250,
                  processResults: function (data) {
                    return {
                        results: $.map(data, function(obj) {
                                return { id: obj.id, text: obj.C020_Descr };
                            })
                      // results: data
                    };
                  },
                  cache: true
                }
        });
}

function select2_bank(id){
        // console.log("id office ="+id)
        var new_data = [];

        $.get( base_url + $('#trc_link').val() +"/get_bank_id/"+id, function( data ) {
                data = JSON.parse(data);

                for (var i = 0; i < data.length; i++) {
                        new_data[i] = {
                                'id' : data[i]['C000_SysID'],
                                'text' : data[i]['C030_Descr']+' - '+data[i]['C010_BankAccNumber']
                        }
                }

                $(".bank_select2").empty();
                new_data.unshift({'id' : "", 'text' : "-- Pilih Akun Bank --"});

                $(".bank_select2").select2({
                  data: new_data
                })
        });
}

function check_saldo_return(saldo, jumlah){
        //Store the password field objects into variables ...
        var pass1 = document.getElementById('show_password');
        var pass2 = document.getElementById('confirm_password');
        //Store the Confimation Message Object ...
        var message = document.getElementById('confirmMessage');
        //Set the colors we will be using ...
        var goodColor = "#66cc66";
        var badColor = "#ff6666";
        //Compare the values in the password field 
        //and the confirmation field
        if(saldo >= 0){
            //The passwords match. 
            //Set the color to the good color and inform
            //the user that they have entered the correct password 
            // pass2.style.backgroundColor = goodColor;
            // message.style.color = goodColor;
            message.innerHTML = "";
            return true;
        }else{
            //The passwords do not match.
            //Set the color to the bad color and
            //notify the user.
            // pass2.style.backgroundColor = badColor;
            message.style.color = badColor;
            message.innerHTML = "Saldo anda tidak cukup";
            return false;
        }
    }
  
// function get_bank_by_id(office_id, id){
//         $.get( base_url + $('#trc_link').val() +"/get_bank_id/"+office_id, function( data ) {
//                 data = JSON.parse(data);
//                 var item = '-';
//                 console.log(data)
//                 console.log(data[0]['C000_SysID'])
//                 console.log(office_id)
//                 console.log(id)
//                 for (var i = 0; i < data.length; i++) {
//                         if(data[i]['C000_SysID'] == id){
//                                 item = data[i]['C030_Descr']+' - '+data[i]['C010_BankAccNumber'];
//                         }
//                 }

//                 return item;
//         });
// }

function get_first_panel_view(){
        var panelCode = $('.nav-tabs').find('.active').attr('panel-code');
        $('.tab-content').html('');
        $.get(base_url + 'common_transaction/get_panel_view/' + panelCode, function (data) {
                $('.tab-content').html(data);
                $('.loading-block').hide();
                $('.tab-content').show();
        });   
}

function initElements() {
        trc_type_code = $('#trc_type_code').val();

        banks2 = $('#dt-banks').val();
        banks2 = JSON.parse(banks2);
        $("#dt-banks").remove();

        if ($('#dt-operational-types').val() != null) {
                operational_types = $('#dt-operational-types').val();
                operational_types = JSON.parse(operational_types);

                $("#dt-operational-types").remove();
        }

        if ($('#dt-projects').val() != null) {
                projects = $('#dt-projects').val();
                projects = JSON.parse(projects);

                $("#dt-projects").remove();
        }

        if ($('#dt-office-id').val() != null) {
                office_id = $('#dt-office-id').val();
                $("#dt-office-id").remove();
        }

        if ($('#dt-office-level').val() != null) {
                office_level = Number($('#dt-office-level').val());
                $("#dt-office-level").remove();
        }
}

function initNomor() {
        $('.nomor').each(function (i, obj) {
//                console.log(obj);
                $(obj).val().replace(/[^\d]/, '');
                $(obj).val(numeral($(obj).val()).format('0,0'));
        });
}

function change_format_number(number){
    return numeral(number).format('0,0');
}

function initTabPosition() {
        var p = $.urlParam('p');

        p = p != null ? p : 1;
        p -= 1;

        $('.nav-tabs').children('li').each(function (i, obj) {
                $(obj).attr('class', '');
                if (i == p) {
                        $(obj).attr('class', 'active');

                        var url = window.location.href;
                        url = url.split("?")[0] + "?p=" + (i + 1);
                        window.history.pushState("string", "Title", url);
                }

                $(obj).on('click', function () {
                        var url = window.location.href;
                        url = url.split("?")[0] + "?p=" + ($(this).index() + 1);
                        window.history.pushState("string", "Title", url);

                        reset_balance_header(obj);
                });
        });

        $('.nav-tabs').find('li').on('click', function () {
                $('.tab-content').hide();
                $('.tab-content').html('');
                $('.loading-block').show();

                var panelCode = $(this).attr('panel-code');
                $('.tab-content').html('');
                $.get(base_url + 'common_transaction/get_panel_view/' + panelCode, function (data) {
                        // $('.tab-content').html('');
                        $('.tab-content').html(data);
                        $('.loading-block').hide();
                        $('.tab-content').show();
                });
        });

        $('.tab-pane').each(function (i, obj) {
                $(obj).removeClass('active in');

                if (i == p) {
                        $(obj).addClass('active in');
                }
        });
}

$.urlParam = function (name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results == null) {
                return null;
        } else {
                return results[1] || 0;
        }
}
function calc_trcplg() {
        var row = $('#tbl_listTrc').find('tr');
        if (row.length > 0) {
                $.each(row, function (a, b) {
                        var x = $(b).find('[name="AmountService"]').val();
                        var y = $(b).find('[name="AmountInTrc"]').val();
                        var z = $(b).find('[name="AmountOutTrc"]').val();
                        if (x != "" && y != "" && z != "") {
                                var q = (parseFloat(x) + parseFloat(z)) - parseFloat(y);
                                var vkas = parseFloat(y) - parseFloat(z);
                                $(b).find('#kas').val(q);
                                $(b).find('#hutang').val(vkas);

                        }




                });
        }
}

function get_doc_day(panelCode, limit, offset) {
        $.get(base_url + "common_transaction/get_doc_day/" + panelCode + "/" + limit + "/" + offset, function (data) {
                var page = JSON.parse(data);
                serialize_rekaps(page['data'], offset == 0);
                var fn = window['get_tr_' + panelCode];
                if (typeof fn === 'function') {
                        var listTr = fn();
                        
                }
        });
}

function serialize_rekaps(rekaps, isFirstPage) {
        recordsInPage = [];
        if (isFirstPage) { // ADD "NEW" Records at the beginning of table
                updatedRecords = JSON.stringify(updatedRecords);
                $.each(updatedRecords, function (key, updated_record) {
                        var keyPrefix = key.split('_');
                        if (keyPrefix !== '0') {
                                return;
                        }
                        recordsInPage.push(updated_record);
                });
        }
        $.each(rekaps, function (key, rekap) {
                var id = rekap['C012_TrcID'] + '_' + rekap['C000_LineID'];
                if (id in updatedRecords) {
                        recordsInPage.push(updatedRecords[id]);
                } else {
                        var record = {};
                        record['old'] = rekap;
                        record['new'] = rekap;
                        record['status'] = 0;
                        recordsInPage.push(record);
                }
        });
}


//Add Button Transaksi Pelanggan
$(document).on('click', '.addrow-trc', function () {
        var bank_rows = '';
        var operational_type_rows = '';
        for (var bank_name in banks2) {
                var accounts = banks2[bank_name];
                for (var i = 0; i < accounts.length; i++) {
                        var account = accounts[i];
                        bank_rows += '<option value="' + account['C000_SysID'] + '">' + bank_name + ' [' + account['C010_BankAccNumber'] + ']</option>';
                }
        }
        for (i = 0; i < operational_types.length; i++) {
                var operational_type = operational_types[i];
                operational_type_rows +=
                        '<option value="'
                        + operational_type['SysID'] + '">'
                        + operational_type['Descr'].substr(operational_type['Descr'].indexOf(" ") + 1);
                +'</option>';
        }

        var class_name = 'so' + ($('#tbl_listTrc').children().length + 1);

        var html_add = '<tr style="display: none" class="line ' + class_name + ' item" idx=' + ($('#tbl_listTrc').children().length + 1) + '>' +
                '<td>' + ($('#tbl_listTrc').children().length + 1) + '</td>' +
                '<td>' +
                '<select name="MLedgerID" class="form-control input-post" seq_id="3">' +
                '<option value="0">-- Select --</option>0' +
                operational_type_rows +
                '</select>' +
                '<label style="display:none" class="error">Mohon dipilih!</label>' +
                '</td>' +
                '<td>' +
                '<div class="jns-bayar" style="display:block">' +
                '<select name="MLedgerID" class="sel-jns-bayar m-ledger-mutasi form-control m-legder-pelanggan input-post" seq_id="1,2">' +
                '<option value="0">-- Select --</option>' +
                '<option value="2">KAS</option>' +
                '<option value="3">BANK</option>' +
                '</select>' +
                '</div>' +
                '<div class="input-group m-t-5 div-bank-account" style="display:none">' +
                '<select name="SubLedger1ID" seq_id="1,2" class="form-control input-post bank-account-pelanggan" >' +
                '<option value="0">-- Select Bank Acc --</option>' +
                bank_rows +
                '</select>' +
                '<span class="close-bank-account-pelanggan input-group-addon bg-red p-10" style="cursor:pointer"><i class="icons-office-52"></i></span>' +
                '</div>' +
                '<label style="display:none" class="error">Mohon dipilih!</label>' +
                '</td>' +
                '<td style="max-width: 250px;">' +
//                '<td colspan="2" style="max-width: 500px;">' +
                '<input type="text" sytle="float: left; width:50%;" id="AmountInTrc" name="Amount" seq_id="1" class="form-control input-post input-sm nomor trc text-right" value="0">' +
                '<label style="display:none" class="error">Input "Masuk" atau "Keluar" harus lebih besar dari nol!</label></td>' +
                '<td style="max-width: 250px;">' +
                '<input type="text" sytle="float: right; width:50%;" id="AmountOutTrc" name="Amount" seq_id="2" class="form-control input-post input-sm nomor trc text-right" value="0">' +
                '<label style="display:none" class="error">Input "Masuk" atau "Keluar" harus lebih besar dari nol!</label></td>' +
                '<td><input type="text" id="AmountService" class="form-control input-sm nomor trc text-right" value="0"></td>' +
                '<td><input type="text" id="kas" minplus="1" class="form-control input-mutasi input-sm nomor trc text-right" value="0" readonly></td>' +
                '<td><input type="text" id="hutang" name="Amount" seq_id="3" class="form-control input-sm input-post nomor text-right" value="0" readonly></td>' +
                '<td><a href="javascript:;" onclick="delete_trc(' + ($('#tbl_listTrc').children().length + 1) + ')" class="remrow-trc btn btn-sm btn-danger"><i class="fa fa-remove"></i></a></td>' +
                '</tr>';
        $('#tbl_listTrc').append(html_add);
        inputSelect();
        init_header();

        $('.' + class_name).show();
});

function delete_trc(idx) {
        console.log(idx);
        bootbox.confirm("Delete transaksi...?", function (result) {
                if (result) {
                        $(".so" + idx).remove();
                        // rearange_no();
                        console.log(this);
                } else {
                }
        });
}
// function rearenge_no(){
//         var no = $('#tbl_listTrc').children().length;
//         for (var i = 0; i < no; i++) {
//                 //Things[i]
//                 $('.idx'+).html('changed value');

//         }
// }
//Add Button Transaksi Program
$(document).on('click', '.addrow-trcproj', function () {
        var project_rows = '';
        for (var projectID in projects) {
                var project = projects[projectID];
                project_rows += '<option value="'
                        + projectID + '">'
                        + project['C020_Descr']
                        + '</option>';
        }

        var html_add = '<tr>' +
                '<td>1</td>' +
                '<td>' +
                '<select id="selsal_program" name="SubLedger4ID" class="C010_TrcTypeID form-control">' +
                '<option value="0">-- Select --</option>' +
                project_rows +
                '</select>' +
                '</td>' +
                '<td><input type="text" name="sisa_kas_prg" value="0" class="form-control input-sm nomor text-right" readonly></td>' +
                '<td><input type="text" name="AmountOutTrc" value="0" class="form-control input-sm nomor text-right"></td>' +
                '<td><a href="javascript:;" class="remrow-trc btn btn-sm btn-danger"><i class="fa fa-remove"></i></a></td>' +
                '</tr>';
        $('#tbl_listTrcProj').append(html_add);
        inputSelect();
});

//Remove Button Transaksi Program


$(document).on('change', '#selsal_program', function () {
        var val = this.value;
        console.log(val);
        var sisa_kas_prg = $('[name="sisa_kas_prg"]');
        if (val == '0') {
                sisa_kas_prg.val('0');
        } else {
                var project = projects[val];
                sisa_kas_prg.val(project['C040_Value']);
        }
});

$(document).on('change', '[name="trans_kasbank_asal"]', function () {
        var val = this.value;
        if (val != '3') { // IF NOT BANK
                $('#seltrans_bank_asal').select2("val", "0");
                $('#seltrans_bank_asal').trigger("change");
                $('#dvtrans_bank_asal').fadeOut();
                return;
        }
        $('#dvtrans_bank_asal').fadeIn();
        get_saldo();
});

$(document).on('change', '#seltrans_bank_asal', function () {
        var val = this.value;
        if (val == '0') {
                $('#dvtrans_bankacc_asal').fadeOut();
                return;
        }

        var select = $('#seltrans_bankacc_asal');
        var option0 = select.children(":first");

        select.select2("val", "0");

        select.empty();
        select.append(option0);

        var accounts = banks2[val];
        for (var i = 0; i < accounts.length; i++) {
                var account = accounts[i];
                var html_row = '<option value="' + account['C000_SysID'] + '">' + account['C010_BankAccNumber'] + '</option>';
                select.append(html_row);
        }

        $('#dvtrans_bankacc_asal').fadeIn();
        get_saldo();
});
/*********************************************************************************/
$(document).on('change', '#tp_transfer', function () {
        console.log(this.value);
        if ($(this).val() == "") {
                $('#tmpl_int_trans').hide();
                $('#tmpl_ext_trans').hide();
        } else if ($(this).val() == "1") {
                $('#tmpl_ext_trans').hide();
                $('#tmpl_int_trans').fadeIn();
        } else {
                $('#tmpl_int_trans').hide();
                $('#tmpl_ext_trans').fadeIn();
        }

});

$(document).on('keyup', 'input.nomor', function (event) {
        $(this).val($(this).val().replace(/[^\d]/, ''));
        $(this).val(numeral($(this).val()).format('0,0'));
});

$(document).on('keyup', '.input-sm.trc', function (event) {
        var tr = $(this).closest('tr');
        var x = $(tr).find('#AmountService').val().replace(/,/g, "");
        var y = $(tr).find('#AmountInTrc').val().replace(/,/g, "");
        var z = $(tr).find('#AmountOutTrc').val().replace(/,/g, "");
        if (x != "" && y != "" && z != "") {
                var vkas = parseFloat(y) - parseFloat(z);
                $(tr).find('#kas').val(numeral(vkas).format('0,0'));
                var q = (parseFloat(y) - parseFloat(z)) - parseFloat(x);
                $(tr).find('#hutang').val(numeral(q).format('0,0'));

                update_balance_header($(this).closest('.sub-trc'));
        }

        initNomor();
});

$(document).on('keyup', '.amount-program', function (event) {
        var tr = $(this).closest('tr');
        var saved_sisa_kas_prg = tr.find('[name="saved_sisa_kas_prg"]').val();
        var amount_out_trc = $(this).val().replace(/,/g, "");

        var sisa_kas_prg = saved_sisa_kas_prg - amount_out_trc;

        tr.find('[name="sisa_kas_prg"]').val(sisa_kas_prg);

        initNomor();
});

function repoFormatResult(repo) {
        var markup = '<div class="row">' +
                '<div class="col-md-12">' +
                '<div class="row">' +
                '<div class="col-md-12">[' + repo.C010_Code + '] ' + repo.C020_Descr + '</div>' +
                '</div>';
        if (repo.C020_Descr) {
                //markup += '<div>' + repo.C020_Descr + '</div>';
        }
        markup += '</div></div>';
        return markup;
}
function repoFormatSelection(repo) {
        return repo.C020_Descr;
}
/*
 $(document).on('change', '[name="office_level"]', function () {
 var parent = $(this).closest('col-md-12');
 parent.find('[name="MLedgerID"]').closest('.form-group').hide();
 parent.find('[name="bank_name"]').closest('.form-group').hide();
 parent.find('[name="SubLedger1ID"]').closest('.form-group').hide();
 console.log($(this).val());
 
 //        if ($(this).val() == "0") {
 //                return;
 //        }
 //        console.log(parent.find('[name="SLedgerID"]').length);
 //        if (parent.find('[name="SLedgerID"]').length == 0) {
 //                return;
 //        }
 parent.find('[name="SLedgerID"]').select2({
 placeholder: "Search Office",
 minimumInputLength: 1,
 ajax: {// instead of writing the function to execute the request we use Select2's convenient helper
 url: base_url + "transactions/search_office?lvl=" + $('[name="office_level"]').val(),
 dataType: 'json',
 quietMillis: 250,
 data: function (term, page) {
 return {
 q: term, // search term
 };
 },
 results: function (data, page) { // parse the results into the format expected by Select2.
 // since we are using custom formatting functions we do not need to alter the remote JSON data
 return {results: data.items};
 },
 cache: true
 },
 initSelection: function (element, callback) {
 // the input tag has a value attribute preloaded that points to a preselected repository's id
 // this function resolves that id attribute to an object that select2 can render
 // using its formatResult renderer - that way the repository name is shown preselected
 var id = $(element).val();
 if (id !== "") {
 $.ajax(base_url + "transactions/search_office?lvl=" + $('[name="office_level"]').val() + id, {
 dataType: "json"
 }).done(function (data) {
 callback(data);
 });
 }
 },
 formatResult: repoFormatResult, // omitted for brevity, see the source of this page
 formatSelection: repoFormatSelection, // omitted for brevity, see the source of this page
 dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
 escapeMarkup: function (m) {
 return m;
 } // we do not want to escape markup since we are displaying html in results
 });
 });
 */
$(document).on('click', '.cancel-trc', function () {
        if (confirm('Cancel Insert Data...?')) {
                location.reload();
        }
});

$(document).on('change', '[name="trans_jenis_asal"]', function () {
        var val = $(this).val();
        if (val == 2) {
                $('#dvtrans_prog_asal').fadeIn();
        } else {
                $('#dvtrans_prog_asal').fadeOut();
        }
        get_saldo();
});
/*
 $(document).on('change', $('#tmpl_int_trans').find('[name"SLedgerID"]'), function () {
 if ($(this).val() != "") {
 $('#dvseltrans_kasbank_tujuan').fadeIn();
 $('#dvtrans_bank_tujuan').hide();
 $(this).parent('div').html('');
 $('#dvseltrans_kasbank_tujuan').find('div').html('<select name="MLedgerID" class="form-control"><option value="">-- Select --</option><option value="2">KAS</option><option value="3">BANK</option></select>');
 $(this).select2({minimumResultsForSearch: -1});
 //                inputSelect();
 }
 });
 */
//
// $(document).on('change','[name="trans_kasbank_tujuan"]',function(){
//         if ($('[name="trans_kasbank_tujuan"]').val() == "3"){
//                 $('#dvtrans_bank_tujuan').fadeIn();
//                 $('#dvtrans_bank_tujuan').select2("val", "0");
//                 $('#dvtrans_bank_tujuan').trigger("change");
//         } else {
//                 $('#dvtrans_bank_tujuan').hide();
//                 $('#dvtrans_bankacc_tujuan').hide();
//         }
// });
/*
 $(document).on('change', '#seltrans_bank_tujuan', function () {
 var val = this.value;
 if (val == '0') {
 $('#dvtrans_bankacc_tujuan').fadeOut();
 return;
 }
 
 var select = $('#seltrans_bankacc_tujuan');
 var option0 = select.children(":first");
 
 select.select2("val", "0");
 
 select.empty();
 select.append(option0);
 
 var accounts = banks2[val];
 for (var i = 0; i < accounts.length; i++) {
 var account = accounts[i];
 var html_row = '<option value="' + account['C000_SysID'] + '">' + account['C010_BankAccNumber'] + '</option>';
 select.append(html_row);
 }
 
 $('#dvtrans_bankacc_tujuan').fadeIn();
 get_saldo();
 });
 */
$(document).on('change', '[name="trans_kasbank_tujuan"]', function () {
        var val = this.value;
        if (val != '3') { // IF NOT BANK
                $('#seltrans_bank_tujuan').select2("val", "0");
                $('#seltrans_bank_tujuan').trigger("change");
                $('#dvtrans_bank_tujuan').fadeOut();
                return;
        }
        console.log(banks2)
        $('#dvtrans_bank_tujuan').fadeIn();
        get_saldo();
});


function get_saldo() {

        if ($('[name="trans_jenis_asal"]').val() == '1') {
                var sl = 1;
                if ($('[name="trans_kasbank_asal"]').val() == '2') {
                        var sl2 = 2;
                        var sl3 = 0;
                        var sl4 = 0;
                } else if ($('[name="trans_kasbank_asal"]').val() == '3') {
                        var sl2 = 3;
                        var sl3 = $('#seltrans_bankacc_asal').val();
                        var sl4 = 0;
                } else {
                        var sl2 = 0;
                        sl3 = 0;
                        sl4 = 0;
                }

                $.post(base_url + "transactions/get_saldo", {opr: 'get_saldo', sl: sl, sl2: sl2, sl3: sl3, sl4: sl4}).done(function (data) {
                        $('[name="val-total"]').val(numeral(data).format('0,0'));
                });
        } else if ($('[name="trans_jenis_asal"]').val() == '2') {
                var sl = 2;
                if ($('[name="trans_kasbank_asal"]').val() == '2') {
                        var sl2 = 2;
                        var sl3 = 0;
                } else if ($('[name="trans_kasbank_asal"]').val() == '3') {
                        var sl2 = 3;
                        var sl3 = $('#seltrans_bankacc_asal').val();
                } else {
                        var sl2 = 0;
                        sl3 = 0;
                }

                var sl4 = $('#seltrans_prog_asal').val();

                $.post(base_url + "transactions/get_saldo", {opr: 'get_saldo', sl: sl, sl2: sl2, sl3: sl3, sl4: sl4}).done(function (data) {
                        $('[name="val-total"]').val(numeral(data).format('0,0'));
                });
        }
}

$(document).on('change', '.m-legder-pelanggan', function () {
        if ($(this).val() == '3') {
                $(this).closest('td').find('.bank-account-pelanggan').parent('div').fadeIn();
                $(this).closest('td').find('.bank-account-pelanggan').select2("val", "0");
                $(this).parent('div').hide();
        }
});

$(document).on('click', '.close-bank-account-pelanggan', function () {
        $(this).closest('td').find('.bank-account-pelanggan').select2("val", "0");
        $(this).closest('td').find('.m-legder-pelanggan').select2("val", "0");
        $(this).closest('td').find('.m-legder-pelanggan').trigger("change");
        $(this).closest('td').find('.m-legder-pelanggan').parent('div').show();
        $(this).parent('div').hide();
});

$(document).on('change', '.sub-ledger-2', function () {
        console.log('22');
        var val = $(this).val();
        console.log(val);
        if (val == 2) {
                $(this).closest('.form-field').find('.div-sub-ledger-3').fadeIn();
        } else {
                $(this).closest('.form-field').find('.div-sub-ledger-3').fadeOut();
        }
//        get_saldo();
});

$(document).on('change', '.m-ledger', function () {
        var val = this.value;
        if (val != '3') { // IF NOT BANK
                $(this).closest('.form-field').find('.bank-name').val('0');
                $(this).closest('.form-field').find('.bank-name').trigger("change");
                $(this).closest('.form-field').find('.div-bank-name').fadeOut();
                $(this).closest('.form-field').find('.div-bank-account').fadeOut();
                return;
        }

        $(this).closest('.form-field').find('.div-bank-name').fadeIn();
});

$(document).on('change', '.bank-name', function () {
        var val = this.value;
        /*if (val == '0') {
         $(this).closest('.form-field').find('.div-bank-account').fadeOut();
         return;
         }*/

        var select = $(this).closest('.form-field').find('select.bank-account');
        var option0 = select.children(":first");

        select.select2("val", "0");

        select.empty();
        select.append(option0);

        if (val == '0') {
                return;
        }

        var accounts = banks2[val];
        for (var i = 0; i < accounts.length; i++) {
                var account = accounts[i];
                var html_row = '<option value="' + account['C000_SysID'] + '">' + account['C010_BankAccNumber'] + '</option>';
                select.append(html_row);
        }

        $(this).closest('.form-field').find('.div-bank-account').fadeIn();
});

function init_balance() {
        $.each($('[name="MLedgerID"].input-get-balance'), function (i, element) {
                $(element).trigger('change');
        });
}

$(document).on('change', '.input-get-balance', function () {
        var row = {};
        var parent = $(this).closest('.form-field');
        $.each($(parent).find('.input-get-balance'), function (i, obj) {
                var name = $(obj).attr('name');
                if (name != null) {
                        row[name] = $(obj).val();
                }
        });

        console.log(row);
        $.post(base_url + "transactions/sum_balance", {
                data: row
        }).done(function (data) {
                console.log(data);

                parent.find('[name="balance"]').val(data);
                parent.find('[name="balance"]').trigger('keyup');
        });
});

function init_transfer_internal_keluar() {
        var parent = $('#tmpl_int_trans');
        var office_level_to = parent.find('[name="office_level"]');
        var office = parent.find('#office');
        var s_ledger_id = parent.find('[name="SLedgerIDTo"]');
        var bank_name = parent.find('[name="bank_name"]');
        var bank_account = parent.find('[name="SubLedger1IDTo"]');

        office_level_to.change(function () {
                var val = Number(office_level_to.val());

                if (val != 0 && val < office_level) {
                        $.get(base_url + 'transactions/get_office?' + 'office_level=' + $(office_level_to).val(), function (data) {
                                var json_data = JSON.parse(data);
                                var parent_office = json_data.length > 0 ? json_data[0] : null;
                                if (parent_office != null) {
                                        console.log(parent_office['name']);
                                        $(office).val(parent_office['name']);
                                        $(office).attr('readonly', '');
                                        on_select_office_to(parent_office['id']);
                                }
                        });
                        return;
                }

                reset_bank();

                $(office).removeAttr('readonly')
                $(office).val('');
                $(s_ledger_id).val('0');

                $(office).typeahead('destroy');
                office.typeahead({
                        onSelect: function (item) {
                                on_select_office_to(item.value);
                        },
                        ajax: base_url + 'transactions/get_office?' + 'office_level=' + $(office_level_to).val(),
                });
        });

        if (office_level_to.val() != '0') {
                $(office_level_to).trigger('change');
        }

        parent.find('.m-ledger-int-trans').change(function () {
                var val = $(this).val();
                console.log(val);
                if (val != 3) {
                        parent.find('.div-bank').fadeOut();
                } else {
                        parent.find('.div-bank').fadeIn();
                }
        });

        $(bank_name).change(function () {
                $(bank_account).select2("val", "0");
                $(bank_account).trigger("change");

                var val = $(this).val();
                init_bank_account($(s_ledger_id).val(), val);
        });

        if ($(office_level_to).val() > 0) {
                $(office_level_to).trigger("change");
        }
}

function on_select_office_to(office_id_to) {
        $('[name="SLedgerIDTo"]').val(office_id_to);
        reset_bank();
        init_bank_name(office_id_to);
}

function reset_bank() {
        var parent = $('#tmpl_int_trans');
        var bank_name = parent.find('[name="bank_name"]');
        var bank_account = parent.find('[name="SubLedger1IDTo"]');

        $(bank_name).select2("val", "0");
        $(bank_name).empty();
        $(bank_name).append('<option value="0">-- Select --</option>');

        $(bank_account).select2("val", "0");
        $(bank_account).empty();
        $(bank_account).append('<option value="0">-- Select --</option>');
}

function init_bank_name(office_id) {
        var parent = $('#tmpl_int_trans');
        var bank_name = parent.find('[name="bank_name"]');

        $(bank_name).empty();
        $(bank_name).append('<option value="0">-- Select --</option>');

        $.get(base_url + "transactions/get_bank_name?office_id=" + office_id, function (data) {
                console.log(data);
                data = jQuery.parseJSON(data);
                for (var i = 0; i < data.length; i++) {
                        var row = data[i];
                        var html_row = '<option value="' + row['name'] + '">' + row['description'] + '</option>';
                        $(bank_name).append(html_row);
                }
        });
}

function init_bank_account(office_id, bank_name) {
        var parent = $('#tmpl_int_trans');
        var bank_account = parent.find('[name="SubLedger1IDTo"]');

        $(bank_account).empty();
        $(bank_account).append('<option value="0">-- Select --</option>');

        $.get(base_url + "transactions/get_bank_account?office_id=" + office_id + "&bank_name=" + bank_name, function (data) {
                console.log(data);
                data = jQuery.parseJSON(data);
                for (var i = 0; i < data.length; i++) {
                        var row = data[i];
                        var html_row = '<option value="' + row['id'] + '">' + row['account'] + '</option>';
                        $(bank_account).append(html_row);
                }
        });
}

function init_header() {
        $('.input-mutasi').on('input', function () {
                update_balance_header($(this).closest('.sub-trc'));
        });

        $('select.m-ledger-mutasi').on('change', function () {
                console.log($(this).val());
                update_balance_header($(this).closest('.sub-trc'));
        });
}

function update_balance_header(parent) {
        var mutasi_kas = 0;
        var mutasi_bank = 0;
        $.each($(parent).find('.line'), function (i, element) {
                var kas_or_bank = $(element).find('select.m-ledger-mutasi').val();
                if (kas_or_bank == null) {
                        kas_or_bank = $(element).find('.m-ledger-mutasi').val();
                }
                kas_or_bank = kas_or_bank != null ? kas_or_bank : 0;

                if (kas_or_bank == 2) { //kas
                        mutasi_kas += Number($(element).find('.input-mutasi').val().replace(/,/g, "")) * Number($(element).find('.input-mutasi').attr('minplus'));
                } else if (kas_or_bank == 3) {
                        mutasi_bank += Number($(element).find('.input-mutasi').val().replace(/,/g, "")) * Number($(element).find('.input-mutasi').attr('minplus'));
                }
                console.log(mutasi_kas);
        });


        $('.nav-tabs li.active').attr('mutasi-kas', mutasi_kas);
        $('.nav-tabs li.active').attr('mutasi-bank', mutasi_bank);

        reset_balance_header($('.nav-tabs li.active'));
}

function reset_balance_header(parent_li) {
        var mutasi_kas = $(parent_li).attr('mutasi-kas');
        mutasi_kas = mutasi_kas != null ? mutasi_kas : 0;
        mutasi_kas = Number(mutasi_kas);
        var mutasi_bank = $(parent_li).attr('mutasi-bank');
        mutasi_bank = mutasi_bank != null ? mutasi_bank : 0;
        mutasi_bank = Number(mutasi_bank);

        console.log('mutasi_kas > ' + mutasi_kas);

        var pre_kas = Number($('.pre-cash-balance').val().replace(/,/g, ""));
        var pre_bank = Number($('.pre-bank-balance').val().replace(/,/g, ""));

        console.log('pre_kas > ' + pre_kas);

        var post_kas = pre_kas + mutasi_kas;
        var post_bank = pre_bank + mutasi_bank;

        console.log('post_kas > ' + post_kas);

        var mit_kas = Number($('.cash-mit-balance').val().replace(/,/g, ""));
        var mit_bank = Number($('.bank-mit-balance').val().replace(/,/g, ""));

        console.log('mit_kas > ' + mit_kas);

        var total_kas = post_kas + mit_kas;
        var total_bank = post_bank + mit_bank;

        console.log('total_kas > ' + total_kas);

        $('.cash-mutation-balance').val(mutasi_kas);
        $('.bank-mutation-balance').val(mutasi_bank);
        $('.post-cash-balance').val(post_kas);
        $('.post-bank-balance').val(post_bank);
        $('.total-cash-balance').val(total_kas);
        $('.total-bank-balance').val(total_bank);

        initNomor();
}

function generate_updated_record_key(){
        /*var lineID = 1;
        for (var key in updatedRecords) {
               var updatedRecord = updatedRecords[key];
               var status = updatedRecord['status'];
               if(status == 1){ //INSERT
                    lineID++;   
               }        
        }

        return '0_' + lineID;
        */
       return ("" + Math.random()).replace('.', '').replace(/^0+/, '');
}

