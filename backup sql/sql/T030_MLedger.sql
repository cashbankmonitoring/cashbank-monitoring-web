GO
/****** Object:  Table [dbo].[T030_MLedger]    Script Date: 09/30/2016 14:07:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T030_MLedger](
	[SysID] [smallint] NULL,
	[Code] [nvarchar](50) NULL,
	[Descr] [nvarchar](100) NULL,
	[SeqIdx] [smallint] NULL,
	[SettlementGroupID] [smallint] NOT NULL,
	[ParentID] [smallint] NULL,
	[IsDetail] [bit] NULL,
	[Lft] [smallint] NULL,
	[Rgt] [smallint] NULL,
	[MAcctLevel] [smallint] NULL,
	[TypeML] [smallint] NULL,
	[CF_rptType] [smallint] NULL,
	[CF_Group] [nchar](10) NULL,
	[BS_rptType] [smallint] NULL,
	[PL_rptType] [smallint] NULL,
	[TB_Group] [smallint] NULL,
	[Spasi] [nchar](10) NULL,
	[IsNeraca] [bit] NULL,
	[parTopID] [int] NULL,
	[KasCnt] [bit] NULL,
	[KasBankCnt] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Default [DF_T030_MLedger_SettlementGroupID]    Script Date: 09/30/2016 14:07:22 ******/
ALTER TABLE [dbo].[T030_MLedger] ADD  CONSTRAINT [DF_T030_MLedger_SettlementGroupID]  DEFAULT ((1)) FOR [SettlementGroupID]
GO
