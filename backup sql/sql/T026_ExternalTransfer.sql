GO
/****** Object:  Table [dbo].[T026_ExternalTransfer]    Script Date: 10/03/2016 12:47:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T026_ExternalTransfer](
	[C000_TrcID] [int] NULL,
	[C001_LineID] [smallint] NOT NULL,
	[C002_DateTransfer] [datetime] NULL,
	[C003_Name] [nvarchar](100) NULL,
	[C004_PartnerID] [int] NULL,
	[C005_BankAccName] [nvarchar](255) NULL,
	[C006_BankName] [nvarchar](100) NULL,
	[C007_BankAccNumber] [nvarchar](100) NULL
) ON [PRIMARY]
GO
/****** Object:  Default [DF_T026_ExternalTransfer_C001_LineID]    Script Date: 10/03/2016 12:47:02 ******/
ALTER TABLE [dbo].[T026_ExternalTransfer] ADD  CONSTRAINT [DF_T026_ExternalTransfer_C001_LineID]  DEFAULT ((1)) FOR [C001_LineID]
GO
