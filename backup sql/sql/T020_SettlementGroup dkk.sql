GO
/****** Object:  Table [dbo].[T020_SGType]    Script Date: 09/30/2016 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T020_SGType](
	[SysID] [smallint] NOT NULL,
	[Descr] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[T020_SGMember]    Script Date: 09/30/2016 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T020_SGMember](
	[SysID] [int] NOT NULL,
	[GroupID] [int] NOT NULL,
	[OfficeID] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[T020_SettlementGroup]    Script Date: 09/30/2016 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T020_SettlementGroup](
	[SysID] [int] NOT NULL,
	[TypeID] [smallint] NOT NULL,
	[OfficeID] [int] NOT NULL
) ON [PRIMARY]
GO
