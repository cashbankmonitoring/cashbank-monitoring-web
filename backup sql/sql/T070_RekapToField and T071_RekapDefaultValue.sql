GO
/****** Object:  Table [dbo].[T071_RekapDefaultValue]    Script Date: 09/28/2016 16:17:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T071_RekapDefaultValue](
	[C001_SubTrcTypeID] [smallint] NULL,
	[C072_Amount1] [float] NULL,
	[C073_Amount2] [float] NULL,
	[C073_Amount3] [float] NULL,
	[C200_SLedgerID] [smallint] NULL,
	[C210_MLedgerID] [smallint] NULL,
	[C211_SubLedger1ID] [smallint] NULL,
	[C212_SubLedger2ID] [smallint] NULL,
	[C213_SubLedger3ID] [smallint] NULL,
	[C214_SubLedger4ID] [smallint] NULL,
	[C215_Description] [nvarchar](100) NULL,
	[C220_MLedgerIDTo] [smallint] NULL,
	[C201_SLedgerIDTo] [smallint] NULL,
	[C222_SubLedger1IDTo] [smallint] NULL,
	[C223_SubLedger2IDTo] [smallint] NULL,
	[C224_SubLedger3IDTo] [smallint] NULL,
	[C225_SubLedger4IDTo] [smallint] NULL,
	[C226_DescriptionTo] [nvarchar](100) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[T070_RekapToField]    Script Date: 09/28/2016 16:17:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T070_RekapToField](
	[C001_SubTrcTypeID] [smallint] NULL,
	[C072_Amount1] [nvarchar](50) NULL,
	[C073_Amount2] [nvarchar](50) NULL,
	[C073_Amount3] [nvarchar](50) NULL,
	[C200_SLedgerID] [nvarchar](50) NULL,
	[C210_MLedgerID] [nvarchar](50) NULL,
	[C211_SubLedger1ID] [nvarchar](50) NULL,
	[C212_SubLedger2ID] [nvarchar](50) NULL,
	[C213_SubLedger3ID] [nvarchar](50) NULL,
	[C214_SubLedger4ID] [nvarchar](50) NULL,
	[C215_Description] [nvarchar](50) NULL,
	[C220_MLedgerIDTo] [nvarchar](50) NULL,
	[C201_SLedgerIDTo] [nvarchar](50) NULL,
	[C222_SubLedger1IDTo] [nvarchar](50) NULL,
	[C223_SubLedger2IDTo] [nvarchar](50) NULL,
	[C224_SubLedger3IDTo] [nvarchar](50) NULL,
	[C225_SubLedger4IDTo] [nvarchar](50) NULL,
	[C226_DescriptionTo] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Default [DF_T070_RekapToField_C001_SubTrcTypeID]    Script Date: 09/28/2016 16:17:38 ******/
ALTER TABLE [dbo].[T070_RekapToField] ADD  CONSTRAINT [DF_T070_RekapToField_C001_SubTrcTypeID]  DEFAULT ((0)) FOR [C001_SubTrcTypeID]
GO
/****** Object:  Default [DF_T071_RekapDefaultValue_C215_Description]    Script Date: 09/28/2016 16:17:38 ******/
ALTER TABLE [dbo].[T071_RekapDefaultValue] ADD  CONSTRAINT [DF_T071_RekapDefaultValue_C215_Description]  DEFAULT (N'') FOR [C215_Description]
GO
/****** Object:  Default [DF_T071_RekapDefaultValue_C226_DescriptionTo]    Script Date: 09/28/2016 16:17:38 ******/
ALTER TABLE [dbo].[T071_RekapDefaultValue] ADD  CONSTRAINT [DF_T071_RekapDefaultValue_C226_DescriptionTo]  DEFAULT (N'') FOR [C226_DescriptionTo]
GO