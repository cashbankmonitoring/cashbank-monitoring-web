GO
/****** Object:  Table [dbo].[T026_InternalTransferStatus]    Script Date: 10/03/2016 12:22:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T026_InternalTransferStatus](
	[TrcID] [int] NULL,
	[LineID] [smallint] NULL,
	[Status] [tinyint] NOT NULL,
	[CreatedAt] [datetime] NULL,
	[UpdatedAt] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Default [DF_T026_InternalTransfer_LineID]    Script Date: 10/03/2016 12:22:07 ******/
ALTER TABLE [dbo].[T026_InternalTransferStatus] ADD  CONSTRAINT [DF_T026_InternalTransfer_LineID]  DEFAULT ((1)) FOR [LineID]
GO
/****** Object:  Default [DF_T026_InternalTransfer_Status]    Script Date: 10/03/2016 12:22:07 ******/
ALTER TABLE [dbo].[T026_InternalTransferStatus] ADD  CONSTRAINT [DF_T026_InternalTransfer_Status]  DEFAULT ((0)) FOR [Status]
GO
